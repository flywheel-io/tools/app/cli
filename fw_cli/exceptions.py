"""Error classes."""

import typing as t

from click.exceptions import ClickException, get_text_stderr
from rich.console import Console
from typer import Abort, Exit

from .styles import red

__all__ = ["Abort", "Error", "Exit"]


class Error(ClickException):
    """Error that Click handles and displays before exit - with code and color."""

    def __init__(self, message: str, exit_code: int = 1) -> None:
        """Init error with message and exit code."""
        super().__init__(message)
        self.exit_code = exit_code

    def show(self, file: t.Optional[t.IO] = None) -> None:
        """Show the error message."""
        if file is None:
            file = get_text_stderr()
        console = Console(soft_wrap=True, file=file)
        console.print(f"{red('Error:')} {self.message}")
