"""Flywheel CLI."""

from importlib.metadata import version

__version__ = version(__name__)
ENV_PREFIX = "FW_CLI"
