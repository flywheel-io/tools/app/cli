"""Gear Exchage Interface module."""

import base64
import json
from bisect import bisect_left
from typing import Optional

from requests import HTTPError, Session

from ..exceptions import Error


class ExchangeInterace:
    """Class to interface with the gear exchange repository.

    Methods:
        find_gear: Find and return manifest for specified gear
    """

    _ept = "https://gitlab.com/api/v4/projects/36522442/repository/files"

    def __init__(self) -> None:
        """Inits ExchangeInterface with the exchange endpoint, requests session."""
        self._sess = Session()

    def find_gear(self, name: str, version: str = "latest") -> Optional[dict | None]:
        """Find manifest for desired gear.

        Args:
            name: name of desired gear
            version (optional): desired gear version

        Returns:
            Downloaded gear manifest.
        """
        # Pull exchange.json file
        self._get_exchange_json()

        if (idx := self._get_gear_idx(name)) == len(self._exc):
            return None

        if (subset := self._exc[idx])[0]["name"] != name:
            return None

        found_gear = None
        if version == "latest":
            for gear in subset:
                if gear["latest"]:
                    found_gear = gear
        else:
            for gear in subset:
                if gear["version"] == version:
                    found_gear = gear

        if not found_gear:
            return None

        try:
            resp = self._get_file(found_gear["exchange-path"])
        except HTTPError as e:
            raise Error(f"{e}\nError in downloading gear manifest.")

        manifest = self._decode_file_content(resp)
        return manifest

    def _get_file(self, fpath: str, ref: str = "master") -> dict:
        """Download a file from the exchange given a filepath."""
        enc_fpath = fpath.replace("/", r"%2F")
        r = self._sess.get(f"{self._ept}/{enc_fpath}", params={"ref": ref})
        r.raise_for_status()
        return r.json()

    def _get_exchange_json(self) -> None:
        try:
            resp = self._get_file("exchange.json", ref="gh-pages-json")
            self._exc = self._decode_file_content(resp)
        except HTTPError as e:
            raise Error(f"{e}\nError in downloading exchange metadata.")

    def _get_gear_idx(self, name: str) -> int:
        return bisect_left(self._exc, name, key=lambda x: x[0].get("name"))

    def _decode_file_content(self, response_json: dict) -> dict:
        """Decode api response content."""
        try:
            b64content = base64.b64decode(response_json["content"]).decode("utf-8")
            decoded = json.loads(b64content)
        except Exception as e:
            # UnicodeDecodeError, JSONDecodeError, binascii.Error
            raise Error(f"{e}\nError in decoding file from exchange.")
        return decoded
