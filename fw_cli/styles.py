"""Styling shorthands."""

import typing as t
from functools import partial

from rich import get_console
from rich import print as rprint

PULSE_SIZE = 20


def ansi(style: str, text: str) -> str:
    """Apply style on text."""
    return f"[{style}]{text}[/]"


def ansi_fn(style: str) -> t.Callable:
    """Return ansi partial with the given style."""
    return partial(ansi, style)


def status(status_: str, has_failed: bool = False) -> str:
    """Colorize status."""
    clrs = {"success": "green", "completed": "green", "failed": "red"}
    color = clrs.get(status_.strip(), "yellow")
    color = "yellow" if has_failed and color == "green" else color
    return ansi(color, status_)


red = ansi_fn("red")
green = ansi_fn("green")
yellow = ansi_fn("yellow")
blue = ansi_fn("blue")
magenta = ansi_fn("magenta")
cyan = ansi_fn("cyan")

bold = ansi_fn("bold")
dim = ansi_fn("dim")
underline = ansi_fn("underline")


def header(text: str, thickness: int = 0):
    """Helper to write text with a header under it."""
    num = len(text)
    if thickness == 1:
        rprint(text)
        rprint("*" * num)
    elif thickness == 0:
        rprint(text)
        rprint("-" * num)
    else:  # pragma: no cover
        rprint(text)
        rprint("=" * num)


def to_ansi(text: str) -> str:
    """Convert rich styles to ansi."""
    console = get_console()
    segments = list(console.render_str(text).render(console))
    return console._render_buffer(segments)
