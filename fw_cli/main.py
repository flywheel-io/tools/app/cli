"""Main CLI entrypoint for the top-level fw command."""

import logging
import platform
import sys
import traceback
from typing import Optional

from click import ClickException
from fw_client import ConnectError, ServerError, TimeoutException, ValidationError
from fw_logging import setup_logging
from rich import print as rprint
from typer import Option
from typer.completion import show_callback as show_completion

from . import ENV_PREFIX, __version__
from .commands.admin import admin
from .commands.gear import gear
from .commands.job import job
from .commands.login import login, logout
from .commands.ls import ls
from .commands.todo import cp
from .commands.update import update
from .commands.xfer import exp_cmd, imp_cmd
from .exceptions import Abort, Error, Exit
from .patch import FWGroup, FWTyper
from .state import get_state
from .styles import cyan, to_ansi

log = logging.getLogger(__name__)
banner = f"Flywheel CLI (beta) {cyan(__version__)}"
wrap_errors = True


def show_version(version_option: bool) -> None:
    """Display the CLI version and exit when using --version."""
    if not version_option:
        return
    rprint(banner)
    raise Exit()


def global_options(  # noqa: PLR0913
    profile: str = Option(
        "default",
        "--profile",
        metavar="NAME",
        envvar=f"{ENV_PREFIX}_PROFILE",
        help="Login profile name to use",
        show_envvar=False,
        show_default=False,
    ),
    debug: bool = Option(
        False,
        "--debug",
        envvar=f"{ENV_PREFIX}_DEBUG",
        help="Enable logging to stderr",
        show_envvar=False,
        show_default=False,
        hidden=True,
    ),
    container_client: Optional[str] = Option(
        "docker",
        "-c",
        "--container-client",
        metavar="NAME",
        envvar=f"{ENV_PREFIX}_CONTAINER_CLIENT",
        help="Override default container client",
        hidden=True,
    ),
    connect_timeout: int = Option(
        10,
        "--connect-timeout",
        metavar="SEC",
        envvar=f"{ENV_PREFIX}_HTTP_CONNECT_TIMEOUT",
        help="Override default HTTP connect timeout seconds",
        show_envvar=False,
        show_default=False,
    ),
    read_timeout: int = Option(
        30,
        "--read-timeout",
        metavar="SEC",
        envvar=f"{ENV_PREFIX}_HTTP_READ_TIMEOUT",
        help="Override default HTTP read timeout seconds",
        show_envvar=False,
        show_default=False,
    ),
    ssl_verify: str = Option(
        "",
        "--ssl-verify",
        metavar="PATH",
        envvar=f"{ENV_PREFIX}_SSL_VERIFY",
        help="Use SSL CA cert file/dir or set to 'no' to disable validation",
        show_default=False,
        show_envvar=False,
    ),
    completion: bool = Option(
        False,
        "--completion",
        help="Show shell completion and exit",
        callback=show_completion,
        is_eager=True,
        show_default=False,
    ),
    version: bool = Option(
        False,
        "-V",
        "--version",
        help="Show the CLI version and exit",
        callback=show_version,
        is_eager=True,
        show_default=False,
    ),
) -> None:
    """Setup logging and update the state with the global options."""
    # TODO make this so eager that it happens before version/update/completion
    state = get_state()
    state.profile_name = profile
    state.debug = debug
    state.connect_timeout = connect_timeout
    state.read_timeout = read_timeout
    state.ssl_verify = ssl_verify
    state.container_client = container_client
    if debug:
        setup_logging(level="DEBUG", handler="stderr")
    else:
        state.log_file.parent.mkdir(parents=True, exist_ok=True)
        setup_logging(level="DEBUG", handler="file", filename=state.log_file)
    log.info(f"Flywheel CLI {__version__} on {platform.platform()}")
    log.info(f"Command: {sys.argv}")
    log.info(f"Using container client {state.container_client}")


class FWRoot(FWGroup):
    """Command group with custom help formatting and error handling."""

    def get_help(self, ctx):
        """Add version banner when displaying the main help."""
        return f"{to_ansi(banner)}\n\n{super().get_help(ctx)}"

    def format_help_text(self, ctx, formatter):
        """Do not display the description on the main command."""
        # by default this would print the global_options docstring

    def invoke(self, ctx):
        """Add error handling - log trace instead of printing."""
        try:
            super().invoke(ctx)
        # let click exceptions through - they are formatted before exiting
        # commands should raise an instance of these when handling errors
        except (Abort, ClickException, Exit):
            raise
        # log connection/server errors and re-raise for display
        except (ConnectError, ServerError, TimeoutException, ValidationError) as exc:
            msg = str(exc)
            log.error(msg)
            raise Error(msg) from None
        # TODO custom / nicer handling of some additional common scenarios:
        # eg. 401 api key expired, etc.
        # log unhandled errors with trace and re-raise for display
        except Exception as exc:
            *trace, exc_str = traceback.format_exception(exc, limit=-1)
            trace_str = "".join(trace).strip().replace("call last", "call")
            msg = f"Unhandled {exc_str}{trace_str}"
            log.error(msg, exc_info=True)
            # TODO show trace when verbose?
            # TODO offer uploading last run log.tar.gz?
            if wrap_errors:
                raise Error(msg) from exc
            # allow re-raising exceptions as-is for debugging in tests
            raise  # pragma: no cover


fw = FWTyper(
    name="fw-beta",
    help="Flywheel Command Line Interface",
    cls=FWRoot,
    callback=global_options,
    add_completion=False,
)
fw.add_command(login)
fw.add_command(cp, hidden=True)
fw.add_command(ls, hidden=True)
fw.add_command(imp_cmd, name="import")
fw.add_command(imp_cmd, name="imports", alias_of="import")
fw.add_command(exp_cmd)
fw.add_command(exp_cmd, name="exports", alias_of="export")
fw.add_command(gear)
fw.add_command(job)
fw.add_command(admin)
fw.add_command(update)
fw.add_command(logout)


if __name__ == "__main__":
    fw(prog_name="fw-beta")  # pragma: no cover
