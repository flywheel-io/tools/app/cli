"""List command."""

import datetime
import re
import typing as t
from functools import cache

from fw_utils import hrsize
from pydantic import BaseModel
from rich import print as rprint
from rich.table import Column, Table
from typer import Argument, Option

from .. import styles
from ..state import get_state
from .utils import autopager

TYPES = ["group", "project", "subject", "session", "acquisition", "analysis", "file"]
FILE_R_ACTIONS = {"files_download", "files_view_contents", "files_view_metadata"}
FILE_W_ACTIONS = {"files_modify_metadata"}
FILE_D_ACTIONS = {"files_delete_non_device_data"}
CONT_R_ACTIONS = {"containers_view_metadata"}
CONT_W_ACTIONS = {
    "containers_create_hierarchy",
    "containers_modify_metadata",
    "files_create_upload",
}
CONT_D_ACTIONS = {"containers_delete_hierarchy"}
PROJ_D_ACTIONS = {"containers_delete_project"}


class Permissions(BaseModel):
    """Permissions model."""

    r: bool = False
    w: bool = False
    d: bool = False


class Node(BaseModel):
    """Node model."""

    type: str
    name: str
    modified: datetime.datetime
    perms: Permissions
    size: t.Optional[int]

    @property
    def sort_key(self) -> t.Tuple[int, str]:
        """Return node sort key."""
        # TODO natsorted
        return TYPES.index(self.type), self.name.lower()

    def __lt__(self, other) -> bool:
        """Return True if this node comes before the other when sorted."""
        return self.sort_key < other.sort_key


def ls(
    path: t.Optional[str] = Argument(
        None,
        help="Flywheel hierarchy path to list [optional]",
    ),
    exhaustive: bool = Option(
        False,
        "-a",
        "--all",
        help="Show all items using site admin role",
    ),
    # ids: bool = Option(
    #     False,
    #     "--ids",
    #     help="Show container and file IDs",
    # ),
) -> None:
    """List files and containers in FW.

    Examples:
    fw-beta ls fw://group/Project
    fw-beta ls --all
    """
    # TODO site-admin --all
    # TODO tab completion (speed? caching?)
    # TODO optional sort by created (newest first)
    # TODO scale (is resolve endpoint viable? unlikely. pagination?)
    # TODO optionally display IDs/URLs? (analyses will be tricky)
    # TODO support json output (maybe yaml, too?)
    # TODO tree? interactivity beyond autocomplete?
    path_list = path.replace("fw://", "").split("/") if path else []
    nodes = sorted(resolve(path_list, exhaustive=exhaustive))
    if not nodes:
        rprint("Nothing to show")
    else:
        autopager(format_table(nodes))


# RESOLVE


def resolve(path: t.List[str], exhaustive: bool = False) -> t.List[Node]:
    """Return list of nodes from the resolve response."""
    state = get_state()
    user_id = state.profile.id
    result = state.fw.post(
        "/api/resolve", json={"path": path}, params={"exhaustive": exhaustive}
    )
    project = result.path[1] if len(result.path) > 1 else None
    name_keys = {"group": "_id", "file": "name"}
    nodes = []
    for child in result.children:
        name_key = name_keys.get(child.node_type, "label")
        perms = project.permissions if project else child.permissions
        perm = next((perm for perm in perms if perm["_id"] == user_id), None)
        node = Node(
            type=child.node_type,
            name=child[name_key],
            perms=get_rwd_perms(child, perm),
            modified=child.modified,
            size=child.get("size"),
        )
        nodes.append(node)
    return nodes


def get_rwd_perms(child, perm) -> Permissions:
    """Return RWD perms for the resolved child node and permissions."""
    perms = Permissions()
    if not perm:
        return perms
    if child.node_type == "group":
        perms.r = perm.access in {"admin", "rw", "ro"}
        perms.w = perm.access in {"admin", "rw"}
        perms.d = perm.access in {"admin"}
    else:
        role_actions = [get_actions(role) for role in perm.role_ids]
        user_actions = set.union(*role_actions)
        is_file = child.node_type == "file"
        r_actions = FILE_R_ACTIONS if is_file else CONT_R_ACTIONS
        w_actions = FILE_W_ACTIONS if is_file else CONT_W_ACTIONS
        d_actions = FILE_D_ACTIONS if is_file else CONT_D_ACTIONS
        if child.node_type == "project":
            d_actions = PROJ_D_ACTIONS
        elif is_file and child.origin.type == "device":
            d_actions.add("files_delete_device_data")
        perms.r = r_actions.issubset(user_actions)
        perms.w = w_actions.issubset(user_actions)
        perms.d = d_actions.issubset(user_actions)
    return perms


@cache
def get_actions(role_id: str) -> t.Set[str]:
    """Return the set of actions for the given role."""
    return set(get_state().fw.get(f"/api/roles/{role_id}").actions)


# FORMAT


SPACER = "-"
STYLES: t.Dict[str, t.Callable] = {
    "perm_t": styles.ansi_fn("bold blue"),
    "perm_t_alt": styles.bold,
    "perm_r": styles.ansi_fn("bold green"),
    "perm_w": styles.ansi_fn("bold yellow"),
    "perm_d": styles.ansi_fn("bold red"),
    "size": styles.ansi_fn("bold green"),
    "unit": styles.green,
    "modified": styles.blue,
    "container": styles.ansi_fn("bold blue"),
}


def format_table(nodes: t.List[Node]) -> Table:
    """Return a list of nodes formatted as a table."""
    header: t.List[t.Union[str, Column]] = [
        "Perms",
        Column("Size", justify="right"),
        "Modified",
        "Name",
    ]
    table = Table(*header, box=None, padding=(0, 1, 0, 0))
    for node in nodes:
        table.add_row(*format_node(node))
    return table


def format_node(node: Node) -> t.List[str]:
    """Return the formatted columns for a node."""
    return [
        format_perms(node),
        format_size(node),
        STYLES["modified"](node.modified.strftime("%Y-%m-%d %H:%M")),
        STYLES["container"](node.name) if node.type != "file" else node.name,
    ]


def format_perms(node: Node) -> str:
    """Return the formatted permissions column for a file."""
    type_map = {"subject": "S", "analysis": "A", "file": "."}
    type_char = type_map.get(node.type, node.type[0])
    type_style = STYLES["perm_t_alt" if type_char in {"A", "."} else "perm_t"]
    perms = [
        type_style(type_char),
        STYLES["perm_r"]("r") if node.perms.r else SPACER,
        STYLES["perm_w"]("w") if node.perms.w else SPACER,
        STYLES["perm_d"]("d") if node.perms.d else SPACER,
    ]
    return "".join(perms)


def format_size(node: Node) -> str:
    """Return the formatted size column for a file."""
    if node.type != "file":
        return SPACER
    match = re.match(r"([\d\.]+)(\w+)", hrsize(node.size or 0))
    size, unit = match.groups() if match else (None, None)  # type: ignore
    return f"{STYLES['size'](size)}{STYLES['unit'](unit)}"  # type: ignore
