"""Update the CLI."""

import os
import platform
import subprocess

import httpx
from rich import print as rprint
from typer import Argument

from ..exceptions import Error, Exit

FW_DIST = "https://storage.googleapis.com/flywheel-dist"


def update(
    version: str = Argument("stable", metavar="[VERSION]", help="Version to update to"),
) -> None:  # pragma: no cover
    """Update the CLI.

    Use the VERSION argument to update to a specific version:

    - latest - the latest dev build (only available if installed via script)
    - stable - the latest tagged version (default)
    - <tag>  - previously tagged version
    """
    # validate the specified version
    install_script = "install.ps1" if platform.system() == "Windows" else "install.sh"
    installer_url = f"{FW_DIST}/fw-cli/{version}/{install_script}"
    response = httpx.head(installer_url)
    if response.status_code in (403, 404):
        raise Error(f"Could not find the specified version: {version})")

    # detect the original installation method and use the same to update
    if os.getenv("FW_CLI_INSTALL_DIR"):
        rprint(f"Updating CLI to {version} using {install_script}")
        if platform.system() == "Windows":
            cmd = ["powershell", "-Command", f"irm {installer_url} | iex"]
            process = subprocess.run(cmd, shell=False, check=False)
            return Exit(process.returncode)
        # installed via the recommended script which sets the install dir
        curl, bash = ["curl", installer_url], ["bash"]
        process_curl = subprocess.Popen(curl, stdout=subprocess.PIPE)
        process_wc = subprocess.Popen(bash, stdin=process_curl.stdout)
        process_curl.stdout.close()
        process_wc.communicate()
        return Exit(process_wc.returncode)

    # TODO consistently rename and enable pypi publishing (git=pypi=docker=fw-cli)
    # TODO uncomment this section when pypi publishing is enabled
    # if "/site-packages/" in __file__:
    #     # installed via pip from pypi
    #     rprint(f"Updating CLI to {version} using pip")
    #     if version == "stable":
    #         spec = ""
    #     elif re.match(r"\d+\.\d+\.\d+", version):
    #         spec = f"=={version}"
    #     else:
    #         raise Error(f"Only SemVer versions supported via pip (got {version})")
    #     raise Exit(subprocess.call(f"pip install --update fw-cli{spec}"))

    # installed from source (either from git+https:// or a local clone)
    raise Error("Cannot update CLI because it wasn't installed via install.sh")
