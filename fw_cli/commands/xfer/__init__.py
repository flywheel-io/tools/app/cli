"""Imports/exports/storages commands."""

from ...patch import FWTyper
from . import exports, imports, storages

# SUB-COMMANDS


imp_schedule_help = "Schedule imports to run later or repeatedly"
imp_schedule = FWTyper(name="schedule", help=imp_schedule_help)
imp_schedule.add_command(imports.create_schedule, name="create")
imp_schedule.add_command(imports.get_schedule, name="get")
imp_schedule.add_command(imports.list_schedules, name="list")
imp_schedule.add_command(imports.list_schedules, name="ls", alias_of="list")
imp_schedule.add_command(imports.update_schedule, name="update")
imp_schedule.add_command(imports.cancel_schedule, name="cancel")

exp_schedule_help = "Schedule exports to run later or repeatedly"
exp_schedule = FWTyper(name="schedule", help=exp_schedule_help)
exp_schedule.add_command(exports.create_schedule, name="create")
exp_schedule.add_command(exports.get_schedule, name="get")
exp_schedule.add_command(exports.list_schedules, name="list")
exp_schedule.add_command(exports.list_schedules, name="ls", alias_of="list")
exp_schedule.add_command(exports.update_schedule, name="update")
exp_schedule.add_command(exports.cancel_schedule, name="cancel")


# COMMANDS


imp_cmd = FWTyper(name="import", help=imports.__doc__)
imp_cmd.add_command(imports.run)
imp_cmd.add_command(imp_schedule)
imp_cmd.add_command(imp_schedule, name="schedules", alias_of="schedule")
imp_cmd.add_command(imports.get)
imp_cmd.add_command(imports.list_, name="list")
imp_cmd.add_command(imports.list_, name="ls", alias_of="list")
imp_cmd.add_command(imports.cancel)
imp_cmd.add_command(imports.rerun)

exp_cmd = FWTyper(name="export", help=exports.__doc__)
exp_cmd.add_command(exports.run)
exp_cmd.add_command(exp_schedule)
exp_cmd.add_command(exp_schedule, name="schedules", alias_of="schedule")
exp_cmd.add_command(exports.get)
exp_cmd.add_command(exports.list_, name="list")
exp_cmd.add_command(exports.list_, name="ls", alias_of="list")
exp_cmd.add_command(exports.cancel)
exp_cmd.add_command(exports.rerun)

storage_cmd = FWTyper(help=storages.__doc__)
storage_cmd.add_command(storages.create)
storage_cmd.add_command(storages.list_, name="list")
storage_cmd.add_command(storages.list_, name="ls", alias_of="list")
storage_cmd.add_command(storages.get)
storage_cmd.add_command(storages.update)
storage_cmd.add_command(storages.delete)
