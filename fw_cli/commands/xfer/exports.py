"""Export data from FW to a storage."""

import enum
import gzip
import math
import re
import time
import typing as t
from datetime import UTC, datetime
from http import HTTPStatus

from fw_client import ClientError, NotFound
from fw_meta.exports import ExportLevel, ExportRule
from fw_storage.config import ConfigOverride as StorageConfigOverride
from fw_utils import AttrDict, get_datetime, hrsize, hrtime
from fw_utils import quantify as q
from pydantic import BaseModel, Field
from rich import print as rprint
from rich.console import Console, Group
from rich.live import Live
from rich.spinner import Spinner
from rich.table import Column, Table
from rich.text import Text
from typer import Argument, BadParameter, Context, Exit, Option

from ... import styles as s
from ...exceptions import Error
from ...state import get_state
from .. import utils
from . import schedules, storages
from .utils import FINALS, ReportFormat, get_status, has_failed, validate_filters


def fail_fast_callback(ctx: Context, value: t.Optional[str]) -> t.Optional[str]:
    """Validate fail-fast."""
    if ctx.resilient_parsing:  # pragma: no cover
        return value
    if value is not None and not re.match(r"^(0|[1-9][0-9]*)%?$", value):
        raise BadParameter("Invalid number/percent")
    return value


class UnzipPath(enum.StrEnum):
    """Unzip member naming strategies."""

    basename = "basename"
    underscore = "underscore"
    original = "original"


class Overwrite(enum.StrEnum):
    """Existing file overwrite strategies."""

    never = "never"
    auto = "auto"
    always = "always"


class Refs(BaseModel):
    """Export references."""

    project: str
    snapshot: t.Optional[str]
    storage: str


class ExportCreate(BaseModel):
    """Export create / POST payload schema."""

    label: str
    description: t.Optional[str]
    refs: Refs
    rules: t.List[ExportRule]
    overwrite_existing: Overwrite
    delete_extra: bool
    dry_run: bool
    limit: t.Optional[int]
    fail_fast: t.Optional[str]
    ignore_conflicts: bool
    storage_override: t.Optional[StorageConfigOverride] = Field(discriminator="type")


project_opt: str = Option(
    ...,
    "-p",
    "--project",
    help="Flywheel project to export files from",
)

storage_opt: str = Option(
    ...,
    "-s",
    "--storage",
    help="Destination storage to export files to",
)

snapshot_opt: t.Optional[str] = Option(
    None,
    metavar="SNAPSHOT",
    help="Use a specific snapshot instead of creating one",
)

label_opt: t.Optional[str] = Option(
    None,
    "--label",
    metavar="TXT",
    help=r"Label to assign to the export  [default: generated]",
)

description_opt: t.Optional[str] = Option(
    None,
    "--description",
    metavar="TXT",
    help=r"Description to assign to the export",
)

level_opt: ExportLevel = Option(
    ExportLevel.acquisition,
    "--level",
    metavar="LVL",
    show_default=False,
    help=f"Hierarchy level to export files from  [{s.cyan('acquisition')}]",
)

include_opt: t.List[str] = Option(
    [],
    "-i",
    "--include",
    metavar="FILT",
    show_default=False,
    help=rf"Include only files matching filter {s.cyan('FILT')}  \[multi allowed]",
)

exclude_opt: t.List[str] = Option(
    [],
    "-e",
    "--exclude",
    metavar="FILT",
    show_default=False,
    help=rf"Exclude any files matching filter {s.cyan('FILT')}  \[multi allowed]",
)

path_opt: str = Option(
    None,
    "--path",
    metavar="TPL",
    show_default=False,
    help=f"Destination path template  [{s.cyan('{prj}/{sub}/{ses}/{acq}/{file}')}]",
)

unzip_opt: bool = Option(
    False,
    "--unzip",
    show_default=False,
    help="Unzip archives when exporting to the destination",
)

unzip_path_opt: UnzipPath = Option(
    UnzipPath.basename,
    "--unzip-path",
    metavar="TYPE",
    show_default=False,
    help=f"Unzip member naming  [{s.cyan('basename')}|underscore|original]",
)

metadata_opt: bool = Option(
    False,
    "--metadata",
    show_default=False,
    help="Export .metadata.json files alongside the data",
)

rules_opt: t.List[str] = Option(
    [],
    "-r",
    "--rule",
    metavar="RULE",
    show_default=False,
    help=r"Additional export rule  \[inline YAML, multi allowed]",
)

overwrite_existing_opt: Overwrite = Option(
    Overwrite.auto,
    "--overwrite",
    metavar="WHEN",
    show_default=False,
    help=f"Overwrite existing files  [{s.cyan('auto')}|never|always]",
)

delete_extra_opt: bool = Option(
    False,
    "--delete-extra",
    show_default=False,
    help="Remove files that are not part of the export",
)

dry_run_opt: bool = Option(
    False,
    "--dry-run",
    show_default=False,
    help="Process files without actually transferring them",
)

limit_opt: t.Optional[int] = Option(
    None,
    "--limit",
    metavar="N",
    help=f"Process max {s.cyan('N')} files and skip the rest",
)

fail_fast_opt: t.Optional[str] = Option(
    None,
    "--fail-fast",
    metavar="N[%]",
    callback=fail_fast_callback,
    help="Stop processing after reaching a failure threshold",
)

wait_opt = Option(
    None,
    "--wait/--no-wait",
    is_eager=True,
    help="Wait for the export to finish before exit",
)

ignore_conflicts_opt: bool = Option(
    False,
    "--ignore-conflicts",
    help="Enable multiple exports on overlapping storages",
)

storage_override_opt: t.Optional[str] = Option(
    None,
    "--storage-config",
    metavar="CFG",
    help=r"Override default storage config \[inline YAML]",
)

# COMMANDS


def get_export_payload(  # noqa: PLR0913
    project: str,
    storage: str,
    snapshot: t.Optional[str],
    label: t.Optional[str],
    description: t.Optional[str],
    level: ExportLevel,
    include: t.List[str],
    exclude: t.List[str],
    path: t.Optional[str],
    unzip: bool,
    unzip_path: UnzipPath,
    metadata: bool,
    rules: t.List[str],
    overwrite_existing: Overwrite,
    delete_extra: bool,
    dry_run: bool,
    limit: t.Optional[int],
    fail_fast: t.Optional[str],
    ignore_conflicts: bool,
    storage_override: t.Optional[str],
) -> dict:
    """Return export creation payload from CLI args w/ project lookup."""
    if limit is not None and limit <= 0:
        # TODO consider requiring>0/ignoring=0 via model (if given)
        raise Error("Positive integer expected for --limit")  # pragma: no cover
    project_id = utils.lookup_project_id(project)
    try:
        storage_type = storages.get_storage(storage)["config"]["type"]
    except ClientError as exc:  # pragma: no cover
        msg = exc.response.json()["message"]
        if msg.startswith("imports_manage or exports_manage"):
            new_msg = msg.replace("imports_manage or exports_manage", "exports_manage")
            raise Error(new_msg) from None
        raise
    override_dict: t.Optional[dict] = None
    if storage_override:
        override_dict = utils.load_storage_override(storage_type, storage_override)
    if not label:
        timestamp = datetime.now(tz=UTC).strftime("%Y-%m-%d %H:%M:%S")
        # TODO brainstorm session w/ PO to design default labels
        label = f"Export - created via CLI at {timestamp}"
    first_rule = {
        "level": level,
        "include": include,
        "exclude": exclude,
        "path": path,
        "unzip": unzip,
        "unzip_path": unzip_path,
        "metadata": metadata,
    }
    rule_dicts = [utils.load_model(ExportRule, rule) for rule in rules]
    rule_dicts.insert(0, first_rule)
    export_payload = ExportCreate(
        label=label,
        description=description,
        refs={"project": project_id, "storage": storage, "snapshot": snapshot},
        rules=rule_dicts,
        overwrite_existing=overwrite_existing,
        delete_extra=delete_extra,
        dry_run=dry_run,
        limit=limit,
        fail_fast=fail_fast,
        ignore_conflicts=ignore_conflicts,
        storage_override=override_dict,
    )
    return export_payload.model_dump(exclude_none=True)


def run(  # noqa: PLR0913
    project: str = project_opt,
    storage: str = storage_opt,
    snapshot: t.Optional[str] = snapshot_opt,
    label: t.Optional[str] = label_opt,
    description: t.Optional[str] = description_opt,
    level: ExportLevel = level_opt,
    include: t.List[str] = include_opt,
    exclude: t.List[str] = exclude_opt,
    path: t.Optional[str] = path_opt,
    unzip: bool = unzip_opt,
    unzip_path: UnzipPath = unzip_path_opt,
    metadata: bool = metadata_opt,
    rules: t.List[str] = rules_opt,
    overwrite_existing: Overwrite = overwrite_existing_opt,
    delete_extra: bool = delete_extra_opt,
    dry_run: bool = dry_run_opt,
    limit: t.Optional[int] = limit_opt,
    fail_fast: t.Optional[str] = fail_fast_opt,
    ignore_conflicts: bool = ignore_conflicts_opt,
    storage_override: t.Optional[str] = storage_override_opt,
    wait: bool = wait_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    r"""Export data from a project via the connector service.

    Examples:
    fw-beta export run \
        --project fw://group/Project \
        --storage d34db33fd34db33fd34db33f \
        --include type=dicom \
        --unzip
    """
    export_payload = get_export_payload(
        project=project,
        storage=storage,
        snapshot=snapshot,
        label=label,
        description=description,
        level=level,
        include=include,
        exclude=exclude,
        path=path,
        unzip=unzip,
        unzip_path=unzip_path,
        metadata=metadata,
        rules=rules,
        overwrite_existing=overwrite_existing,
        delete_extra=delete_extra,
        dry_run=dry_run,
        limit=limit,
        fail_fast=fail_fast,
        ignore_conflicts=ignore_conflicts,
        storage_override=storage_override,
    )
    wait = True if wait is None else wait  # default wait on run
    exp = get_state().fw.post("/xfer/exports", json=export_payload)
    if not wait:
        utils.print_op_started(output, "Export", exp)
        return
    get(exp["_id"], wait=wait, fail=True, output=output)


def get(  # noqa PLR0913
    export_id: str = Argument(
        ...,
        metavar="EXPORT",
        help="Export ID to show the details for",
    ),
    wait: bool = wait_opt,
    fail: bool = Option(
        False,
        "--fail",
        help="Exit with status code 1 if the import failed",
    ),
    report: t.Optional[ReportFormat] = Option(
        None,
        "--report",
        metavar="FMT",
        help=r"Print import report in the given format \[jsonl|csv]",
    ),
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """Get export by ID.

    Examples:
    fw-beta export get <EXPORT>
    """
    fw = get_state().fw
    exp = get_export(export_id)
    finished = get_status(exp) in FINALS
    report = utils.resolve_typer_option_default(report)
    if report and not finished:
        rprint("Reports are only available after completion")
        return
    if report:
        report_url = f"/xfer/exports/{export_id}/report?format={report.value}"
        while not (url := fw.get(report_url).url):
            # TODO stderr spinner / notification when generating large CSV
            time.sleep(1)
        with fw.stream("GET", url) as resp, gzip.open(resp.raw, "rt") as rf:
            for line in rf:
                print(line, end="")
        return
    wait = False if wait is None else wait  # default no-wait on get
    if output == utils.OutputFormat.text:
        show_export_progress(export_id, wait=wait)
        return
    # if json/yaml output requested with wait
    # and not finished shows progress in text mode on stderr
    if wait and not finished:  # pragma: no cover
        stderr_console = Console(stderr=True)
        show_export_progress(export_id, wait=wait, console=stderr_console)
    exp = get_export(export_id, join=["project", "storage"])
    if output == utils.OutputFormat.yaml:
        utils.autopager(utils.format_yaml(exp))
    else:
        utils.autopager(utils.format_json(exp))
    raise Exit(code=1 if fail and exp.status2 == "failed" else 0)


def list_(
    filters: t.Optional[t.List[str]] = Option(
        None,
        "--filter",
        metavar="EXPR",
        help="Filter exports using expressions",
    ),
    after_id: t.Optional[str] = Option(
        None,
        metavar="ID",
        help="Page token to show results after",
    ),
    limit: int = Option(
        25,
        metavar="INT",
        help="Page limit",
    ),
    all_: bool = Option(
        False,
        "--all",
        show_default=False,
        help="Display previous runs of scheduled exports",
    ),
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """List exports.

    Examples:
    fw-beta exports list --filter status=running
    """
    params: dict = {"limit": limit, "join": ["project", "storage"]}
    if filters:  # pragma: no cover
        params["filter"] = ",".join(validate_filters(filters))
    if after_id:  # pragma: no cover
        params["after_id"] = after_id
    if all_:  # pragma: no cover
        params["all"] = True
    try:
        exports = get_state().fw.get("/xfer/exports", params=params)
    except ClientError as exc:  # pragma: no cover
        # older version of xfer does not support join on project and storage
        if exc.status_code == HTTPStatus.UNPROCESSABLE_ENTITY:
            del params["join"]
            exports = get_state().fw.get("/xfer/exports", params=params)
        else:
            raise
    if output == utils.OutputFormat.text:
        rprint(format_page(exports))
    elif output == utils.OutputFormat.yaml:  # pragma: no cover
        utils.autopager(utils.format_yaml(exports))
    else:  # pragma: no cover
        utils.autopager(utils.format_json(exports))


def cancel(
    export_id: str = Argument(
        ...,
        metavar="EXPORT",
        help="Export ID to cancel",
    ),
    wait: bool = wait_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """Cancel an export by ID.

    Examples:
    fw-beta export cancel <ID>
    """
    wait = True if wait is None else wait  # default wait on cancel
    try:
        exp = get_state().fw.post(f"/xfer/exports/{export_id}/cancel")
    except ClientError as exc:  # pragma: no cover
        msg = exc.response.json()["message"]
        if msg.startswith("Cannot cancel"):
            raise Error(msg) from None
        raise
    get(exp["_id"], wait=wait, output=output)


def rerun(
    export_id: str = Argument(
        ...,
        metavar="EXPORT",
        help="Export ID to re-run",
    ),
    wait: bool = wait_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """Rerun an export by ID.

    Examples:
    fw-beta export rerun <ID>
    """
    wait = True if wait is None else wait  # default wait on rerun
    try:
        exp = get_state().fw.post(f"/xfer/exports/{export_id}/rerun")
    except ClientError as exc:  # pragma: no cover
        msg = exc.response.json()["message"]
        if msg.startswith("Cannot re-run"):
            raise Error(msg) from None
        raise
    get(exp["_id"], wait=wait, output=output)


def create_schedule(  # noqa: PLR0913
    project: str = project_opt,
    storage: str = storage_opt,
    snapshot: t.Optional[str] = snapshot_opt,
    label: t.Optional[str] = label_opt,
    description: t.Optional[str] = description_opt,
    level: ExportLevel = level_opt,
    include: t.List[str] = include_opt,
    exclude: t.List[str] = exclude_opt,
    path: t.Optional[str] = path_opt,
    unzip: bool = unzip_opt,
    unzip_path: UnzipPath = unzip_path_opt,
    metadata: bool = metadata_opt,
    rules: t.List[str] = rules_opt,
    overwrite_existing: Overwrite = overwrite_existing_opt,
    delete_extra: bool = delete_extra_opt,
    dry_run: bool = dry_run_opt,
    limit: t.Optional[int] = limit_opt,
    fail_fast: t.Optional[str] = fail_fast_opt,
    ignore_conflicts: bool = ignore_conflicts_opt,
    storage_override: t.Optional[str] = storage_override_opt,
    cron: t.Optional[str] = schedules.cron_opt,
    start: t.Optional[str] = schedules.start_opt,
    end: t.Optional[str] = schedules.end_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """Export data from a project via the connector service."""
    if not label:
        label = "Scheduled export - created via CLI"
    operation = get_export_payload(
        project=project,
        storage=storage,
        snapshot=snapshot,
        label=label,
        description=description,
        level=level,
        include=include,
        exclude=exclude,
        path=path,
        unzip=unzip,
        unzip_path=unzip_path,
        metadata=metadata,
        rules=rules,
        overwrite_existing=overwrite_existing,
        delete_extra=delete_extra,
        dry_run=dry_run,
        limit=limit,
        fail_fast=fail_fast,
        ignore_conflicts=ignore_conflicts,
        storage_override=storage_override,
    )
    operation["type"] = "export"
    schedules.create_schedule(cron, start, end, operation, output)


def get_schedule(
    schedule_id: str = schedules.schedule_id_get_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """Get export schedule."""
    schedules.get_schedule(schedule_id, "export", output)


def list_schedules(
    filters: t.Optional[t.List[str]] = schedules.filters_opt,
    after_id: t.Optional[str] = schedules.after_id_opt,
    limit: int = schedules.limit_opt,
    all_: bool = schedules.all_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """List export schedules."""
    filters = filters or []
    filters.append("operation.type==export")
    if not all_:
        filters.append("active==true")
    params = {"filter": ",".join(filters), "limit": limit}
    if after_id:  # pragma: no cover
        params["after_id"] = after_id
    schedules.list_schedules(params, output)


def update_schedule(
    schedule_id: str = schedules.schedule_id_update_arg,
    cron: t.Optional[str] = schedules.cron_opt,
    start: t.Optional[str] = schedules.start_opt,
    end: t.Optional[str] = schedules.end_opt,
    output: utils.OutputFormat = utils.output_arg,
):
    """Update a scheduled export by ID."""
    fw = get_state().fw
    schedule = fw.get(f"/xfer/schedules/{schedule_id}")
    if (op_type := schedule.operation.type) != "export":
        raise Error(f"Unexpected schedule operation type: {op_type}")
    schedules.update_schedule(schedule_id, cron, start, end, output)


def cancel_schedule(
    schedule_id: str = schedules.schedule_id_cancel_arg,
    output: utils.OutputFormat = utils.output_arg,
):
    """Cancel a scheduled export by ID."""
    fw = get_state().fw
    schedule = fw.get(f"/xfer/schedules/{schedule_id}")
    if (op_type := schedule.operation.type) != "export":
        raise Error(f"Unexpected schedule operation type: {op_type}")
    schedules.cancel_schedule(schedule_id, output)


# HELPERS


def get_export(export_id: str, **params) -> AttrDict:
    """Get an export."""
    try:
        return get_state().fw.get(f"/xfer/exports/{export_id}", params=params)
    except NotFound as exc:
        raise Error(exc.json()["message"]) from None


def show_export_progress(
    export_id: str,
    wait: bool = False,
    console: Console = None,
) -> None:
    """Display and or wait progress of an export."""
    exp = get_export(export_id, join=["project", "storage"])
    src = f"fw://{exp.project.parents.group}/{exp.project.label}"
    dst = exp.get("storage", {}).get("url", "-")

    def get_progress():
        return get_state().fw.get(f"/xfer/exports/{export_id}/progress2")

    def format_lines(stats):
        yield format_status(export_id, src, dst, stats, wait)
        yield from format_progress(stats, show_pbar=wait)

    progress = get_progress()
    group = Group(*format_lines(progress))
    with Live(group, refresh_per_second=20, console=console) as live:
        while wait and get_status(progress) not in FINALS:  # pragma: no cover
            time.sleep(1)
            progress = get_progress()
            live.update(Group(*format_lines(progress)), refresh=True)
    if get_status(progress) == "failed":
        status_msg = progress.get("status_reason") or "Unknown error"
        report_cmd = f"fw-beta export get {export_id} --report=csv"
        raise Error(f"{status_msg}\n> {report_cmd}")


def format_status(
    export_id: str, src: str, dst: str, progress: AttrDict, wait: bool
) -> Group:
    """Format status and source/destination lines."""
    id_line = Text.assemble(("Export", "bold"), (f" {export_id}", "cyan"))
    cols: t.Any = [id_line]
    # show spinner if status is not final
    if wait and get_status(progress) not in FINALS:  # pragma: no cover
        cols.append(Spinner("dots"))
    else:
        cols.append(" ")
    # show status
    cols.append(s.status(get_status(progress), has_failed(progress)))
    if progress.get("elapsed_time"):
        cols.append(f" {hrtime(progress.elapsed_time)}")
    if progress.get("stage"):
        cols.append(f"- {progress.stage.replace('_', ' ')} ...")
    grid = Table.grid(padding=(0, 1))
    grid.add_row(*cols)
    return Group(
        grid,
        Text.assemble(("Project", "bold"), f" {src}"),
        Text.assemble(("Storage", "bold"), f" {dst}"),
    )


def format_progress(stats: AttrDict, show_pbar: bool = False) -> t.Iterable[Table]:
    """Format line with progress bar."""
    totals = stats.get("totals", {})
    cols: t.Any = []
    df, db = totals.get("discovered_files") or 0, totals.get("discovered_bytes") or 0
    ff, fb = totals.get("failed_files") or 0, totals.get("failed_bytes") or 0
    sf, sb = totals.get("skipped_files") or 0, totals.get("skipped_bytes") or 0
    cf, cb = totals.get("created_files") or 0, totals.get("created_bytes")
    rf, rb = totals.get("replaced_files") or 0, totals.get("replaced_bytes")
    tr_f = cf + rf
    tr_b = cb + rb
    pf, pb = totals.get("processed_files") or 0, totals.get("processed_bytes") or 0
    # show progress bar
    if show_pbar:  # pragma: no cover
        pbar = utils.MultiColorProgressBar(
            width=50,
            completed=tr_b,
            failed=fb,
            skipped=sb,
            total=(db or 1),
        )
        cols.append(pbar)
        # show percentage
        percentage = f"{math.ceil(pf / df * 100) if pf and df else 0}%"
        cols.append(f"{percentage:>4}")
        # show speed if available
        if (
            stats.get("current_speed")
            and stats.current_speed.get("bytes_per_sec") is not None
        ):
            cols.append(f"{hrsize(stats.current_speed.bytes_per_sec)}/s")
        grid = Table.grid(padding=(0, 1))
        grid.add_row(*cols)
        yield grid
    # progress stats
    cols.clear()
    grid = Table.grid(padding=(0, 1))
    add_row = grid.add_row
    if df and db:
        add_row(s.bold("Discovered"), f"{q(df, 'file'):>15}", f"{hrsize(db):>10}")
    if pf and pb:
        add_row(s.bold("Processed"), f"{q(pf, 'file'):>15}", f"{hrsize(pb):>10}")
    if sf and sb:
        add_row(s.bold("Skipped"), f"{q(sf, 'file'):>15}", f"{hrsize(sb):>10}")
    if tr_f or tr_b:
        add_row(s.green("Transferred"), f"{q(tr_f, 'file'):>15}", f"{hrsize(tr_b):>10}")
    if ff or fb:
        add_row(s.red("Failed"), f"{q(ff, 'file'):>15}", f"{hrsize(fb):>10}")
    if grid.rows:
        yield grid


def format_page(page: dict) -> t.Union[Text, Table]:
    """Format list of exports page as text."""
    if not page["results"]:
        return Text("Nothing to show")
    cols = [
        Column("ID", max_width=24),
        Column("Status", max_width=22),
        Column("Elapsed", max_width=8),
        Column("Completed", max_width=12),
        Column("Source", max_width=35, no_wrap=True),
        Column("Destination", max_width=35, no_wrap=True),
    ]
    table = Table(*cols, box=None, padding=(0, 2, 0, 0))
    for exp in page["results"]:
        cells = []
        cells.append(s.cyan(exp["_id"]))
        cells.append(s.status(get_status(exp), has_failed(exp)))
        total_time, completed = exp.get("total_time"), exp.get("completed")
        cells.append(hrtime(total_time) if total_time else "-")
        c_ago = None
        if completed:
            c_ago = (get_datetime() - get_datetime(completed)).total_seconds()
        cells.append(f"{hrtime(c_ago)} ago" if c_ago else "-")
        prj, storage = exp.get("project"), exp.get("storage")
        cells.append(f"fw://{prj['parents']['group']}/{prj['label']}" if prj else "-")
        cells.append(storage["url"] if storage else "-")
        table.add_row(*cells)
    return table
