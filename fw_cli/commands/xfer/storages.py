"""Manage external storages for hosted exports."""

import enum
import time
import typing as t
from datetime import datetime

from fw_client import errors as client_errors
from fw_storage import config
from fw_utils import AttrDict
from rich import print as rprint
from rich.live import Live
from rich.table import Table
from rich.text import Text
from typer import Argument, Option

from ...exceptions import Error, Exit
from ...state import get_state
from ...styles import cyan, status
from .. import utils
from ..utils import OutputFormat, autopager, format_json, format_yaml


class StorageMode(enum.StrEnum):
    """Storage mode - enabling imports, exports."""

    all = "all"
    imp = "import"
    exp = "export"


def create(  # noqa: PLR0913
    url: str = Option(
        None,
        "-u",
        "--url",
        metavar="URL",
        help="Storage connection URL",
    ),
    provider: str = Option(
        None,
        metavar="PRVD",
        help="Create storage from existing core-api storage provider",
    ),
    label: t.Optional[str] = Option(
        None,
        metavar="TXT",
        help=r"Label to assign to the storage  [default: generated]",
    ),
    group: t.Optional[str] = Option(
        None,
        "-g",
        "--group",
        metavar="GRP",
        help="Group to bind storage use and permissions to",
    ),
    project: t.Optional[str] = Option(
        None,
        "-p",
        "--project",
        metavar="PRJ",
        help="Project to bind storage use and permissions to",
    ),
    connector: t.Optional[str] = Option(
        None,
        metavar="ID",
        help="Connector to attach the storage to",
    ),
    mode: StorageMode = Option(
        StorageMode.all,
        "-m",
        "--mode",
        metavar="M",
        show_default=False,
        help=(
            f"Set mode to allow only imports or exports  [{cyan('all')}|import|export]"
        ),
    ),
    scan: t.Optional[bool] = Option(
        None,
        "--scan/--no-scan",
        help="Scan files for malware when importing from the storage",
    ),
    wait: bool = Option(
        True,
        "--wait/--no-wait",
        help="Wait for the status check to finish before exit",
    ),
) -> None:
    r"""Register a storage for imports and exports.

    Examples:
    fw-beta admin storage create \
        --url "s3://bucket?access_key_id=...&secret_access_key=..."
    fw-beta admin storage create --url "gs://bucket?application_credentials=..."
    fw-beta admin storage create --url "az://account/container?access_key=..."

    URLs:
    _Type_  _Format_               _Params_
    Amazon  s3://bkt\[/pfx]         access_key_id, secret_access_key
    Google  gs://bkt\[/pfx]         application_credentials
    Azure   az://acct/cont\[/pfx]   access_key

    Creds:
    Credentials may be provided explicitly with query-params as in the examples
    above or can be inferred from environment variables listed below.

    AWS_ACCESS_KEY_ID / AWS_SECRET_ACCESS_KEY
    GOOGLE_APPLICATION_CREDENTIALS  (may be a filepath)
    AZURE_ACCESS_KEY
    """
    if url and provider:
        raise Error("Cannot specify both --url and --provider")
    if not (url or provider):
        raise Error("Either --url or --provider required")
    if not label:
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        label = f"Created via CLI at {timestamp}"
    payload: dict = {
        "label": label,
        "refs": {
            "group": group,
            "project": project,
            "connector": connector,
        },
    }
    if url:
        # TODO implement proper factory in fw-storage
        CONFIG_CLASSES = {
            "fs": config.FSConfig,
            "s3": config.S3Config,
            "gs": config.GSConfig,
            "az": config.AZConfig,
        }
        config_cls = CONFIG_CLASSES.get(url[:2])
        # cls.from_url handles env loading & validation
        conf = config_cls.from_url(url)  # type: ignore
        payload["url"] = conf.full_url
        # fs:// should use an absolute path in remote/connector context
        # TODO revisit when/where relpaths are resolved (and defer it)
        if url.startswith("fs://") and url[5] != "/":
            raise Error("Absolute fs:// storage path required")
    else:
        payload["provider"] = provider
    if mode == "all":
        payload["imports_enabled"] = True
        payload["exports_enabled"] = True
    if mode == "import":
        payload["imports_enabled"] = True
        payload["exports_enabled"] = False
    if mode == "export":
        payload["imports_enabled"] = False
        payload["exports_enabled"] = True
    if scan is not None:  # pragma: no cover
        payload["malware_scan"] = scan

    fw = get_state().fw
    storage = fw.post("/xfer/storages", json=payload)
    text = format_storage(storage)
    with Live(text, auto_refresh=False) as live:  # pragma: no cover
        while wait and storage.status_check not in {"success", "failed"}:
            time.sleep(1)
            storage = fw.get(f"/xfer/storages/{storage['_id']}")
            live.update(format_storage(storage), refresh=True)

    if storage.status_check == "failed":
        raise Exit(code=1)
    raise Exit(code=0)


def list_(
    filters: t.Optional[t.List[str]] = Option(
        None,
        "--filter",
        metavar="EXPR",
        help="Filter storages using expressions",
    ),
    sort: t.Optional[str] = Option(
        None,
        metavar="FIELD",
        help="Sort results by the given field[:order]",
    ),
    after_id: t.Optional[str] = Option(
        None,
        metavar="ID",
        help="Page token to show results after",
    ),
    limit: int = Option(
        25,
        metavar="INT",
        help=r"Page limit",
    ),
    output: OutputFormat = utils.output_arg,
) -> None:
    """List storages.

    Examples:
    fw-beta admin storages list --filter status_check=success
    """
    fw = get_state().fw
    params: dict = {"limit": limit}
    if filters:
        params["filter"] = ",".join(filters)
    if sort:
        params["sort"] = sort
    if after_id:
        params["after_id"] = after_id
    response = fw.get("/xfer/storages", params=params)
    if output == OutputFormat.text:
        rprint(format_page(response))
    elif output == OutputFormat.yaml:
        autopager(format_yaml(response))
    else:
        autopager(format_json(response))


def get(
    storage_id: str = Argument(
        ...,
        metavar="STORAGE",
        help="Storage ID to show the details for",
    ),
    output: OutputFormat = utils.output_arg,
) -> None:
    """Get storage by ID.

    Examples:
    fw-beta admin storage get <STORAGE>
    """
    storage = get_storage(storage_id)
    if output == OutputFormat.text:
        rprint(format_storage(storage))
    elif output == OutputFormat.yaml:
        autopager(format_yaml(storage))
    else:
        autopager(format_json(storage))

    if storage.status_check == "failed":
        raise Exit(code=1)
    raise Exit(code=0)


def update(  # noqa: PLR0913
    storage_id: str = Argument(
        ...,
        metavar="STORAGE",
        help="Storage ID to update",
    ),
    label: t.Optional[str] = Option(
        None,
        metavar="TXT",
        help="Label to assign to the storage",
    ),
    config: t.List[str] = Option(
        [],
        metavar="KEY=VAL",
        help="Storage config options",
    ),
    mode: t.Optional[StorageMode] = Option(
        None,
        "-m",
        "--mode",
        metavar="M",
        show_default=False,
        help="Set mode to allow only imports or exports  [import|export]",
    ),
    scan: t.Optional[bool] = Option(
        None,
        "--scan/--no-scan",
        help="Scan files for malware when importing from the storage",
    ),
    wait: bool = Option(
        True,
        "--wait/--no-wait",
        help="Wait for the status check to finish before exit",
    ),
) -> None:
    """Update an existing storage by ID."""
    storage = get_storage(storage_id)
    payload: dict = {}
    if config:
        kv_pairs = [c.split("=", maxsplit=1) for c in config]
        invalid = [kv[0] for kv in kv_pairs if len(kv) == 1]
        if invalid:
            raise Error(f"Missing value for `--config`: {', '.join(invalid)}")
        nulls = ["none", "null"]
        payload["config"] = {k: None if v.lower() in nulls else v for k, v in kv_pairs}
        immutable = set(payload["config"].keys()) & {"type", "path", "bucket"}
        if immutable:
            raise Error(f"Storage {', '.join(immutable)} cannot be changed")
        payload["config"]["type"] = storage.config.type
    if label:
        payload["label"] = label
    if mode == "all":  # pragma: no cover
        payload["imports_enabled"] = True
        payload["exports_enabled"] = True
    if mode == "import":  # pragma: no cover
        payload["imports_enabled"] = True
        payload["exports_enabled"] = False
    if mode == "export":  # pragma: no cover
        payload["imports_enabled"] = False
        payload["exports_enabled"] = True
    if scan is not None:
        payload["malware_scan"] = scan

    fw = get_state().fw
    storage = fw.patch(f"/xfer/storages/{storage_id}", json=payload)
    text = format_storage(storage)
    with Live(text, auto_refresh=False) as live:  # pragma: no cover
        while wait and storage.status_check not in {"success", "failed"}:
            time.sleep(1)
            storage = fw.get(f"/xfer/storages/{storage['_id']}")
            live.update(format_storage(storage), refresh=True)

    if storage.status_check == "failed":  # pragma: no cover
        raise Exit(code=1)
    raise Exit(code=0)


def delete(
    storage_id: str = Argument(
        ...,
        metavar="STORAGE",
        help="Storage ID to delete",
    ),
) -> None:
    """Delete a storage by ID."""
    try:
        get_state().fw.delete(f"/xfer/storages/{storage_id}")
    except client_errors.NotFound:
        raise Error("Storage not found") from None
    rprint("Storage deleted")


def get_storage(storage_id: str, **params) -> AttrDict:
    """Get a storage."""
    if not utils.ID_RE.match(storage_id):
        raise Error(f"Invalid storage id: {storage_id}")
    try:
        return get_state().fw.get(f"/xfer/storages/{storage_id}", params=params)
    except client_errors.NotFound as exc:
        raise Error(exc.json()["message"]) from None
    except client_errors.ClientError as exc:  # pragma: no cover
        msg = exc.response.json()["message"]
        if msg.startswith("imports_manage or exports_manage"):
            new_msg = msg.replace("imports_manage or exports_manage", "imports_manage")
            raise Error(new_msg) from None
        raise


def format_storage(storage: dict) -> Table:
    """Format single storage as text."""
    table = Table(show_header=False, box=None, padding=(0, 1, 0, 0))
    table.add_column(style="bold", justify="right")
    table.add_row("ID", cyan(storage["_id"]))
    table.add_row("URL", storage["url"])
    table.add_row("Label", storage["label"])
    if storage.get("provider"):
        table.add_row("Reference", "True")
    table.add_row("Check", status(storage["status_check"]))
    if storage.get("status_check_reason"):
        table.add_row("Reason", storage["status_check_reason"])
    if storage.get("notes"):
        table.add_row("Notes", storage["notes"])
    return table


def format_page(page: dict) -> t.Union[Text, Table]:
    """Format storages page as text."""
    if not page["results"]:
        return Text("Nothing to show")
    header = ["ID", "Type", "Check", "Label"]
    table = Table(*header, box=None, padding=(0, 1, 0, 0))
    for storage in page["results"]:
        table.add_row(
            cyan(storage["_id"]),
            storage["config"]["type"],
            status(storage["status_check"]),
            storage["label"],
        )
    return table
