"""Common xfer related utils."""

import enum
import re


class ReportFormat(enum.StrEnum):
    """Available operation report formats."""

    jsonl = "jsonl"
    csv = "csv"


FINALS = {"success", "completed", "skipped", "failed", "canceled", "cancelled"}
COMPAT = {"canceling": "cancelling", "canceled": "cancelled"}


def get_status(op_or_progress: dict) -> str:
    """Return operation status."""
    status = op_or_progress.get("status2", op_or_progress.get("status")) or ""
    return COMPAT.get(status, status)


def is_final(op_or_progress: dict) -> bool:
    """Return True if the operation is in a final state."""
    return get_status(op_or_progress) in FINALS


def has_failed(op_or_progress: dict) -> bool:
    """Return True if the operation has failed files."""
    return op_or_progress.get("totals", {}).get("failed_files", 0) > 0


FILTER_OPS = ["=~", "==", "!=", "<=", ">=", "<", ">"]
OPERATOR_RE = re.compile(rf"({'|'.join(FILTER_OPS).replace('==', '==?')})")
STATUS2_COMPAT = {
    "canceling": "cancelling",
    "canceled": "cancelled",
    "success": "completed",
}
STATUS_COMPAT = {v: k for k, v in STATUS2_COMPAT.items()}


def validate_filters(filters: list[str]) -> list[str]:
    """Validate list filters."""
    validated_filters = set()
    for filter_str in filters:
        field, op, val = parse_filter(filter_str)
        if field in ["status", "status2"]:
            val = STATUS_COMPAT.get(val, val)
            validated_filters.add(f"status{op}{val}")
            val = STATUS2_COMPAT.get(val, val)
            validated_filters.add(f"status2{op}{val}")
        else:
            validated_filters.add(f"{field}{op}{val}")
    return sorted(validated_filters)


def parse_filter(filter_str: str) -> tuple[str, str, str]:
    """Parse filter string."""
    expr_split = OPERATOR_RE.split(filter_str, maxsplit=1)
    if len(expr_split) != 3:
        raise ValueError(f"Invalid filter expression: {filter_str}")
    return expr_split
