"""Xfer schedule models and helpers."""

import typing as t

from croniter import croniter
from fw_client import NotFound
from fw_utils import AttrDict, get_datetime
from pydantic import BaseModel, field_validator
from rich import print as rprint
from rich.table import Column, Table
from rich.text import Text
from typer import Argument, Option

from ... import styles as s
from ...exceptions import Error
from ...state import get_state
from .. import utils

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


def validate_cron(cls, value: t.Optional[str]) -> t.Optional[str]:
    """Validate cron expression."""
    if value:
        assert croniter.is_valid(value), "invalid cron expression"
    return value


class ScheduleCreate(BaseModel):
    """Schedule POST payload schema."""

    start: t.Optional[utils.DateTime] = None
    end: t.Optional[utils.DateTime] = None
    cron: t.Optional[str] = None
    operation: dict

    validate_cron = field_validator("cron")(validate_cron)


class ScheduleUpdate(BaseModel):
    """Schedule PATCH payload schema."""

    start: t.Optional[utils.DateTime] = None
    end: t.Optional[utils.DateTime] = None
    cron: t.Optional[str] = None

    validate_cron = field_validator("cron")(validate_cron)


def create_schedule(
    cron: t.Optional[str],
    start: t.Optional[str],
    end: t.Optional[str],
    operation: dict,
    output: utils.OutputFormat,
) -> None:
    """Create a schedule."""
    schedule_create = ScheduleCreate(
        start=start,
        end=end,
        cron=cron,
        operation=operation,
    )
    payload = schedule_create.model_dump(exclude_none=True)
    schedule = get_state().fw.post("/xfer/schedules", json=payload)
    # TODO design text output
    if output == utils.OutputFormat.text:
        rprint(f"Created schedule with ID {schedule['_id']}")
    elif output == utils.OutputFormat.yaml:  # pragma: no cover
        utils.autopager(utils.format_yaml(schedule))
    else:  # pragma: no cover
        utils.autopager(utils.format_json(schedule))


def get_schedule(schedule_id: str, op_type: str, output: utils.OutputFormat) -> None:
    """Get a schedule by id and type."""
    params = {"join": ["project", "storage"]}
    try:
        schedule = get_state().fw.get(f"/xfer/schedules/{schedule_id}", params=params)
    except NotFound as exc:
        raise Error(exc.json()["message"]) from None
    if (got_type := schedule.operation.type) != op_type:
        raise Error(f"Unexpected schedule operation type: {got_type}")
    if output == utils.OutputFormat.text:
        print_schedule(schedule)
    elif output == utils.OutputFormat.yaml:  # pragma: no cover
        utils.autopager(utils.format_yaml(schedule))
    else:  # pragma: no cover
        utils.autopager(utils.format_json(schedule))


def list_schedules(params: dict, output: utils.OutputFormat) -> None:
    """List schedules."""
    params.setdefault("join", ["project", "storage"])
    page = get_state().fw.get("/xfer/schedules", params=params)
    if output == utils.OutputFormat.text:
        rprint(format_schedule_page(page))
    elif output == utils.OutputFormat.yaml:  # pragma: no cover
        utils.autopager(utils.format_yaml(page))
    else:  # pragma: no cover
        utils.autopager(utils.format_json(page))


def update_schedule(
    schedule_id: str,
    cron: t.Optional[str],
    start: t.Optional[str],
    end: t.Optional[str],
    output: utils.OutputFormat,
) -> None:
    """Update a schedule by id."""
    update_dict = {"cron": cron, "start": start, "end": end}
    update_dict = {k: v for k, v in update_dict.items() if v is not None}
    update_dict = {k: v if v != "null" else None for k, v in update_dict.items()}
    update = ScheduleUpdate(**update_dict)
    payload = update.model_dump(exclude_unset=True)
    schedule = get_state().fw.patch(f"/xfer/schedules/{schedule_id}", json=payload)
    # TODO design text output
    if output == utils.OutputFormat.text:
        rprint(f"Updated schedule with ID {schedule_id}")
    elif output == utils.OutputFormat.yaml:  # pragma: no cover
        utils.autopager(utils.format_yaml(schedule))
    else:  # pragma: no cover
        utils.autopager(utils.format_json(schedule))


def cancel_schedule(schedule_id: str, output: utils.OutputFormat) -> None:
    """Cancel a schedule by id."""
    schedule = get_state().fw.patch(
        f"/xfer/schedules/{schedule_id}", json={"active": False}
    )
    # TODO design text output
    if output == utils.OutputFormat.text:
        rprint(f"Canceled schedule with ID {schedule._id}")
    elif output == utils.OutputFormat.yaml:  # pragma: no cover
        utils.autopager(utils.format_yaml(schedule))
    else:  # pragma: no cover
        utils.autopager(utils.format_json(schedule))


def print_schedule(schedule: AttrDict) -> None:
    """Print schedule."""
    rprint(Text.assemble(("ID", "bold"), f" {schedule['_id']}"))
    if not schedule["active"]:
        rprint(Text.assemble(("Active", "bold"), f" {schedule['active']}"))
    if schedule.get("start"):
        rprint(Text.assemble(("Start", "bold"), f" {fmt_dt(schedule['start'])}"))
    if schedule.get("end"):
        rprint(Text.assemble(("End", "bold"), f" {fmt_dt(schedule['end'])}"))
    if schedule.get("cron"):
        rprint(Text.assemble(("Cron", "bold"), f" {schedule['cron']}"))

    project = f"fw://{schedule.project.parents.group}/{schedule.project.label}"
    rprint(Text.assemble(("Project", "bold"), f" {project}"))
    rprint(Text.assemble(("Storage", "bold"), f" {schedule.storage.url}"))


def format_schedule_page(page: dict) -> t.Union[Text, Table]:
    """Format list of schedules page as text."""
    if not page["results"]:
        return Text("Nothing to show")
    cols = [
        Column("ID", max_width=24),
        Column("Source", max_width=40),
        Column("Destination", max_width=40),
    ]
    table = Table(*cols, box=None, padding=(0, 2, 0, 0))
    get_state().fw
    for sch in page["results"]:
        project = f"fw://{sch.project.parents.group}/{sch.project.label}"
        storage = sch.storage.url
        optype = sch["operation"]["type"]
        src, dst = (storage, project) if optype == "import" else (project, storage)
        cells = [s.cyan(sch["_id"]), src, dst]
        table.add_row(*cells)
    return table


def fmt_dt(timestamp: str) -> str:
    """Format datetime with seconds precision for text outputs."""
    return get_datetime(timestamp).strftime(DATETIME_FORMAT)


schedule_id_get_opt: str = Argument(
    ...,
    metavar="SCHEDULE",
    help="Schedule ID to show the details for",
)

schedule_id_update_arg: str = Argument(
    ...,
    metavar="ID",
    help="Schedule ID to update",
)

schedule_id_cancel_arg: str = Argument(
    ...,
    metavar="ID",
    help="Schedule ID to cancel",
)

cron_opt: t.Optional[str] = Option(
    None,
    "--cron",
    help="Cron spec to re-run the periodically",
)

start_opt: t.Optional[str] = Option(
    None,
    "--start",
    metavar="DATETIME",
    help="Start time to run once (or enable periodic cron at)",
)

end_opt: t.Optional[str] = Option(
    None,
    "--end",
    metavar="DATETIME",
    help="End time to stop periodic cron runs",
)

filters_opt: t.Optional[t.List[str]] = Option(
    None,
    "--filter",
    metavar="EXPR",
    help="Filter schedules using expressions",
)

after_id_opt: t.Optional[str] = Option(
    None,
    metavar="ID",
    help="Page token to show results after",
)

limit_opt: int = Option(
    25,
    metavar="INT",
    help=r"Page limit",
)

all_opt: bool = Option(
    False,
    "--all",
    help="Display all schedules",
)
