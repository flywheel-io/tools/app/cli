"""Import data from a storage to FW."""

import asyncio
import enum
import gzip
import json
import logging
import math
import re
import time
import traceback
import typing as t
from collections import defaultdict, deque
from copy import deepcopy
from dataclasses import dataclass
from datetime import UTC, datetime, timedelta
from functools import partial
from itertools import islice
from pathlib import Path
from threading import Thread

import aiofiles
from fw_client import ClientError, Conflict, NotFound
from fw_meta.imports import ImportLevel, ImportRule
from fw_storage import create_storage_client
from fw_storage.config import ConfigOverride as StorageConfigOverride
from fw_utils import AttrDict, get_datetime, hrsize, hrtime
from fw_utils import quantify as q
from pydantic import BaseModel, Field
from rich import print as rprint
from rich.console import Console, Group
from rich.live import Live
from rich.markup import escape
from rich.spinner import Spinner
from rich.table import Column, Table
from rich.text import Text
from rich.tree import Tree
from typer import Argument, BadParameter, Context, Exit, Option

from ... import styles as s
from ...exceptions import Error
from ...state import CONCURRENT_CHUNK_UPLOADS, CONCURRENT_FILE_UPLOADS, get_state
from .. import utils
from . import schedules, storages
from .utils import ReportFormat, get_status, has_failed, is_final, validate_filters

log = logging.getLogger(__name__)
filt_str = rf"files matching filter {s.cyan('FILT')}  \[multi allowed]"

DONE = object()
TEN_MB = 10 << 20


class ConflictStrategy(enum.StrEnum):
    """Action to perform on conflict."""

    skip = "skip"
    update = "update"
    force_update = "force_update"  # not advertized
    review = "review"


class MissingMeta(enum.StrEnum):
    """Action to perform on missing metadata."""

    fail = "fail"
    skip = "skip"


class UIDScope(enum.StrEnum):
    """UID scope."""

    site = enum.auto()
    group = enum.auto()
    project = enum.auto()
    none = enum.auto()


def limit_callback(ctx: Context, value: t.Optional[int]) -> t.Optional[int]:
    """Validate limit."""
    if ctx.resilient_parsing:  # pragma: no cover
        return value
    if value is not None and value <= 0:
        raise BadParameter("Positive integer expected")
    return value


def fail_fast_callback(ctx: Context, value: t.Optional[str]) -> t.Optional[str]:
    """Validate fail-fast."""
    if ctx.resilient_parsing:  # pragma: no cover
        return value
    if value is not None and not re.match(r"^(0|[1-9][0-9]*)%?$", value):
        raise BadParameter("Invalid number/percent")
    return value


def resume_local_import(ctx: Context, value: t.Optional[str]) -> t.Optional[str]:
    """Resume a previously interrupted local upload."""
    if ctx.resilient_parsing or not value:  # pragma: no cover
        return value

    imp = get_import(value, join=["storage", "blob_session"])
    err_prefix = "Cannot use --resume-local: import"
    if is_final(imp):
        raise Error(f"{err_prefix} already terminated ({get_status(imp)})")
    if not (blob_session := imp.get("blob_session")):
        raise Error(f"{err_prefix} storage is not local ({imp.storage.url})")
    if not (local_path := blob_session.get("local_path")):
        raise Error(f"{err_prefix} storage is not local ({imp.storage.url})")
    src_dir = Path(local_path).expanduser().resolve()
    if not src_dir.is_dir():
        raise Error(f"{err_prefix} local path is not a directory ({src_dir})")
    uploader = LocalFileUploader(imp, blob_session, src_dir)
    uploader.start()

    try:
        get_(
            imp["_id"],
            tree=ctx.params.get("tree", False),
            wait=True,
            fail=True,
            output=ctx.params.get("output", utils.OutputFormat.text),
            local_uploader=uploader,
        )
    finally:
        if uploader:
            uploader.stop()
            uploader.join()
    raise Exit()


project_opt: str = Option(
    ...,
    "-p",
    "--project",
    metavar="PRJ",
    help="Flywheel project to import files to",
)
storage_opt: str = Option(
    ...,
    "-s",
    "--storage",
    metavar="ID",
    help="Storage to import files from",
)
label_opt: t.Optional[str] = Option(
    None,
    "--label",
    metavar="TXT",
    help=r"Label to assign to the import  [default: generated]",
)
description_opt: t.Optional[str] = Option(
    None,
    "--description",
    metavar="TXT",
    help=r"Description to assign to the import",
)
level_opt: t.Optional[ImportLevel] = Option(
    None,
    "-l",
    "--level",
    metavar="LVL",
    help="Flywheel container hierarchy level to import files to",
)
include_opt: t.List[str] = Option(
    [],
    "-i",
    "--include",
    metavar="FILT",
    show_default=False,
    help=f"Include only {filt_str}",
)
exclude_opt: t.List[str] = Option(
    [],
    "-e",
    "--exclude",
    metavar="FILT",
    show_default=False,
    help=f"Exclude any {filt_str}",
)
type_opt: t.Optional[str] = Option(
    None,
    "-t",
    "--type",
    metavar="TYPE",
    help="Data type to import matching files as",
)
zip_opt: t.Optional[bool] = Option(
    None,
    "--zip",
    help="Import multiple grouped files zipped together",
)
mappings_opt: t.List[str] = Option(
    [],
    "-m",
    "--mapping",
    metavar="SRC=DST",
    help=r"Metadata mapping patterns  \[multi allowed]",
)
defaults_opt: t.List[str] = Option(
    None,
    "--default",
    metavar="DST=VAL",
    help=r"Metadata fallback defaults  \[multi allowed]",
    hidden=True,
)
overrides_opt: t.List[str] = Option(
    None,
    "--override",
    metavar="DST=VAL",
    help=r"Metadata manual overrides  \[multi allowed]",
    hidden=True,
)
rules_opt: t.List[str] = Option(
    [],
    "-r",
    "--rule",
    metavar="RULE",
    show_default=False,
    help=r"Additional import rules  \[inline YAML, multi allowed]",
    hidden=True,
)
conflict_strategy_opt: t.Optional[ConflictStrategy] = Option(
    None,
    "--conflict-strategy",
    metavar="S",
    show_default=False,
    help=f"Conflict strategy  [{s.cyan('skip')}|update|review]",
)
dry_run_opt: bool = Option(
    False,
    "--dry-run",
    show_default=False,
    help="Process files without actually transferring them",
)
limit_opt: t.Optional[int] = Option(
    None,
    "--limit",
    metavar="N",
    callback=limit_callback,
    help="Stop processing after reaching a given file limit",
)
fail_fast_opt: t.Optional[str] = Option(
    None,
    "--fail-fast",
    metavar="N[%]",
    callback=fail_fast_callback,
    help="Stop processing after reaching a failure threshold",
)
missing_meta_opt: MissingMeta = Option(
    MissingMeta.fail,
    "--missing-meta",
    metavar="MODE",
    show_default=False,
    help=f"Fail or skip items with missing metadata [{s.cyan('fail')}|skip]",
)
storage_override_opt: t.Optional[str] = Option(
    None,
    "--storage-config",  # TODO consider renaming upstream
    metavar="CFG",
    help=r"Override default storage config \[inline YAML]",
)
uid_scope_opt: UIDScope | None = Option(
    None,
    "--uid-scope",
    metavar="SCOPE",
    help=r"Scope of UID uniqueness \[site|group|project|none]",
)
malware_scan_opt: bool | None = Option(
    None,
    "--scan/--no-scan",
    hidden=True,
    help="Scan files for malware when importing",
)
tree_opt = Option(
    False,
    "--tree",
    help="Display last import items in a hierarchy",
    is_eager=True,
)
wait_opt = Option(
    None,
    "--wait/--no-wait",
    help="Wait for the import to finish before exit",
)
resume_local_opt = Option(
    None,
    "--resume-local",
    metavar="IMPORT",
    help="Resume a previously interrupted local upload",
    callback=resume_local_import,
    is_eager=True,
)


def run(  # noqa: PLR0913
    project: str = project_opt,
    storage: str = storage_opt,
    label: t.Optional[str] = label_opt,
    description: t.Optional[str] = description_opt,
    level: t.Optional[ImportLevel] = level_opt,
    include: t.List[str] = include_opt,
    exclude: t.List[str] = exclude_opt,
    type: t.Optional[str] = type_opt,
    zip: t.Optional[bool] = zip_opt,
    mappings: t.List[str] = mappings_opt,
    defaults: t.List[str] = defaults_opt,
    overrides: t.List[str] = overrides_opt,
    rules: t.List[str] = rules_opt,
    conflict_strategy: t.Optional[ConflictStrategy] = conflict_strategy_opt,
    dry_run: bool = dry_run_opt,
    limit: t.Optional[int] = limit_opt,
    fail_fast: t.Optional[str] = fail_fast_opt,
    missing_meta: MissingMeta = missing_meta_opt,
    storage_override: t.Optional[str] = storage_override_opt,
    uid_scope: t.Optional[UIDScope] = uid_scope_opt,
    malware_scan: t.Optional[bool] = malware_scan_opt,
    resume_local: t.Optional[str] = resume_local_opt,
    tree: bool = tree_opt,
    wait: bool = wait_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    r"""Import data from a storage to a project via the connector service.

    Example:
    fw-beta import run \
        --project fw://group/Project \
        --storage d34db33fd34db33fd34db33f \
        --exclude 'path=~.DS_Store' \
        --mapping 'path={subject.label}/{session.label}/{acquisition.label}/*'
    """
    blob_session = src_dir = None
    if Path(storage).is_dir():
        src_dir = Path(storage).expanduser().resolve()
        wait = True
        project_id = utils.lookup_project_id(project)
        payload = {"project_id": project_id, "local_path": str(src_dir)}
        blob_session = get_state().fw.post("/xfer/blob-sessions", json=payload)

    wait = True if wait is None else wait  # default wait on run
    payload = get_import_payload(
        project=project if not blob_session else None,
        storage=storage if not blob_session else None,
        blob_session=blob_session._id if blob_session else None,
        label=label,
        description=description,
        level=level,
        include=include,
        exclude=exclude,
        type=type,
        zip=zip,
        mappings=mappings,
        defaults=defaults,
        overrides=overrides,
        rules=rules,
        conflict_strategy=conflict_strategy,
        dry_run=dry_run,
        limit=limit,
        fail_fast=fail_fast,
        missing_meta=missing_meta,
        storage_override=storage_override,
        uid_scope=uid_scope,
        malware_scan=malware_scan,
    )
    try:
        imp = get_state().fw.post("/xfer/imports", json=payload)
    except ClientError as exc:
        payload = rules_backward_compat(exc, payload)
        imp = get_state().fw.post("/xfer/imports", json=payload)
    if not wait:
        utils.print_op_started(output, "Import", imp)
        return

    uploader = None
    if blob_session:
        wait = True
        uploader = LocalFileUploader(imp, blob_session, src_dir)
        uploader.start()

    try:
        get_(
            imp["_id"],
            tree=tree,
            wait=wait,
            fail=True,
            output=output,
            local_uploader=uploader,
        )
    finally:
        if uploader:
            uploader.stop()
            uploader.join()


def get(  # noqa PLR0913
    import_id: str = Argument(
        ...,
        metavar="IMPORT",
        help="Import ID to show the details for",
    ),
    wait: bool = wait_opt,
    fail: bool = Option(
        False,
        "--fail",
        help="Exit with status code 1 if the import failed",
    ),
    tree: bool = tree_opt,
    report: t.Optional[ReportFormat] = Option(
        None,
        "--report",
        metavar="FMT",
        help=r"Print import report in the given format \[jsonl|csv]",
    ),
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """Get import by ID.

    Examples:
    fw-beta import get <IMPORT>
    """
    get_(import_id, wait, fail, tree, report, output)


def get_(  # noqa: PLR0913
    import_id,
    wait: bool = False,
    fail: bool = False,
    tree: bool = False,
    report: ReportFormat | None = None,
    output: utils.OutputFormat = utils.OutputFormat.text,
    local_uploader=None,
):
    """Low level get import by ID function shared between multiple commands."""
    fw = get_state().fw
    imp = get_import(import_id)
    report = utils.resolve_typer_option_default(report)
    if report and not is_final(imp):
        rprint("Reports are only available after completion")
        return
    if report:
        report_url = f"/xfer/imports/{import_id}/report?format={report.value}"
        while not (url := fw.get(report_url).url):
            # TODO stderr spinner / notification when generating large CSV
            time.sleep(1)
        with fw.stream("GET", url) as resp, gzip.open(resp.raw, "rt") as rf:
            for line in rf:
                print(line, end="")
        return
    wait = False if wait is None else wait  # default no-wait on get
    show = partial(
        show_import_progress,
        import_id,
        tree=tree,
        wait=wait,
        local_uploader=local_uploader,
    )
    if output == utils.OutputFormat.text:
        show()
        return
    if wait and not is_final(imp):  # pragma: no cover
        stderr_console = Console(stderr=True)
        show(console=stderr_console)
    imp = get_import(import_id, join=["project", "storage"])
    if output == utils.OutputFormat.yaml:
        utils.autopager(utils.format_yaml(imp))
    else:
        utils.autopager(utils.format_json(imp))
    raise Exit(code=1 if fail and imp.status2 == "failed" else 0)


def list_(
    filters: t.Optional[t.List[str]] = Option(
        None,
        "--filter",
        metavar="EXPR",
        help="Filter imports using expressions",
    ),
    after_id: t.Optional[str] = Option(
        None,
        metavar="ID",
        help="Page token to show results after",
    ),
    limit: int = Option(
        25,
        metavar="INT",
        help=r"Page limit",
    ),
    all_: bool = Option(
        False,
        "--all",
        show_default=False,
        help="Display previous runs of scheduled imports",
    ),
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """List imports.

    Examples:
    fw-beta imports list --filter status=running
    """
    # TODO how to display after_id in text mode
    params: dict = {"limit": limit, "join": ["project", "storage", "blob_session"]}
    if filters:  # pragma: no cover
        params["filter"] = ",".join(validate_filters(filters))
    if after_id:  # pragma: no cover
        params["after_id"] = after_id
    if all_:  # pragma: no cover
        params["all"] = True
    imports = get_state().fw.get("/xfer/imports", params=params)
    if output == utils.OutputFormat.text:
        rprint(format_page(imports))
    elif output == utils.OutputFormat.yaml:  # pragma: no cover
        utils.autopager(utils.format_yaml(imports))
    else:  # pragma: no cover
        utils.autopager(utils.format_json(imports))


def cancel(
    import_id: str = Argument(
        ...,
        metavar="IMPORT",
        help="Import ID to cancel",
    ),
    wait: bool = wait_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """Cancel an import by ID.

    Examples:
    fw-beta import cancel <ID>
    """
    wait = True if wait is None else wait  # default wait on cancel
    try:
        get_state().fw.post(f"/xfer/imports/{import_id}/cancel")
    except ClientError as exc:  # pragma: no cover
        msg = exc.response.json()["message"]
        if msg.startswith("Cannot cancel"):
            raise Error(msg) from None
        raise
    get(import_id, tree=False, wait=wait, output=output)


def rerun(
    import_id: str = Argument(
        ...,
        metavar="IMPORT",
        help="Import ID to re-run",
    ),
    wait: bool = wait_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """Re-run an import by ID.

    Examples:
    fw-beta import rerun <ID>
    """
    wait = True if wait is None else wait  # default wait on rerun
    try:
        imp = get_state().fw.post(f"/xfer/imports/{import_id}/rerun")
    except ClientError as exc:  # pragma: no cover
        msg = exc.response.json()["message"]
        if msg.startswith("Cannot re-run"):
            raise Error(msg) from None
        raise
    get(imp["_id"], tree=False, wait=wait, output=output)


def create_schedule(  # noqa: PLR0913
    project: str = project_opt,
    storage: str = storage_opt,
    label: t.Optional[str] = label_opt,
    description: t.Optional[str] = description_opt,
    level: t.Optional[ImportLevel] = level_opt,
    include: t.List[str] = include_opt,
    exclude: t.List[str] = exclude_opt,
    type: t.Optional[str] = type_opt,
    zip: t.Optional[bool] = zip_opt,
    mappings: t.List[str] = mappings_opt,
    defaults: t.List[str] = defaults_opt,
    overrides: t.List[str] = overrides_opt,
    rules: t.List[str] = rules_opt,
    conflict_strategy: t.Optional[ConflictStrategy] = conflict_strategy_opt,
    dry_run: bool = dry_run_opt,
    limit: t.Optional[int] = limit_opt,
    fail_fast: t.Optional[str] = fail_fast_opt,
    missing_meta: MissingMeta = missing_meta_opt,
    storage_override: t.Optional[str] = storage_override_opt,
    uid_scope: t.Optional[UIDScope] = uid_scope_opt,
    malware_scan: t.Optional[bool] = malware_scan_opt,
    cron: t.Optional[str] = schedules.cron_opt,
    start: t.Optional[str] = schedules.start_opt,
    end: t.Optional[str] = schedules.end_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """Create import schedule."""
    if not label:
        label = "Scheduled import - created via CLI"
    operation = get_import_payload(
        project=project,
        storage=storage,
        label=label,
        description=description,
        level=level,
        include=include,
        exclude=exclude,
        type=type,
        zip=zip,
        mappings=mappings,
        defaults=defaults,
        overrides=overrides,
        rules=rules,
        conflict_strategy=conflict_strategy,
        dry_run=dry_run,
        limit=limit,
        fail_fast=fail_fast,
        missing_meta=missing_meta,
        storage_override=storage_override,
        uid_scope=uid_scope,
        malware_scan=malware_scan,
    )
    operation["type"] = "import"
    try:
        schedules.create_schedule(cron, start, end, operation, output)
    except ClientError as exc:
        operation = rules_backward_compat(exc, operation)
        schedules.create_schedule(cron, start, end, operation, output)


def get_schedule(
    schedule_id: str = schedules.schedule_id_get_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """Get import schedule."""
    schedules.get_schedule(schedule_id, "import", output)


def list_schedules(
    filters: t.Optional[t.List[str]] = schedules.filters_opt,
    after_id: t.Optional[str] = schedules.after_id_opt,
    limit: int = schedules.limit_opt,
    all_: bool = schedules.all_opt,
    output: utils.OutputFormat = utils.output_arg,
) -> None:
    """List import schedules."""
    filters = filters or []
    filters.append("operation.type==import")
    if not all_:
        filters.append("active==true")
    params = {"filter": ",".join(filters), "limit": limit}
    if after_id:  # pragma: no cover
        params["after_id"] = after_id
    schedules.list_schedules(params, output)


def update_schedule(
    schedule_id: str = schedules.schedule_id_update_arg,
    cron: t.Optional[str] = schedules.cron_opt,
    start: t.Optional[str] = schedules.start_opt,
    end: t.Optional[str] = schedules.end_opt,
    output: utils.OutputFormat = utils.output_arg,
):
    """Update a scheduled import by ID."""
    fw = get_state().fw
    schedule = fw.get(f"/xfer/schedules/{schedule_id}")
    if (op_type := schedule.operation.type) != "import":
        raise Error(f"Unexpected schedule operation type: {op_type}")
    schedules.update_schedule(schedule_id, cron, start, end, output)


def cancel_schedule(
    schedule_id: str = schedules.schedule_id_cancel_arg,
    output: utils.OutputFormat = utils.output_arg,
):
    """Cancel a scheduled import by ID."""
    fw = get_state().fw
    schedule = fw.get(f"/xfer/schedules/{schedule_id}")
    if (op_type := schedule.operation.type) != "import":
        raise Error(f"Unexpected schedule operation type: {op_type}")
    schedules.cancel_schedule(schedule_id, output)


# MODELS


def get_default_label():
    """Get default import label."""
    timestamp = datetime.now(tz=UTC).strftime("%m/%d/%Y %H:%M:%S")
    # TODO brainstorm session w/ PO to design default labels
    return f"Import via CLI from {timestamp}"


class Refs(BaseModel):
    """Import references."""

    project: str | None = None
    storage: str | None = None
    blob_session: str | None = None


PartialField: t.Any = partial(partial, Field)
StorageOverride = PartialField(discriminator="type")


class ImportCreate(BaseModel):
    """Import create / POST payload schema."""

    label: str = Field(default_factory=get_default_label)
    description: t.Optional[str] = None
    refs: Refs
    dry_run: bool
    limit: t.Optional[int] = None
    fail_fast: t.Optional[str] = None
    rules: t.Optional[t.List[ImportRule]] = None
    conflict_strategy: t.Optional[ConflictStrategy] = None
    missing_meta: t.Optional[MissingMeta] = None
    storage_override: t.Optional[StorageConfigOverride] = StorageOverride(None)
    uid_scope: UIDScope | None = None
    malware_scan: t.Optional[bool] = None


# HELPERS


def get_import(import_id: str, **params) -> AttrDict:
    """Get an import."""
    try:
        return get_state().fw.get(f"/xfer/imports/{import_id}", params=params)
    except NotFound as exc:
        raise Error(exc.json()["message"]) from None


def get_import_payload(**kwargs) -> dict:
    """Return ImportCreate model instance."""
    project_id: t.Optional[str] = None
    override_dict: t.Optional[dict] = None
    if project := kwargs.pop("project", None):
        project_id = utils.lookup_project_id(project)
    if storage := kwargs.pop("storage", None):
        storage_type = storages.get_storage(storage)["config"]["type"]
        if storage_override := kwargs.pop("storage_override", None):
            override_dict = utils.load_storage_override(storage_type, storage_override)
    blob_session = kwargs.pop("blob_session", None)
    first_rule = {
        "level": kwargs.pop("level", None),
        "include": kwargs.pop("include", []),
        "exclude": kwargs.pop("exclude", []),
        "type": kwargs.pop("type", None),
        "zip": kwargs.pop("zip", None),
        "mappings": kwargs.pop("mappings", []),
        "defaults": kwargs.pop("defaults", []),
        "overrides": kwargs.pop("overrides", []),
    }
    rules = [utils.load_model(ImportRule, rule) for rule in kwargs.pop("rules", [])]
    if first_rule := {k: v for k, v in first_rule.items() if v}:
        rules.insert(0, first_rule)
    imp = ImportCreate(
        refs={"project": project_id, "storage": storage, "blob_session": blob_session},
        rules=rules or None,
        storage_override=override_dict,
        **{k: v for k, v in kwargs.items() if v is not None},
    )
    return imp.model_dump(mode="json", exclude_none=True)


def show_import_progress(  # noqa: PLR0915
    import_id: str,
    tree: bool = False,
    wait: bool = False,
    local_uploader: t.Any = None,
    console: Console = None,
) -> None:
    """Display import progress and optionally wait and live-update until completion."""
    imp = get_import(import_id, join=["project", "storage", "blob_session"])
    prj = imp.project
    dst = f"fw://{prj.parents.group}/{prj.label}"
    if local_uploader:
        src = imp.get("blob_session", {}).get("local_path", "-")
    else:
        src = imp.get("storage", {}).get("url", "-")

    progress_stream = ProgressStream(import_id, tree, wait)
    progress_stream.start()
    progress = next(progress_stream)
    is_ctrl_c = upload_interrupt = None

    def render(progress):
        yield format_status(import_id, src, dst, progress, wait)
        yield from format_progress(
            progress, show_pbar=wait, local_uploader=local_uploader
        )
        if tree:  # pragma: no cover
            yield from format_tree(dst, progress)
        elif wait:
            yield from format_lines(progress)
        if upload_interrupt and not is_final(progress):  # pragma: no cover
            yield f"{upload_interrupt}: cancelling the operation ..."
        elif upload_interrupt:  # pragma: no cover
            yield f"{upload_interrupt}: operation cancelled."

    group = Group(*render(progress))
    with Live(group, refresh_per_second=10, console=console) as live:
        try:
            while wait and not is_final(progress):
                time.sleep(1)
                progress = next(progress_stream)
                live.update(Group(*render(progress)), refresh=True)
                if local_uploader:
                    local_uploader.raise_for_error()
                progress_stream.raise_for_error()
        except KeyboardInterrupt as exc:  # pragma: no cover
            progress = next(progress_stream)
            if local_uploader and not is_final(progress):
                is_ctrl_c = isinstance(exc, KeyboardInterrupt)
                upload_interrupt = "Received CTRL+C" if is_ctrl_c else format_exc(exc)
                get_state().fw.post(f"/xfer/imports/{import_id}/cancel")
                while not is_final(progress):  # pragma: no cover
                    time.sleep(1)
                    progress = next(progress_stream)
                    live.update(Group(*render(progress)), refresh=True)
            raise
        except (Exception, ExceptionGroup) as exc:
            if local_uploader:
                resume_cmd = f"fw-beta import run --resume-local {import_id}"
                cancel_cmd = f"fw-beta import cancel {import_id}"
                raise Error(
                    "Exception while uploading files for local import:\n"
                    f"{format_exc(exc)}\n\n"
                    f"To resume the upload:\n> {resume_cmd}\n"
                    f"To cancel the import:\n> {cancel_cmd}"
                )
        finally:
            live.update(Group(*render(progress)), refresh=True)
            progress_stream.join()
            progress = next(progress_stream)
            progress["items"] = []
            live.update(Group(*render(progress)), refresh=True)
            if get_status(progress) == "failed":
                status_msg = progress.get("status_reason") or "Unknown error"
                report_cmd = f"fw-beta import get {import_id} --report=csv"
                raise Error(f"{status_msg}\n> {report_cmd}")


def format_status(
    import_id: str, src: str, dst: str, progress: "Progress2", wait: bool
) -> Group:
    """Format status and source/destination lines."""
    id_line = Text.assemble(("ID", "bold"), (f" {import_id}", "cyan"))
    cols: t.Any = [id_line]
    # show spinner if status is not final
    if wait and not is_final(progress):  # pragma: no cover
        cols.append(Spinner("dots"))
    else:
        cols.append(" ")
    # show status
    cols.append(s.status(get_status(progress), has_failed(progress)))
    if elapsed_time := progress.get("elapsed_time"):
        cols.append(f" {hrtime(elapsed_time)}")
    if stage := progress.get("stage"):
        cols.append(f"- {stage.replace('_', ' ')} ...")
    grid = Table.grid(padding=(0, 1))
    grid.add_row(*cols)
    return Group(
        grid,
        Text.assemble(("Storage", "bold"), f" {src}"),
        Text.assemble(("Project", "bold"), f" {dst}"),
    )


def format_progress(
    progress: "Progress2",
    show_pbar: bool = False,
    local_uploader: t.Optional["LocalFileUploader"] = None,
) -> t.Iterable[Table]:
    """Format progress information."""
    totals: dict[str, int] = defaultdict(int)
    totals.update(progress.get("totals", {}))
    df, db = totals["discovered_files"], totals["discovered_bytes"]
    pf, pb = totals["processed_files"], totals["processed_bytes"]
    cf, cb = totals["success_files"], totals["success_bytes"]
    sf, sb = totals["skipped_files"], totals["skipped_bytes"]
    rf, rb = totals["review_files"], totals["review_bytes"]
    ff, fb = totals["failed_files"], totals["failed_bytes"]
    if local_uploader:
        yield from local_uploader.render()
    # show progress bar
    if show_pbar and (not is_final(progress) or is_final(progress) and df):
        pbar = utils.MultiColorProgressBar(
            width=50,
            completed=cf or 0,
            skipped=sf or 0,
            failed=ff or 0,
            review=rf or 0,
            total=df or 1,
            pulse=progress.get("stage") == "scanning",
        )
        cols = [s.bold("Import")]
        cols.append(pbar)
        # show percentage
        if not (local_uploader and local_uploader.running):
            percentage = f"{math.ceil(pf / df * 100) if pf and df else 0}%"
            cols.append(f"{percentage:>4}")
        else:
            cols.append("    ")
        # show speed if available
        current_speed = progress.get("current_speed") or {}
        bytes_per_sec = current_speed.get("bytes_per_sec")
        if bytes_per_sec is not None:  # pragma: no cover
            speed = f"{hrsize(bytes_per_sec)}/s"
            cols.append(f"{speed:>7}")
        grid = Table.grid(padding=(0, 1))
        grid.add_row(*cols)
        yield grid

    # progress stats
    grid = Table.grid(padding=(0, 1))
    add_row = grid.add_row
    if df is not None and db is not None:
        add_row(s.bold("Discovered"), f"{q(df, 'file'):>15}", f"{hrsize(db):>10}")
    if pf is not None and pb is not None:
        add_row(s.bold("Processed"), f"{q(pf, 'file'):>15}", f"{hrsize(pb):>10}")
    if cf or cb:
        add_row(s.green("Success"), f"{q(cf, 'file'):>15}", f"{hrsize(cb):>10}")
    if sf or sb:
        add_row(s.yellow("Skipped"), f"{q(sf, 'file'):>15}", f"{hrsize(sb):>10}")
    if rf or rb:
        add_row(s.yellow("Review"), f"{q(rf, 'file'):>15}", f"{hrsize(rb):>10}")
    if ff or fb:
        add_row(s.red("Failed"), f"{q(ff, 'file'):>15}", f"{hrsize(fb):>10}")
    if grid.rows:
        yield grid


def format_lines(
    progress: "Progress2", lines: int = 20
) -> t.Iterable[Table]:  # pragma: no cover
    """Format import event tail as a table of src/dst items."""
    files: dict = {}
    for item in reversed(progress.get("items", [])):
        stat = item.get("src_stat", {})
        info = AttrDict(
            status=item.get("status"),
            size=hrsize(stat.get("size")) if stat else "",
            dst_path=item.get("dst_path"),
            reason=item.get("reason"),
        )
        files.setdefault(item.get("src_path"), info)
        if len(files) >= lines:
            break
    if not files:
        return
    col = partial(Column, no_wrap=True)
    cols = [
        col(s.bold("Status"), min_width=10, max_width=10),
        col(s.bold("Size"), min_width=6),
        col(s.bold("Source"), max_width=50),
        col(s.bold(f"Destination{s.dim('/Reason')}"), max_width=50),
    ]
    table = Table(*cols, box=None, padding=(0, 2, 0, 0), header_style=None)
    for src, info in sorted(files.items()):
        msg = s.dim(info.reason) if info.reason else info.dst_path
        table.add_row(s.status(info.status), info.size, src, msg)
    yield table


def format_tree(
    proj: str, progress: "Progress2", lines: int = 20
) -> t.Iterable[Tree]:  # pragma: no cover
    """Format import event tail in the fw tree hierarchy."""
    root = Node(name=proj)
    for item in reversed(progress.get("items", [])):
        item = AttrDict(item)
        dst_stat = item.get("dst_stat", {})
        if not (is_final(item) and dst_stat):
            continue
        node = root
        if sub := dst_stat.get("subject"):
            node = node.add(sub)
            if ses := dst_stat.get("session"):
                node = node.add(ses)
                if acq := dst_stat.get("acquisition"):
                    node = node.add(acq)
        node.add(dst_stat.file)
        if root.count() >= lines:
            break
    if root:
        yield root.tree()


def format_page(page: dict) -> t.Union[Text, Table]:
    """Format list of imports page as text."""
    if not page["results"]:
        return Text("Nothing to show")
    cols = [
        Column("ID", max_width=24),
        Column("Status", max_width=22),
        Column("Elapsed", max_width=8),
        Column("Completed", max_width=12),
        Column("Source", max_width=35, no_wrap=True),
        Column("Destination", max_width=35, no_wrap=True),
    ]
    table = Table(*cols, box=None, padding=(0, 2, 0, 0))
    for imp in page["results"]:
        cells = []
        cells.append(s.cyan(imp["_id"]))
        cells.append(s.status(get_status(imp), has_failed(imp)))
        cells.append(hrtime(imp["total_time"]))
        c_ago = None
        if completed := imp.get("completed"):
            c_ago = (get_datetime() - get_datetime(completed)).total_seconds()
        cells.append(f"{hrtime(c_ago)} ago" if c_ago else "-")
        if imp.get("blob_session"):
            src = imp.get("blob_session", {}).get("local_path", "-")
        else:
            src = imp.get("storage", {}).get("url", "-")
        cells.append(src)
        prj = imp["project"]
        cells.append(f"fw://{prj['parents']['group']}/{prj['label']}")
        table.add_row(*cells)
    return table


class Progress2(AttrDict):
    """Semi-formal progress2 model with stats/events backwards-compat."""

    def __init__(self, *args, **kw) -> None:
        """Init progress2 with an empty items deque capped at 100."""
        super().__init__(*args, **kw)
        self.setdefault("items", deque(maxlen=100))

    def set_progress(self, progress: dict) -> None:
        """Apply updates from an xfer progress2 model."""
        self.pop("stage", None)
        self["items"].extend(progress.pop("items", []))
        self.update(progress)


class ProgressStream(Thread):
    """Thread to stream import progress asynchronously."""

    def __init__(self, import_id: str, tree: bool, wait: bool):
        """Init with an empty progress."""
        super().__init__(daemon=True)
        self.prog = Progress2()
        self.tree = tree
        self.wait = wait
        self.fw = get_state().fw
        self.url = f"/xfer/imports/{import_id}"
        self.running = False
        self.exception = None

    def run(self) -> None:
        """Run progress streaming thread."""
        self.running = True
        url = f"{self.url}/progress2/sse?wait={self.wait}&tree={self.tree}"
        try:
            with self.fw.stream("GET", url) as resp:
                for event in resp.iter_events():
                    # TODO fix performance - capping items in xfer at 25 for now
                    # NOTE w/ 100, the progress display lagged 2x behind actual
                    # NOTE release new http client, try new chunksize or go jsonl
                    if event.type == "progress":
                        prog = json.loads(event.data)
                        self.prog.set_progress(prog)
                    if not self.running:  # pragma: no cover
                        break
        except Exception as exc:  # pragma: no cover
            log.exception("Unexpected progress stream error")
            self.exception = exc
        self.running = False

    def raise_for_error(self):
        """Raise if an exception occurred."""
        if self.exception:  # pragma: no cover
            raise self.exception

    def join(self, timeout: t.Optional[float] = None) -> None:
        """Set the running flag to false and wait until the thread terminates."""
        self.running = False
        super().join(timeout)

    def __next__(self) -> Progress2:
        """Return progress totals and items since the last call."""
        return self.prog


class Node(dict):  # pragma: no cover
    """Minimal fw hierarchy node for drawing rich trees."""

    upsert_colors = {
        "create": "green",
        "created": "green",
        "skip": "yellow",
        "skipped": "yellow",
        "update": "magenta",
        "updated": "magenta",
    }

    def __init__(self, stat: dict = None, **kwargs):
        """Init node from an import event dst_stat or kwargs."""
        stat = stat or {}
        stat |= kwargs
        self.name = stat.get("label") or stat.get("name") or ""
        self.upsert = stat.get("upsert_result") or stat.get("upsert_intent")
        self.upsert = self.upsert or stat.get("upsert")

    def __str__(self) -> str:
        """Return string representation."""
        node_str = escape(self.name)
        node_str = node_str if not self else f"[bold blue]{node_str}[/bold blue]"
        if self.upsert:
            # TODO consider using a more neutral color for skip - white?
            # TODO use redis to cache lookup results per operation
            color = self.upsert_colors[self.upsert]
            node_str += f" ([{color}]{self.upsert}[/{color}])"
        return node_str

    def add(self, stat: dict) -> "Node":
        """Add the given dst_stat dict as a sub-node."""
        node = type(self)(stat)
        return self.setdefault(node.name, node)

    def count(self) -> int:
        """Return the total (recursive) count of nodes in the tree."""
        return 1 + sum(sub.count() for sub in self.values())

    def tree(self, parent=None) -> Tree:
        """Return the tree as a rich Tree instance for display."""
        node = parent.add(str(self)) if parent else Tree(str(self))
        for _, value in sorted(self.items()):
            value.tree(node)
        return node


def rules_backward_compat(exc: ClientError, payload: dict) -> dict:
    """Rules backward compat to work with xfer where at least one rule is required."""
    if exc.status_code == 422:
        msg = exc.response.json()["message"]
        if re.search(r"rules.*at least 1 item", msg):
            log.debug("Fallback to single, empty rule", exc_info=exc)
            payload["rules"] = [{}]
            return payload
    raise exc  # pragma: no cover


class LocalFileUploader(Thread):
    """Local file uploader thread."""

    def __init__(self, imp: AttrDict, blob_session: AttrDict, source_dir: Path) -> None:
        """Initiate the local file uploader thread instance."""
        super().__init__(daemon=True)
        self.imp = imp
        self.blob_session = blob_session
        self.rules = [ImportRule(**rule) for rule in imp.rules]
        self.source_dir = source_dir
        self.running = False
        self.scanning = False
        self.queue = asyncio.Queue()
        self.totals = UploadTotals(timestamp=get_datetime())
        self.exception = None
        self.loop = asyncio.new_event_loop()

    def run(self) -> None:
        """Run the uploader."""
        self.running = True
        asyncio.set_event_loop(self.loop)
        try:
            self.loop.run_until_complete(self._run())
        except asyncio.CancelledError:  # pragma: no cover
            self.running = False
        except (Exception, ExceptionGroup) as exc:  # pragma: no cover
            msg = "Unexpected upload error"
            log.exception(msg)
            self.exception = exc
        finally:
            self.loop.close()
        self.running = False

    async def _run(self):
        """Run the uploader main loop."""
        async with asyncio.TaskGroup() as tg:
            tg.create_task(self._iter_files())
            tg.create_task(self._upload_files())

    async def _iter_files(self):
        """Iter files in the source directory."""
        self.scanning = True
        last_path = self.blob_session.get("last_abspath", "").split("/", maxsplit=3)[-1]
        seq_no = self.blob_session.get("last_seq_no", 0)
        with create_storage_client(f"fs://{self.source_dir.as_posix()}") as client:
            iter_files = islice(client.ls(), self.imp.get("limit"))
            while file := await asyncio.to_thread(next, iter_files, DONE):
                # TODO we will need to include these in the final import report
                if file is DONE:
                    await self.queue.put(file)
                    break
                if self.match_rules(file):
                    if last_path and file.path <= last_path:
                        self.totals.inc(
                            total_files=1,
                            total_bytes=file.size,
                            completed_files=1,
                            completed_bytes=file.size,
                            prev=False,
                        )
                        continue
                    seq_no += 1
                    await self.queue.put((seq_no, file))
                    self.totals.inc(total_files=1, total_bytes=file.size)
        self.scanning = False

    async def _upload_files(self):
        """Upload files from the queue."""
        tasks = []
        try:
            files_semaphore = asyncio.Semaphore(CONCURRENT_FILE_UPLOADS)

            while (file := await self.queue.get()) is not DONE:
                await files_semaphore.acquire()
                task = asyncio.create_task(self._upload_file(*file))
                task.add_done_callback(lambda _: files_semaphore.release())
                tasks.append(task)
                await asyncio.gather(*[t for t in tasks if t.done()])
            await asyncio.gather(*tasks)
        except asyncio.CancelledError:  # pragma: no cover
            for task in tasks:
                task.cancel()
            await asyncio.wait(tasks)
            raise

        await get_state().fw.apost(
            f"/xfer/blob-sessions/{self.blob_session._id}/finish"
        )

    async def _upload_file(self, seq_no, file):
        """Upload a single file."""
        fw = get_state().fw
        payload = {
            "blob_session_id": self.blob_session._id,
            "path": Path(file.path).as_posix(),
            "seq_no": seq_no,
        }
        is_multipart = file.size > TEN_MB
        params = {"multipart": is_multipart}
        blob = await fw.apost("/xfer/blobs", json=payload, params=params)
        file_path = self.source_dir / file.path
        chunk_semaphore = asyncio.Semaphore(CONCURRENT_CHUNK_UPLOADS)
        chunk_tasks = []
        chunk_index, parts = 0, None

        async with aiofiles.open(file_path, "rb") as f:
            if is_multipart:  # pragma: no cover
                while data := await f.read(TEN_MB):
                    await chunk_semaphore.acquire()
                    coro = self._upload_chunk(blob._id, data, chunk_index + 1)
                    task = asyncio.create_task(coro)
                    task.add_done_callback(lambda _: chunk_semaphore.release())
                    chunk_tasks.append(task)
                    chunk_index += 1
                    await asyncio.gather(*[t for t in chunk_tasks if t.done()])
                parts = await asyncio.gather(*chunk_tasks)
            else:
                await self._upload_chunk(blob._id, await f.read(), None)

        try:
            payload = {"parts": parts} if parts else None
            await fw.apost(f"/xfer/blobs/{blob._id}/finish", json=payload)
        except Conflict:  # pragma: no cover
            pass  # should only happen when retrying and it is ok
        self.totals.inc(completed_files=1)

    async def _upload_chunk(
        self, blob_id: str, data: bytes, part: int | None = None
    ) -> dict:
        """Upload single chunk of data to the blob."""
        fw = get_state().fw
        params = {"part": part} if part is not None else {}
        upload = await fw.apost(f"/xfer/blobs/{blob_id}/upload", params=params)
        headers = upload.get("headers", {})
        headers["Authorization"] = None
        upload_url = upload.upload_url
        response = await fw.aput(upload_url, content=data, headers=headers, raw=True)
        response.raise_for_status()
        etag = response.headers.get("etag", "").strip('"') or None
        self.totals.inc(completed_bytes=len(data))
        return {"etag": etag, "part": part}

    def match_rules(self, data) -> tuple[int | None, ImportRule | None]:
        """Return first matching rule based on include/exclude filters."""
        return next((r for r in self.rules if r.match(data)), None)

    def render(self):
        """Return a progress bar for the upload."""
        cols: t.Any = [s.bold("Upload")]
        if not self.scanning and not self.totals.total_files:
            return
        pbar = utils.MultiColorProgressBar(
            width=50,
            completed=self.totals.completed_bytes,
            total=self.totals.total_bytes or 1,
            pulse=self.scanning,
        )
        cols.append(pbar)
        # show percentage
        cf = self.totals.completed_bytes
        tf = self.totals.total_bytes
        percentage = f"{math.ceil(cf / tf * 100) if cf and tf else 0}%"
        cols.append(f"{percentage:>4}")
        # show speed if available
        current_speed = self.totals.current_speed
        if self.running and current_speed:  # pragma: no cover
            bytes_per_sec = current_speed.bytes_per_sec
            if bytes_per_sec is not None:
                speed = f"{hrsize(bytes_per_sec)}/s"
                cols.append(f"{speed:>7}")
        grid = Table.grid(padding=(0, 1))
        grid.add_row(*cols)
        yield grid

    def raise_for_error(self):
        """Raise if an exception occurred."""
        if self.exception:  # pragma: no cover
            raise self.exception

    def stop(self):
        """Stop the uploader."""
        if self.loop.is_closed():
            return
        for task in asyncio.all_tasks(self.loop):  # pragma: no cover
            task.cancel()

    def join(self, timeout: t.Optional[float] = None):
        """Wait until the thread terminates."""
        super().join(timeout)


@dataclass
class UploadSpeed:
    """Upload speed metrics."""

    files_per_sec: float
    bytes_per_sec: float


class UploadTotalsDelta:
    """Delta of two upload totals samples."""

    def __init__(self, t0: "UploadTotals", t1: "UploadTotals") -> None:
        """Init differential object from two UploadTotals samples."""
        t0, t1 = sorted([t0, t1], key=lambda t: t.timestamp)
        self.t0, self.t1 = t0, t1
        self.elapsed_time = (t1.timestamp - t0.timestamp).total_seconds()
        self.completed_files = t1.completed_files - t0.completed_files
        self.completed_bytes = t1.completed_bytes - t0.completed_bytes

    @property
    def speed(self) -> t.Optional[UploadSpeed]:
        """Return file and byte processing speed between the two samples."""
        if not self.elapsed_time:
            return None  # pragma: no cover
        return UploadSpeed(
            files_per_sec=self.completed_files / self.elapsed_time,
            bytes_per_sec=self.completed_bytes / self.elapsed_time,
        )


@dataclass
class UploadTotals:
    """Upload totals metrics."""

    timestamp: datetime
    total_files: int = 0
    total_bytes: int = 0
    completed_files: int = 0
    completed_bytes: int = 0

    _prev: t.Optional["UploadTotals"] = None

    @property
    def remaining_files(self) -> int:  # pragma: no cover
        """Return the remaining files to upload."""
        return self.total_files - self.completed_files

    @property
    def remaining_bytes(self) -> int:  # pragma: no cover
        """Return the remaining bytes to upload."""
        return self.total_bytes - self.completed_bytes

    def inc(self, *, prev: bool = True, **kwargs):
        """Increment the upload totals."""
        delta = timedelta(seconds=5)
        if prev and (not self._prev or self._prev.timestamp - self.timestamp > delta):
            self._prev = deepcopy(self)
        self.timestamp = get_datetime()
        for k, v in kwargs.items():
            setattr(self, k, getattr(self, k) + v)

    @property
    def current_speed(self) -> t.Optional[UploadSpeed]:
        """Return the current upload speed."""
        if not self._prev:  # pragma: no cover
            return None
        return (self - self._prev).speed

    def __sub__(self, other: "UploadTotals") -> UploadTotalsDelta:
        """Return the delta between two upload totals samples."""
        return UploadTotalsDelta(self, other)


def format_exc(exc: BaseException) -> str:
    """Return exception formatted as a string."""
    if isinstance(exc, asyncio.CancelledError):
        return str(exc)  # pragma: no cover
    if isinstance(exc, ExceptionGroup):
        return "\n".join(format_exc(e) for e in exc.exceptions)
    return "".join(traceback.format_exception_only(type(exc), exc)).strip()
