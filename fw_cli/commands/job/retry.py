"""Job Pull module."""

import logging
import typing as t
from http import HTTPStatus

from fw_client import ClientError, ServerError
from rich import print as rprint
from typer import Argument, Option, confirm

from ...exceptions import Error
from ...state import get_state

log = logging.getLogger(__name__)

HIERARCHY_REVERSED = ["acquisition", "session", "subject", "project"]


def job_retry(
    job_id: str = Argument(..., help="ID of job to retry"),
    force: bool = Option(
        False,
        "-f",
        "--force",
        help="Force relaunch if job has already been retried. Bypasses prompt",
    ),
):
    """Retry a job, creating a new job if it has already been retried."""
    state = get_state()
    try:
        j_id = state.fw.post(f"/api/jobs/{job_id}/retry")
        rprint(f"Job {job_id} retried with job id: {j_id.id}")
        return
    except (ClientError, ServerError) as exc:
        if exc.status_code == HTTPStatus.FORBIDDEN:
            raise Error("User does not have permissions for this action.") from exc
        if exc.status_code != HTTPStatus.CONFLICT:
            raise
    msg = "Job already retried, "
    if force:
        rprint(msg + "creating a new job with identical configuration.")
        proceed = True
    else:
        proceed = confirm(msg + "create a new job with identical configuration?")
    if proceed:
        job = state.fw.get(f"/api/jobs/{job_id}")
        data = {
            "attempt": job.attempt + 1,
            "batch": job.batch,
            "compute_provider_id": job.compute_provider_id,
            "config": job.config.config,
            "gear_id": job.gear_id,
            "inputs": {k: file_ref_from_obj(v) for k, v in job.config.inputs.items()},
            "label": job.label,
            "origin": {"type": "user", "id": state.profile.id},
            "parents": job.parents,
            "previous_job_id": job.id,
            "tags": job.tags,
        }
        # Need to provide parent container for destination if analysis.
        if job.destination.type == "analysis":
            data["destination"] = find_parent_for_analysis(job)
        else:
            data["destination"] = job.destination
        resp = state.fw.post("/api/jobs/add", json=data)
        rprint(f"Job successfully retried with id {resp['_id']}")


def file_ref_from_obj(file_: dict) -> dict:
    """Helper to return a FileReference compatible with jobs add endpoint."""
    obj = file_.get("object", {})
    # New core
    if "file_id" in obj and "version" in obj:
        return {"file_id": obj.get("file_id", ""), "version": obj.get("version", 1)}
    # Old core
    return {
        "type": file_.get("hierarchy", {}).get("type", ""),
        "id": file_.get("hierarchy", {}).get("id", ""),
        "name": file_.get("location", {}).get("name", ""),
    }


def find_parent_for_analysis(file_: dict) -> t.Optional[dict]:
    """Return container ref of parent for an analysis."""
    # Find first non-None id
    parents = file_.get("parents", {})
    for lvl in HIERARCHY_REVERSED:
        if parents.get(lvl):
            return {"type": lvl, "id": parents.get(lvl)}
    return None  # pragma: no cover
