"""Interact with and control FW jobs."""

from ...patch import FWTyper
from . import ls, pull, retry

job = FWTyper(name="job", help=__doc__)
job.add_command(ls.job_ls, name="ls", alias_of="list")
job.add_command(pull.job_pull, name="pull")
job.add_command(retry.job_retry, name="retry")
