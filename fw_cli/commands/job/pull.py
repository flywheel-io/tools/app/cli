"""Job Pull module."""

import json
import logging
from concurrent.futures import ThreadPoolExecutor
from http import HTTPStatus
from pathlib import Path
from typing import Any, Dict, Optional, cast

from fw_client import ClientError, FWClient, ServerError
from fw_gear.manifest import Manifest
from fw_utils.formatters import pluralize
from rich import print as rprint
from rich.progress import Progress, TaskID
from typer import Argument, Option

from ...containers import get_container_client
from ...exceptions import Error
from ...state import get_state
from ...styles import yellow
from ..gear.utils import (
    attempt_to_correct_manifest,
    default_progress_bar,
    download_gear,
)

log = logging.getLogger(__name__)


def job_pull(
    job_id: str = Argument(..., help="ID of job to pull"),
    output_dir: Path = Argument(Path.cwd(), help="Output directory path"),
    image: bool = Option(True, help="Pull gear image if not present."),
):
    """Pull job assets and generate docker command to run job locally."""
    state = get_state()
    rprint(f"Getting job {job_id}")
    # Get the job
    job = state.fw.get(f"/api/jobs/{job_id}")

    # Build the output directories
    gear = job.gear_info  # type: ignore
    job_folder = f"{gear.name}-{gear.version}-{job.id}"  # type: ignore
    root_dir = Path(output_dir).expanduser().absolute() / job_folder
    for sub_dir in ["work", "input", "output"]:
        (root_dir / sub_dir).mkdir(parents=True, exist_ok=True)

    # Pull job assets
    inputs = job.config.get("inputs")  # type: ignore
    input_dir = root_dir / "input"
    input_map_f = root_dir / ".input_map.json"
    input_map = {}
    rprint(f"Pulling manifest for {gear.name}")
    manifest = pull_config(state.fw, job, root_dir)
    if inputs:
        rprint("Pulling job inputs")
        with default_progress_bar as prog, ThreadPoolExecutor(max_workers=4) as pool:
            futures = {}
            for i_name, i_value in inputs.items():
                if i_name == "api-key" or i_value["base"] != "file":
                    log.debug(f"Skipping input {i_name} as it's not a file")
                    continue
                task_id = prog.add_task(
                    f"Downloading {i_name}", i_value["location"]["name"]
                )
                (input_dir / i_name).mkdir(exist_ok=True)
                futures[task_id] = pool.submit(
                    pull_input, state.fw, i_value, input_dir / i_name, prog, task_id
                )
                docker_path = i_value["location"]["path"]
                filename = i_value["location"]["name"]
                input_map[docker_path] = str((input_dir / i_name) / filename)
            for _, v in futures.items():
                res = v.result()
                if res:
                    raise Error(res)

    with open(input_map_f, "w", encoding="utf-8") as fp:
        json.dump(input_map, fp)
    if image:
        client = get_container_client()
        im_tag = cast(str, manifest.get_docker_image_name_tag() or "")
        if not im_tag:
            rprint(yellow("No image tag found, can't locally tag image"))
        im = client.inspect(im_tag)
        if not im:
            rprint(f"Pulling image {im_tag}")
            download_gear(state.fw, client, gear)


def pull_config(client: FWClient, job: Any, root_dir: Path) -> Manifest:
    """Creates config.json, manifest.json and saves it at `root_dir`."""
    gear = dict(client.get(f"/api/gears/{job.gear_id}").gear)  # type: ignore
    attempt_to_correct_manifest(gear)

    # GEAR-3593: populate config.job with job id
    job["config"]["job"] = {"id": job["id"]}

    # Dump to config file
    config_file = Path(root_dir) / "config.json"
    manifest_file = Path(root_dir) / "manifest.json"
    with open(config_file, "w", encoding="utf-8") as fp:
        json.dump(job["config"], fp, indent=4)
    with open(manifest_file, "w", encoding="utf-8") as fp:
        json.dump(gear, fp, indent=4)
    return Manifest(manifest_file)


def pull_input(
    client: FWClient,
    i_file: Dict[str, Any],
    dest: Path,
    progress: Progress,
    task_id: TaskID,
) -> Optional[str]:
    """Pull input file from Flywheel."""
    filename = i_file["location"]["name"]
    parent_id = i_file["hierarchy"]["id"]
    parent_type = i_file["hierarchy"]["type"]
    version = i_file.get("object", {}).get("version")
    container = pluralize(parent_type)
    url = f"/api/{container}/{parent_id}/files/{filename}"
    params = {"version": int(version)} if version else {}
    try:
        with open(dest / filename, "wb") as file:
            with client.stream("GET", url, params=params) as response:
                progress.update(task_id, total=int(response.headers["Content-Length"]))
                for data in response.iter_content():
                    file.write(data)
                    progress.update(task_id, advance=len(data))
    except (ServerError, ClientError) as exc:
        if exc.response.status_code == HTTPStatus.NOT_FOUND:
            return "Input file doesn't exist"
        return f"Unhandled error downloading input: {exc}"

    return None
