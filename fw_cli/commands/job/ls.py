"""Job list module."""

import typing as t
from datetime import timedelta

from dateutil import parser
from fw_client import FWClient
from fw_client.errors import ClientError, InvalidJSONError, ServerError
from rich.console import Console
from rich.table import Column, Table
from typer import Option

from ... import styles as s
from ...exceptions import Error
from ...state import get_state
from .. import utils


def render_state(state: str) -> str:  # pragma: no cover
    """Render 'state' column values."""
    if state == "complete":
        return s.green("complete")
    if state == "failed":
        return s.red("failed")
    if state == "cancelled":
        return s.yellow("cancelled")
    if state == "pending":
        return "pending"
    if state == "running":
        return "running"
    return "Unknown"


def render_date(date: str) -> str:
    """Render date-time column values."""
    parsed = parser.isoparse(date)
    return parsed.strftime("%b %d, %Y %H:%M:%S")


def render_origin(origin: t.Optional[str]) -> str:  # pragma: no cover
    """Render origin column values."""
    if origin:
        return origin
    return "System"


def render_time_ms(time_ms: int) -> str:  # pragma: no cover
    """Render time millisecond fields."""
    time_d = timedelta(milliseconds=time_ms)
    return str(time_d)


JOB_FIELDS = {
    "attempt": ("Attempt", None),
    "created": ("Created", render_date),
    "destination.id": ("Destination ID", None),
    "destination.type": ("Destination type", None),
    "gear_id": ("Gear ID", None),
    "gear_info.category": ("Gear category", None),
    "gear_info.name": ("Gear name", None),
    "gear_info.version": ("Gear version", None),
    "_id": ("ID", None),
    "modified": ("Modified", None),
    "origin.id": ("Origin", render_origin),
    "previous_job_id": ("Previous job id", None),
    "profile.total_time_ms": ("Total time", render_time_ms),
    "retried": ("Retried", None),
    "saved_files": ("Saved files", None),
    "state": ("State", render_state),
    "tags": ("Tags", None),
}

DEFAULT_FIELDS = [
    "gear_info.name",
    "gear_info.version",
    "_id",
    "origin.id",
    "state",
    "created",
]


JOB_STATES = {"pending", "running", "failed", "complete", "cancelled"}


def job_ls(  # noqa: PLR0913
    gear_name: t.List[str] = Option([], "-g", "--gear", help="Filter by name of gear"),
    user: t.List[str] = Option([], "-u", "--user", help="Filter by user"),
    gear_state: t.List[str] = Option([], "-s", "--state", help="Filter by job state"),
    project: t.List[str] = Option(
        [], "-p", "--project", help="Filter by project ID or path"
    ),
    f_filter: str = Option(
        "",
        "-f",
        "--filter",
        help="Optional filter for gears, overrides `simple_filter`",
    ),
    limit: int = Option(100, "-l", "--limit", help="Number of jobs to return"),
    ascending: bool = Option(
        False, "-a", "--ascending", help="Sort jobs ascending by creation date"
    ),
    output: utils.OutputFormat = utils.output_arg,
):
    """List jobs on Flywheel.

    Examples:
        `job ls -g my-gear`
        List jobs for gear `my-gear`

        `job ls -u <email> -s pending`
        List my current pending jobs
    """
    filter_ = ""
    state = get_state()
    if f_filter:
        filter_ = f_filter
    else:
        filter_ = build_filter(state.fw, gear_name, user, gear_state, project)
    try:
        with Console().status("Getting list of jobs"):
            jobs = state.fw.get(
                "/api/jobs",
                params={
                    "filter": filter_,
                    "limit": limit,
                    "sort": f"created:{'asc' if ascending else 'desc'}",
                },
            )
        if output == utils.OutputFormat.text:
            utils.autopager(job_table(jobs, DEFAULT_FIELDS))
        if output == utils.OutputFormat.yaml:
            utils.autopager(utils.format_yaml(jobs))
        else:
            utils.autopager(utils.format_json(jobs))
    except (ClientError, InvalidJSONError, ServerError) as e:
        raise Error(f"Could not get list of jobs: {repr(e)}") from e


def job_table(jobs: t.List[dict], fields) -> Table:
    """Render job table."""
    columns = [Column(JOB_FIELDS[field][0], overflow="fold") for field in fields]
    # TODO: Add width/min_width to allow paging through width?
    table = Table("", *columns, row_styles=["dim", ""], box=None)
    for idx, job in enumerate(jobs):
        row = []
        for field in fields:
            render_fn = JOB_FIELDS[field][1]
            # Get value, could be nested with multiple '.'
            f_path = field.split(".")
            val = job
            for path_component in f_path:
                val = val.get(path_component, {})
            # Call render function on it if it has one.
            if render_fn and callable(render_fn):
                val = render_fn(val)  # type: ignore
            row.append(str(val))
        table.add_row(str(idx), *row)
    return table


def build_filter(
    fw: FWClient,
    name: t.List[str],
    user: t.List[str],
    state: t.List[str],
    project: t.List[str],
) -> str:
    """Build filter from user input."""
    if any(len(v) > 1 for v in [name, user, state, project]):
        major, minor, *_ = fw.core_version.split(".")  # type: ignore
        # Only 16.5 and up supports this
        if (int(major) < 16) or (int(major) == 16 and int(minor) < 5):  # noqa
            raise Error("Multiple values per filter not supported on Flywheel <16.5.0")
    components = [
        component("gear_info.name", name),
        component("origin.id", user),
        component("state", state),
        component(
            "parents.project",
            [utils.lookup_project_id(proj) for proj in project],
        ),
    ]
    return ",".join([c for c in components if c])


def component(key: str, val: t.List[str]) -> t.Optional[str]:
    """Build component of filter."""
    c: t.Optional[str] = None
    if len(val) > 1:
        c = f"{key}=|["  # type: ignore
        c += ",".join(val)
        c += "]"
    elif len(val) > 0:
        c = f"{key}={val[0]}"
    return c
