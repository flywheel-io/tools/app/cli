"""Manage site configuration and resources."""

from ..patch import FWTyper
from .xfer import storage_cmd

admin = FWTyper(name="admin", help=__doc__)

admin.add_command(storage_cmd, name="storage")
admin.add_command(storage_cmd, name="storages", alias_of="storage")
