"""Commands package."""

# fix Windows console to support ANSI escape codes on stdout/stderr
from colorama import just_fix_windows_console

just_fix_windows_console()
