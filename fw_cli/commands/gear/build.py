"""Gear build module."""

import json
import logging
import re
import typing as t
from pathlib import Path

import typer
from fw_gear.manifest import Manifest
from rich import print as rprint

from ...containers import get_container_client
from ...exceptions import Error, Exit

# from ...state import get_state
from ...styles import green, red, yellow
from .utils import extract_and_remove_param, validate_manifest

log = logging.getLogger(__name__)

DOCKER_STEP_RE = re.compile(r"^Step (\d+)\/(\d+)")
IGNORE: t.Set[str] = {"TERM", "HOSTNAME", "HOME", "LSCOLORS"}


def gear_build(  # noqa: PLR0913
    directory: Path = typer.Argument(
        Path.cwd(), help="Directory to mount (optional, default=$PWD"
    ),
    update_env: bool = typer.Option(
        True, help="Update environment in manifest from built image"
    ),
    env_ignore: t.List[str] = typer.Option(
        [],
        "-i",
        "--env-ignore",
        help="<key>=[YES/no] variables to customize the default ignore list",
    ),
    dry_run: bool = typer.Option(
        False,
        "--env-dry-run",
        help="Don't update environment, only show what would be updated",
    ),
    pass_thru: t.List[str] = typer.Argument(None, hidden=True),
) -> None:
    """Build a gear container in the specified directory.

    If no directory is specified, the current directory is used.

    By default `gear build` will update the environment key in your manifest
    with the environment variables in the built image.

    Docker and Podman arguments can be passed through to the container by
    using `--` followed by the args. In this case, the `directory` argument
    must be passed explicitly (e.g. "." for the current directory).

    Examples:
        `gear build`
            Build the gear in the current directory.

        `gear build --env-ignore EDITOR`
            Build gear image, update environment and also ignore the
            `EDITOR` variable.

        `gear build --env-dry-run`
            Build gear image, don't update environment key in manifest, instead
            print out what would be updated.

        `gear build . -- --pull --no-cache`
            Build the gear in the current directory with `--pull` and `--no-cache`
            options passed to the underlying container build command.
    """
    if pass_thru is None:
        pass_thru = []
    # Validate args
    image_tag, mount_dir, manifest = validate_args(directory)
    image_tag = (
        extract_and_remove_param(pass_thru, "--tag")
        or extract_and_remove_param(pass_thru, "-t")
        or image_tag
    )
    # Build image
    rprint("Building container image")
    client = get_container_client()
    client.build(str(mount_dir.resolve()), image_tag, pass_thru)
    if update_env:
        ignorelist = handle_ignore(env_ignore)
        # Docker client
        rprint("Getting container environment")
        res = client.run(
            image_tag,
            entrypoint="printenv",
            capture=True,
        )
        if not res:
            rprint(red("Could not get environment output from docker container."))
            raise Exit(1)
        rprint("Handling container environment")
        handle_env(
            res.stdout, manifest, directory / "manifest.json", ignorelist, dry_run
        )


def validate_args(directory: Path) -> t.Tuple[str, Path, Manifest]:
    """Helper function to validate arguments."""
    # Manifest must exist
    manifest = validate_manifest(directory)
    docker_file = directory / "Dockerfile"

    # Dockerfile must exist
    if not docker_file.exists():
        raise Error(
            f"Could not find Dockerfile."
            f" Please make sure there is a Dockerfile in {directory.resolve()}"
        )
    image_tag = manifest.get_docker_image_name_tag()  # type: ignore

    return image_tag, directory, manifest  # type: ignore


def handle_ignore(block: t.List[str]) -> t.Set[str]:
    """Validate arguments."""
    ignorelist = IGNORE.copy()
    for item in block:
        if "=" not in item:
            ignorelist.add(item)
        else:
            try:
                key, val = str(item).split("=")
                if val.lower() == "yes":
                    ignorelist.add(key)
                elif val.lower() == "no":
                    if key in ignorelist:
                        ignorelist.remove(key)
                else:
                    raise ValueError()
            except ValueError as e:
                raise Error(
                    "Please enter `ignore` in the format <key>=<value> where value"
                    " is either 'yes' or 'no' or just <key> to assume yes"
                ) from e
    return ignorelist


def handle_env(
    out: bytes,
    manifest: Manifest,
    manifest_file: Path,
    ignorelist: t.Set[str],
    dry_run: bool,
) -> None:
    """Handle environment variables."""
    docker_env = {}
    try:
        env = out.decode().split("\n")
    except (UnicodeDecodeError, ValueError):
        log.debug("Could not parse docker output.", exc_info=True)
        rprint(yellow(f"Could not parse: \n{out!r}\nNot updating environment."))
        return

    for var in env:
        if len(var) == 0:
            continue
        var = var.strip("\r")  # noqa
        key, _, val = var.partition("=")
        if len(val) < 1:
            rprint(yellow(f"Skipping invalid env variable '{var}'"))
            continue
        if key not in ignorelist:
            docker_env[key] = val

    # If manifest already has some environment keys in it, keep those
    existing_env = dict(manifest.environment or {})
    existing_env.update(docker_env)
    manifest.manifest["environment"] = existing_env
    if dry_run:
        res_str = "Would write to manifest.json env: "
        res_str += f"{json.dumps(manifest.manifest.get('environment'), indent=2)}"
    else:
        res_str = "Successfully wrote environment to Manifest: "
        manifest.to_json(str(manifest_file.resolve()))
    rprint(green(res_str))
