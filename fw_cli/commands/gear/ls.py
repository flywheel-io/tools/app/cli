"""Gear list module."""

from rich import print as rprint
from typer import Argument, Option

from ...state import get_state
from ...styles import yellow
from .. import utils
from .utils import gear_table, get_gears_by_filter


def list_gears(  # noqa: PLR0913
    simple_filter: str = Argument(
        "",
        help="Simple gear name filter, equivalent to `-f gear.name=~<value>`",
        show_default=False,
    ),
    f_filter: str = Option(
        "",
        "-f",
        "--filter",
        help="Optional filter for gears, overrides `simple_filter`",
    ),
    limit: int = Option(100, "-l", "--limit", help="Number of gears to return"),
    all_versions: bool = Option(
        False, "-a", "--all-versions", help="Return all gear versions."
    ),
    disabled: bool = Option(
        False, "-d", "--disabled", help="Include disabled gears as well."
    ),
    output: utils.OutputFormat = utils.output_arg,
):
    """List installed gears with a simple or full filter.

    Examples:
        `gear list test` OR `gear list --filter gear.name=~test`
            Search for gears containing 'test' in the name

        `gear list --filter gear.name=test -a`
            Search for all versions (but not disabled versions) of gear with
            name `test`

        `gear list --filter gear.name=test -a -d`
            Search for all versions (including disabled versions) of a gear
            with name `test`

        `gear list --filter created>2021-10-01`
            Search for gears uploaded in the last month

        `gear list --filter created>2021-10-01T00:00:00,gear.name=test`
            Combine filters, search for gear name and created with time control
    """
    state = get_state()
    filter_ = ""
    if simple_filter:
        filter_ = f"gear.name=~{simple_filter}"
        if f_filter:
            msg = f"Full filter {f_filter} overrides simple filter {simple_filter}"
            rprint(yellow(msg))
            filter_ = f_filter
    else:
        filter_ = f_filter
    gears = get_gears_by_filter(
        filter_,
        state.fw,
        limit=limit,
        all_versions=all_versions,
        include_invalid=disabled,
    )
    if output == utils.OutputFormat.text:
        utils.autopager(gear_table(dict(enumerate(gears))))
    if output == utils.OutputFormat.yaml:
        utils.autopager(utils.format_yaml(gears))
    else:
        utils.autopager(utils.format_json(gears))
