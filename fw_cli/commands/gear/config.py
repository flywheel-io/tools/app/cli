"""Gear config module."""

import json
import logging
import shutil
import tempfile
import textwrap
import typing as t
from pathlib import Path

from fw_client import ClientError, FWClient, ServerError
from fw_gear.config import Config
from fw_gear.file import File
from fw_utils import AttrDict, pluralize
from rich import print as rprint
from typer import Argument, Exit, Option

from ...exceptions import Error
from ...state import get_state
from ...styles import green, header, red, yellow
from .utils import ID_REGEX, default_progress_bar, validate_config, validate_manifest

log = logging.getLogger(__name__)

tmpdir = Path(tempfile.gettempdir())


def gear_config(  # noqa: PLR0912, PLR0913, PLR0915
    path: Path = Argument(
        Path.cwd(),
        help="Path to folder with manifest",
    ),
    new: bool = Option(
        False, "-n", "--new", help="Create a new config from manifest in `directory`."
    ),
    show: bool = Option(
        False, "-s", "--show", help="Show all config and input options."
    ),
    input_items: t.Optional[t.List[str]] = Option(
        None,
        "-i",
        "--input",
        help=(
            "Key value pair of <input_name>=<value>.\n"
            "Try --show option to see available values.\n"
            "Can be chained together: ex. -i test=val -i test1=/tmp/dicom.zip"
        ),
        metavar="<KEY>=<VALUE>",
    ),
    config_items: t.Optional[t.List[str]] = Option(
        None,
        "-c",
        "--config",
        help=(
            "Key value pair of <config_name>=<value>. \n"
            "Try --show option to see available values."
            "Can be chained together: ex. -c debug=true -c test1=my_val"
        ),
        metavar="<KEY>=<VALUE>",
    ),
    destination: t.Optional[str] = Option(
        None,
        "-d",
        "--destination",
        help="Destination for gear run. FW path or ID.",
    ),
) -> None:
    """Build config.json for local gear running/testing.

    Examples:
        `gear config --new`
            Create a new config from default options in manifest.

        `gear config --show`
            Show possible configuration and input options

        `gear config -c debug=true -i input_file=/home/flywheel/nifti1.nii.gz`
            Set the debug option to true and add a file input

        `gear config -i api_key=$my_api_key`
            Set input api key from environment
    """
    # Gear config new or update
    config = None
    state = get_state()
    manifest = validate_manifest(path, err=False)
    if new:
        if not manifest:
            raise Error(f"A manifest is required in directory {path}")
        saved = save_backup(path / "config.json")
        if saved:
            rprint(f"Config already exists, saved backup config to {saved.name}")
        config = Config.default_config_from_manifest(manifest)
        rprint(green("Config generated"))

    if show:
        manifest = validate_manifest(path, err=False)
        if not manifest:
            raise Error(
                "Could not find manifest in directory. "
                "Cannot show available parameters."
            )
        header("Available options:", thickness=1)
        header("Inputs:")
        for name, val in manifest.inputs.items():
            rprint(format_val(name, val))
        header("Config: ")
        for name, val in manifest.config.items():
            rprint(format_val(name, val))
        raise Exit()

    # Try to load config if not just generated
    if not config:
        rprint("Loading config.json")
        config = validate_config(path)

    if destination:
        if ID_REGEX.match(destination):
            dest = state.fw.get(f"/api/containers/{destination}")
        else:
            dest_path = destination.replace("fw://", "").split("/")
            try:
                result = state.fw.post("/api/resolve", json={"path": dest_path})
            except (ClientError, ServerError) as e:
                raise Error("Could not look up destination project") from e
            dest = result.path[-1]
        rprint(f"Adding destination id {dest._id} ({dest.container_type})")
        config.destination = {  # type: ignore
            "id": dest._id,
            "type": dest.container_type,
        }
    if input_items:
        for input_val in input_items:
            try:
                name, val = input_val.split("=")
            except ValueError:
                raise Error(
                    f"Could not parse input {input_val}. "
                    "Please use <key>=<value> syntax."
                )

            # Allow adding api-key without manifest.
            if name in ["api_key", "api-key"]:
                rprint(f"Setting api-key input in {config._path}")  # type: ignore
                config.add_input("api-key", val, type_="api-key")  # type: ignore
                continue
            # Need manifest to update other inputs
            if not manifest:
                msg = f"Need manifest in current dir to update input {name}. Skipping"
                rprint(yellow(msg))
                continue
            if name not in manifest.inputs:
                rprint(yellow(f"Input '{name}' not in manifest, skipping."))
                continue
            if fw_input := val.startswith("fw://"):
                val, info = add_fw_input(state.fw, val)
            rprint(f"Adding input {name} at {str(val)}")
            base = manifest.inputs.get(name, {}).get("base", "file")
            try:
                rprint(f"Setting {name} input in {config._path}")  # type: ignore
                file_ = File.from_sdk(info, AttrDict(client=None)) if fw_input else None
                config.add_input(name, str(val), type_=base, file_=file_)  # type: ignore
            except ValueError as err:
                raise Error(str(err))
            populate_input_map(
                path,
                str(val),
                config.inputs[name]["location"]["path"],  # type: ignore
            )
    if config_items:
        conf_update = {}
        for config_val in config_items:
            try:
                name, _, val = config_val.partition("=")
                if not val:
                    raise ValueError
            except ValueError:
                raise Error(
                    f"Could not parse config {config_val}. "
                    "Please use <key>=<value> syntax."
                )
            if not manifest:
                msg = f"Need manifest in current dir to update config {name}. Skipping"
                rprint(yellow(msg))
                continue
            definition = manifest.config.get(name)  # type: ignore
            if not definition:
                msg = f"Config value {name} not present in manifest. Not setting."
                rprint(yellow(msg))
                continue
            val = handle_config_def(name, val, definition)
            msg = (
                f"Setting {name} config value in {config._path} "  # type: ignore
                f"to {val} ({type(val)})"
            )
            rprint(green(msg))
            conf_update[name] = val
        config.update_opts(conf_update)  # type: ignore
    if config:
        write_path = (path / "config.json").resolve()
        config.to_json(write_path)
        rprint(green(f"Config written to {write_path}"))


def add_fw_input(client: FWClient, val: str) -> tuple[Path, dict]:
    """Add input from an Fw path.

    Args:
        client (FWClient): API client
        val (str): fw path

    Returns:
        Path: path to downloaded file
        dict: file metadata from Flywheel
    """
    path = val.replace("fw://", "").split("/")
    result = client.post("/api/resolve", json={"path": path})
    file_ = result.path[-1]
    if file_.container_type != "file":
        raise Error(f"Path {val} is not a file")
    parent = result.path[-2]
    rprint(f"Found Flywheel file {file_.name}")
    dst_dir = tmpdir / "downloads"
    dst_dir.mkdir(exist_ok=True)
    container = pluralize(parent.container_type)
    dst_file = dst_dir / file_.name
    rprint(f"Downloading input to {dst_file}")
    url = f"/api/{container}/{parent._id}/files/{file_.name}"
    with default_progress_bar as progress:
        task_id = progress.add_task(f"Downloading {file_.name}")
        with client.stream("GET", url) as resp, open(dst_file, "wb") as dest_file:
            progress.update(task_id, total=int(resp.headers["Content-Length"]))
            for data in resp.iter_content():
                dest_file.write(data)
                progress.update(task_id, advance=len(data))
    info_url = f"/api/{container}/{parent._id}/files/{file_.name}/info"
    info = client.get(info_url)
    return dst_file, info


def format_val(name: str, val: t.Dict[str, str]) -> str:
    """Helper function pretty format manifest input/config options."""
    title = f"* Name: {name}\n"
    if "base" in val:
        details = f"- Type: {val.get('base')}\n"
        if val.get("base") == "file":
            details += f"- Allowed File Types: {val.get('type')}\n"
    else:
        details = f"- Type: {val.get('type')}\n"
    if "description" in val:
        details += "- Description: "
        details += "\n\t".join(textwrap.wrap(val.get("description", ""), width=70))
        details += "\n"
    if "optional" in val:
        details += f"- Optional: {val.get('optional')}\n"
    if "default" in val:
        details += f"- Default: {val.get('default')}\n"
    return title + textwrap.indent(details, prefix="  ")


def populate_input_map(path, local_path, docker_path):
    """Populate a map of local inputs to gear inputs."""
    input_map_f = (path / ".input_map.json").resolve()
    input_map = {}
    try:
        with open(input_map_f, "r", encoding="utf-8") as obj:
            input_map = json.load(obj)
    except FileNotFoundError:
        input_map_f.parents[0].mkdir(parents=True, exist_ok=True)
        input_map_f.touch()
    except json.JSONDecodeError:  # pragma: no cover
        log.debug(f"Can't decode input map at {input_map_f}")
    input_map.update({docker_path: local_path})

    with open(input_map_f, "w", encoding="utf-8") as obj:
        json.dump(input_map, obj)


def save_backup(conf):
    """Helper function to save backup config.json."""
    if conf.exists() and conf.is_file():
        check_path = conf.parents[0] / "config.json.back"
        shutil.move(str(conf), str(check_path))
        return check_path
    return None


def handle_config_def(name: str, val_str: str, definition: dict) -> t.Any:
    """Helper to handle definition of config option in the manifest.

    Tries to check whether value is allowed, and return properly typed value

    Raises:
        ValueError: Only if an option _can_ be typed, and is explicitely not
        allowed by an enum value.
    """
    # NOTE: Alot of this could probably done by just validating the input
    # against the jsonschema def in the manifest, but type conversion from
    # string needs to happen first, and I don't see a straightforward way to
    # decouple checking validity and type checking... - Nate
    type_def = definition.get("type")
    enum_def = definition.get("enum")
    val: t.Any = val_str

    # Check if given value is allowed in enum options.
    def _check_enum(x):
        if not enum_def:
            return
        if x not in enum_def:
            raise ValueError(
                f"Config option {name}: given value {val_str} not in "
                f"allowed values {enum_def}"
            )

    # Default to str if no type is defined
    if not type_def:
        _check_enum(val_str)
        return val
    # Simple types
    if type_def == "string":
        val = str(val_str)
    elif type_def == "boolean":
        bool_dict = {"false": False, "true": True, "f": False, "t": True}
        try:
            val = bool_dict[val_str.lower()]
        except KeyError:
            msg = (
                f"Cannot convert '{name}' with value '{val_str}' to "
                "bool, setting to False.\n"
                "Acceptable values: True, False, true, false, T, F, t, f."
            )
            rprint(red(msg))
            val = False
    # Number can be float or int in python, try converting as int first, then
    # fallback to float, finally to string.
    elif type_def == "number":
        try:
            val = int(val)
        except ValueError:
            try:
                val = float(val)
            except ValueError:
                msg = (
                    f"Cannot convert '{name}' with value '{val_str}' to "
                    "number, leaving as string."
                )
                rprint(red(msg))
    # Convert/validate every value in array recursively
    elif type_def == "array":
        item_def = definition.get("items", {})
        val = [
            handle_config_def(f"{name}[{i}]", it, item_def)
            for i, it in enumerate(val_str.split(","))
        ]
        return val
    else:
        msg = f"JSON-schema type {type_def}, not handled. Defaulting to string"
        rprint(red(msg))
    # After type inference check enum options again
    _check_enum(val)
    return val
