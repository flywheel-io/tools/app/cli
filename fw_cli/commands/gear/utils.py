"""Utilities for gear commands."""

import enum
import logging
import re
import shutil
import subprocess as sp
import textwrap
import typing as t
from http import HTTPStatus
from pathlib import Path

from fw_client import ClientError, FWClient, ServerError
from fw_gear.config import Config, ConfigValidationError
from fw_gear.invocation_schema import get_master_schema
from fw_gear.manifest import Manifest, ManifestValidationError
from rich import print as rprint
from rich.progress import (
    BarColumn,
    DownloadColumn,
    Progress,
    Task,
    TextColumn,
    TimeElapsedColumn,
    TimeRemainingColumn,
    TransferSpeedColumn,
)
from rich.table import Column, Table

from ... import styles as s
from ...containers import ContainerClient
from ...exceptions import Error, Exit

log = logging.getLogger(__name__)

ID_REGEX = re.compile(r"^[0-9a-fA-F]{24}$")


class StrEnum(enum.StrEnum):
    """Class of helpers for enumerated strings."""

    @classmethod
    def keys(cls: t.Type["StrEnum"]) -> t.KeysView[str]:
        """Return class keys."""
        return cls.__members__.keys()


class GearCategory(StrEnum):
    """Enum of gear categories."""

    analysis = "analysis"
    converter = "converter"
    utility = "utility"
    classifier = "classifier"
    qa = "qa"


def get_manifest(path: Path) -> t.Optional[Manifest]:
    """Get manifest at file ignoring validation."""
    manifest = None
    if path.exists() and path.is_file():
        manifest = Manifest(manifest=path)
    return manifest


# Ignore coverage for now
def validate_manifest(workdir: Path, err: bool = True) -> t.Optional[Manifest]:
    """Validate manifest.json return manifest object if successful, fail otherwise."""
    manifest_path = Path(workdir) / "manifest.json"
    manifest = get_manifest(manifest_path)
    if not manifest:
        if err:
            raise Error(f"A manifest is required in directory {workdir}")
        return None
    try:
        manifest = Manifest(manifest=manifest_path)
        manifest.validate()
    except ManifestValidationError as exc:  # pragma: no cover
        raise ValueError(str(exc)) from exc
    return manifest


def validate_config(
    workdir: Path, err: bool = True
) -> t.Optional[Config]:  # pragma: no cover
    """Validate config.json return config object if successful, fail otherwise."""
    config_path = Path(workdir) / "config.json"
    if not config_path.is_file():
        if err:
            raise Error(
                "A config must be present.  If you want to create a config for "
                "your gear, please run `gear config --new`"
            )
        return None
    try:
        conf = Config(None, None, path=config_path)
    except ConfigValidationError as exc:
        raise Error(str(exc)) from exc
    return conf


def attempt_to_correct_manifest(gear: t.Dict):
    """Take in a manifest and attempt to correct.

    Uses default manifest types from jsonschema to correct null values.
    """
    # TODO: Remove when
    # [FLYW-9293](https://flywheelio.atlassian.net/browse/FLYW-9293) is fixed.
    schema = get_master_schema()
    keys = gear.keys()
    for k in list(keys):
        # Replace `None` with the default value for the specified type of the
        # property in the schema
        if gear[k] is None:
            type_ = schema["properties"].get(k, {}).get("type")
            if type_ == "object":
                gear[k] = {}
            elif type_ == "array":
                gear[k] = []
            elif type_ == "string":
                gear[k] = ""


def mount_dirs(directory: Path) -> t.List[str]:
    """Return list of default directories to mount in docker format."""
    dirs = []
    dirs_to_mount = [
        (directory / "input", "/flywheel/v0/input"),
        (directory / "output", "/flywheel/v0/output"),
        (directory / "work", "/flywheel/v0/work"),
        (directory / "config.json", "/flywheel/v0/config.json"),
        (directory / "manifest.json", "/flywheel/v0/manifest.json"),
    ]
    for local_p, docker_p in dirs_to_mount:
        if not local_p.exists():
            rprint(s.yellow(f"{local_p} does not exist, skipping."))
            continue
        dirs.append(f"{local_p}:{docker_p}")
    return dirs


def download_gear(
    fw: FWClient,
    client: ContainerClient,
    gear: t.Dict,
) -> t.Dict:
    """Download gear from a FW site.

    Args:
        fw: FW API Client
        client: Container Client
        gear: gear manifest

    Returns:
        Downloaded gear document.
    """
    domain = fw.base_url.netloc.decode()
    gear_tag = f"{domain}/{gear['name']}:{gear['version']}"

    def print_gear(msg):
        rprint(f"[bold]({gear_tag})[/bold] {msg}")

    print_gear("Downloading manifest")
    gear_doc = fw.get(f"/api/gears/{gear['id']}")
    manifest = Manifest(gear_doc.gear)

    # Pull the image based on rootfs-url
    print_gear("Downloading image")
    rootfs_url = gear_doc.exchange["rootfs-url"]

    # Attempt to get tag from manifest for local image.
    # If im_tag, this takes precedence over gear_tag.
    im_tag = manifest.get_docker_image_name_tag()
    tag = im_tag or gear_tag

    if rootfs_url.startswith("docker://"):
        gear_image = rootfs_url.removeprefix("docker://")
        client.pull(gear_image)
        client.tag(gear_image, tag)
    else:
        run_cmd(["docker", "import", rootfs_url, tag])

    im = client.inspect(tag)
    print_gear(f"Local image tagged as: \n\t{im.get('RepoTags', '')}")  # type: ignore
    return dict(gear_doc)


def upload_gear(
    fw: FWClient,
    client: ContainerClient,
    src_image: str,
    gear_doc: t.Dict,
):
    """Upload a gear to a FW site.

    Args:
        fw: FW API Client
        client: Container Client
        src_image: Local gear image name
        gear_doc (t.Dict): Gear document.
    """
    domain = fw.base_url.netloc.decode()
    gear_name = gear_doc["gear"]["name"]
    version = gear_doc["gear"]["version"]
    dst_gear = f"{domain}/{gear_name}"
    gear_tag = f"{dst_gear}:{version}"

    def print_gear(msg):
        rprint(f"[bold]({gear_tag})[/bold] {msg}")

    print_gear(f"Tagging image locally as {gear_tag}")
    im = client.tag(src_image, gear_tag)
    if not im:
        raise RuntimeError(f"Could not find image {src_image}")
    gear_doc.update(
        {
            "exchange": {
                "rootfs-url": f"docker://{dst_gear}:{version}",
                "rootfs-hash": im.get("Id"),
            }
        }
    )
    print_gear("Getting permission to push image...")
    try:
        ticket = fw.post("/api/gears/prepare-add", json=gear_doc)
    except (ServerError, ClientError) as exc:
        if exc.response.status_code == HTTPStatus.CONFLICT:
            print_gear("Gear already exists")
            raise Exit(0)
        raise Error(f"Unhandled error: {exc}") from exc
    # 4: Push image
    print_gear("Uploading to Docker registry...")
    digest = client.push(gear_tag)
    if not digest:
        raise RuntimeError("Could not get digest from image push.")
    print_gear("Registering gear on server...")
    res = fw.post(
        "/api/gears/save",
        json={
            "ticket": ticket["ticket"],  # type: ignore
            "repo": gear_name,
            "pointer": digest,
        },
    )
    gear_id = None
    gear_id = getattr(res, "_id", None)
    if not gear_id:
        gear_id = getattr(res, "gear", None)
    if not gear_id:
        log.debug(f"Could not find gear ID in response {res}")
        gear_id = "Not Found"
    msg = (
        f"Uploaded gear with id {gear_id} "
        "You should now see your gear in "
        "the Flywheel web interface or find it with `gear list`"
    )
    print_gear(msg)


def write_run_sh(
    cmdline: str,
    directory: Path,
    image: str,
) -> None:
    """Helper to write a run.sh script."""
    run_script = directory / "run.sh"
    log.info("Writing run.sh script")
    cmd = textwrap.wrap(
        cmdline, width=88, break_long_words=False, break_on_hyphens=False
    )
    with open(run_script, "w", encoding="utf-8") as fp:
        fp.write("#!/usr/bin/env bash \n\n")
        fp.write(f"IMAGE={image}\n\n")
        fp.write("# Command:\n")
        for i, line in enumerate(cmd):
            if image in line:
                line = line.replace(image, "$IMAGE")  # noqa
            prefix = "\t" if i > 0 else ""
            fp.write(f"{prefix}{line} \\\n")


def parse_gear_name_or_id(
    names_or_ids: t.List[str],
) -> t.Tuple[t.List[str], t.List[str]]:
    """Parse names or ids from a list of possibly both by regex.."""
    names = []
    ids = []
    for name_or_id in names_or_ids:
        if ID_REGEX.match(name_or_id):
            ids.append(name_or_id)
        else:
            names.append(name_or_id)

    return names, ids


def gear_dict_from_names_or_ids(
    names_or_ids: t.List[str], fw: FWClient, **kwargs
) -> t.Dict:
    """Helper to get gear dictionary from names or ids."""
    names, ids = parse_gear_name_or_id(names_or_ids)
    try:
        gear_dict = gears_dict(names, ids, fw, **kwargs)
    except (ClientError, ServerError) as exc:
        raise Error("Could not get list of gears") from exc
    return gear_dict


def get_gears_by_filter(filter_: str, fw: FWClient, **kwargs):
    """Get all gears matching a name and optional version."""
    params: t.Dict[str, t.Any] = {
        "all_versions": True,
        "include_invalid": True,
    }
    params.update(kwargs)
    if filter_:
        params["filter"] = filter_
    res = fw.get("/api/gears", params=params)
    gear_dict = []
    for g in res:
        enabled = g.disabled is None
        gear_dict.append(
            {
                "id": g._id,
                "name": g.gear.name,
                "label": g.gear.label,
                "version": g.gear.version,
                "enabled": enabled,
            }
        )
    return gear_dict


def get_gear_by_id(id_: str, fw: FWClient):
    """Get a single gear by id."""
    res = fw.get(f"/api/gears/{id_}")
    enabled = res.disabled is None
    return {
        "id": res._id,
        "name": res.gear.name,
        "label": res.gear.label,
        "version": res.gear.version,
        "enabled": enabled,
    }


def gears_dict(names: t.List[str], ids: t.List[str], fw: FWClient, **kwargs) -> t.Dict:
    """Create a list of gear dictionaries from names or ids.

    Returns:
        A dictionary with indices as keys.  This is so that when the gears are
        rendered as a table, the indices chosen by a user can look up the
        specific gear, sort of like an OrderedDict but without need for
        re-ordering and with the ability to access like a list and retain
        references when an item is poped.
    """
    idx = 0
    gear_dict: t.Dict = {}
    for name in names:
        if ":" in name:
            n, v = name.split(":")
            filter_ = f"gear.name={n},gear.version={v}"
        else:
            filter_ = f"gear.name={name}"
        for g in get_gears_by_filter(filter_, fw, **kwargs):
            gear_dict[idx] = g
            idx += 1
    for id_ in ids:
        gear_dict[idx] = get_gear_by_id(id_, fw)
        idx += 1
    return gear_dict


def split_gear_dict(gear_dict: dict) -> t.Tuple[list, list]:
    """Split gear dictionary into unique gears and duplicates.

    Duplicates need to be rendered as a table for the user to choose, and
    unique ones can be automatically handled.

    Returns
        tuple:
            - Keys of unique gears in the input dictionary
            - Keys of duplicate gears (which need rendering) in the input
              dictionary
    """
    super_dict: dict = {}
    # Make a dictionary of {<gear_name>: [<ids>]}
    # If a gear-name is unique (i.e. only one version) then it will only have a
    # single index in it's value list.
    for idx, gear in gear_dict.items():
        if gear["name"] in super_dict:
            super_dict[gear["name"]].append(idx)
            continue
        super_dict[gear["name"]] = [idx]
    unique = []
    duplicate = []
    # Sort super dictionary into unique (1) and duplicate (>1)
    for gear, idxs in super_dict.items():
        if len(idxs) == 1:
            unique.append(idxs[0])
        else:
            duplicate.extend(idxs)
    return unique, duplicate


def gear_table(gear_dict: t.Dict) -> Table:  # pragma: no cover
    """Get table of gears."""
    table = Table("", "id", "name", "label", "version", "enabled")
    for idx, g in gear_dict.items():
        table.add_row(
            str(idx),
            g["id"],
            g["name"],
            g["label"],
            g["version"],
            s.bold(s.green("yes")) if g["enabled"] else s.bold(s.red("no")),
        )
    return table


class ProgressWithHeader(Progress):  # pragma: no cover
    """Override progress class to show headers."""

    def reset_tasks(self) -> None:
        """Remove all tasks from self."""
        self._tasks = {}

    def make_tasks_table(self, tasks: t.Iterable[Task]) -> Table:
        """Get a table to render the Progress display."""
        table_columns = (
            (
                Column(no_wrap=True)
                if isinstance(_column, str)
                else _column.get_table_column().copy()
            )
            for _column in self.columns
        )
        table = Table(
            *table_columns,
            padding=(0, 1),
            expand=self.expand,
            box=None,
            show_header=True,
            show_footer=False,
            show_edge=False,
        )

        for task in tasks:
            if not task.visible:
                continue
            cols = (
                column.format(task=task) if isinstance(column, str) else column(task)
                for column in self.columns
            )
            table.add_row(*cols)
        return table


download_columns = (
    TextColumn(
        "[progress.description]{task.description}",
        table_column=Column(no_wrap=True, header="Status"),
    ),
    BarColumn(table_column=Column(header="Progress")),
    TextColumn(
        "[progress.percentage]{task.percentage:>3.0f}%",
        table_column=Column(no_wrap=True, header="Percentage"),
    ),
    DownloadColumn(table_column=Column(header="Downloaded")),
    TransferSpeedColumn(table_column=Column(header="Speed")),
    TimeElapsedColumn(table_column=Column(header="Elapsed")),
    TimeRemainingColumn(table_column=Column(header="Remaining")),
)
default_progress_bar = ProgressWithHeader(*download_columns, refresh_per_second=10)


def run_cmd(cmd, **kwargs) -> sp.CompletedProcess:
    """Run a command (safe, non shell)."""
    kwargs.setdefault("check", True)
    assert not kwargs.get("shell"), "Do not use shell=True"
    if shutil.which(cmd[0]) is None:
        raise Error(f"Missing required command {cmd[0]!r}")
    sp.run(cmd, **kwargs)  # noqa: PLW1510


def extract_and_remove_param(params: t.List[str], param: str) -> t.Optional[str]:
    """Extract and remove the first instance of a parameter and its value from a list.

    This function searches for the specified parameter in the list. If the parameter
    is found as a standalone item, it removes it and the next item in the list,
    assuming it is the value. If the parameter is found in the form of "param=value",
    it extracts the value and removes the parameter from the list. The function should
    not be used for repeated parameters.

    Args:
        params (List[str]): The list of parameters to search and modify.
        param (str): The parameter to extract and remove.

    Returns:
        Optional[str]: The extracted value of the parameter, or None if the parameter
        is not found.
    """
    if param in params:
        idx = params.index(param)
        params.pop(idx)
        ret = params.pop(idx)
    elif any(f"{param}=" in s for s in params):
        ret = next(s.split("=")[1] for s in params if f"{param}=" in s)
        params[:] = [s for s in params if f"{param}=" not in s]
    else:
        ret = None
    return ret
