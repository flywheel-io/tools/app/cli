"""Gear create module."""

import inspect
import json
import logging
import re
import shutil
import subprocess as sp
import textwrap
import typing as t
from pathlib import Path

import typer
from jinja2 import Environment, FileSystemLoader, select_autoescape
from rich import print as rprint
from rich.prompt import Prompt

from ...exceptions import Error
from ...styles import bold, cyan, green, magenta, yellow
from .utils import GearCategory, StrEnum, run_cmd

log = logging.getLogger(__name__)


images = [
    "python:3.8-buster",
    "python:3.7-buster",
    "flywheel/python:main",
    "flywheel/python-gdcm:main",
    "ubuntu:latest",
    "neurodebian:latest",
]


class Template(StrEnum):
    """Enum of template types."""

    basic = "basic"
    flywheel_poetry = "flywheel_poetry"


NAME_RE = re.compile(r"^[a-z0-9\-]{1,100}$")
LABEL_RE = re.compile(r"^[a-zA-Z0-9\- ]{1,100}$")


def get_author() -> str:
    """Helper function to get author string from git if available."""
    default = "Firstname Lastname <email@example.com>"
    name, email = b"", b""
    if not shutil.which("git"):
        return default
    try:
        run_cmd = sp.check_output
        name = run_cmd(["git", "config", "--get", "user.name"])
        email = run_cmd(["git", "config", "--get", "user.email"])
    except sp.CalledProcessError:
        log.debug("Couldn't get author name or email.", exc_info=True)
    res = ""
    if name and email:
        name_str = name.decode("utf-8").strip("\n")
        email_str = email.decode("utf-8").strip("\n")
        res = f"{name_str} <{email_str}>"
    if res:
        return res
    return default


def gear_create(  # noqa: PLR0912, PLR0913
    directory: Path = typer.Argument(
        Path.cwd(), help="Directory to create gear in (optional, default=$PWD)"
    ),
    label: str = typer.Option(
        "My new gear",
        "-l",
        "--label",
        prompt=True,
        help="Enter a Human friendly label for your gear (see details above).",
    ),
    name: str = typer.Option(
        "new-gear",
        "-n",
        "--name",
        prompt=True,
        help="Enter a machine friendly name for your gear (see details above).",
    ),
    author: str = typer.Option(
        get_author(),
        "-a",
        "--author",
        prompt=True,
        help="Enter a gear author, e.g. `Firstname Lastname <name@email.com>",
    ),
    image: str = typer.Option(
        "",
        "-i",
        "--image",
        help="Base image from choice (see image details above) or custom image.",
    ),
    template: str = typer.Option(
        "",
        "-t",
        "--template",
        help="Template for building gear (see template details above)",
    ),
    category: str = typer.Option(
        "",
        "-c",
        "--category",
        help=("Gear category." + cyan(f"\nOne of ({'|'.join(GearCategory)})")),
    ),
    description: str = typer.Option(
        "Awesome new gear",
        "-d",
        "--description",
        help="Gear description.",
        prompt=True,
    ),
    sdk_enabled: bool = typer.Option(
        False, "-s", "--sdk", help="Enable SDK usage in this gear", prompt=True
    ),
    sdk_readonly: bool = typer.Option(
        False, "--sdk-readonly", help="Make SDK read-only in this gear. Implies `--sdk`"
    ),
    cite: str = typer.Option(
        "", "--cite", help="Gear citation information.", prompt=True
    ),
    lic: str = typer.Option("MIT", "--license", help="Gear license, defaults to MIT."),
    url: str = typer.Option("", "--url", help="Project source URL."),
) -> None:
    """Create a new gear for development.

    Create a new gear in the specified directory using the specified template,
    base docker image, and provided information.
    """
    if (directory / "manifest.json").exists():
        rprint(yellow(f"Manifest already exists in {directory}.  Exiting."))
        return
    if not image:
        image = Prompt.ask(
            "\nEnter image name. Choose one of\n"
            + green("[")
            + green(list_to_choices(images))
            + green("]")
            + "\nor any other valid Docker image. "
            + bold(magenta(r"\[default: python:3.8-buster]"))
        )
        rprint("\n")
        if not image:
            image = "python:3.8-buster"
    if not NAME_RE.match(name):
        raise Error(
            "Gear name is invalid, must contain only "
            "lowercase letters, numbers, and hyphens"
        )
    if not LABEL_RE.match(label):
        msg = "Gear label is invalid, must contain only letters, numbers, and hyphens"
        raise Error(msg)
    if not template:
        template = Prompt.ask(
            "Choose a gear template"
            + green(" [")
            # if you include sq brackets in main expression line, choices not displayed
            + green(f"{list_to_choices(list(Template.keys()))}")
            + green("] ")
            + bold(magenta("(required)"))
        )
        rprint("\n")
    if template not in Template.keys():
        raise Error(
            f"Unknown template {template}, choose one of {list(Template.keys())}"
        )
    if not category:
        category = Prompt.ask(
            "Choose a gear category"
            + green(" [")
            + green(f"{list_to_choices(list(GearCategory.keys()))}")
            + green("] ")
            + bold(magenta("(required)"))
        )
        rprint("\n")
    if category not in GearCategory.keys():
        raise Error(
            f"Unknown category {category}, choose one of {list(GearCategory.keys())}"
        )
    rprint(green(f"Using docker image '{image}' with template '{template}'"))
    manifest: dict[str, t.Any] = {
        "author": author,
        "cite": cite,
        "command": "./run.py",
        "config": {},
        "description": description,
        "environment": {},
        "custom": {
            "gear-builder": {
                "image": f"{name}:0.1.0",
                "category": category,
            },
            "flywheel": {"show-job": True},
        },
        "inputs": {
            "input-file": {
                "base": "file",
                "optional": False,
                "description": "Input file for the gear",
            },
        },
        "label": label,
        "name": name,
        "source": url,
        "url": url,
        "license": lic,
        "version": "0.1.0",
    }
    if sdk_enabled or sdk_readonly:
        api_key = {"api-key": {"base": "api-key", "read-only": sdk_readonly}}
        manifest["inputs"].update(api_key)
    if template == Template.basic:
        basic_template(directory, manifest, image)
    elif template == Template.flywheel_poetry:
        proj_path = "flywheel-io/scientific-solutions/etc/utils/skeleton-utils.git"
        git_template(proj_path, directory, manifest, image, branch="mustache-format")
    rprint("Done, now build your gear with `gear build`")


def basic_template(directory: Path, manifest: dict, image: str):
    """Basic template implementation."""
    with open(directory / "manifest.json", "w", encoding="utf-8") as fp:
        json.dump(manifest, fp, indent=2)

    with open(directory / "run.py", "w", encoding="utf-8") as fp:
        run_file = """
        #!/usr/bin/env python
        from fw_gear import GearContext
        # See docs at https://flywheel-io.gitlab.io/scientific-solutions/lib/fw-gear/

        def main(context):
            # Get input defined in manifest
            input_file = context.get_input_path("input-file")

            print(f"I found an input 'input_file' at {input_file}")

        if __name__ == '__main__':
            # Initialize Gear Context
            with GearContext() as context:
                context.init_logging()
                main(context)
        """
        fp.write(inspect.cleandoc(run_file))

    with open(directory / "Dockerfile", "w", encoding="utf-8") as fp:
        dockerfile = f"""
            FROM {image} as base

            ENV FLYWHEEL=/flywheel/v0
            WORKDIR ${{FLYWHEEL}}

            COPY run.py requirements.txt ${{FLYWHEEL}}/

            RUN pip install -r requirements.txt

            RUN chmod +x run.py
            ENTRYPOINT ["./run.py"]
            """
        fp.write(inspect.cleandoc(dockerfile))

    with open(directory / "requirements.txt", "w", encoding="utf-8") as fp:
        fp.write("flywheel-gear-toolkit\n")


def git_template(
    proj_path: str,
    directory: Path,
    manifest: dict,
    image: str,
    branch: t.Optional[str] = None,
):
    """Handler for flywheel_poetry template."""
    if not shutil.which("git"):
        raise Error("Cannot use this template without `git` installed.")
    tmpl_dir = directory / "templates"
    if tmpl_dir.exists():
        raise Error(f"Destination {directory} already contains template folder.")
    add_branch: list[str] = []
    if branch:
        add_branch = ["--branch", branch]
    url = f"https://gitlab.com/{proj_path}"
    clone_cmd = ["git", "clone", "--depth", "1", *add_branch, url, str(tmpl_dir)]
    try:
        run_cmd(clone_cmd)
        run_cmd(["git", "remote", "remove", "origin"], cwd=str(tmpl_dir))
    except sp.CalledProcessError as exc:
        raise Error(f"Could not clone repository: ({exc})") from exc
    render_files: dict[str, str] = {
        "pyproject.toml": "pyproject.toml",
        "run.py.j2": "run.py",
        "Dockerfile": "Dockerfile",
        "README.md": "README.md",
    }
    for item in tmpl_dir.glob("*"):
        if item.name in render_files:
            continue
        shutil.move(item, directory)
    env = Environment(
        loader=FileSystemLoader(searchpath=tmpl_dir),
        autoescape=select_autoescape(),
    )
    gear_package = manifest["name"].replace("-", "_")
    ctx = {
        "gear_label": manifest["label"],
        "gear_name": manifest["name"],
        "image": image,
        "gear_package": gear_package,
        "description": manifest["description"],
    }
    for name, out_file in render_files.items():
        template = env.get_template(name)
        with open(directory / out_file, "w", encoding="utf-8") as fp:
            fp.write(template.render(ctx))
    with open(directory / "manifest.json", "w", encoding="utf-8") as fp:
        json.dump(manifest, fp, indent=2)
    shutil.move(directory / "fw_gear_skeleton", directory / f"fw_gear_{gear_package}")
    shutil.rmtree(tmpl_dir)
    msg = (
        "Successfully initialized template repo, use `git remote add origin "
        "<url>` to link to existing remote repository."
    )
    rprint(green(msg))


def list_to_choices(lst: list[str]) -> str:
    """Convert list to string to mimic typer choices."""
    return "\n".join(
        textwrap.wrap(
            " | ".join(lst), width=70, break_long_words=False, break_on_hyphens=False
        )
    )
