"""Build and interact with FW gears."""

from ...patch import FWGroup, FWTyper
from . import (
    build,
    common,
    config,
    create,
    export,
    install,
    ls,
    permission,
    run,
    upload,
    version,
)

gear = FWTyper(
    name="gear",
    cls=FWGroup,
    callback=common.gear_options,
    help=__doc__,
    invoke_without_command=True,
)


gear.add_command(build.gear_build, name="build")
gear.add_command(config.gear_config, name="config")
gear.add_command(create.gear_create, name="create")
gear.add_command(export.export_gear, name="export")
gear.add_command(install.gear_install, name="install")
gear.add_command(ls.list_gears, name="ls", alias_of="list")
gear.add_command(permission.permission, name="permission")
gear.add_command(
    run.run_gear, name="run", context_settings={"ignore_unknown_options": True}
)
gear.add_command(upload.gear_upload, name="upload")
gear.add_command(version.gear_version, name="version")
