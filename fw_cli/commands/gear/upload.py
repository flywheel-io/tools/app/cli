"""Gear Upload module."""

import logging
from pathlib import Path
from typing import cast

from typer import Argument, Option, prompt

from ...containers import get_container_client
from ...exceptions import Error
from ...state import get_state
from .utils import GearCategory, upload_gear, validate_manifest

log = logging.getLogger(__name__)


def gear_upload(
    path: Path = Argument(Path.cwd(), help="Directory containing gear to upload."),
    category: GearCategory = Option(
        None,
        "-c",
        "--category",
        help=(
            "Gear category. Overwrites value found in manifest "
            ".custom.gear-builder.category\n"
            f"One of ({'|'.join(GearCategory)})"
        ),
    ),
) -> None:
    """Build, tag and upload a gear to the Flywheel registry.

    Docker build, tag and push to Flywheel instance registry with configuration
    extracted from manifest.json, register gear to Flywheel instance.
    """
    state = get_state()
    manifest = validate_manifest(path)
    assert manifest

    # GearDoc
    gear_category = manifest.get_value(  # type:ignore
        "custom.gear-builder.category"
    )
    if category:
        gear_category = category
    if gear_category not in GearCategory.keys():
        gear_category = prompt(
            "Could not determine category from manifest, "
            f"please enter category (one of [{'|'.join(GearCategory)}])"
        )
        if gear_category not in GearCategory.keys():
            raise Error(f"Gear category {gear_category} not allowed.")
    gear_doc = {
        "category": gear_category,
        "gear": manifest.manifest.to_dict(),  # type: ignore
    }

    # gear-doc.custom.gear-builder.image
    src_image = cast(str, manifest.get_docker_image_name_tag())
    if not src_image:
        raise Error("Could not find docker image in manifest")
    client = get_container_client()
    upload_gear(state.fw, client, src_image, gear_doc)
    # TODO: Add mechanism to upload to multiple sites.
    # for api_key in key:
    #    fw = get_fw_client(api_key)
    #    add_gear(fw, profile, src_image, gear_doc)
