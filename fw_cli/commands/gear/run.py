"""Gear run module."""

import json
import logging
import shutil
import tempfile
import typing as t
from pathlib import Path

from fw_gear.config import Config
from fw_gear.manifest import Manifest
from rich import print as rprint
from typer import Argument, Option

from ...containers import get_container_client
from ...exceptions import Error
from ...styles import green, red, yellow
from ..utils import collapse_path
from .utils import (
    extract_and_remove_param,
    mount_dirs,
    validate_config,
    validate_manifest,
    write_run_sh,
)

log = logging.getLogger(__name__)

tmpdir = Path(tempfile.gettempdir())


def run_gear(  # noqa: PLR0913
    location: Path = Argument(
        Path.cwd(),
        help="Location of the gear directory, defaults to current directory",
    ),
    prepare: bool = Option(
        False,
        "-p",
        "--prepare",
        help="Prepare gear run in <prepare-dir>",
    ),
    prepare_dir: t.Optional[Path] = Option(
        None,
        "-d",
        "--prepare-dir",
        show_default=False,
        help=(
            "Location where to prepare gear run directory\n"
            rf"\[Default: {tmpdir}/gear/<gear_name>_<gear_version>]"
        ),
    ),
    pass_thru: t.List[str] = Argument(None, hidden=True),
):
    """Run a Flywheel gear locally.

    Can either be used in conjunction with `job pull` to pull a gear job that
    has already ran and mimic the run locally and run locally, or can be used
    with `gear config` to build up a config.json from scratch and run a gear
    which has not been run on Flywheel yet.

    Docker and Podman arguments can be passed through to the container by
    using `--` followed by the args. In this case, the `location` argument
    must be passed explicitly (e.g. "." for the current directory).

    Note that the `--entrypoint` argument is handled differently than all other
    pass-through Docker/Podman arguments. An entrypoint passed in with this flag
    will be interpreted as the command to be run in the container (with the
    `-c` flag). The actual entrypoint of the container is always "/bin/sh".

    Examples:
        `gear run . -- --entrypoint 'python run.py' --interactive`
            Run the gear in the current directory (from `job pull` or `gear
            run --prepare`) interactively, replacing the command in the
            manifest with python run.py.

        `gear run /var/testgear_0.0.1 -- --volume="$(pwd)/config:/flywheel/v0/input"`
            Run the gear in the directory `/var/testgear_0.0.1` and additionally
            mount the folder `$(pwd)/config` to the container at `/flywheel/v0/input`.
    """
    location = location.resolve()
    gear_dir = None
    if pass_thru is None:
        pass_thru = []
    if not (location.exists() and location.is_dir()):
        raise Error(f"Cannot find gear location {location.resolve()}")
    if prepare:
        config, manifest, gear_dir = populate_target(location, prepare_dir)
    else:
        # Require valid manifest and config
        manifest = validate_manifest(location)
        config = validate_config(location)
        gear_dir = location
    assert manifest is not None
    assert config is not None
    entrypoint = extract_and_remove_param(pass_thru, "--entrypoint")
    command = entrypoint or manifest.manifest.get("command", "")
    image = manifest.get_docker_image_name_tag()
    vols = mount_dirs(gear_dir)
    env = manifest.environment or None
    populate_inputs(location, config, manifest, gear_dir)
    if prepare:
        rprint(green(f"Success! Prepared gear structure in {gear_dir}"))
        return
    rprint(f"Creating container from {image}")
    client = get_container_client()
    res = client.run(
        image,
        entrypoint=command,
        volumes=vols,
        env=env,
        pass_thru=pass_thru,
    )
    rprint(f"Args passed to container: {res.args}")
    rprint("Dumping run command in run.sh")
    write_run_sh(" ".join(res.args), gear_dir, image)


def populate_target(
    wdir: Path, target: t.Optional[Path]
) -> t.Tuple[Config, Manifest, Path]:
    """Populate target directory with gear structure."""
    workdir = Path(wdir).resolve()
    # Require valid manifest
    mani = validate_manifest(workdir)
    # Require valid config
    config = validate_config(workdir)
    # Populate directory structure
    if target is None:
        target = tmpdir / f"gear/{mani.name}_{mani.version}"  # type:ignore
    rprint(f"Building gear structure in {target}.")
    target.mkdir(parents=True, exist_ok=True)
    for path in ["input", "output", "work"]:
        new_dir = target / path
        new_dir.mkdir(exist_ok=True)
    # Copy over Config and Manifest.
    shutil.copy(workdir / "config.json", target / "config.json")
    shutil.copy(workdir / "manifest.json", target / "manifest.json")
    # Copy over input map if it exists
    input_map = (workdir / ".input_map.json").resolve()
    if input_map.exists() and input_map.is_file():
        shutil.copy(input_map, target / ".input_map.json")
    return config, mani, target  # type: ignore


def populate_inputs(
    location: Path, config: Config, manifest: Manifest, target: Path
) -> None:
    """Perform setup actions."""
    rprint("Populating inputs.")
    input_map = {}
    input_map_f = (location / ".input_map.json").resolve()
    try:
        with open(input_map_f, "r", encoding="utf-8") as fp:
            input_map = json.load(fp)
    except FileNotFoundError:
        msg = (
            "Could not find input map, inputs won't be populated. If you want "
            "to use inputs, try adding them with `gear config`"
        )
        rprint(yellow(msg))
        return
    for key, val in manifest.inputs.items():  # type: ignore
        # Loop through manifest inputs, if defined in config should
        #   also be defined in input map.  Try to copy from source to
        #   destination as specified in input map.
        if val["base"] == "file" and key in config.inputs:
            input_item = target / f"input/{key}"
            input_item.mkdir(exist_ok=True)
            conf_val = config.inputs[key]
            docker_path = conf_val["location"]["path"]
            file_name = conf_val["location"]["name"]
            if docker_path in input_map:
                source = Path(input_map[docker_path]).expanduser().resolve()
                dest = target / f"input/{key}/{file_name}"
                if source == dest:
                    continue
                msg = (
                    f"* Found file input with source path: {collapse_path(source)}, "
                    f"\n\tcopying to destination: {collapse_path(dest)}\n"
                )
                rprint(green(msg))
                try:
                    shutil.copy(source, dest)
                except shutil.SameFileError:  # pragma: no cover
                    # Ignore same file error that comes when file exists.
                    pass
                except Exception:
                    msg = f"* Couldn't copy input {key}: {file_name}"
                    msg += " please ensure it exists.\n"
                    rprint(red(msg))
                    log.debug(msg, exc_info=True)
            else:
                msg = (
                    f"* Local path for input {key} not found. "
                    f"Try adding with `gear config --{key} <path>\n"
                )
                rprint(yellow(msg))
