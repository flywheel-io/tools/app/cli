"""Gear permission module."""

import logging
import re
import typing as t
from http import HTTPStatus

from fw_client import ClientError, FWClient, ServerError
from pydantic import BaseModel
from rich import box
from rich import print as rprint
from rich.table import Table
from typer import Argument, Option

from ...exceptions import Error
from ...state import get_state
from ..utils import autopager

log = logging.getLogger(__name__)

PERMISSION_TYPES = ["projects", "users"]


class GearPermissions(BaseModel):
    """Gear permissions model."""

    default: bool = False
    projects: t.List[str] = []
    users: t.List[str] = []

    def __init__(self, perm_dict):
        """Set default to True if no permissions set."""
        super().__init__(**(perm_dict or {}))
        if not perm_dict:
            self.default = True


def permission(  # noqa: PLR0913
    gear_name: str = Argument(..., help="Gear name", show_default=False),
    add: bool = Option(False, "-a", "--add", help="Add the provided permissions"),
    delete: bool = Option(
        False, "-d", "--delete", help="Delete the provided permissions"
    ),
    reset: bool = Option(False, "-r", "--reset", help="Reset permissions to default"),
    project: t.List[str] = Option(
        [], "-p", "--project", help="Project ID(s) to add or remove"
    ),
    user: t.List[str] = Option([], "-u", "--user", help="User ID(s) to add or remove"),
):
    """Show or modify gear permissions."""
    state = get_state()
    version = state.fw.get("/api/version")
    if "fix_add_gear_series_for_all_gears" not in version.applied_fixes:
        raise Error(
            "Gear permissions not supported on Flywheel version "
            f"{version.flywheel_release}"
        )
    if not (add or delete or reset):
        # Show permissions
        series = state.fw.get(f"/api/gears/{gear_name}/series")
        permissions = GearPermissions(series.permissions)
        render_gear_permissions(series.name, permissions, state.fw)
        return
    if (add + delete + reset) > 1:
        raise Error("Only one of (add | delete | reset) may be used at a time")
    if reset:
        rprint(f"Resetting {gear_name} permissions")
        try:
            out = modify_permissions(gear_name, None, False, state.fw)
            render_gear_permissions(gear_name, out, state.fw)
        except (ClientError, ServerError) as e:
            raise Error(f"Could not reset gear permissions: {repr(e)}") from e
    else:
        rprint(f"Changing {gear_name} permissions")
        series = state.fw.get(f"/api/gears/{gear_name}/series")
        permissions = GearPermissions(series.permissions)
        for key, val in [("projects", project), ("users", user)]:
            for el in val:
                perms = getattr(permissions, key)
                id_ = parse_input(key, el, state.fw)
                if add:
                    if id_ not in perms:
                        perms.append(id_)
                elif id_ in perms:
                    perms.remove(id_)
        try:
            out = modify_permissions(gear_name, permissions, True, state.fw)
            render_gear_permissions(gear_name, out, state.fw)
        except (ClientError, ServerError) as e:
            raise Error(f"Could not update gear permissions: {repr(e)}") from e


def parse_input(type_: str, val: str, fw: FWClient) -> str:
    """Parse a given permission input."""
    if type_ != "projects" or re.match(r"^[0-9a-fA-F]{24}$", val):
        return val
    rprint(f"Project '{val}' not an ID, attempting to look up")
    path_list = val.replace("fw://", "").split("/")
    try:
        resp = fw.post("/api/resolve", json={"path": path_list})
        if resp and len(resp.get("path", [])) > 0:
            return resp.get("path", [])[-1].get("_id")
        raise Error(f"No project returned for path '{val}'")
    except (ClientError, ServerError) as e:
        if e.response.status_code == HTTPStatus.NOT_FOUND:
            raise Error(
                f"Could not find project '{val}'.\n"
                "It may not exist or you don't have permissions to access it."
            ) from e
        raise Error(f"Error looking up project: {repr(e)}") from e


def render_gear_permissions(name: str, perms: GearPermissions, fw: FWClient) -> None:
    """Render a gear series."""
    main_table = Table(title=f"{name} can run for: ", box=box.SIMPLE)
    # Create a column and sub-table for each permission type
    for perm in PERMISSION_TYPES:
        main_table.add_column(perm.capitalize())
    tables = {key: Table(box=None) for key in PERMISSION_TYPES}
    for table in tables.values():
        table.add_column("ID")
    tables["projects"].add_column("Label")

    # Add row to sub-tables for each entry in each permission type
    for type_ in PERMISSION_TYPES:
        for el in getattr(perms, type_):
            if type_ == "projects":
                proj = fw.get(f"/api/projects/{el}")
                tables[type_].add_row(proj._id, f"{proj.parents.group}/{proj.label}")
            else:
                tables[type_].add_row(el)

    if not any([tables[type_].row_count for type_ in PERMISSION_TYPES]):
        for type_ in PERMISSION_TYPES:
            tables[type_].add_row("None")

    if perms.default:
        rprint(f"No permissions set on gear '{name}'")
    else:
        main_table.add_row(*tables.values())
        autopager(main_table)


def modify_permissions(
    gear_name: str,
    permissions: t.Optional[GearPermissions],
    is_restricted: bool,
    fw: FWClient,
) -> GearPermissions:
    """Modify gear permissions."""
    if not permissions:
        permissions_res = fw.delete(f"/api/gears/{gear_name}/permissions")
        output_permissions = GearPermissions(permissions_res)
    else:
        payload = permissions.model_dump(exclude={"default"})
        permissions_res = fw.put(f"/api/gears/{gear_name}/permissions", json=payload)
        output_permissions = GearPermissions(permissions_res.permissions)

    restricted_res = fw.put(
        f"/api/gears/{gear_name}/series", json={"is_restricted": is_restricted}
    )
    if restricted_res.is_restricted != is_restricted:
        raise Error(f"Failed to correctly set gear '{gear_name}' restriction.")

    return output_permissions
