"""Command to export gear from Flywheel instance to another."""

import json
import logging
import tempfile
import typing as t
from pathlib import Path

from fw_gear.manifest import Manifest
from rich import print as rprint
from rich.console import Console
from rich.prompt import Prompt
from typer import Argument, Exit, Option

from fw_cli.containers import get_container_client

from ...exceptions import Error
from ...state import State, get_fw_client, get_state
from .common import parse_to_handle
from .utils import (
    attempt_to_correct_manifest,
    download_gear,
    gear_dict_from_names_or_ids,
    gear_table,
    split_gear_dict,
    upload_gear,
)

log = logging.getLogger(__name__)


def export_gear(
    names_or_ids: t.List[str] = Argument(
        ..., help="List of gear(s) to export by name (+ version) or id"
    ),
    dest: str = Option(
        "",
        "-d",
        "--destination",
        help=("Destination profile for sites to which to export the gear(s)."),
    ),
    all_versions: bool = Option(
        False,
        "-a",
        "--all-versions",
        help="Show all versions of gears specified with just a <name>",
    ),
    path: Path = Option(
        Path(tempfile.gettempdir(), "gear/download"),
        "-p",
        "--path",
        help="Directory to download gears into",
    ),
) -> None:
    """Export gear(s) from Flywheel to another site or local.

    Specify a gear or list of gears and an optional profile name for the
    destination site. The destination profile specified must match an existing
    logged in profile, and the user must also be logged into a source site.

    Examples:
        `fw-beta gear export 619542005c0c1baeeb68981a test-1 test-2:0.1.2`
            This will download
                * The gear with ID 619542005c0c1baeeb68981a
                * The latest version of `test-1`
                * Version 0.1.2 of `test-2`

        `fw-beta --profile my-site gear export -a test-1 -d my-other-site`

            This will present a list of `test-1` versions from the site matching
            profile `my-site` from which you can choose which version(s) to export
            to destination site with profile `my-other-site`.
    """
    state = get_state()
    export = True
    fw, profile = None, None
    if not dest:
        export = False
    else:
        for p in state.config.profiles:
            if p.name == dest:
                profile = p
        if not profile:
            raise Error(
                f"Could not find profile {dest}. Please login using `--profile {dest}`"
            )
        fw = get_fw_client(api_key=profile.api_key)
    gear_dict, idx_list, to_handle_export = get_and_render_gears(
        state, names_or_ids, all_versions
    )
    num_gears = len(idx_list)
    if not all(export_idx in gear_dict for export_idx in idx_list):
        msg = f"Invalid gear number {idx_list}"
        if to_handle_export:
            msg = f" Could not parse user input: {to_handle_export}"
        raise Error(msg)
    client = get_container_client()
    rprint(f"Exporting {num_gears} gears")
    for export_idx in idx_list:
        g = gear_dict.get(export_idx)
        assert g, f"Could not get gear at idx {export_idx}"
        gear_name = f"{g['name']}:{g['version']}"
        rprint(f"\n[underline][bold]{gear_name}[/bold]: Exporting[/underline]")
        gear_doc = download_gear(state.fw, client, g)
        attempt_to_correct_manifest(gear_doc["gear"])
        manifest = Manifest(gear_doc["gear"])
        if export:
            assert profile and fw, "Could not get destination profile."
            rprint(
                f"[bold]({gear_name})[/] Uploading to {profile.name} ({fw.base_url})"
            )
            # Log client into destination repo
            client.login(profile=profile)
            upload_gear(
                fw,
                client,
                t.cast(str, manifest.get_docker_image_name_tag()),
                gear_doc,
            )
            return

        gear = gear_doc["gear"]
        gear_unique = f"{gear['name']}_{gear['version']}"
        gear_dir = path / gear_unique
        gear_dir.mkdir(parents=True, exist_ok=True)
        with open(gear_dir / "manifest.json", "w", encoding="utf-8") as fp:
            json.dump(gear_doc["gear"], fp, indent=2)  # type: ignore
        gear_tag = f"{g['name']}:{g['version']}"
        rprint(f"[bold]{gear_tag}[/]: Gear downloaded to {gear_dir / 'manifest.json'}")


def get_and_render_gears(
    state: State, names_or_ids: t.List[str], all_versions: bool
) -> t.Tuple[dict, list, t.Optional[str]]:
    """Helper to get and render gear table if necessary."""
    gear_dict = gear_dict_from_names_or_ids(
        names_or_ids, state.fw, all_versions=all_versions, include_invalid=all_versions
    )
    if not gear_dict:
        rprint("No gears found. Exiting")
        raise Exit()
    idx_list, needs_rendering = split_gear_dict(gear_dict)
    to_handle = None
    if needs_rendering:
        console = Console()
        to_render = {k: v for k, v in gear_dict.items() if k in needs_rendering}
        console.print(gear_table(to_render))
        to_handle = Prompt.ask(
            "Multiple versions found, please select which one(s) to export.",
            console=console,
        )
        idx_list.extend(parse_to_handle(to_handle))
    return gear_dict, idx_list, to_handle
