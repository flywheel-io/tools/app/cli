"""Gear Install module."""

from fw_client import ClientError, ServerError
from rich import print as rprint
from typer import Option, confirm, prompt

from ...containers import ContainerException, DockerClient
from ...exceptions import Error
from ...exchange.interface import ExchangeInterace
from ...state import get_state
from ...styles import yellow
from .utils import GearCategory, get_gears_by_filter, upload_gear


def gear_install(
    name: str = Option(
        None,
        "-n",
        "--name",
        prompt=True,
        help="The name of the gear to install.",
    ),
    version: str = Option(
        "latest",
        "-v",
        "--version",
        prompt=True,
        help=("Version of the gear to install. Latest version pulled if unspecified."),
    ),
    category: GearCategory = Option(
        None,
        "-c",
        "--category",
        help=(
            "Gear category. Overwrites value found in manifest "
            ".custom.gear-builder.category\n"
            f"One of ({'|'.join(GearCategory)})"
        ),
    ),
    yes: bool = Option(
        False,
        "--yes",
        help=("Skip confirmation for gear installation."),
    ),
) -> None:
    # ruff: noqa: PLR0913, PLR0912 (too many args / branches)
    """Install a gear from the gear exchange."""
    state = get_state()

    filter_str = f"gear.name={name},gear.version={version}"

    if (version != "latest") and get_gears_by_filter(filter_str, state.fw):
        rprint(yellow(f"{name}:{version} is already installed. Exiting."))
        return

    exc = ExchangeInterace()

    manifest = exc.find_gear(name, version=version)

    if manifest:
        gear_name = manifest["gear"]["name"]
        gear_version = manifest["gear"]["version"]
        new_filter_str = f"gear.name={gear_name},gear.version={gear_version}"

        if (version == "latest") and get_gears_by_filter(new_filter_str, state.fw):
            rprint(yellow(f"Latest {gear_name} gear is already installed. Exiting."))
            return

        conf_str = f"Found {gear_name}:{gear_version} manifest. Upload to site?"
        if yes or confirm(conf_str, default=True):
            manifest["category"] = category or get_gear_category(manifest)

            try:
                private = manifest["gear"]["custom"]["flywheel"]["private"]
            except KeyError:
                private = False

            if private:
                rprint(
                    yellow(
                        "Manifest indicates gear is private. Attempting to pull docker image."
                    )
                )
                try:
                    client, image = pull_private_docker_img(manifest)
                except ContainerException as e:
                    if "docker login" in e.err:
                        raise Error(
                            "Failed to pull private gear image. Login to private docker registry with 'docker login' and retry."
                        )
                    raise Error(f"{e}\nFailed to pull private gear image.")
                try:
                    upload_gear(state.fw, client, image, manifest)
                    rprint(f"Upload of {gear_name}:{gear_version} succeeded.")
                except (ClientError, ServerError) as e:
                    raise Error(f"{e}\nUpload of {gear_name}:{gear_version} failed.")
            else:
                try:
                    state.fw.post(f"/api/gears/{gear_name}", json=manifest)
                    rprint(f"Upload of {gear_name}:{gear_version} succeeded.")
                except (ClientError, ServerError) as e:
                    raise Error(f"{e}\nUpload of {gear_name}:{gear_version} failed.")

        else:
            raise Error("Abandoned gear upload to site. Exiting.")
    else:
        raise Error("Failed to find manifest. Exiting.")


def pull_private_docker_img(manifest) -> tuple[DockerClient, str]:
    """Pull private docker image from manifest."""
    docker_client = DockerClient()
    image = manifest["gear"]["custom"]["gear-builder"]["image"]
    docker_client.pull(image, skip_login=True)
    return docker_client, image


def get_gear_category(manifest) -> str:
    """Get gear category from manifest."""
    try:
        gear_category = manifest["gear"]["custom"]["gear-builder"]["category"]
    except KeyError:
        gear_category = prompt(
            "Could not determine category from manifest, "
            f"please enter category (one of [{'|'.join(GearCategory)}])",
            type=GearCategory,
        )
    return gear_category
