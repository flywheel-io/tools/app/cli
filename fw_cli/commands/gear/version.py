"""Gear version module."""

import logging
import re
import typing as t
from pathlib import Path

import typer
from fw_gear.manifest import Manifest
from rich import print as rprint

from ...styles import green, red, yellow
from .utils import get_manifest

log = logging.getLogger(__name__)


def gear_version(
    version: t.Optional[str] = typer.Argument(
        "", help="New version for gear (optional).", show_default=False
    ),
    directory: Path = typer.Option(
        Path.cwd(), "-d", "--directory", help="Directory containing gear (optional)."
    ),
):
    """Show or update the version of a gear in the specified directory.

    Examples:
        `gear version`
            Print out current gear version

        `gear version 1.2.1 -d /tmp/gear/my-gear`
            Update the gear at `/tmp/gear/my-gear` to version 1.2.1
    """
    manifest_file = directory / "manifest.json"
    manifest = get_manifest(manifest_file)
    if not manifest:
        rprint(red(f"No gear manifest found in {Path.cwd()}. Exiting"))
        return
    if not version:
        rprint(f"Current version: {manifest.name}:{manifest.version}")
        return
    update_gear_version(manifest, version)


# https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string
semver_re_raw = (
    r"(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)"
    r"(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)"
    r"(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?"
    r"(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?"
)

# Allow wrapped algorithms to be whatever
fw_semver_re = re.compile(rf"^{semver_re_raw}_.*$")
semver_re = re.compile(semver_re_raw)


def update_gear_version(manifest: Manifest, version):
    """Bump gear version and image to requested version."""
    if not (fw_semver_re.match(version) or semver_re.match(version)):
        rprint(yellow(f"NOTE: Provided version {version} does not look like a semver"))
    image: str = manifest.manifest.get("custom.gear-builder.image", "")
    name = manifest.name
    existing_version = manifest.version
    if image:
        if ":" not in image:
            rprint(yellow("Warning: 'custom.gear-builder.image' field has no version"))
            name = image
        else:
            name, _ = image.split(":")
    manifest.manifest.update({"version": version})
    manifest.manifest.setdefault("custom", {}).setdefault("gear-builder", {}).update(
        {"image": f"{name}:{version}"}
    )
    rprint(green(f"Bumping gear version from {existing_version} to {version}"))
    manifest.to_json(manifest._path)
