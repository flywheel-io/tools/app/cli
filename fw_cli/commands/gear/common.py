"""Common gear option commands."""

import logging
import typing as t
from pathlib import Path

import typer
from fw_client import ClientError, ServerError
from rich import print as rprint
from rich.prompt import Prompt

from ...exceptions import Error
from ...state import get_state
from ...styles import green, red
from .utils import gear_table, gears_dict, get_manifest, parse_gear_name_or_id

log = logging.getLogger(__name__)


def exclusive(ctx: typer.Context, param: typer.CallbackParam, value: str):
    """Helper callback to enforce exlusive options."""
    # Add cli option to group if it was called with a value
    if value is not None and any(ctx.params.values()):
        raise typer.BadParameter(f"`{param.name}` can only be used by itself")
    return value


def gear_options(
    enable: t.Optional[str] = typer.Option(
        None,
        "--enable",
        help="Enable gears by ID or name and version",
        metavar="GEAR",
        callback=exclusive,
    ),
    disable: t.Optional[str] = typer.Option(
        None,
        "--disable",
        help="Enable gears by ID or name and version",
        metavar="GEAR",
        callback=exclusive,
    ),
    validate: t.Optional[Path] = typer.Option(
        None,
        "--validate",
        help="Validate gear manifest",
        metavar="PATH",
        callback=exclusive,
    ),
) -> None:
    """Gear common option commands."""
    # Don't need a manifest
    if enable or disable:
        name_or_id = enable or disable
        _change_availability(name_or_id, bool(enable))  # type: ignore
        return
    if validate:
        if validate.is_file():
            manifest_file = validate
        else:
            manifest_file = validate / "manifest.json"
        manifest = get_manifest(manifest_file)
        if not manifest:
            rprint(red(f"No gear manifest found in {validate}. Exiting"))
            return
        rprint(f"Validating manifest at {manifest_file}")
        manifest.validate()
        rprint(green(f"No issues found in {manifest_file}!"))


def _change_availability(name_or_id: str, enable) -> None:  # noqa: PLR0912
    """Handle enable/disable."""
    state = get_state()
    if not state.profile.is_admin:
        raise Error("Must be site admin to use this command")
    names, ids = parse_gear_name_or_id([name_or_id])
    try:
        gear_dict = gears_dict(names, ids, state.fw)
    except (ClientError, ServerError) as exc:
        raise Error("Could not get list of gears") from exc
    method = "enable" if enable else "disable"
    if len(gear_dict) == 0:
        rprint("No gears found, nothing to do! Exiting")
        return
    to_handle = None
    rprint(green("Found: "))
    rprint(gear_table(gear_dict))
    if len(gear_dict) > 1:
        to_handle = Prompt.ask(f"Enter the gear number(s) to {method}")
        if not to_handle:
            rprint("Nothing to do. Exiting")
            return
        idx_list = parse_to_handle(to_handle)
    else:
        idx_list = [0]
    changed = False
    for idx in idx_list:
        try:
            g = gear_dict[idx]
        except KeyError as exc:
            raise Error(f"Could not parse {to_handle}") from exc
        if g["enabled"] == enable:
            rprint(f"{g['name']}@{g['version']}, no availability change, skipping.")
            continue
        try:
            state.fw.post(f"/api/gears/{g['id']}/{method}")
            g["enabled"] = enable
            changed = True
        except (ClientError, ServerError) as e:
            raise Error(f"Could not {method} {g['name']}") from e
    if changed:
        rprint(green("Result: "))
        rprint(gear_table(gear_dict))


def parse_to_handle(to_handle: str) -> t.List[int]:
    """Parse input of gears to handle."""
    to_return: t.List[int] = []
    sections = [to_handle]
    try:
        if "," in to_handle:
            sections = to_handle.split(",")
        for section in sections:
            if "-" in section:
                from_, to = section.split("-")
                take_range = list(range(int(from_), int(to) + 1))
            else:
                take_range = [int(section)]
            for val in take_range:
                to_return.append(val)
    except (IndexError, ValueError) as exc:
        raise Error(f"Could not parse {to_handle}: {exc.args}") from exc
    return to_return
