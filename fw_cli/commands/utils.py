"""Utilities shared across multiple commands."""

import enum
import json
import re
import typing as t
from collections import defaultdict
from datetime import datetime
from difflib import get_close_matches
from functools import lru_cache
from http import HTTPStatus
from itertools import chain, islice
from pathlib import Path
from shutil import get_terminal_size

import fw_storage.config
import yaml
from fw_client import ClientError
from fw_utils import AttrDict, format_datetime, get_datetime
from pydantic import BaseModel, BeforeValidator, PlainSerializer
from rich import get_console
from rich import print as rprint
from rich.console import Console, ConsoleOptions, RenderableType, RenderResult
from rich.pager import SystemPager
from rich.progress_bar import ProgressBar
from rich.segment import Segment
from rich.style import Style
from rich.syntax import Syntax
from rich.text import Text
from typer import Option, echo_via_pager
from typer.models import OptionInfo

from ..exceptions import Error
from ..state import get_state
from ..styles import PULSE_SIZE, cyan, status, to_ansi

# TODO consolidate redundancy with ls
TYPES = ["group", "project", "subject", "session", "acquisition", "analysis", "file"]
NAME_KEY: t.Dict[str, str] = defaultdict(lambda: "label", group="_id", file="name")
ID_RE = re.compile(r"[0-9a-f]{24}")
FWPath = t.TypeVar("FWPath", str, t.List[str])
ModelType = t.TypeVar("ModelType", bound=BaseModel)
DateTime = t.Annotated[
    datetime,
    BeforeValidator(get_datetime),
    PlainSerializer(format_datetime),
]


class OutputFormat(enum.StrEnum):
    """Output formats."""

    text = "text"
    json = "json"
    yaml = "yaml"


output_arg: OutputFormat = Option(
    "text",
    "-o",
    "--output",
    metavar="FMT",
    show_default=False,
    help=f"Output format  [{cyan('text')}|json|yaml]",
)


def format_json(data: dict) -> t.Union[Text, Syntax]:
    """Format dict as json string with colors if terminal."""
    text = json.dumps(data, indent=4)
    console = get_console()
    if console.is_terminal:  # pragma: no cover
        return Syntax(text, "json", theme="ansi_dark")
    return Text(text)


def format_yaml(data: dict) -> t.Union[Text, Syntax]:
    """Format dict as yaml string with colors if terminal."""
    text = yaml.safe_dump(data)
    console = get_console()
    if console.is_terminal:  # pragma: no cover
        return Syntax(text, "yaml", theme="ansi_dark")
    return Text(text)


def load_model(cls: t.Type[ModelType], data: str) -> dict:
    """Return pydantic model dict loaded from an inline YAML string."""
    keys = cls.model_fields.keys()
    data = re.sub(r"^\s*\{?\s*", "{", data)
    data = re.sub(r"\s*\}?\s*$", "}", data)
    data = re.sub(rf"({'|'.join(keys)})\s*:\s*", r"\1: ", data)
    return yaml.safe_load(data)


def load_storage_override(type: str, data: str) -> dict:
    """Return storage config override dict from an inline YAML string."""
    cls = getattr(fw_storage.config, f"{type.upper()}ConfigOverride")
    data_dict = load_model(cls, data)
    data_dict["type"] = type
    return data_dict


yaml.SafeDumper.add_representer(AttrDict, yaml.SafeDumper.represent_dict)


class Node:
    """Node model."""

    def __init__(self, node: dict):
        """Init node from a dict as returned in responses."""
        self.id = node["_id"]
        self.type = node["node_type"]
        self.name = node[NAME_KEY[self.type]]

    @property
    def sort_key(self) -> t.Tuple[int, str]:  # pragma: no cover
        """Return node sort key."""
        # TODO natsort
        return TYPES.index(self.type), self.name.lower()

    def __lt__(self, other) -> bool:  # pragma: no cover
        """Return True if this node comes before the other when sorted."""
        return self.sort_key < other.sort_key


# TODO flexible/generic id <-> path lookup/conversion
# TODO lazy tree traversal w/ new/efficient xfer endpoints


def lookup_project(path_or_id: str):
    """Resolve project by path or id."""
    path = split_path(path_or_id)
    if len(path) == 1 and ID_RE.match(path[0]):
        try:
            response = get_state().fw.get(f"/api/projects/{path[0]}")
        except ClientError as exc:
            if exc.status_code in [HTTPStatus.NOT_FOUND, HTTPStatus.FORBIDDEN]:
                return None
            raise  # pragma: no cover
        response["node_type"] = "project"
        return Node(response)
    if len(path) == 2:  # noqa
        return lookup(path)
    raise ValueError("Invalid project path/id")


def lookup_project_id(proj: str) -> str:
    """Get project ID looking up path if necessary."""
    try:
        resp = lookup_project(proj)
    except ValueError as exc:
        raise Error(str(exc)) from exc
    if not resp:
        # TODO unify. this is repeated in commands/gears/permission
        raise Error(
            f"Could not find project '{proj}'.\n"
            "It may not exist or you don't have permissions to access it."
        )
    return resp.id


# TODO diskcache memoization
# TODO request timeout - maybe globally in state?
def lookup(path: FWPath) -> t.Optional[Node]:
    """Return nodes resolved for the given path."""
    path = split_path(path) if isinstance(path, str) else path  # type: ignore
    try:
        response = get_state().fw.post("/api/lookup", json={"path": path})
    except ClientError as exc:  # pragma: no cover
        if exc.status_code in [HTTPStatus.NOT_FOUND, HTTPStatus.FORBIDDEN]:
            return None
        raise
    return Node(response)


def resolve(path: t.List[str]) -> t.List[Node]:  # pragma: no cover
    """Return list of (child) nodes resolved for the given path."""
    # TODO handle 404 as []?
    response = get_state().fw.post("/api/resolve", json={"path": path})
    return sorted(Node(child) for child in response.children)


def split_path(path: str) -> t.List[str]:
    """Return fw-path splitted to a list of strings."""
    return path.replace("fw://", "").split("/")


def join_path(path: t.List[str]) -> str:  # pragma: no cover
    """Return fw-path joined from a list of strings."""
    return f"fw://{'/'.join(path)}"


def complete_path(fwpath: str, max_len: int = 6) -> t.List[str]:  # pragma: no cover
    """Return possible fw-path completions for the given fragment."""
    if "fw://".startswith(fwpath):
        fwpath = "fw://"
    path = split_path(fwpath)
    if len(path) > max_len:
        return []
    parents = path[:-1]
    hint = path[-1] if path else ""
    path_: t.List[str] = []
    for name in parents:
        matches = get_matches(name, resolve(path_))
        path_.append(matches[0].name)
    nodes = get_matches(hint, resolve(path_))
    paths = []
    for node in sorted(nodes):
        node_path = join_path(path_ + [node.name])
        if len(path) < max_len and node.type != "file":
            node_path += "/"
        paths.append(node_path)
    return paths


def get_matches(
    name: str, candidates: t.List[Node]
) -> t.List[Node]:  # pragma: no cover
    """Return nodes matching the given name fragment."""
    name = name.lower()
    candidate_map = {c.name.lower(): c for c in candidates}
    matches = []
    matches.extend([c for c in candidate_map if c.startswith(name)])
    for cutoff in (0.8, 0.6, 0.4):
        if matches:
            break
        matches.extend(get_close_matches(name, candidate_map, n=20, cutoff=cutoff))
    return [candidate_map[c] for c in matches]


def autopager(
    text: t.Union[str, t.Iterable[str], RenderableType],
) -> None:  # pragma: no cover
    """Automatically use pager based on the terminal height."""
    console = get_console()
    # use rich pager if text is a rich renderable object
    if hasattr(text, "__rich_console__"):
        with console.pager(pager=AutoPager(), styles=True):
            console.print(text)
        return
    if isinstance(text, str):
        text = map(lambda line: f"{line}\n", text.splitlines())
    text = t.cast(t.Iterable[str], text)
    term_height = get_terminal_size()[1]
    head = list(islice(text, term_height))
    if len(head) < term_height - 1:
        rprint("".join(head).rstrip())
    else:
        echo_via_pager(map(to_ansi, chain(head, text)))


def print_op_started(output: OutputFormat, op_type: str, op: dict):
    """Print the id of the started operation."""
    if output == OutputFormat.text:
        id_line = Text.assemble(
            (op_type.title(), "bold"),
            (f" {op['_id']}", "cyan"),
            f" {status('started')}",
        )
        autopager(id_line)
    elif output == OutputFormat.yaml:
        autopager(format_yaml(op))
    else:
        autopager(format_json(op))


def collapse_path(path: Path) -> str:
    """Collapse/condense path fish-like."""
    name = ""
    parts = path.parts
    home = Path.home().parts
    if home == path.parts[: len(home)]:
        name = "~/"
        parts = parts[len(home) :]
    for part in parts[:-1]:
        if part == "/":
            name += part
        else:
            name += part[0] + "/"
    name += path.parts[-1]
    return name


class AutoPager(SystemPager):
    """Use system pager as needed based on terminal height."""

    def show(self, content: str) -> None:
        """Use the same pager used by pydoc if content does not fit in terminal."""
        if content.count("\n") > get_terminal_size()[1] - 1:  # pragma: no cover
            super().show(content)
        else:
            rprint(content.strip("\n"))


class MultiColorProgressBar(ProgressBar):
    """Custom progress bar to display skipped/failed with different colors."""

    def __init__(self, *args, skipped=0, failed=0, review=0, **kwargs):
        """Initialize progress bar with extra skipped and failed values."""
        self.skipped = skipped
        self.failed = failed
        self.review = review
        kwargs.setdefault("pulse_style", "green")
        super().__init__(*args, **kwargs)

    @lru_cache(maxsize=16)
    def _get_pulse_segments(
        self,
        fore_style: Style,
        back_style: Style,
        color_system: str,
        no_color: bool,
        ascii: bool = False,
    ) -> t.List[Segment]:  # pragma: no cover
        """Get a list of segments to render a pulse animation."""
        bar = "-" if ascii else "━"
        segments: t.List[Segment] = []
        segments += [Segment(bar, fore_style)]
        segments += [Segment(bar, back_style)] * ((self.width or PULSE_SIZE) - 1)
        return segments

    def __rich_console__(
        self, console: Console, options: ConsoleOptions
    ) -> RenderResult:
        """Return progress bar's renderable segments."""
        width = min(self.width or options.max_width, options.max_width)
        is_ascii = options.legacy_windows or options.ascii_only

        should_pulse = self.pulse or self.total is None
        if should_pulse:  # pragma: no cover
            yield from self._render_pulse(console, width, ascii=is_ascii)
            return

        bar = "-" if is_ascii else "━"
        half_bar_left = " " if is_ascii else "╺"

        style = console.get_style(self.style)
        complete_style = console.get_style("green")
        skipped_style = console.get_style("yellow")
        review_style = console.get_style("cyan")
        failed_style = console.get_style("red")

        total = self.total or 0
        completed = min(total, max(0, self.completed))
        skipped = min(total, max(0, self.skipped))
        review = min(total, max(0, self.review))
        failed = min(total, max(0, self.failed))

        complete_halves = int(width * 2 * completed / total) if total else width * 2
        complete_bar_count = complete_halves // 2 + complete_halves % 2
        skipped_halves = int(width * 2 * skipped / total) if total else width * 2
        skipped_bar_count = skipped_halves // 2 + skipped_halves % 2
        review_halves = int(width * 2 * review / total) if total else width * 2
        review_bar_count = review_halves // 2 + review_halves % 2
        failed_halves = int(width * 2 * failed / total) if total else width * 2
        failed_bar_count = failed_halves // 2 + failed_halves % 2

        if complete_bar_count:
            yield Segment(bar * (complete_bar_count), complete_style)
        if skipped_bar_count:
            yield Segment(bar * (skipped_bar_count), skipped_style)
        if review_bar_count:
            yield Segment(bar * (review_bar_count), review_style)
        if failed_bar_count:
            yield Segment(bar * (failed_bar_count), failed_style)

        if console.no_color:  # pragma: no cover
            return

        remaining_bars = width - complete_bar_count
        if remaining_bars and console.color_system is not None:  # pragma: no cover
            if complete_bar_count:
                yield Segment(half_bar_left, style)
                remaining_bars -= 1
            if remaining_bars:
                yield Segment(bar * remaining_bars, style)


def resolve_typer_option_default(value):
    """Return the resolved typer option default unless set otherwise."""
    return value.default if isinstance(value, OptionInfo) else value
