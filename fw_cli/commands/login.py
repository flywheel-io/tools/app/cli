"""Login and logout commands."""

from fw_client import ValidationError
from rich import print as rprint
from typer import Context, Exit, Option

from .. import ENV_PREFIX
from ..exceptions import Error
from ..state import Profile, get_fw_client, get_state
from ..styles import cyan


def show_status(ctx: Context, status_option: bool) -> None:
    """Display the current login status and exit when using --status."""
    if not status_option or ctx.resilient_parsing:
        return
    state = get_state()
    if not state.has_profile:
        rprint("Not logged in")
    else:
        rprint(f"Logged in to {format_profile(state.profile)}")
    raise Exit()


def show_list(ctx: Context, list_option: bool) -> None:
    """Display the saved profiles and exit when using --list."""
    if not list_option or ctx.resilient_parsing:
        return
    state = get_state()
    if not state.config.profiles:
        rprint("No login profiles found")
    else:
        for profile in state.config.profiles:
            active = profile.name == state.profile_name
            prefix = "*" if active else " "
            rprint(f"{prefix} {format_profile(profile)}")
    raise Exit()


def login(
    status: bool = Option(
        False,
        "--status",
        is_eager=True,
        callback=show_status,
        show_default=False,
        help="Show the login status and exit",
    ),
    list_: bool = Option(
        False,
        "--list",
        is_eager=True,
        callback=show_list,
        show_default=False,
        help="List saved login profiles and exit",
    ),
    api_key: str = Option(
        None,
        "--api-key",
        hidden=True,
        prompt="Flywheel API key",
        hide_input=True,
        envvar=f"{ENV_PREFIX}_API_KEY",
        show_envvar=False,
        show_default=False,
    ),
):
    """Log in using a Flywheel API key.

    Examples:
    fw-beta login
    fw-beta login --status
    """
    state = get_state()
    # TODO TODO gracefully handle 400 500 errors
    try:
        fw = get_fw_client(api_key=api_key)
    except ValidationError:
        raise Error("Invalid API key format - expected <site>:<key>")
    auth_status = fw.get("/api/auth/status")
    profile = Profile(
        name=state.profile_name,
        api_key=api_key,
        id=auth_status["origin"]["id"],
        is_admin=auth_status["user_is_admin"],
        is_drone=auth_status["is_device"],
    )
    if not state.has_profile:
        state.config.profiles.append(profile)
        state.profile = profile
    elif state.profile != profile:
        state.profile.__dict__.update(profile.__dict__)
    state.save_config()
    rprint(f"Logged in to {format_profile(state.profile)}")


def logout(
    all_: bool = Option(
        False,
        "-a",
        "--all",
        show_default=False,
        help="Log out from all saved profiles",
    ),
):
    """Log out and remove profile credentials."""
    state = get_state()
    if all_ and state.config.profiles:
        state.config.profiles.clear()
        state.save_config()
        rprint("Logged out from all saved profiles")
    elif state.has_profile:
        state.config.profiles.remove(state.profile)
        state.save_config()
        rprint(f"Logged out from {format_profile(state.profile)}")
    else:
        rprint("Not logged in")


def format_profile(profile: Profile) -> str:
    """Format a profile as a colorized string."""
    site = profile.api_key.rsplit(":", maxsplit=1)[0]
    user = f"drone {profile.id}" if profile.is_drone else profile.id
    return f"{cyan(site)} as {cyan(user)} (profile: {profile.name})"
