"""Global state."""

import functools
import logging
import os
import typing as t
from contextlib import suppress
from datetime import datetime
from pathlib import Path

import yaml
from fw_client import FWClient
from fw_utils import Cached
from pydantic import BaseModel

from . import ENV_PREFIX, __version__
from .exceptions import Error

log = logging.getLogger(__name__)

LOGS_RETAINED = int(os.getenv(f"{ENV_PREFIX}_LOGS_RETAINED", "50"))
RETRY_BACKOFF_FACTOR = float(os.getenv(f"{ENV_PREFIX}_RETRY_BACKOFF_FACTOR", "0.1"))
RETRY_TOTAL = float(os.getenv(f"{ENV_PREFIX}_RETRY_TOTAL", "3"))
CONNECT_TIMEOUT = int(os.getenv(f"{ENV_PREFIX}_CONNECT_TIMEOUT", "10"))
READ_TIMEOUT = int(os.getenv(f"{ENV_PREFIX}_READ_TIMEOUT", "30"))
SSL_VERIFY = os.getenv(f"{ENV_PREFIX}_SSL_VERIFY", "")
CONCURRENT_FILE_UPLOADS = int(os.getenv(f"{ENV_PREFIX}_CONCURRENT_FILE_UPLOADS", "8"))
CONCURRENT_CHUNK_UPLOADS = int(os.getenv(f"{ENV_PREFIX}_CONCURRENT_CHUNK_UPLOADS", "4"))


@functools.lru_cache()
def get_state() -> "State":
    """Return the app state (cached/singleton)."""
    return State()


class Profile(BaseModel):
    """Profile model."""

    name: str
    api_key: str
    id: str
    is_admin: bool
    is_drone: bool


class Config(BaseModel):
    """Config model."""

    profiles: t.List[Profile] = []


def get_config(config_file: Path) -> Config:
    """Return the loaded configuration."""
    # TODO is_dir, perm, invalid/migrate
    if not config_file.exists():
        return Config()
    with open(config_file, "r", encoding="utf8") as file:
        return Config(**yaml.safe_load(file))


def get_profile(config: Config, profile_name: str) -> Profile:
    """Return the current profile."""
    for profile in config.profiles:
        if profile.name == profile_name:
            return profile
    # TODO cover, consider auto-prompting (after adding non-interactive)
    raise Error("Login required")  # pragma: no cover


def get_fw_client(api_key: str, **kwargs) -> FWClient:
    """Return a Core-API client for the given API key."""
    connect_timeout = kwargs.pop("connect_timeout", CONNECT_TIMEOUT)
    read_timeout = kwargs.pop("read_timeout", READ_TIMEOUT)
    ssl_verify = kwargs.pop("ssl_verify", SSL_VERIFY)
    kwargs.setdefault("verify", validate_ssl_verify(ssl_verify))
    return FWClient(
        api_key=api_key,
        # TODO remove when updating to newer client which has these set
        headers={"X-Accept-Feature": "Subject-Container"},
        client_name="fw-cli",
        client_version=__version__,
        retry_backoff=RETRY_BACKOFF_FACTOR,
        retry_total=RETRY_TOTAL,
        timeout=(connect_timeout, read_timeout),
        **kwargs,
    )


def get_fw_client_for_state(
    profile: Profile,
    connect_timeout: int = CONNECT_TIMEOUT,
    read_timeout: int = READ_TIMEOUT,
    ssl_verify: str = SSL_VERIFY,
) -> FWClient:
    """Return a Core-API client for the given API key - no kwargs for state."""
    return get_fw_client(
        profile.api_key,
        connect_timeout=connect_timeout,
        read_timeout=read_timeout,
        ssl_verify=ssl_verify,
    )


def validate_ssl_verify(ssl_verify: bool | str) -> bool | str:  # pragma: no cover
    """Return validated SSL verify parameter."""
    if isinstance(ssl_verify, bool):
        return ssl_verify
    if ssl_verify.lower() in ("yes", "true", ""):
        return True
    if ssl_verify.lower() in ("no", "false"):
        return False
    if Path(ssl_verify).exists():
        # NOTE requests handles CA bundle paths in verify
        # TODO replace w/ loaded SSL ctx for httpx where this is deprecated
        return ssl_verify
    raise ValueError(f"Invalid SSL verify parameter: {ssl_verify!r}")


class State:
    """CLI state."""

    debug = False
    profile_name = "default"
    container_client: str = "docker"
    connect_timeout: int = CONNECT_TIMEOUT
    read_timeout: int = READ_TIMEOUT
    ssl_verify: str = SSL_VERIFY

    config = Cached(get_config)
    profile = Cached(get_profile)
    fw = Cached(get_fw_client_for_state)

    def __init__(self) -> None:
        """Initialize state."""
        app_dir = os.getenv("FW_CLI_CONFIG_DIR", "~/.fw")
        self.app_dir = Path(app_dir).expanduser().resolve()
        self.config_file = self.app_dir / "config.yml"
        timestamp = datetime.now().strftime("%Y%m%dT%H%M%S")
        self.log_file = self.app_dir / f"logs/{timestamp}.{os.getpid()}.log"
        self.ssl_verify = validate_ssl_verify(self.ssl_verify)
        self.secure_config()
        self.truncate_logs()

    def __setattr__(self, name: str, value: t.Any) -> None:
        """Set an attribute value by name on the instance."""
        if name == "ssl_verify":
            value = validate_ssl_verify(value)
        super().__setattr__(name, value)

    def save_config(self) -> None:
        """Save the CLI config to disk."""
        self.app_dir.mkdir(parents=True, exist_ok=True)
        with open(self.config_file, "w", encoding="utf8") as file:
            yaml.safe_dump(self.config.model_dump(), file)
        self.secure_config()

    def secure_config(self) -> None:
        """Ensure secure config file permissions."""
        if self.app_dir.exists():
            self.app_dir.chmod(0o700)
        if self.config_file.exists():
            self.config_file.chmod(0o600)
        for log_file in self.app_dir.glob("logs/*"):
            with suppress(FileNotFoundError):
                log_file.chmod(0o600)

    def truncate_logs(self, keep: int = LOGS_RETAINED) -> None:
        """Remove old log files, only keeping the N most recent ones."""

        def sort_key(path: Path) -> float:
            with suppress(FileNotFoundError):
                return path.stat().st_ctime
            return 0.0  # pragma: no cover

        log_files = sorted(self.app_dir.glob("logs/*"), key=sort_key, reverse=True)
        for log_file in log_files[keep:]:
            log_file.unlink(missing_ok=True)

    @property
    def has_profile(self) -> bool:
        """Return True if there is an active profile."""
        return any(prof.name == self.profile_name for prof in self.config.profiles)
