"""Patch Click and Typer for customized help texts."""

import re
import subprocess
import webbrowser
from functools import partial
from typing import List, Optional

import rich
import typer
import typer.main
from click import Command, Context, HelpFormatter, Option, Parameter, formatting
from typer.completion import completion_init
from typer.core import TyperCommand, TyperGroup, _typer_format_options

from .styles import bold, cyan, to_ansi, underline

completion_init()

DOCS_URL = "https://flywheel-io.gitlab.io/tools/app/cli"


# TODO ditch typer and use argparse w/ custom help formatting for performance
class FWContext(Context):
    """Flywheel custom context to support command aliases."""

    def __init__(self, *args, alias_of: Optional[str] = None, **kwargs):
        """Init with custom ctx settings."""
        self.alias_of = alias_of
        super().__init__(*args, **kwargs)


Command.context_class = FWContext


class FWTyper(typer.Typer):
    """Customized Typer with ctx settings and helper methods."""

    fw_ctx = {"help_option_names": ["-h", "--help"], "obj": {}}

    def __init__(self, **kwargs):
        """Init with custom ctx settings."""
        ctx = kwargs.get("context_settings", {})
        kwargs["context_settings"] = {**self.fw_ctx, **ctx}
        kwargs.setdefault("no_args_is_help", True)
        super().__init__(**kwargs)
        self.cmd_names = []

    def add_command(self, cmd, alias_of: Optional[str] = None, **kwargs):
        """Add a command (typer/func) with custom ctx settings."""
        ctx = kwargs.get("context_settings", {})
        kwargs["context_settings"] = {**self.fw_ctx, **ctx}
        if alias_of:
            kwargs["context_settings"]["alias_of"] = alias_of
        if isinstance(cmd, typer.Typer):
            name = kwargs.setdefault("name", cmd.info.name)
            self.add_typer(cmd, cls=FWGroup, **kwargs)
        else:
            name = kwargs.setdefault("name", typer.main.get_command_name(cmd.__name__))
            self.command(cls=FWCommand, **kwargs)(cmd)

        assert name, f"Cannot get command name for {cmd}"
        self.cmd_names.append(name)


def open_docs(ctx: Context, _param: Parameter, docs_option: bool) -> None:
    """Callback to open docs link for a specific command."""
    if not docs_option:
        return
    cmd_parts = ctx.command_path.split(" ")
    alias_of = ctx.alias_of  # type: ignore
    if alias_of:  # pragma: no cover
        cmd_parts[-1] = alias_of
    url = f"{DOCS_URL}/{'/'.join(cmd_parts)}"
    rich.print(f"Opening docs in your browser: {url}")
    # kinda hack to hide browser logs in the terminal after cli exited
    popen, devnull = subprocess.Popen, subprocess.DEVNULL
    subprocess.Popen = partial(popen, stdout=devnull, stderr=devnull)  # type: ignore
    webbrowser.open(url)
    raise typer.Exit()


doc_opt = Option(
    ["--docs"],
    is_flag=True,
    is_eager=True,
    expose_value=False,
    callback=open_docs,
    help="Show HTML docs for command",
)


class CommandMixin(Command):
    """Mixin to add --docs option and alias support."""

    def __init__(self, **kwargs):
        """Disable typer rich markup mode."""
        super().__init__(**kwargs)
        self.rich_markup_mode = None

    def get_params(self, ctx: Context) -> List[Parameter]:
        """Add --docs option to commands of this class."""
        *params, help_opt = super().get_params(ctx)
        return [*params, doc_opt, help_opt]


class FWGroup(CommandMixin, TyperGroup):
    """Group with added --docs option and alias support."""

    def __init__(self, **kwargs):
        """Hide the command if an alias."""
        if kwargs.get("context_settings", {}).get("alias_of"):
            kwargs["hidden"] = True
        super().__init__(**kwargs)

    def get_command(self, ctx: Context, cmd_name: str) -> Optional[Command]:
        """Get command by alias if not found."""
        if cmd := self.commands.get(cmd_name):
            return cmd
        for cmd in self.commands.values():
            if cmd.context_settings.get("alias_of") == cmd_name:
                return cmd
        return None


class FWCommand(CommandMixin, TyperCommand):
    """Command with custom --docs option and alias support."""

    def __init__(self, **kwargs):
        """Hide the command if an alias."""
        if kwargs.get("context_settings", {}).get("alias_of"):
            kwargs["hidden"] = True
        super().__init__(**kwargs)


orig_get_group = typer.main.get_group
orig_get_help = Command.get_help
orig_write_dl = HelpFormatter.write_dl

header_re = re.compile(r"\n  ([^\n]+):\n")


def header_repl(m):  # pragma: no cover
    """Help text header."""
    return f"\n{bold(m.group(1).upper())}\n"


under_re = re.compile(r"(\W)_([^_]+)_(\W)")
under_repl = r"\1" + underline(r"\2") + r"  \3"


def get_group(typer_inst):
    """Return click group for typer instance with cmd_names list."""
    group = orig_get_group(typer_inst)
    group.cmd_names = typer_inst.cmd_names
    return group


def get_help(self, ctx):
    """Return the command help text after some customization."""
    help_text = orig_get_help(self, ctx)
    help_text = header_re.sub(header_repl, help_text)  # custom headers
    help_text = under_re.sub(under_repl, help_text)  # underlines
    help_text = help_text.replace("Usage:", f"{bold('USAGE')}\n ")  # usage header
    help_text = help_text.replace("required", cyan("required"))  # required arg/opt
    help_text = help_text.replace("[default: ", r"\[default: ")  # Escape default
    for regex, repl in ctx.obj.get("help_repl", {}).items():
        help_text = re.sub(regex, repl, help_text)  # pragma: no cover
    return to_ansi(f"{help_text}\n")  # end with an empty line


def list_commands(self, ctx):
    """Return the commands in the order they were added."""
    return getattr(self, "cmd_names", self.commands)


def typer_format_options(self, ctx, formatter):  # pragma: no cover
    """Output commands first and options second."""
    self.format_commands(ctx, formatter)
    _typer_format_options(self, ctx=ctx, formatter=formatter)


def write_heading(self, heading):
    """Write help section headers bold and capitalized."""
    self.write(f"{'':>{self.current_indent}}{bold(heading.upper())}\n")


def write_dl(self, rows, col_max=30, col_spacing=2):
    """Write definition lines (cmd/arg/opt) with the left col colorized."""
    rows = [(cyan(arg), text.rstrip(".")) for arg, text in rows]
    return orig_write_dl(self, rows, col_max, col_spacing)


def wrap_text(
    text: str,
    width: int = 78,
    initial_indent: str = "",
    subsequent_indent: str = "",
    preserve_paragraphs: bool = False,
):
    """Disable text wrapping but keep indentation to preserve docsting format."""
    # TODO add test to ensure every help text line's length fits 80 chars
    return initial_indent + re.sub(r"\n(\S)", rf"\n{subsequent_indent}\1", text)


Command.get_help = get_help  # type: ignore
HelpFormatter.write_heading = write_heading  # type: ignore
HelpFormatter.write_dl = write_dl  # type: ignore
formatting.wrap_text = wrap_text  # type: ignore
typer.main.get_group = get_group  # type: ignore
TyperGroup.format_options = typer_format_options  # type: ignore
TyperGroup.list_commands = list_commands  # type: ignore

rich.reconfigure(highlighter=None, soft_wrap=True)
