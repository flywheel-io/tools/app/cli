"""Generic wrapper around container clients."""

import json
import logging
import shlex
import shutil
import subprocess as sp
import textwrap
import typing as t
from abc import ABC, abstractmethod
from itertools import chain
from pathlib import Path

from .exceptions import Error
from .state import Profile, get_fw_client, get_state

log = logging.getLogger(__name__)


class ContainerException(RuntimeError):
    """Type for an exception when running a container command."""

    def __init__(self, *args, **kwargs):
        """Initialize ContainerException."""
        self.cmd_args = args[0]
        self.r_code = kwargs.pop("r_code", None)
        self.out = kwargs.pop("out", "") or ""
        self.err = kwargs.pop("err", "") or ""
        super().__init__(*args, **kwargs)

    def __str__(self):
        """String representation."""
        cmd_args = self.cmd_args
        if len(cmd_args) > 5:  # noqa
            cmd_args = cmd_args[:5]
            cmd_args.append("...")

        return (
            f"Exception while running command: {' '.join(cmd_args)}\n"
            f"Return code: {self.r_code}, details: {self.err}"
        )


def parse_repo_digest(in_: str) -> t.Tuple[t.List[str], str, str]:
    """Parse OCI RepoDigest.

    Args:
        in_: Input RepoDigest

    Returns:
        tuple:
            - domain components
            - image name
            - image digest
    """
    repo, _, digest = in_.partition("@")
    repo_parts = repo.split("/")
    image_name = repo_parts[-1]
    return repo_parts[:-1], image_name, digest


def parse_image_name(in_: str) -> t.Tuple[t.List[str], str, str]:
    """Parse OCI Image name.

    Args:
        in_: Input image name

    Returns:
        tuple:
            - domain components
            - image name
            - image tag
    """
    repo, _, tag = in_.partition(":")
    repo_parts = repo.split("/")
    image_name = repo_parts[-1]
    return repo_parts[:-1], image_name, tag


def get_container_client():
    """Get correct container client based on state."""
    state = get_state()
    c_client = state.container_client
    for subcls in ContainerClient.__subclasses__():
        if subcls.type_ == c_client:
            return subcls()
    raise NotImplementedError(f"Container Client: '{c_client}' not implemented")


class ContainerClient(ABC):  # pragma: no cover
    """Generic client supporting multiple frontends."""

    type_ = ""

    @abstractmethod
    def cmd(self) -> t.List[str]:
        """Get base command."""
        raise NotImplementedError()

    def build(  # noqa: PLR0913
        self, path: str, tag: str, pass_thru: t.List[str]
    ):
        """Build container image.

        Args:
            path: Path to image dir.
            tag: Tag to apply to image
            pass_thru: Pass through arguments
        """
        cmdline = [*self.cmd(), "build"]
        if self.type_ == "podman":
            cmdline.append("--format=docker")
        cmdline.extend(["-t", tag])
        if pass_thru:
            cmdline.extend([shlex.quote(str(a)) for a in pass_thru])
        cmdline.append(path)
        run(cmdline)

    def run(  # noqa: PLR0913
        self,
        image: str,
        entrypoint: t.Optional[str] = None,
        volumes: t.Optional[t.List] = None,
        env: t.Optional[t.Dict[str, str]] = None,
        capture: bool = False,
        pass_thru: t.Optional[t.List] = None,
    ) -> sp.CompletedProcess:
        """Run container image.

        Args:
            image: Image to run
            entrypoint: Override entrypoint
            volumes: List of volumes to attach `<host_path>:<dest_path>`
            env: Override environment
            capture: True to capture output.
            pass_thru: List of additional arguments to pass through

        Returns:
            Completed Process object or None
        """
        cmdline = [*self.cmd(), "run"]

        args = []
        if volumes:
            cmdline.extend(chain.from_iterable([["-v", v] for v in volumes]))
        # Split entrypoint into command, args
        if entrypoint:
            # If entrypoint is specified, pass entire command to `/bin/sh -c`
            args = ["-c", f"{entrypoint}"]
            cmdline.append("--entrypoint=/bin/sh")
        if pass_thru:
            cmdline.extend([shlex.quote(str(a)) for a in pass_thru])
        if env:
            cmdline.extend([f"-e {k}='{v}'" for k, v in env.items()])

        cmdline.append(image)
        cmdline.extend(args)
        res = run(cmdline, capture=capture)
        return res

    def inspect(self, image: str) -> t.Optional[dict]:
        """Inspect container image."""
        cmdline = [*self.cmd(), "inspect", image]
        try:
            proc = run(cmdline, capture=True)
        except ContainerException as exc:
            if "no such object" in exc.err.lower():
                return None
            raise
        # NOTE: Let these raise if not valid json is returned.
        out = proc.stdout.decode()
        im_list = json.loads(out)
        if len(im_list) == 0:
            return None
        if len(im_list) > 1:
            log.warning(f"Expected one image, found {len(im_list)}")
        return im_list[0]

    def tag(self, target: str, tag: str) -> t.Optional[dict]:
        """Apply a tag to a container image."""
        cmdline = [*self.cmd(), "tag", target, tag]
        run(cmdline, capture=True)
        return self.inspect(target)

    def pull(self, target: str, skip_login: bool = False):
        """Pull a container image from a remote repository."""
        if not skip_login:
            self.login()
        cmdline = [*self.cmd(), "pull", target]
        run(cmdline, capture=True)

    def push(self, target: str) -> t.Optional[str]:
        """Push an image to a remote repository.

        Args:
            target: Target image

        Returns:
            Image digest if found.
        """
        self.login()
        cmdline = [*self.cmd(), "push", target]
        run(cmdline)
        im = self.inspect(target)
        if not im:
            return ""
        t_domain, t_name, _ = parse_image_name(target)
        for it in im.get("RepoDigests", []):
            domain, name, digest = parse_repo_digest(it)
            if domain == t_domain and name == t_name:
                return digest
        return ""

    def login(self, profile: t.Optional[Profile] = None):
        """Log into a remote repository."""
        if profile:
            curr_profile = profile
        else:
            state = get_state()
            curr_profile = state.profile
        fw = get_fw_client(curr_profile.api_key)
        domain = str(fw.base_url)
        cmdline = [
            *self.cmd(),
            "login",
            "-u",
            curr_profile.id,
            "-p",
            curr_profile.api_key,
            domain,
        ]
        run(cmdline, capture=True)


class DockerClient(ContainerClient):
    """Docker container client."""

    type_ = "docker"

    def cmd(self) -> t.List[str]:
        """Get the root command."""
        debug = get_state().debug
        c = [self.type_]
        if debug:
            c.append("-D")
        return c


class PodmanClient(ContainerClient):
    """Client for podman container engine."""

    type_ = "podman"

    def cmd(self) -> t.List[str]:
        """Get base command."""
        debug = get_state().debug
        c = [self.type_]
        if debug:
            c.extend(["--log-level", "debug"])
        return c


def run(
    argv: t.List[t.Any],
    in_: t.Optional[bytes] = None,
    env: t.Optional[t.Dict[str, str]] = None,
    cwd: Path = Path.cwd(),
    capture: bool = False,
) -> sp.CompletedProcess:
    """Run a subprocess.

    Args:
        argv: List of arguments.
        in_: Input to be written to subprocess stdin.
        env: Environment for subprocess.
        cwd: Working directory for subprocess.
        capture: True to capture stdout and stderr.

    Raises:
        ContainerException: If subprocess returns non-zero exit code

    Returns:
        Completed Process object.
    """
    if shutil.which(argv[0]) is None:
        raise Error(f"Missing required command {argv[0]!r}")
    proc = sp.run(
        argv, input=in_, env=env, cwd=cwd, capture_output=capture, check=False
    )
    if proc.returncode != 0:
        out = proc.stdout.decode(errors="replace") if proc.stdout else None
        err = proc.stderr.decode(errors="replace") if proc.stderr else None
        width = shutil.get_terminal_size()[0]
        cmd = "\n".join(
            textwrap.wrap(
                " ".join(argv),
                width=width,
                break_long_words=False,
                break_on_hyphens=False,
                subsequent_indent="\t",
            )
        )
        log.debug(
            "Exception while running command: \n"
            + f"\n{cmd}\n\n"
            + f"Return Code: {proc.returncode}\n"
        )
        raise ContainerException(argv, r_code=proc.returncode, out=out, err=err)
    return proc
