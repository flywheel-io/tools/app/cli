"""Mkdocs plugin for FLywheel CLI."""

import base64
import multiprocessing
import re
import typing as t
from functools import partial
from pathlib import Path
from sys import platform

import mkdocs.plugins
import mkdocs.utils
import typer
from click import Command, Group
from fw_utils import inflate_dotdict
from jinja2 import FileSystemLoader, Template

DIR = Path("docs")


class FWCliPlugin(mkdocs.plugins.BasePlugin):
    """Mkdocs plugin to generate and template CLI docs."""

    def __init__(self, *args, **kwargs):
        """Initialize the plugin."""
        super().__init__(*args, **kwargs)
        self.usages = {}
        self.template_loader = FileSystemLoader(f"{DIR}/templates")

    def on_serve(self, server, config, builder):
        """Watch CLI source dir to enable livereload on code changes."""
        # TODO can remove this once new mkdocs released where it can be
        # configured from config file
        server.watch(f"{DIR.parent}/fw_cli", builder)
        return server

    def on_pre_build(self, config) -> None:
        """Generate base MD files and collect usage help texts."""
        if platform == "darwin":
            # Needed for Mac OS
            multiprocessing.set_start_method("fork")

        def target(manager):
            import rich

            import fw_cli.main

            # force colors
            rich.reconfigure(highlighter=None, soft_wrap=True, force_terminal=True)
            nav_dict = {}
            usages = {}
            for cmd in walk_commands(typer.main.get_command(fw_cli.main.fw)):
                # create base markdown file for command if not exists
                md_path = cmd["md_path"]
                md_path.parent.mkdir(exist_ok=True)
                short_cmd_name = cmd["command"]
                if cmd["command"] != "fw-beta":
                    short_cmd_name = cmd["command"].replace("fw-beta", "").strip()
                if not md_path.exists():
                    md_path.write_text(f"# `{short_cmd_name}`\n\n{{{{usage}}}}\n")
                # create templated markdowns
                usages[short_cmd_name] = get_usage(cmd["help"])
                # exclude from navigation if hidden
                if cmd["hidden"]:
                    continue
                nav_key = cmd["command"].replace(" ", ".")
                if cmd["type"] == "group":
                    nav_key = f"{nav_key}.{cmd['name']}"
                nav_dict[nav_key] = md_path.relative_to(DIR).as_posix()
            manager["nav_dict"], manager["usages"] = nav_dict, usages

        manager = multiprocessing.Manager()
        results = manager.dict()  # type: ignore
        process = multiprocessing.Process(target=target, args=[results])  # type: ignore
        process.start()
        process.join()
        config["nav"] = nav_dict_to_nav_list(inflate_dotdict(results["nav_dict"]))
        self.usages = results["usages"]

    def on_page_markdown(self, markdown, page, config, files) -> str:
        """Template the loaded markdown using Jinja."""
        template = Template(markdown)
        template.environment.loader = self.template_loader
        match = re.match("# `(?P<command>.*)`", markdown)
        cmd_name = match.group("command") if match else None
        page.meta.setdefault("title", cmd_name)
        usage = self.usages.get(cmd_name)
        base_url = mkdocs.utils.get_relative_url(".", page.url)
        asciinema_ = partial(asciinema, base_url=base_url)
        return template.render(usage=usage, asciinema=asciinema_)


def nav_dict_to_nav_list(nav_dict: dict):
    """Convert navigation dict to list for mkdocs."""
    nav = []
    for cmd, val in nav_dict.items():
        if isinstance(val, dict):
            nav.append({f"<pre>{cmd}</pre>": nav_dict_to_nav_list(val)})
        else:
            nav.append({f"<pre>{cmd}</pre>": val})
    return nav


def get_usage(help_text: str) -> str:
    """Get usage of command."""
    encoded = base64.b64encode(help_text.encode()).decode()
    usage = "## Usage\n\n"
    usage += f'<x-console src="{encoded}"></x-console>\n\n'
    return usage


def walk_commands(
    command: Command, ctx: typer.Context = None, prefix: str = "", hidden: bool = False
) -> t.Iterable[dict]:
    """Get all CLI groups/commands."""
    cmd_name = full_cmd_name = t.cast(str, command.name)
    if not ctx:
        ctx = typer.Context(command, info_name=cmd_name, obj={})
    else:
        ctx = typer.Context(command, parent=ctx, info_name=cmd_name, obj={})
    if prefix:
        full_cmd_name = f"{prefix} {cmd_name}"
    prefix = full_cmd_name if isinstance(command, Group) else prefix
    path = Path(DIR)
    path = path / Path(*prefix.split())
    base_name = "index" if isinstance(command, Group) else cmd_name
    md_path = path / f"{base_name}.md"

    yield {
        "type": "group" if isinstance(command, Group) else "command",
        "name": cmd_name,
        "command": full_cmd_name,
        "md_path": md_path,
        "hidden": hidden,
        "help": command.get_help(ctx),
    }

    if isinstance(command, Group):
        group: Group = command
        for sub_cmd_name in group.list_commands(ctx):
            sub_cmd = group.get_command(ctx, sub_cmd_name)
            assert sub_cmd
            if sub_cmd.context_settings.get("alias_of"):
                continue
            yield from walk_commands(
                sub_cmd, ctx=ctx, prefix=full_cmd_name, hidden=hidden or sub_cmd.hidden
            )


def asciinema(path, poster="npt:0:0.1", theme="fw", rows="25", base_url=""):
    """Return ascciinema HTML tag."""
    return (
        f'<asciinema-player src="{base_url}/assets/asciinema/{path}" '
        f'poster="{poster}" rows="{rows}" theme="{theme}" />'
    )
