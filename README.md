# Flywheel CLI (beta)

Flywheel command line interface for managing data and site configuration.

## Installation

```bash
curl https://storage.googleapis.com/flywheel-dist/fw-cli/stable/install.sh | sh
```

## Usage

```bash
# Most commands require authentication - log in to your site first:
fw-beta login

# Use `--help` to explore the available commands and options:
fw-beta --help
```

## Development

```bash
git clone git@gitlab.com:flywheel-io/tools/app/cli.git
cd cli
poetry install
poetry run fw-beta --help
```

## Building

```bash
docker build . -tflywheel/cli
```

## Linting

```bash
pre-commit run -a
```

## Docs

Docs are generated from markdown files under `docs/` using `mkdocs`
and [published to GitLab Pages](https://flywheel-io.gitlab.io/tools/app/cli).
Start the live-reload server to see the generated HTML while editing:

```bash
mkdocs serve
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
