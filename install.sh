#!/bin/sh
set -aeu
test -z "${TRACE:-}" || set -x

CLI_VERSION=${FW_CLI_VERSION:-latest}
PYTHON_BIN=${FW_CLI_PYTHON_BIN:-}
DIR=${FW_CLI_INSTALL_DIR:-"$HOME/.fw"}
DIST_DIR=$DIR/dist
INST_DIR=$DIR/inst
DIST=https://storage.googleapis.com/flywheel-dist/fw-cli
CMD=fw-beta
COLUMNS=80
USAGE="Usage:
  curl $DIST/$CLI_VERSION/install.sh | sh

Download and install Flywheel CLI beta to \$FW_CLI_INSTALL_DIR [default: ~/.fw]
The '$CMD' command is added to the \$PATH via the current user's shell profile.
"

main() {
    echo "$*" | grep -Eqvx "help|-h|--help" || { echo "$USAGE"; exit; }
    PLATFORM=$(uname -sm | tr "[:upper:]" "[:lower:]" | tr " " "-")
    echo "$PLATFORM" | grep -Eqx "linux-(x86_64|aarch64)|darwin-(x86_64|arm64)" \
        || die "Unsupported platform: $PLATFORM"
    MUSL=$(echo "$PLATFORM" | grep -vq linux || ldd /bin/ls | grep -o musl || :)
    if [ -n "$MUSL" ] && [ -z "$PYTHON_BIN" ]; then
        has python3 || die "Missing required dependency: python3 (musl libc)"
        PYTHON_BIN=$(which python3)
    fi
    DOWNLOAD_CMD=$(has wget && echo "wget -q" || echo "curl -fLSsO")
    SHASUM_CMD=$(has shasum && echo "shasum -a256" || echo sha256sum)
    SHASUM_CMD="$SHASUM_CMD $($SHASUM_CMD --help 2>&1 | grep -o -- --status || printf %s -s)"
    for DEP in grep sed tar "${DOWNLOAD_CMD%% *}" "${SHASUM_CMD%% *}"; do
        has "$DEP" || die "Missing required dependency: $DEP"
    done
    log "Installing Flywheel CLI $CLI_VERSION ($PLATFORM) to $DIR..."
    trap cleanup HUP INT EXIT TERM
    rm -rf "$INST_DIR"
    mkdir -p "$DIST_DIR" "$INST_DIR"
    cd "$DIR" || die "Cannot cd into $DIR"
    if [ -n "$PYTHON_BIN" ]; then
        download "fw-cli-$CLI_VERSION-wheel.tar.gz"
        test -d "$DIR/venv" || "$PYTHON_BIN" -m venv "$DIR/venv"
        PYTHON_BIN=$DIR/venv/bin/python
        $PYTHON_BIN -m pip install -qU pip setuptools wheel
        $PYTHON_BIN -m pip install -qr"$INST_DIR/requirements.txt"
        $PYTHON_BIN -m pip install -q "$INST_DIR"/fw_cli*.whl
    else
        download "fw-cli-$CLI_VERSION-$PLATFORM.tar.gz"
        PYTHON_VERSION=$(cat "$INST_DIR/PYTHON_VERSION")
        download "python-$PYTHON_VERSION-$PLATFORM.tar.gz"
    fi
    create_bin
    test "${FW_CLI_UPDATE_PROFILE:-true}" != true || update_profile
}

download() {(
    cd "$DIST_DIR" || die "Cannot cd into $DIST_DIR"
    ARCHIVE=$1
    CHECKSUM=$1.sha256
    if [ -s "$CHECKSUM" ] && [ -n "${CI_COMMIT_SHA:-}" ]; then
        log "Using cached $CHECKSUM"
    else
        log "Downloading $CHECKSUM"
        rm -f "$CHECKSUM"
        $DOWNLOAD_CMD "$DIST/$CHECKSUM"
    fi
    if checksum "$CHECKSUM"; then
        log "Using cached $ARCHIVE"
    else
        log "Downloading $ARCHIVE"
        rm -f "$ARCHIVE"
        $DOWNLOAD_CMD "$DIST/$ARCHIVE"
    fi
    log "Verifying $CHECKSUM"
    checksum "$CHECKSUM"
    log "Extracting $ARCHIVE"
    tar xzf "$DIST_DIR/$ARCHIVE" -C "$INST_DIR"
)}

create_bin() {
    log "Creating executable in $DIR/$CMD"
    touch "$CMD"
    chmod u+x "$CMD"
    sed "s|\$PYTHON_BIN|${PYTHON_BIN:-$DIR/run/bin/python3}|" >"$CMD" <<'EOF'
#!/bin/sh
test -z "${TRACE:-}" || set -x
DIR=$(cd "$(dirname "$0")" && pwd)
if [ -d "$DIR/inst" ]; then
    rm -rf "$DIR/run"
    mv "$DIR/inst" "$DIR/run"
fi
FW_CLI_INSTALL_DIR="$DIR" exec "$PYTHON_BIN" -Im fw_cli.main "$@"
EOF
}

update_profile() {
    if echo "${SHELL:-}" | grep -q "/bash"; then
        PROFILE="$HOME/.bash_profile"
    elif echo "${SHELL:-}" | grep -q "/zsh"; then
        PROFILE="${ZDOTDIR:-$HOME}/.zshenv"
    else
        log "Skipping profile update (unsupported shell)"
        return
    fi
    PATH_UPDATE="export PATH=\"$DIR:\$PATH\""
    if grep -qx "$PATH_UPDATE" "$PROFILE" 2>/dev/null; then
        log "Skipping profile update (already updated)"
        return
    fi
    log "Updating profile $PROFILE for $SHELL"
    echo "$PATH_UPDATE" >>"$PROFILE"
    log "Start a new shell or source your profile to use '$CMD'"
}

cleanup() {
    EXIT_CODE=$?
    if [ "$EXIT_CODE" = 0 ]; then
        log "Installation complete"
    else
        err "Installation failed"
        test -n "${TRACE:-}" || rm -rf "$INST_DIR"
    fi
    exit "$EXIT_CODE"
}

test -t 1 || INFO=INFO && INFO="\033[1;32mINFO\033[0m"
test -t 1 || ERROR=ERROR && ERROR="\033[1;31mERROR\033[0m"
has() { which "$1" >/dev/null 2>&1; }
log() { printf >&2 "$INFO %b\n" "$*"; }
err() { printf >&2 "$ERROR %b\n" "$*"; }
die() { err "$@"; exit 1; }
checksum() { $SHASUM_CMD -c "$@" >/dev/null 2>&1; }


main "$@"
