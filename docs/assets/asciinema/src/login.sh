#!/usr/bin/env bash

echo -e "\e[1;32m❯\e[0m fw-beta login"
sleep 0.1

expect <<EOF
spawn -noecho fw-beta login
expect "Flywheel API key:"
send -- "$1\r"
expect eof
EOF
sleep 0.1

echo -e "\e[1;32m❯\e[0m fw-beta login --status"
expect <<EOF
spawn -noecho fw-beta login --status
expect eof
EOF
sleep 0.1

echo -e "\e[1;32m❯\e[0m "
