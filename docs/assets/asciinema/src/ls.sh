#!/usr/bin/env bash

echo -e "\e[1;32m❯\e[0m fw-beta ls fw://flywheel"
sleep 0.1
fw-beta ls fw://flywheel
sleep 0.1
echo -e "\e[1;32m❯\e[0m fw-beta ls fw://flywheel/Neuroscience"
fw-beta ls fw://flywheel/Neuroscience
echo -e "\e[1;32m❯\e[0m "
