#!/usr/bin/env bash

echo -e "\e[1;32m❯\e[0m fw-beta gear upload"
sleep 0.2

fw-beta --profile tasks gear upload
sleep 0.2
echo -e "\e[1;32m❯\e[0m "
