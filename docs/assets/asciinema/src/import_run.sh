#!/usr/bin/env bash

echo -e "\e[1;32m❯\e[0m fw-beta import run -p fw://flywheel/Sample -s 64354d9543d8e158608b2654"
sleep 0.1
fw-beta import run -p fw://flywheel/Sample -s 64354d9543d8e158608b2654
echo -e "\e[1;32m❯\e[0m"
