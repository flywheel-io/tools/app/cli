#!/usr/bin/env bash

# WORKDIR should be gear directory with config all set up
WORKDIR="/Users/danielfonseka/Documents/random/fw-beta-tutorialgear"

echo -e "\e[1;32m❯\e[0m fw-beta gear run --prepare"
sleep 0.2
cd $WORKDIR || exit
fw-beta gear run --prepare
sleep 0.2
echo -e "\e[1;32m❯\e[0m "
