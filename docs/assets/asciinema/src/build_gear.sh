#!/usr/bin/env bash

echo -e "\e[1;32m❯\e[0m fw-beta -c docker gear build"
sleep 0.2

fw-beta -c docker gear build
sleep 0.2
echo -e "\e[1;32m❯\e[0m "
