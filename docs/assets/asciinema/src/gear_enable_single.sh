#!/usr/bin/env bash

echo -e "\e[1;32m❯\e[0m fw-beta gear --enable file-classifier:0.4.0"
sleep 0.2

expect <<EOF
spawn -noecho fw-beta gear --enable file-classifier:0.4.0
EOF
sleep 0.2

echo -e "\e[1;32m❯\e[0m "
