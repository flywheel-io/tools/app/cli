#!/usr/bin/env bash

ID=$(fw-beta import run -p fw://flywheel/Sample -s 64371a71210e3c393b916b72 --no-wait -ojson | jq -r ._id)
echo -e "\e[1;32m❯\e[0m fw-beta import get $ID --wait --tree"
sleep 0.1
fw-beta import get "$ID" --wait --tree
echo -e "\e[1;32m❯\e[0m"
