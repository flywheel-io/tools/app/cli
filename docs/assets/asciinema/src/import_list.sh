#!/usr/bin/env bash

echo -e "\e[1;32m❯\e[0m fw-beta import list"
sleep 0.1
fw-beta import list
echo -e "\e[1;32m❯\e[0m fw-beta import list --filter status=failed"
sleep 0.1
fw-beta import list --filter status=failed
echo -e "\e[1;32m❯\e[0m"
