#!/usr/bin/env bash

# shellcheck disable=SC2154
echo -e "\e[1;32m❯\e[0m fw-beta gear config -i api_key=\"$ga_key\""
sleep 0.2

fw-beta gear config -i api_key="$ga_key"
sleep 0.2
echo -e "\e[1;32m❯\e[0m "
