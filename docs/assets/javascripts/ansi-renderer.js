var ansi_up = new AnsiUp;

class XConsole extends HTMLElement {
    constructor() {
        super();
        var wrapper = document.createElement('pre');
        var console = document.createElement('code');
        var text = atob(this.getAttribute('src'));
        var html = ansi_up.ansi_to_html(text);
        console.innerHTML = html;
        console.className = "no-copy"
        this.appendChild(wrapper);
        wrapper.appendChild(console);
    }
}

customElements.define('x-console', XConsole);
