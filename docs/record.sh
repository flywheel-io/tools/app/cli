#!/usr/bin/env bash
test -z "$TRACE" || set -x
set -eu
USAGE="Usage:
$0 (-i) (-n) [COMMAND] [DST]

Record output of the given command.  If -i is specified, command should be
a TCL script to pass to expect for recording an interactive command.

-i  Record an interactive command
-n  Don't allocate a temporary directory
"

read -ra ORIG_TERMSIZE <<<"$(stty size)"
WORK_DIR=$(mktemp -d)

main() {
    echo "$*" | grep -Eqvw -- "-h|--help|help" || { echo "$USAGE"; exit; }
    trap cleanup INT EXIT
    stty rows 30;
    stty columns 100;
    tmp=1
    interactive=0
    while test $# != 0
    do
        case "$1" in
            -i) interactive=1;;
            -n) tmp=0;;
            *) break;;
        esac
        shift
    done
    if [[ $tmp == 1 ]]; then
        cd "$WORK_DIR" || exit
    fi
    if [[ $interactive == 1 ]]; then
        log "Recording output of an interactive command"
        record_interactive "$@"
    else
        log "Recording output"
        record "$@"
    fi
}

record() {
    SOURCE=$(readlink -f "$1")
    DEST=$(readlink -f "$2")
    log "src: $SOURCE, dest: $DEST"
    if ! [[ -x "$SOURCE" ]]; then
        die "Source $SOURCE is not executable"
    fi
    asciinema rec --overwrite -c "$SOURCE" "$DEST"
}

record_interactive() {
    # Partially based on: https://blog.waleedkhan.name/automating-terminal-demos/
    SOURCE=$(readlink -f "$2")
    DEST=$(readlink -f "$3")
    expect <<EOF
set send_human {0.1 0.2 3 0.05 1}
set CTRLC \003

proc expect_prompt {} {
    expect "\\\e\\[1;32m❯\\\e\\[0m "
}

proc run_command {cmd} {
    send -h " \$cmd"
    sleep 1
    send "\r"
}

proc send_text_to_interactive_process {key {addl_sleep 1}} {
    send -h "\$key"
    expect -timeout 1
    sleep \$addl_sleep
}

spawn asciinema rec --overwrite -c "PS1='\\\e\\[1;32m❯\\\e\\[0m ' bash --norc" "$DEST"
expect_prompt
source "$SOURCE"
EOF
}


cleanup() {
    stty rows "${ORIG_TERMSIZE[0]}" columns "${ORIG_TERMSIZE[1]}"
    rm -rf "$WORK_DIR"
    exit $?
}


# logging and formatting utilities
log() { printf "\e[32mINFO\e[0m %s\n" "$*" >&2; }
err() { printf "\e[31mERRO\e[0m %s\n" "$*" >&2; }
die() { err "$@"; exit 1; }
quiet() { "$@" &>/dev/null; }


main "$@"
