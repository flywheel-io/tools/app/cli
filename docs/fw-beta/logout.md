# `logout`

Run `fw-beta logout` to remove your login credentials stored in `~/.fw/config.yml`:

{{asciinema("logout.rec", rows="8")}}

{{usage}}
