# `fw-beta`
<!-- markdownlint-disable code-block-style -->

Flywheel CLI (beta) is a Command Line Interface for managing your data and
site configuration within the
[Flywheel Biomedical Research Data Platform](https://flywheel.io/).

## Installing locally

The `stable` version can be installed on Linux, OSX and Windows using a bootstrap script.

### Linux and OSX

!!! note "Prerequisites"

    The install script requires `bash`, `curl`, `grep`, `tar`,
    and `shasum` (or `sha256sum`) in order to download, extract, and
    verify the CLI.

    **Note:** The CLI only works on Linux and OSX. Windows support is coming soon.

```bash
curl https://storage.googleapis.com/flywheel-dist/fw-cli/stable/install.sh | bash
```

By default, `fw-beta` will be extracted under `~/.fw` with all of its dependencies.
The installation folder can be customized using the envvar `FW_CLI_INSTALL_DIR`.

Shell profiles are automatically updated to include `fw-beta` on the `PATH` for
bash and zsh using `~/.bash_profile` and `~/.zshenv` respectively.

{{asciinema("install.rec", rows="23")}}

### Windows

```powershell
irm https://storage.googleapis.com/flywheel-dist/fw-cli/stable/install.ps1 | iex
```

### Other versions

While `stable` is recommended since it always points to the most recent CLI tag,
other versions are also available if needed:

- `latest` - the bleeding-edge build from the tip of the `main` branch
- `<branch>` - branch-specific builds for feature testing
- `<tag>` - tag builds for explicitly using a specific version

### Tab Completion

Tab completion is supported by the CLI. This can be enabled for `bash` and `zsh`
with the following commands:

```bash
fw-beta --completion >/etc/bash_completion.d/fw-beta
source /etc/bash_completion.d/fw-beta
```

Alteratively, the following commands can also be used to enable tab completion:

```bash
# for bash
fw-beta --completion | tee -a ~/.bashrc
source ~/.bashrc

# for zsh
fw-beta --completion | tee -a ~/.zshrc
source ~/.zshrc
```

## Running in Docker

Alternatively the CLI can be run within the
[`flywheel/cli`](https://hub.docker.com/repository/docker/flywheel/cli) Docker
image:

```bash
docker run --rm -itv$HOME:$HOME -w$HOME \
    -eFW_UID=$(id -u) -eFW_GID=$(id -g) \
    -hfw-beta flywheel/cli \
    fw-beta --help
```

## Environment variables

The Flywheel CLI (beta) can configured via environment variables:

- `FW_CLI_API_KEY` - set an API key directly instead of using `login`
- `FW_CLI_CONNECT_TIMEOUT` - override the default HTTP connection timeout (10s)
- `FW_CLI_READ_TIMEOUT` - override the default HTTP read timeout (30s)
- `FW_CLI_SSL_VERIFY` - use custom CA cert path or disable SSL verification with `no`

## Version skew

Establishing a version skew and making the CLI backward- and forward compatible for
a limited number of Flywheel release versions is planned, but we aren't there yet.

While in beta, if you run into any issues please use the CLI version that was released
in tandem with a specific Flywheel release as listed below:

| Flywheel   | fw-beta   |
|------------|-----------|
| `20.1.x`   | `0.22.x`  |
| `20.0.x`   | `0.21.x`  |
| `19.5.x`   | `0.20.x`  |
| `19.4.x`   | `0.19.x`  |

{{usage}}
