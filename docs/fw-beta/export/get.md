# `export get`

Show export job status and details.

{{usage}}

By default `export get` shows the current state of an export job then exits.
Use `--wait` to monitor the export job status and progress until it finishes.
The command returns with status code 1 if the export job failed.

## Output format

Use `--output=json` to format the output as JSON for e.g. scripting.

## Export report

Use `--report=csv` to print a detailed CSV report for a finished export job.
