# `export schedule create`

Export data periodically or in the future at a specific time.
In addition to the scheduling options, it accepts the same
export settings as the [`fw-beta export run`](../run.md) command.

{{usage}}

## Scheduling options

| Option    | Description                                         |
|-----------|-----------------------------------------------------|
| `--cron`  | Interval at which the export will take place        |
| `--start` | Start time to run once (or enable periodic cron at) |
| `--end`   | End time to stop periodic cron runs                 |

The `--start` option in itself can be used to schedule an export
to run once at a specific time.

```bash
fw-beta export schedule create ... --start "2023-12-31T00:00:00+00:00"
```

The `--start` option along with the `--cron` option can be used to
enable periodic cron at a specific time. For example run an export
operation every Sunday at 00:00 UTC after the `START` time.

```bash
fw-beta export schedule create ... --cron "0 0 * * 0" --start START
```

The `--end` option along with the `--cron` option can be used to
disable periodic cron at a specific time. For example run an export
operation every Sunday at 00:00 UTC until the `END` time.

```bash
fw-beta export schedule create ... --cron "0 0 * * 0" --end END
```

Note that the `--cron` option always assumes UTC but the `--start`/`--end`
options are timezone aware. See <https://devhints.io/cron> for more info.

| Example                               | Meaning                                |
|---------------------------------------|----------------------------------------|
| `--cron "0 0 * * 0"`                  | at 00:00 on every Sunday UTC           |
| `--start "2023-12-31 00:00"`          | `2023-12-31T00:00:00+<LOCAL TIMEZONE>` |
| `--start "2023-12-31T00:00:00+00:00"` | `2023-12-31T00:00:00+00:00`            |
