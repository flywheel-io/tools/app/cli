# `gear upload`

Upload a gear to Flywheel

{{asciinema("upload_gear.rec", rows="20")}}

{{usage}}

## Categories

Flywheel gears are assigned a category, which can either be a `utility` or `analysis`
gear

__Analysis gears__ (`analysis` category) run in special analysis containers which are
immutable once created.  They are used to run some sort of algorithm on the inputs.

__Utility gears__ run on the container from which the inputs are taken and perform some
sort of utility function.  Utility gears can be split into the following categories:

* `utility`: General utility gear
* `converter`: A gear which outputs some conversion of the input(s)
* `classifier`: Applies some classification to the input data
* `qa`: Applies some quality measure to the input.
