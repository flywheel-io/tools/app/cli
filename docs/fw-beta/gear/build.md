# `gear build`

Gear build is a wrapper around either `docker build` or `podman build` that uses
the gear name/version from the manifest to tag the built image.

`gear build` will additionally populate the `environment` key in the manifest with the
environment from the built gear image. See [Gear environment](#gear-environment)

{{asciinema("build_gear.rec", rows="20")}}

{{usage}}

## Pass-through Arguments

All arguments and options that can be passed to a `docker build` or `podman build`
command can also be passed to the `gear build` command as pass-through arguments.
Pass-through arguments should be prepended by `--` (see example below). These
arguments will NOT be validated by the Flywheel CLI. It is the responsibility of
the user to verify the correctness and safety of any pass-through arguments.

Example: Build a gear without caching (`--no-cache`) and while attempting to pull the
newest image version (`--pull`).

`gear build --update-env $EXAMPLE_GEAR_DIRECTORY -- --pull --no-cache`

Note the lone double dash `--` that separates `gear build` arguments/options from
pass-through arguments.

The only pass-through argument with custom behavior is the `--tag / -t` option.
If specified, the built image will be tagged with this value. Otherwise, the image
will be automatically tagged with the gear name and version.

<!-- markdownlint-disable MD046 -->
!!! danger "Issues With Custom Tags"

    Supplying a custom tag (via `--tag / -t`) is NOT recommended as it may break
    other CLI commands (e.g., `gear run`).
<!-- markdownlint-enable MD046 -->

## Gear environment

In Flywheel, the environment variables specified in the Dockerfile are _overridden_ by
the `environment` dictionary in the manifest.  This can be counterintuitive since it
means any environmental variable you need access to during the gear run needs to be in
that dictionary in the manifest.

In order to avoid confusion, `gear build` by default populates that dictionary once the
image has finished building.

You can opt out of this by passing `--no-update-env`, and you can make this perform as a
dry run by passing `--env-dry-run` (prints out the environment it would populate but
doesn't actually populate the field in the manifest)

### Customizing ignored environment variables

By default, `gear update-env` ignores the variables `TERM`, `HOSTNAME`, `LSCOLORS`, and
`HOME` as these are rarely useful in the gear.

You can customize the ignored variables by using the `--env-ignore` option. This option
takes a variable name, an optional `=`, and then a value that can be either `yes` or
`no` (case-insensitive).  It can be used multiple times and will by default add
variables to the ignore list (`--ignore <var>`) but can also remove variables from the
ignore list (`--ignore HOME=no`)

## Notes

### Docker vs. Podman

Docker and Podman are both OCI (open container initiative) clients that
build/run/modify containers and container images.  The biggest difference being
security configuration and defaults:

* Docker requires `root` to run, whereas podman can run as a user.
* Docker uses `docker.io` (dockerhub) as the default registry so any shortnames
  (i.e. `python:latest`, `ubuntu:18.04`) will by default be prefixed with the
  `docker.io` (`docker.io/python:latest`, `docker.io/ubuntu:18.04`).  Podman has
  no defaults, so all registries will have to be configured in the
  [registries.conf](https://github.com/containers/image/blob/main/docs/containers-registries.conf.5.md)
  file.

### The --pull option

In previous versions of this CLI, the `--pull` option for `docker/podman build`
was enabled implicitly. This is no longer the case. To attempt to pull a newer version
of the image during `build`, pass `--pull` as a pass-through argument.
