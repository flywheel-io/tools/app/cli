# `gear permission`

{{usage}}

## Gear Permissions Management

Gear usage can be restricted to certain projects and users on a Flywheel site.
By default, gears have no permissions set and are accessible to all users and projects.

### Modifying Gear Permissions

You can modify gear permissions using the CLI in two ways:

1. **Adding Permissions**: Grant access to a user or project.
2. **Deleting Permissions**: Revoke access from a user or project.

### Adding Permissions

To add permissions to a gear, use the `-a` or `--add` flag:

```sh
fw-beta gear permission -a [OTHER_OPTIONS] GEAR_NAME
```

### Deleting Permissions

To delete permissions from a gear, use the `-d` or `--delete` flag:

```sh
fw-beta gear permission -d [OTHER_OPTIONS] GEAR_NAME
```

### Specifying User or Project

After specifying whether you are adding or deleting permissions, indicate whether
the permissions are for a user or a project:

- **User**: Use the `-u` or `--user` flag followed by the user's email address.
- **Project**: Use the `-p` or `--project` flag followed by the Flywheel path
(`fw://GROUP_NAME/PROJECT_NAME`) or the project ID.

### Example Commands

- **Add User Permission**:

  ```sh
  fw-beta gear permission -a -u user@example.com example_gear
  ```

- **Delete Project Permission**:

  ```sh
  fw-beta gear permission -d -p fw://group/project GEAR_NAME
  ```

### Resetting Permissions

To reset the permissions on a gear and return to the default state where all users
and projects can access the gear, use the `-r` or `--reset` flag:

```sh
fw-beta gear permission -r GEAR_NAME
```

This command will remove all specific permissions, making the gear accessible to
everyone again.

### Permissions Model

Gear permissions in Flywheel operate as a union of project and user permissions lists:

User Permissions: If a user is listed in the gear's user permissions, they can access
the gear in any project, regardless of whether the project is listed in the gear's
project permissions.

Project Permissions: If a project is listed in the gear's project permissions,
all users can access the gear within that project, regardless of whether the users
are listed in the gear's user permissions.

By default, a gear is accessible to all users and projects. However, when you add
a user to the permissions list, all other users are automatically excluded. The same
applies to project permissions. Be cautious to avoid unintentionally removing access
for other users or projects when modifying permissions.
