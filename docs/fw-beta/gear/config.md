# `gear config`

Running a Flywheel gear requires a `config.json` file that tells the gear where the
inputs are located, and what the config values are.

When running a gear on Flywheel, this file is generated automatically.  In order to run
it locally, you will need to generate it.  `gear config` is provided to make that
process simpler.

{{asciinema("add_api_key.rec", rows="8")}}

{{usage}}

## Config option types

The manifest in the current directory is used to apply types to
passed in config options, meaning you can set config options that
have various types, such as:

* boolean   `gear config -c my_bool=true`
  * Valid options: `True`, `true`, `T`, `t`, `False`, `false`, `F`, `f`
* string    `gear config -c my_str='my long string'`
* numbers   `gear config -c my_int=2` OR `gear config -c my_float=2.45`
* arrays    `gear config -c my_array=1,2,3`

> NOTE arrays are parsed by splitting on commas

## File Inputs

When you add a file input with `gear config -i <file_input>=<path/to/file>`, the `gear
config` command populates a file in your current directory called `.input_map.json` that
stores a dictionary mapping the location of the file on your computer to where it should
be mounted to the gear.  When you use `gear run --prepare`, this `.input_map.json` file
is used to populate the gear run directory.

Local filepaths and Flywheel filepaths are both valid. A Flywheel filepath is of the
form `fw://<group>/<project>/<subject>/<session>/<acquisition>/<file>`. When a
Flywheel filepath is supplied, the file will be downloaded and its metadata will
be populated in the `config.json`.
