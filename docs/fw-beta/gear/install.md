# `gear install`

Install a gear from the [Flywheel Exchange][exch] to a Flywheel site.

[exch]: https://docs.flywheel.io/Developer_Guides/dev_gear_exchange/

{{usage}}

The gear will be installed on the Flywheel site linked to the current API
key used for CLI login.

## Gear selection

Gear installation requires a user specified `name` argument that is matched against the
"name" field of every manifest on the exchange to identify a match.

The `version` argument for this command is optional. If not specified, the latest
version of the gear will be found and installed. However, if specified, the argument
will be matched against the "version" field of every manifest on the exchange
to find a match. Note that the latest version of the gear is defined as the version
most recently published to the exchange.

If a gear is found on the exchange matching the user specified `name` and/or `version`,
the user will be asked to confirm the upload. On confirmation, the gear will be
uploaded to the Flywheel site associated with the user's API key.

If no gear is found on the exchange that matches the user specified `name` and/or
`version`, a message will be printed to indicate no match and the cli will exit.

## Private gears

By default, gears listed on the Flywheel Gear Exchange will have publicly available
Docker images. These images are hosted in the Google Container Registry with a url
avaible under the "exchange.rootfs-url" key of the gear's manifest. However, it is
not required that Docker images be public. For private gears, the "exchange.rootfs-url"
key in the manifest is set to point to a private Docker image. In addition,
a new key, "custom.flywheel.private", must be added to the exchange and set to `True`.
If this private key is set to `True` and the "exchange.rootfs-url" points to a Docker
repository for which the user is not currently logged into in the shell, the CLI will
error out and direct the user to log in to that Docker registry in their current shell.
