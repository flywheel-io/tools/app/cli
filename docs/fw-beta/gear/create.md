# `gear create`

Create a new gear from a template

{{asciinema("create_gear.rec", rows="30")}}

{{usage}}

## Gear creation options

### Gear Name vs. Label

The gear _name_ is a machine readable name for the gear, may only contain lowercase
letters, numbers, and hyphens.

The gear _label_ is a human readable name for the gear, and what will show up in the
Flywheel UI, this can contain Letters, numbers, hyphens and spaces

### Gear Templates

There is currently support for three different gear templates:

#### Basic

The basic template provides a barebones template for building a gear with simply a
manifest, Dockerfile, a requirements.txt and a python entrypoint.

#### Flywheel Poetry

A much more involved template that includes a number of Flywheel best practices for gear
development, contains

* Package organization
  * Main entrypoint
  * Package split into `main.py` for main logic, and `parser.py` to parse the
      `GearContext`
  * Tests directory for testing
  * Docs for documentation
* Poetry for dependency management
* CI: `pre-commit` and `.gitlab-ci.yaml` (see `CONTRIBUTING.md` for details)

### Base Images

#### Base python

Contain only python and pip, one of:

* `python:3.8-buster`
* `python:3.7-buster`

#### Flywheel maintained python images

* `flywheel/python:main`
  * Contains `poetry`, and other tools such as `vim`, `jq`, `yq`,
    `curl`, `git`.
* `flywheel/python-gdcm:main`
  * Extends `fw_python` but also contains GDCM (C++ dicom
    library) python library.

#### Linux based images

Base linux images.

* `ubuntu:latest`
* `neurodebian:latest`
