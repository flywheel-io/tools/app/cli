# `gear run`

Run Flywheel gear locally

{{asciinema("run_gear.rec", rows="30")}}

{{usage}}

## Gear info

Flywheel gears run with the following directory structure.

```bash
.
├── config.json             # Config file
├── manifest.json           # Gear manifest
├── input                   # Input directory
│   ├── ...
│   └── input1              # Input1
│       └── input1.nii.gz   # File for Input1
├── output                  # Output directory
└── work                    # Working directory
```

Therefore to mimic a flywheel gear structure, this should be
present locally and mounted into the container that runs the gear.

## Using with `job pull`

`job pull` pulls a job for a gear that has _already_ run on Flywheel, therefore every
component of the above gear structure will already be present. You can then run `gear run`
directly from that directory to run the gear locally. Be sure that necessary keys are
configured, as these are not pulled from Flywheel.

## Using with `gear config`

You can also run a gear that has not been previously run on Flywheel, such as a gear
that you are developing locally. To set up the proper gear directory above, use the
following commands:

Since the gear hasn't been run yet, after building the config, you first run `gear run
--prepare` which create the above directory structure. Subsequently you can run `gear
run` by passing in the gear run directory.

## Pass-through Arguments

All arguments and options that can be passed to a `docker run` or `podman run` command
can also be passed to the `gear run` command as pass-through arguments. Pass-through
arguments should be prepended by `--` (see example below). These arguments will NOT
be validated by the Flywheel CLI. It is the responsibility of the user to verify the
correctness and safety of any pass-through arguments.

Example: Run a gear with GPU support

`gear run $EXAMPLE_GEAR_DIRECTORY -- --gpus all`

Note the lone double dash `--` that separates `gear run` arguments from pass-through
arguments.

Example: Inject additional environment variables on gear run

`gear run $EXAMPLE_GEAR_DIRECTORY -- -e MYVAL=EXAMPLE_ENV_VAR`
