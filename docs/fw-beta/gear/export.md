# `gear export`

Sometimes it's useful to export a gear from one Flywheel site to another without having
to rebuild the gear from a manifest, run-script, and Dockerfile.

This command allows you to either export a gear from one Flywheel site to another, or
download a gear as is to your local machine.

{{usage}}

## Gear selection

You may specify gears by a single value, or a space separated list of any combination of
`<id>` (gear id), `<name>` (gear name), or `<name>:<version>` (gear name and a specific
version).

For each value in the list, if it's an `<id>` or a `<name>:<version>` only that gear
will be exported or downloaded

If only a `<name>` is specified, by default the most recent ENABLED version of that gear
will be exported. If you want to export a different version pass in the flag
`--all-versions` and a list of gears will be presented from which you can choose which
to download.

## Profiles

You must be logged into both the source and destination sites. You can specify source site
similarly to other commands by passing `--profile` before the `gear export` subcommand, and
you can specify destination site with the `-d` option, ex.
`fw-beta --profile <mysite> gear export -d <mysite2>`.

See more information on login profiles [here](../login.md#profiles).
