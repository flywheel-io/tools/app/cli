# `admin storage create`

Register a storage for imports and exports.

{{usage}}

## Reference files in place

To reference files in place when running imports, the source storage must
be created from an existing, read-only storage provider in Core-API with
the `--provider` option.

An example to create a read-only storage provider in Core-API:

```bash
curl -XPOST https://<DOMAIN>/api/site/providers \
    -H "Content-Type: application/json" \
    -H "Authorization: <API_KEY>" \
    -d @- << EOF
{
    "label": "ref-in-place",
    "provider_class": "storage",
    "provider_type": "aws",
    "creds": {"aws_access_key_id": "<ID>", "aws_secret_access_key": "<KEY>"},
    "config": {"region": "<REGION>", "bucket": "<BUCKET>"},
    "access_type": "read-only"
}
EOF
```

Check the Core-API docs for other storage types:
`https://<DOMAIN>/api/docs#/site/create_provider_api_site_providers_post`.

Once the storage provider is created, you can use the provider ID to create
the storage in xfer for imports:

```bash
fw-beta admin storage create --provider PROVIDER_ID
```

{{asciinema("admin_storage_create_provider.rec", rows="8")}}
