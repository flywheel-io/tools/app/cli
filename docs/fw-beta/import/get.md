# `import get`

Show import job status and details.

{{usage}}

By default `import get` shows the current state of an import job then exits.
Use `--wait` to monitor the import job status and progress until it finishes.
The command returns with status code 1 if the import job failed.

## Output format

Use `--output=json` to format the output as JSON for e.g. scripting.

## Import report

Use `--report=csv` to print a detailed CSV report for a finished import job.
