# `import list`

{{asciinema("import_list.rec", rows="10")}}

{{usage}}

## Filters

Imports can be filtered using the `--filter <field><operation><value>` option.
Multiple filters can be passed by repeating the option.

Supported **filter fields**:

- `created`
- `modified`
- `refs.project`
- `refs.storage`
- `refs.schedule`
- `origin.id`
- `label`
- `dry_run`
- `status`

Supported filter **operators**:

- `==`
- `!=`
- `<=`
- `>=`
- `<`
- `>`

## Sorting

Imports can be sorted using the  `--sort <field>[:<order>]` option.

Supported **sorting fields**:

- `created`
- `modified`
- `label`
- `origin.id`

Supported sorting **orders**:

- `asc`
- `desc`
