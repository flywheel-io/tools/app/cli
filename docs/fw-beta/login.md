# `login`

Most commands involving data access require you to be logged in to Flywheel. You
can generate an API key on the UI / Profile page. To login with the CLI, run
`fw-beta login` and enter the key when prompted - this will store the creds in
`~/.fw/config.yml` for subsequent calls:

{{asciinema("login.rec", rows="8")}}

To check whether you are logged in:

```bash
fw-beta login --status
```

{{usage}}

## Profiles

Logging into and switching between multiple Flywheel instances with the CLI is
possible via named profiles. When using *any* `fw-beta` command, including `login`,
you can specify a profile to use (instead of `default`) as a top-level option or
by setting `FW_CLI_PROFILE`:

```bash
fw-beta --profile my-profile login
# or
FW_CLI_PROFILE=my-profile \
fw-beta login
```

Note that logging in to additional profiles as above only stores the credentials.
The `--profile` option (or envvar) needs to be specified on every command run in
order to override the `default` profile.

Use the `--list` option to display all stored login profiles:

```bash
fw-beta login --list
```

<!-- markdownlint-disable MD046 -->
??? example "Profiles Example"

    Profiles in the Flywheel CLI allow you to easily switch between different Flywheel
    sites or perform actions as different users. This feature acts as a shortcut,
    enabling you to quickly change contexts without re-entering credentials.

    #### Setting Up Profiles

    If you have access to multiple Flywheel sites, such as a production site and a
    sandbox testing site, you can set up named profiles to manage them efficiently.

    1. **Create a Profile for the Production Site:**

    To create a profile named `prod` for the production site, use the following
    command and enter your credentials:

    ```sh
    fw-beta --profile prod login
    ```

    2. **Create a Profile for the Sandbox Site:**

    Similarly, to create a profile named `sbx` for the sandbox site, use the following
    command and enter your credentials:

    ```sh
    fw-beta --profile sbx login
    ```

    #### Using Profiles

    Once profiles are set up, you can specify which profile to use for any CLI command
    by prepending the command with the `--profile` option.

    - To list gears on the sandbox site:

    ```sh
    fw-beta --profile sbx gear list
    ```

    - To list gears on the production site:

    ```sh
    fw-beta --profile prod gear list
    ```

    #### Using the Default Profile

    Instead of creating two named profiles, you can use one named profile (e.g., `sbx`
    for the sandbox site) and use the default profile for the production site. The
    default profile does not require the `--profile` flag.

    - To set the default profile for the production site, simply log in without the
    `--profile` flag:

    ```sh
    fw-beta login
    ```

    Enter the credentials for your production site.

    By using profiles, you can easily switch between different Flywheel sites or users,
    making your workflow more efficient and organized.
<!-- markdownlint-enable MD046 -->

## Automation

To use `fw-beta` in scripts without prompting for the API key, simply set the
`FW_CLI_API_KEY` envvar before invoking `fw-beta login`:

```bash
FW_CLI_API_KEY=site.flywheel.io:4PIk3ywQZ9REx4mp13 \
fw-beta login
```

## Logging out

Check out the [`fw-beta logout`](./logout.md) command for removing stored creds.
Note that exiting the shell will not log you out.
