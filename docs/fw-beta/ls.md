# `ls`

List existing containers and files at the Flywheel hierarchy root or given path.

{{asciinema("ls.rec", rows="9")}}

{{usage}}
