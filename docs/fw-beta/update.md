# `update`

Update the CLI.

Use the `VERSION` argument to update to a specific version:

- `latest` - the latest dev build (only available if installed via script)
- `stable` - the latest tagged version (default)
- `<tag>`  - previously tagged version

{{asciinema("update.rec", rows="16")}}

{{usage}}
