#!/usr/bin/env bash
set -Eeuo pipefail
PS4='+ ${BASH_SOURCE#$PWD\/}:${LINENO} ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
cg=$'\033[1;32m'
c0=$'\033[0m'
test -z "${DEBUG:-}" || set -x

main() {
    cd dist
    for file in $(find . -type f -name '*.tar.gz' | sed -E 's|^./||' | sort); do
        file_sha=$file.sha256
        xh -IF "$FLYWHEEL_DIST_URL/$file_sha" >"/tmp/$file_sha" 2>/dev/null || :
        if diff -q "$file_sha" "/tmp/$file_sha" >/dev/null; then
            log "Skipping $file (SHA matches)"
        else
            test -s "/tmp/$file_sha" && action=Overwriting || action=Uploading
            log "$action $FLYWHEEL_DIST_URL/$file"
            gs cp "$file*" "$FLYWHEEL_DIST"
            gs acl ch -u AllUsers:R "$FLYWHEEL_DIST/$file*"
        fi
    done
    refs=("$CI_COMMIT_REF_NAME")
    if [[ "$CI_COMMIT_REF_NAME" = "$CI_DEFAULT_BRANCH" ]]; then
        refs=(latest)  # main -> latest (assumes no ci reruns on older commits)
    elif [[ "$CI_COMMIT_REF_NAME" = "${CI_COMMIT_TAG:-}" ]]; then
        refs=("$CI_COMMIT_TAG")
        vadd_newer() { vlt "$CI_COMMIT_TAG" "$(vget "$1")" || refs+=("$1"); }
        vadd_newer "${CI_COMMIT_TAG%.*}"  # 1.2.3 -> 1.2 (if existing 1.2 <= 1.2.3)
        vadd_newer stable                 # 1.2.3 -> stable (if existing stable <= 1.2.3)
    fi
    log "Plublishing for refs ${refs[*]}"
    for ref in "${refs[@]}"; do
        log "Uploading $FLYWHEEL_DIST_URL/$ref/install.sh"
        gs cp install.sh "$FLYWHEEL_DIST/$ref/install.sh"
        gs acl ch -u AllUsers:R "$FLYWHEEL_DIST/$ref/install.sh"
        log "Uploading $FLYWHEEL_DIST_URL/$ref/install.ps1"
        gs cp install.ps1 "$FLYWHEEL_DIST/$ref/install.ps1"
        gs acl ch -u AllUsers:R "$FLYWHEEL_DIST/$ref/install.ps1"
    done
}

log() {( set +x; printf >&2 "${cg}INFO${c0} %b\n" "$*"; )}

vle() { [[ "$1" = "$(printf "%s\n" "$@"|sort -V|head -n1)" ]]; }
vlt() { [[ "$1" != "$2" ]] && vle "$@"; }
vget() { xh -IF "$FLYWHEEL_DIST_URL/$1/install.sh" \
    | sed -En 's/^CLI_VERSION="?([^"]+)"?$/\1/p' \
    || echo "0.0.0"
}

gs() { gsutil \
    -o "Credentials:gs_service_key_file=$GOOGLE_APPLICATION_CREDENTIALS" \
    -h "Cache-Control:no-cache,max-age=0" -m "$@"
}

main "$@"
