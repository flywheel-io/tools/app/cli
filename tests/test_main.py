"""Test the main / top level command."""

import re

from fw_cli.state import get_state


def test_help(cli):
    result = cli.run("--help")
    assert result.exit_code == 0
    output = result.stdout
    assert output.startswith("Flywheel CLI (beta)")
    assert "\nUSAGE\n  " in output
    assert "\nCOMMANDS\n  " in output
    assert "\nOPTIONS\n  " in output


def test_version(cli):
    result = cli.run("--version")
    assert result.exit_code == 0
    assert re.match(r"Flywheel CLI \(beta\) \d+\.\d+\.\d+.*\n", result.stdout)


def test_logging(cli, env, app_dir, mocker):
    env["FW_CLI_DEBUG"] = "false"
    log_dir = app_dir / "logs"
    assert not log_dir.exists()

    mocker.patch("fw_cli.state.os.getpid", return_value=1)
    cli.run("login --status")
    assert log_dir.exists()
    assert len(list(log_dir.iterdir())) == 1

    mocker.patch("fw_cli.state.os.getpid", return_value=2)
    get_state.cache_clear()
    cli.run("login --status")
    assert len(list(log_dir.iterdir())) == 2

    get_state().truncate_logs(keep=1)
    assert len(list(log_dir.iterdir())) == 1


def test_error_handling(cli, mocker):
    mocker.patch("fw_cli.commands.login.get_fw_client", side_effect=Exception("test"))
    result = cli.run("login", env={"FW_CLI_API_KEY": "foo"})
    assert result.exit_code == 1
    assert "Error: Unhandled Exception: test" in result.stderr


def test_docs(cli, mocker):
    web = mocker.patch("fw_cli.patch.webbrowser")
    res = cli.run("--docs")
    expected = "https://flywheel-io.gitlab.io/tools/app/cli/fw"
    web.open.assert_called_once_with(expected)
    assert res.exit_code == 0
    assert "Opening docs in your browser" in res.stdout


def test_cmd_not_found(cli):
    result = cli.run("abc")
    assert result.exit_code == 2
    assert "No such command 'abc'" in result.stderr


def test_timeout_config_arg(cli, mocker, env):
    fw_mock = mocker.patch("fw_cli.state.get_fw_client", side_effect=Exception("test"))
    env["FW_CLI_HTTP_READ_TIMEOUT"] = "20"
    cli.run("--read-timeout 15 gear list")
    assert fw_mock.call_args.kwargs["connect_timeout"] == 10
    assert fw_mock.call_args.kwargs["read_timeout"] == 15

    cli.run("gear list")
    assert fw_mock.call_args.kwargs["connect_timeout"] == 10
    assert fw_mock.call_args.kwargs["read_timeout"] == 20
