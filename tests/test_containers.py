import json
import subprocess

import pytest

from fw_cli.containers import (
    ContainerException,
    DockerClient,
    PodmanClient,
    get_container_client,
    run,
)
from fw_cli.exceptions import Error
from fw_cli.state import get_state


@pytest.fixture
def process_res():
    def _gen(stdout=b"", stderr=b"", r_code=0):
        return subprocess.CompletedProcess([], r_code, stdout, stderr)

    return _gen


@pytest.fixture
def json_res(process_res):
    def _gen(json_result):
        out = bytes(json.dumps(json_result), encoding="utf-8")
        return process_res(out)

    return _gen


@pytest.mark.parametrize("client", ["docker", "podman"])
def test_set_container_client(cli, client):
    res = cli.run(f"--debug --container-client {client} login --list")
    assert res.exit_code == 0
    assert f"Using container client {client}" in res.stderr


@pytest.mark.parametrize(
    "type_, exp", [("docker", DockerClient), ("podman", PodmanClient)]
)
def test_get_container_client(type_, exp):
    state = get_state()
    state.container_client = type_
    assert isinstance(get_container_client(), exp)


def test_get_container_client_not_implemented():
    state = get_state()
    state.container_client = "test"
    with pytest.raises(NotImplementedError):
        _ = get_container_client()


def test_run_success(mocker, tmp_path):
    run_mock = mocker.patch("fw_cli.containers.sp.run")
    run_mock.return_value.returncode = 0
    res = run(["test", "test1"], in_="asdf", env={"a": "b"}, cwd=tmp_path)
    run_mock.assert_called_once_with(
        ["test", "test1"],
        input="asdf",
        env={"a": "b"},
        cwd=tmp_path,
        capture_output=False,
        check=False,
    )
    assert res == run_mock.return_value


def test_run_raises(mocker, tmp_path):
    run_mock = mocker.patch("fw_cli.containers.sp.run")
    run_mock.return_value.returncode = 1
    with pytest.raises(ContainerException):
        run(["test", "test1"], capture=True, cwd=tmp_path)
    run_mock.assert_called_once_with(
        ["test", "test1"],
        input=None,
        env=None,
        cwd=tmp_path,
        capture_output=True,
        check=False,
    )


@pytest.mark.parametrize("client", ["docker", "podman"])
def test_run_raises_missing_command(client, mocker):
    mocker.patch("fw_cli.containers.shutil.which", return_value=None)
    cmd = [client, "test"]
    with pytest.raises(
        Error,
        match=f"Missing required command {client!r}",
    ):
        run(cmd)


##### Client specific tests


def test_docker_build(tmp_path, mocker):
    run = mocker.patch("fw_cli.containers.run")
    tag = "test:0.1"

    c = DockerClient()
    c.build(
        str(tmp_path),
        tag,
        pass_thru=[
            "--rm",
            "--pull",
            "--build-arg=test=test1",
            "--platform",
            "linux/arm64,darwin/amd64",
        ],
    )
    call = " ".join(run.call_args[0][0])
    assert call.startswith("docker build")
    assert "--pull" in call
    assert f"-t {tag}" in call
    assert "--build-arg=test=test1" in call
    assert "--platform linux/arm64,darwin/amd64" in call
    assert call.endswith(str(tmp_path))


def test_podman_build(tmp_path, mocker):
    run = mocker.patch("fw_cli.containers.run")
    tag = "test:0.1"

    c = PodmanClient()
    c.build(
        str(tmp_path),
        tag,
        pass_thru=[
            "--rm",
            "--pull",
            "--no-cache",
            "--build-arg=test=test1",
            "--platform",
            "linux/arm64,darwin/amd64",
        ],
    )
    call = " ".join(run.call_args[0][0])
    assert call.startswith("podman build --format=docker")
    assert "--pull" in call
    assert "--rm" in call
    assert "--no-cache" in call
    assert f"-t {tag}" in call
    assert "--build-arg=test=test1" in call
    assert "--platform linux/arm64,darwin/amd64" in call
    assert call.endswith(str(tmp_path))


##### General tests


@pytest.mark.parametrize(
    "client, debug, exp",
    [
        (DockerClient(), True, ["docker", "-D"]),
        (DockerClient(), False, ["docker"]),
        (PodmanClient(), True, ["podman", "--log-level", "debug"]),
        (PodmanClient(), False, ["podman"]),
    ],
)
def test_cmd(mocker, client, debug, exp):
    state = mocker.patch("fw_cli.containers.get_state")
    state.return_value.debug = debug
    res = client.cmd()
    assert res == exp


@pytest.mark.parametrize(
    "client,exp", [(DockerClient(), "docker run"), (PodmanClient(), "podman run")]
)
def test_container_run(mocker, client, exp):
    run = mocker.patch("fw_cli.containers.run")
    image = "test:0.1.0"
    entry = "python -m pdb run.py"
    volumes = ["/test:/test"]
    env = {"PATH": "/test"}
    pass_thru = ["--gpus", "all", "-e", "TEST=/test"]

    client.run(
        image,
        entrypoint=entry,
        volumes=volumes,
        env=env,
        capture=True,
        pass_thru=pass_thru,
    )

    call = " ".join(run.call_args[0][0])
    assert run.call_args[1]["capture"]
    assert call.startswith(exp)
    assert "-c python -m pdb run.py" in call
    assert "--gpus all" in call
    assert "-e TEST=/test" in call
    assert "-e PATH='/test'" in call
    assert "-c python -m pdb run.py" in call
    assert image in call


@pytest.mark.parametrize("client", [DockerClient(), PodmanClient()])
@pytest.mark.parametrize(
    "res,exp_out",
    [
        ([{"Id": 1}], {"Id": 1}),
        ([], None),
        ([{"Id": 1}, {"Id": 2}], {"Id": 1}),
    ],
)
def test_container_inspect(mocker, json_res, res, exp_out, client):
    run = mocker.patch("fw_cli.containers.run")
    run.return_value = json_res(res)
    out = client.inspect("")
    assert out == exp_out


@pytest.mark.parametrize(
    "client, msg",
    [
        (DockerClient(), "Error: No such object: python:asdf"),
        (
            PodmanClient(),
            'Error: error inspecting object: no such object: "python:asdf"',
        ),
    ],
)
def test_container_inspect_no_obj(mocker, client, msg):
    run = mocker.patch("fw_cli.containers.run")
    run.side_effect = ContainerException([], r_code=1, err=msg)
    res = client.inspect("")
    assert res is None


@pytest.mark.parametrize("client", [DockerClient(), PodmanClient()])
def test_container_inspect_other_error(mocker, client):
    run = mocker.patch("fw_cli.containers.run")
    run.side_effect = ContainerException([], r_code=1, err="test")
    with pytest.raises(ContainerException):
        _ = client.inspect("")


@pytest.mark.parametrize("client", [DockerClient(), PodmanClient()])
def test_container_tag(mocker, json_res, client):
    run = mocker.patch("fw_cli.containers.run")
    run.side_effect = [
        None,  # Tag
        json_res([{"Id": 1}]),  # Inspect
    ]
    image = "test:0.1.0"
    target = "other-test:0.1.0"
    res = client.tag(image, target)
    assert res
    assert res["Id"] == 1


@pytest.mark.parametrize(
    "client, start", [(DockerClient(), "docker"), (PodmanClient(), "podman")]
)
def test_container_pull(mocker, client, start):
    run = mocker.patch("fw_cli.containers.run")
    client.pull("test")

    login, pull = [" ".join(call[0][0]) for call in run.call_args_list]
    assert login.startswith(f"{start} login")
    assert pull == f"{start} pull test"


@pytest.mark.parametrize(
    "client, start", [(DockerClient(), "docker"), (PodmanClient(), "podman")]
)
@pytest.mark.parametrize(
    "inspect_res, exp",
    [
        ("", ""),
        # No repo digest, return empty digest
        ({"Id": 1}, ""),
        # No repo digest matches image name
        ({"Id": 1, "RepoDigests": ["test.flywheel.io/tester@sha256:aefb1"]}, ""),
        # Repo digest matches name
        (
            {"Id": 1, "RepoDigests": ["test.flywheel.io/test@sha256:aefb1"]},
            "sha256:aefb1",
        ),
    ],
)
def test_container_push(mocker, json_res, inspect_res, exp, client, start):
    run = mocker.patch("fw_cli.containers.run")
    run.side_effect = [
        None,
        None,
        json_res([inspect_res]) if inspect_res else json_res([]),
    ]
    res = client.push("test.flywheel.io/test")
    assert res == exp

    login, push, inspect = [" ".join(call[0][0]) for call in run.call_args_list]
    assert login.startswith(f"{start} login")
    assert push == f"{start} push test.flywheel.io/test"
    assert inspect == f"{start} inspect test.flywheel.io/test"


@pytest.mark.parametrize(
    "client, start", [(DockerClient(), "docker"), (PodmanClient(), "podman")]
)
def test_login(mocker, client, start):
    state = get_state()
    run = mocker.patch("fw_cli.containers.run")
    client.login()

    call = " ".join(run.call_args[0][0])
    domain = state.fw.base_url.netloc.decode()
    assert call.startswith(f"{start} login")
    assert domain in call
    assert f"-u {state.profile.id}" in call
    assert f"-p {state.profile.api_key}" in call
