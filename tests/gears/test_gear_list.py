import json

import pytest
import yaml
from fw_utils import assert_like

from .conftest import gear_response


@pytest.mark.parametrize(
    "cmd, args, expected_filter",
    [
        ("list", "test", "gear.name=~test"),
        ("ls", "--filter=gear.name=test", "gear.name=test"),
        ("ls", "--filter=gear.name=test test", "gear.name=test"),
    ],
)
def test_gear_list(cli, api, cmd, args, expected_filter):
    api.add_response("/api/gears", gear_response([("test", False), ("test1", True)]))
    result = cli.run(f"gear {cmd} {args} -a -d -l 1000")
    assert result.exit_code == 0
    api_request = api.pop_first()
    assert api_request["params"] == {
        "filter": expected_filter,
        "all_versions": "true",
        "include_invalid": "true",
        "limit": "1000",
    }


def test_gear_list_json(cli, api):
    gears = gear_response([("test", False)])
    api.add_response("/api/gears", gears)
    result = cli.run("gear list -ojson")
    assert result.exit_code == 0
    assert result.stdout[0] == "["
    assert_like([expected_gear(gears[0])], json.loads(result.stdout))


def test_gear_list_yaml(cli, api):
    gears = gear_response([("test", False)])
    api.add_response("/api/gears", gears)
    result = cli.run("gear list -oyaml")
    assert result.exit_code == 0
    assert result.stdout[0] != "["
    assert_like([expected_gear(gears[0])], yaml.safe_load(result.stdout))


def expected_gear(gear):
    return {
        "id": gear["_id"],
        "name": gear["gear"]["name"],
        "label": gear["gear"]["label"],
        "version": gear["gear"]["version"],
        "enabled": gear["disabled"] is None,
    }
