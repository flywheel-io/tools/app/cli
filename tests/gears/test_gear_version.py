import json


def test_get_version(cli, default_man):
    tmp_path = default_man()
    res = cli.run(f"gear version -d {tmp_path}")
    assert res.exit_code == 0
    assert "1.2.0_1.0.20201102" in res.stdout


def test_get_version_no_manifest(cli, tmp_path):
    res = cli.run(f"gear version -d {tmp_path}")
    assert res.exit_code == 0
    assert "No gear manifest found" in res.stdout


def test_set_version(cli, default_man):
    tmp_path = default_man()
    res = cli.run(f"gear version 1.2./1_1.0.2 -d {tmp_path}")
    assert res.exit_code == 0
    assert "1.2./1_1.0.2 does not look like a semver" in res.stdout
    assert "from 1.2.0_1.0.20201102 to 1.2./1_1.0.2" in res.stdout
    manifest = {}
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
    assert manifest["version"] == "1.2./1_1.0.2"
    assert (
        manifest.get("custom", {}).get("gear-builder", {}).get("image")
        == "flywheel/dcm2niix:1.2./1_1.0.2"
    )


def test_set_version_not_already_present(cli, default_man):
    tmp_path = default_man()
    manifest = {}
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
    del manifest["custom"]
    with open(tmp_path / "manifest.json", "w", encoding="utf-8") as fp:
        json.dump(manifest, fp)
    res = cli.run(f"gear version 1.2.1 -d {tmp_path}")
    assert res.exit_code == 0
    assert "1.2.1 does not look like a semver" not in res.stdout
    assert "from 1.2.0_1.0.20201102 to 1.2.1" in res.stdout
    manifest = {}
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
    assert manifest["version"] == "1.2.1"
    assert (
        manifest.get("custom", {}).get("gear-builder", {}).get("image")
        == "dcm2niix:1.2.1"
    )


def test_set_version_not_in_gearbuilder(cli, default_man):
    tmp_path = default_man()
    manifest = {}
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
    manifest["custom"]["gear-builder"]["image"] = "dcm2niix"
    with open(tmp_path / "manifest.json", "w", encoding="utf-8") as fp:
        json.dump(manifest, fp)
    res = cli.run(f"gear version 1.2.1 -d {tmp_path}")
    assert res.exit_code == 0
    assert "1.2.1 does not look like a semver" not in res.stdout
    assert "'custom.gear-builder.image' field has no version" in res.stdout
    assert "from 1.2.0_1.0.20201102 to 1.2.1" in res.stdout
    manifest = {}
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
    assert manifest["version"] == "1.2.1"
    assert (
        manifest.get("custom", {}).get("gear-builder", {}).get("image")
        == "dcm2niix:1.2.1"
    )
