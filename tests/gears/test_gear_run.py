import json
import subprocess as sp
import warnings
from pathlib import Path

import pytest
from typer import Exit

from fw_cli.commands.utils import collapse_path
from fw_cli.containers import ContainerException


@pytest.fixture(scope="function")
def _manifest(mocker):
    m_mock = mocker.patch("fw_cli.commands.gear.run.validate_manifest")
    m_mock.return_value.get_docker_image_name_tag.return_value = "flywheel/test:0.0.1"
    return m_mock


@pytest.fixture(scope="function")
def _config(mocker):
    c_mock = mocker.patch("fw_cli.commands.gear.run.validate_config")
    return c_mock


def test_gear_run_location_doesnt_exist(cli):
    res = cli.run("gear run /tmp/random_blob")
    assert res.exit_code == 1
    assert "Cannot find gear location" in res.stderr


def test_gear_run_prepare_manifest_missing(cli, tmp_path):
    res = cli.run(f"gear run --prepare {tmp_path}")
    assert res.exit_code == 1
    assert "A manifest is required in directory" in res.stderr


def test_gear_run_prepare_config_missing(cli, tmp_path, _manifest):
    res = cli.run(f"gear run --prepare {tmp_path}")
    assert res.exit_code == 1
    assert "A config must be present" in res.stderr


def test_gear_run_prepare_structure_present(cli, tmp_path, _manifest, _config, mocker):
    shutil_mock = mocker.patch("fw_cli.commands.gear.run.shutil")
    # Exit early since we're only testing up to this portion
    e_mock = mocker.patch("fw_cli.commands.gear.run.populate_inputs")
    e_mock.side_effect = Exit()
    res = cli.run(f"gear run --prepare --prepare-dir {tmp_path / 'test'} {tmp_path}")
    calls = [list(val)[0] for val in shutil_mock.copy.call_args_list]
    assert len(calls) == 2
    assert (tmp_path / "config.json", tmp_path / "test/config.json") in calls
    assert (tmp_path / "manifest.json", tmp_path / "test/manifest.json") in calls
    for d in ["input", "output", "work"]:
        assert (tmp_path / f"test/{d}").is_dir()
    assert "Building gear structure" in res.stdout


def test_gear_run_mount_dirs_skips(cli, tmp_path, mocker):
    mocker.patch("fw_cli.commands.gear.run.shutil.copy")
    mocker.patch("fw_cli.commands.gear.run.populate_inputs")
    res = cli.run(f"gear run --prepare --prepare-dir {tmp_path / 'test'} {tmp_path}")
    assert "A manifest is required in directory" in res.stderr


def test_gear_run_input_map_missing(cli, tmp_path, _manifest, _config, mocker):
    mocker.patch.object(Path, "mkdir")
    mocker.patch("fw_cli.commands.gear.run.shutil.copy")
    mocker.patch("fw_cli.commands.gear.run.mount_dirs")
    e_mock = mocker.patch("fw_cli.commands.gear.run.write_run_sh")
    e_mock.side_effect = Exit()
    res = cli.run(f"gear run --prepare {tmp_path}")
    assert "Could not find input map" in res.stdout


@pytest.mark.parametrize(
    "in_, out_",
    [
        ("/home/test/documents/test.txt", "~/d/test.txt"),
        ("/var/test/documents/test.txt", "/v/t/d/test.txt"),
    ],
)
def test_collapse_path(mocker, in_, out_):
    home = mocker.patch("fw_cli.commands.utils.Path")
    home.home.return_value = Path("/home/test")
    assert collapse_path(Path(in_)) == out_


def test_gear_run_input_map_success(cli, mocker, default_gear):
    location = default_gear()
    tmp_path = location / "test"
    with open(location / "config.json", "w", encoding="utf-8") as fp:
        json.dump(
            {
                "config": {
                    "anonymize_bids": True,
                    "bids_sidecar": "n",
                    "coil_combine": False,
                    "comment": "",
                    "timezone": "CST",
                },
                # Exists, and has path in inputmap
                "inputs": {
                    "dcm2niix_input": {
                        "location": {
                            "path": "/flywheel/v0/input/dcm2niix_input/my_file.dcm",
                            "name": "my_file.dcm",
                        }
                    },
                    # Doesn't have path in inputmap
                    "rec_file_input": {
                        "location": {
                            "path": "/flywheel/v0/input/rec_file_input/my_file2.txt",
                            "name": "my_file2.txt",
                        }
                    },
                    # Doesn't exist but has file in inputmap
                    "pydeface_template": {
                        "location": {
                            "path": "/flywheel/v0/input/pydeface_template/my_file3.txt",
                            "name": "my_file3.txt",
                        }
                    },
                    # In inputmap but file exists.
                    "debug": {
                        "location": {
                            "path": "/flywheel/v0/input/debug/my_file4.txt",
                            "name": "my_file4.txt",
                        }
                    },
                },
                "destination": {},
            },
            fp,
        )

    with open(location / ".input_map.json", "w", encoding="utf-8") as fp:
        pre = "/flywheel/v0/input"
        json.dump(
            {
                f"{pre}/dcm2niix_input/my_file.dcm": f"{location / 'my_file.dcm'}",
                f"{pre}/pydeface_template/my_file3.txt": f"{location / 'my_file3.txt'}",
                f"{pre}/debug/my_file4.txt": f"{tmp_path / 'input/debug/my_file4.txt'}",
            },
            fp,
        )
    (location / "my_file.dcm").touch()

    mocker.patch("fw_cli.commands.gear.run.mount_dirs")
    with warnings.catch_warnings():
        # TODO: Remove warning filter once code in `gears` package has been fixed to close this file
        # https://gitlab.com/flywheel-io/public/gears/-/blob/master/gears/generator.py?ref_type=heads#L33
        # Otherwise, this raises a ResourceWarning: unclosed file and pytest doesn't know how to handle that
        warnings.simplefilter("ignore")
        res = cli.run(f"gear run --prepare --prepare-dir {tmp_path} {location}")
    assert (tmp_path / "input/dcm2niix_input/my_file.dcm").is_file()
    assert (
        f"Found file input with source path: {collapse_path(location / 'my_file.dcm')}"
        in res.stdout
    )
    assert (
        f"Found file input with source path: {collapse_path(location / 'my_file3.txt')}"
        in res.stdout
    )
    assert (
        f"Found file input with source path: {collapse_path(location / 'my_file4.txt')}"
        not in res.stdout
    )
    assert "Couldn't copy input pydeface_template: my_file3.txt" in res.stdout
    assert "Local path for input rec_file_input not found" in res.stdout


@pytest.fixture
def setup_create_container(cli, mocker, default_gear):
    location = default_gear()
    tmp_path = location / "test"
    mocker.patch("fw_cli.commands.gear.run.populate_inputs")
    run = mocker.patch("fw_cli.containers.run")
    pre = "/flywheel/v0/"
    exp_dirs = ["input", "output", "work", "manifest.json", "config.json"]
    exp_binds = []
    for d in exp_dirs:
        src = tmp_path / d
        dst = pre + d
        exp_binds.append(f"{src}:{dst}")
    cli.run(f"gear run --prepare --prepare-dir {tmp_path} {location}")
    return (location, tmp_path, run, exp_binds)


def test_gear_run_create_container_success(cli, setup_create_container):
    _, location, run, exp_binds = setup_create_container
    cli.run(f"gear run {location}")
    cmd = " ".join(run.call_args[0][0])
    assert "flywheel/dcm2niix:1.2.0_1.0.20201102" in cmd
    assert "-it" not in cmd
    env = [
        "LD_LIBRARY_PATH='/usr/lib/fsl/5.0'",
        "FSLDIR='/usr/share/fsl/5.0'",
        "PATH='/usr/lib/fsl/5.0:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'",
        "FSLOUTPUTTYPE='NIFTI_GZ'",
    ]
    for e in env:
        assert f"-e {e}" in cmd
    for v in exp_binds:
        assert v in cmd


@pytest.mark.parametrize(
    "entrypoint",
    [
        "--entrypoint 'python run.py'",
        "--entrypoint='python run.py'",
    ],
)
def test_gear_run_extra_kwargs(cli, setup_create_container, mocker, entrypoint):
    mocker.patch("fw_cli.commands.gear.run.write_run_sh")
    _, location, run, exp_binds = setup_create_container
    res = cli.run(
        f"gear run {location} "
        "-- --gpus all -e TEST=test --it "
        f"{entrypoint} -v test:test:ro"
    )
    assert res.exit_code == 0
    cmd = " ".join(run.call_args[0][0])
    exp_binds = ["test:test:ro"]
    assert "flywheel/dcm2niix:1.2.0_1.0.20201102" in cmd
    assert "-it" in cmd
    assert "-c python run.py" in cmd
    assert "--gpus all" in cmd
    env = [
        "LD_LIBRARY_PATH='/usr/lib/fsl/5.0'",
        "FSLDIR='/usr/share/fsl/5.0'",
        "PATH='/usr/lib/fsl/5.0:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'",
        "FSLOUTPUTTYPE='NIFTI_GZ'",
        "TEST=test",
    ]
    for e in env:
        assert f"-e {e}" in cmd
    for v in exp_binds:
        assert v in cmd


def test_gear_run_docker_error(cli, _manifest, _config, tmp_path, mocker):
    mocker.patch.object(Path, "mkdir")
    _manifest.return_value.environment = {}
    _manifest.return_value.manifest.get.return_value = ""
    run = mocker.patch("fw_cli.containers.run")
    run.side_effect = ContainerException(["test"], r_code=1)
    res = cli.run(f"gear run {tmp_path}")
    assert res.exit_code == 1
    assert "Exception while running command: test" in res.stderr


def test_gear_run_no_prepare(cli, _manifest, _config, tmp_path, mocker):
    _manifest.return_value.environment = {}
    _manifest.return_value.manifest.get.return_value = ""
    run = mocker.patch("fw_cli.containers.run")

    def side_effect(*args, **_kwargs):
        return sp.CompletedProcess(args[0], 0)

    run.side_effect = side_effect
    res = cli.run(f"gear run {tmp_path}")
    assert res.exit_code == 0
    assert "Dumping run command in run.sh" in res.stdout
    run.assert_called_once()
    exp = (
        "#!/usr/bin/env bash \n\n"
        "IMAGE=flywheel/test:0.0.1\n\n"
        "# Command:\n"
        "docker -D run $IMAGE \\\n"
    )

    with open(tmp_path / "run.sh", "r", encoding="utf-8") as fp:
        assert fp.read() == exp
