import shutil

import pytest

from ..conftest import assets


def gear_response(gear_def):
    gears = []
    for id_, invalid in gear_def:
        gears.append(
            {
                "_id": id_,
                "disabled": "test" if invalid else None,
                "gear": {
                    "label": "Test gear",
                    "name": "test-gear",
                    "version": "0.1.0",
                    "custom": {"flywheel": {"invalid": invalid}},
                },
            }
        )
    return gears


@pytest.fixture
def default_man(tmp_path):
    def gen():
        shutil.copy(
            assets / "dcm2niix.json",
            tmp_path / "manifest.json",
        )
        shutil.copy(
            assets / "dcm2niix_default_config.json",
            tmp_path / "config.json",
        )
        return tmp_path

    return gen
