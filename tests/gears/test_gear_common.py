import json

import pytest

from .conftest import gear_response


@pytest.fixture
def manifest():
    """Return a default manifest for a gear."""
    return {
        "name": "test",
        "version": "0.0.1",
        "author": "",
        "config": {},
        "description": "",
        "inputs": {},
        "label": "",
        "license": "MIT",
        "source": "",
        "url": "",
        "custom": {"gear-builder": {"image": "flywheel/test:0.0.1"}},
    }


def test_gear_validate_manifest_success(cli, app_dir, manifest):
    with open(app_dir / "manifest.json", "w", encoding="utf-8") as fp:
        json.dump(manifest, fp)
    res = cli.run(f"gear --validate {app_dir}")
    assert res.exit_code == 0
    assert "Validating manifest" in res.stdout


def test_gear_validate_manifest_fail(cli, app_dir, manifest):
    del manifest["license"]
    with open(app_dir / "manifest.json", "w", encoding="utf-8") as fp:
        json.dump(manifest, fp)
    res = cli.run(f"gear --validate {app_dir / 'manifest.json'}")
    assert res.exit_code == 1
    assert "'license' is a required property" in res.stderr


def test_gear_validate_manifest_no_manifest(cli, app_dir):
    res = cli.run(f"gear --validate {app_dir}")
    assert res.exit_code == 0
    assert "No gear manifest found" in res.stdout


def test_gear_common_mutually_exclusive(cli):
    res = cli.run("gear --disable test --enable test")
    assert res.exit_code == 2
    res2 = cli.run("gear --disable test --validate")
    assert res2.exit_code == 2


def test_gear_enable_disable_not_admin(cli):
    res = cli.run("gear --enable asdf")
    assert res.exit_code == 1
    assert "Must be site admin" in res.stderr


@pytest.mark.parametrize(
    "disabled, to_handle, exp",
    [
        (True, "0,1", 1),
        (True, "0", 0),
    ],
)
def test_gear_enable_multiple(cli_admin, api, disabled, to_handle, exp):
    api.add_response(
        "/api/gears", gear_response([("test", disabled), ("test1", disabled)])
    )
    api.add_response("/api/gears/test/enable", method="POST", status=200)
    api.add_response("/api/gears/test1/enable", method="POST", status=400)
    res = cli_admin.run("gear --enable test-gear", input=to_handle)
    assert res.exit_code == exp


def test_gear_enable_no_to_handle(cli_admin, api):
    api.add_response("/api/gears", gear_response([("test", True), ("test1", True)]))
    res = cli_admin.run("gear --enable test-gear", input="\n")
    assert res.exit_code == 0
    assert "Nothing to do" in res.stdout


@pytest.mark.parametrize(
    "disabled, to_handle, exp",
    [
        (False, "0-2", 1),
        (False, "0", 0),
    ],
)
def test_gear_disable_multiple(cli_admin, api, disabled, to_handle, exp):
    api.add_response(
        "/api/gears", gear_response([("test", disabled), ("test1", disabled)])
    )
    api.add_response("/api/gears/test/disable", method="POST", status=200)
    api.add_response("/api/gears/test1/disable", method="POST", status=400)
    res = cli_admin.run("gear --disable test-gear", input=to_handle)
    assert res.exit_code == exp


def test_gear_enable_disable_multiple_requests(cli_admin, api):
    gears = gear_response([("1", False), ("2", False)])
    gears[1]["gear"]["version"] = "0.0.1"
    api.add_response("/api/gears", gears)
    api.add_response("/api/gears/1/disable", method="POST", status=200)
    api.add_response("/api/gears/2/disable", method="POST", status=200)
    res = cli_admin.run(
        "gear --disable test-gear",
        input="0-1",
    )
    assert api.requests[0]["params"]["filter"] == ("gear.name=test-gear")
    assert api.requests[1]["url"] == "/api/gears/1/disable"
    assert api.requests[2]["url"] == "/api/gears/2/disable"
    assert res.exit_code == 0


def test_gear_availability_single(cli_admin, api):
    api.add_response(
        "/api/gears/60b1390b3f46316f94f2f34a",
        gear_response([("60b1390b3f46316f94f2f34a", True)])[0],
    )
    api.add_response(
        "/api/gears/60b1390b3f46316f94f2f34a/enable", method="POST", status=200
    )
    res = cli_admin.run(
        "gear --enable 60b1390b3f46316f94f2f34a",
    )
    assert res.exit_code == 0
    assert "Result" in res.stdout


def test_gear_availability_no_gears_found(cli_admin, api):
    api.add_response("/api/gears", [])
    res = cli_admin.run(
        "gear --enable test",
    )
    assert res.exit_code == 0


def test_gear_availability_nothing_to_do_to_handle(cli_admin, api):
    api.add_response("/api/gears", gear_response([("test", True), ("test1", True)]))
    res = cli_admin.run("gear --enable test-gear", input="\n")
    assert res.exit_code == 0
    assert "Nothing to do. Exiting" in res.stdout


def test_gear_enable_disable_cant_get_gears(cli_admin, api):
    api.add_response("/api/gears", status=403)
    res = cli_admin.run("gear --enable test-gear", input="y")
    assert res.exit_code == 1
    assert "Could not get list of gears" in res.stderr


@pytest.mark.parametrize(
    "func, disabled, e, out",
    [
        # Only fails when it tries to enable/disable.  If gear is
        #   Already in the state the function tries to get it into
        #   It will skip the POST and succeed.
        ("enable", True, 1, "Could not enable"),
        ("disable", True, 0, "no availability change"),
        ("enable", False, 0, "no availability change"),
        ("disable", False, 1, "Could not disable"),
    ],
)
def test_gear_enable_disable_cant_enable_disable(
    cli_admin, api, func, out, e, disabled
):
    api.add_response("/api/gears", gear_response([("test", disabled)]))
    api.add_response(f"/api/gears/test/{func}", method="POST", status=400)
    res = cli_admin.run(f"gear --{func} test-gear", input="0")
    assert res.exit_code == e
    assert out in res.stderr if e else out in res.stdout


@pytest.mark.parametrize("to_handle", ["a,b", "0,2"])
def test_gear_enable_disable_parse_error(cli_admin, api, to_handle):
    api.add_response("/api/gears", gear_response([("test", True), ("test", True)]))
    api.add_response("/api/gears/test/enable", method="POST", status=200)
    res = cli_admin.run("gear --enable test-gear", input=to_handle)
    assert res.exit_code == 1
    assert "Could not parse" in res.stderr


def test_gear_docs(cli, mocker):
    web = mocker.patch("fw_cli.patch.webbrowser")
    res = cli.run("gear --docs")
    # cli fixture called with prog_name="fw"
    web.open.assert_called_once_with(
        "https://flywheel-io.gitlab.io/tools/app/cli/fw/gear"
    )
    assert res.exit_code == 0
    assert "Opening docs in your browser" in res.stdout
