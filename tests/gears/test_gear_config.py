import json
import shutil

import pytest

from .conftest import assets


def test_create_no_manifest(cli, default_man):
    tmp_path = default_man()
    (tmp_path / "manifest.json").unlink()
    res = cli.run(f"gear config --new {tmp_path}")
    assert res.exit_code == 1
    assert "A manifest is required" in res.stderr


def test_create_config(cli, tmp_path):
    shutil.copy(assets / "dcm2niix.json", tmp_path / "manifest.json")
    res = cli.run(f"gear config --new {tmp_path}")
    assert res.exit_code == 0
    assert (tmp_path / "config.json").exists()
    assert (tmp_path / "config.json").is_file()
    assert "Config generated" in res.stdout
    actual = {}
    with open(tmp_path / "config.json", "r") as fp:
        actual = json.load(fp)
    with open(assets / "dcm2niix_default_config.json", "r") as fp:
        exp = json.load(fp)
        assert actual == exp


def test_create_config_saves_backup(cli, default_man):
    tmp_path = default_man()
    res = cli.run(f"gear config --new {tmp_path}")
    assert res.exit_code == 0
    assert (tmp_path / "config.json").exists()
    assert (tmp_path / "config.json").is_file()
    assert (tmp_path / "config.json.back").exists()
    assert (tmp_path / "config.json.back").is_file()
    assert "saved backup" in res.stdout


def test_show_no_manifest(tmp_path, cli):
    res = cli.run(f"gear config --show {tmp_path}")
    assert res.exit_code == 1
    assert "Could not find" in res.stderr


def test_show(tmp_path, cli):
    shutil.copy(assets / "dcm2niix.json", tmp_path / "manifest.json")
    res = cli.run(f"gear config --show {tmp_path}")
    assert res.exit_code == 0
    assert "Available options" in res.stdout


def test_add_api_key(default_man, cli):
    tmp_path = default_man()
    res = cli.run(f"gear config -i api_key=test {tmp_path}")
    assert res.exit_code == 0
    assert "Setting api-key" in res.stdout
    with open(tmp_path / "config.json", "r") as fp:
        config = json.load(fp)
        assert "api-key" in config["inputs"]
        assert config["inputs"]["api-key"]["base"] == "api-key"
        assert config["inputs"]["api-key"]["key"] == "test"


def test_add_input_cannot_parse(default_man, cli):
    tmp_path = default_man()
    res = cli.run(f"gear config -i test_input:/home/test.txt {tmp_path}")
    assert res.exit_code == 1
    assert "Could not parse input test_input:/home/test.txt" in res.stderr


def test_add_input_not_in_manifest(default_man, cli):
    tmp_path = default_man()
    res = cli.run(f"gear config -i test_input=/home/test.txt {tmp_path}")
    assert res.exit_code == 0
    assert "Input 'test_input' not in manifest" in res.stdout


def test_add_input_file_not_exist(default_man, cli):
    tmp_path = default_man()
    res = cli.run(f"gear config -i dcm2niix_input=/home/test.txt {tmp_path}")
    assert res.exit_code == 1
    assert "Adding input dcm2niix_input" in res.stdout
    assert "Cannot resolve file input at /home/test.txt" in res.stderr


def test_add_valid_input_file(default_man, cli):
    tmp_path = default_man()
    (tmp_path / "nifti.nii.gz").touch()
    res = cli.run(
        f"gear config -i dcm2niix_input={tmp_path / 'nifti.nii.gz'} {tmp_path}"
    )
    assert res.exit_code == 0
    assert "Adding input dcm2niix_input" in res.stdout
    assert (tmp_path / ".input_map.json").exists()
    with open(tmp_path / ".input_map.json", "r") as fp:
        input_map = json.load(fp)
        assert input_map["/flywheel/v0/input/dcm2niix_input/nifti.nii.gz"] == str(
            tmp_path / "nifti.nii.gz"
        )
    # Update input
    (tmp_path / "nifti2.nii.gz").touch()
    res = cli.run(
        f"gear config -i dcm2niix_input={tmp_path / 'nifti2.nii.gz'} {tmp_path}"
    )
    assert res.exit_code == 0
    assert "Adding input dcm2niix_input" in res.stdout
    assert (tmp_path / ".input_map.json").exists()
    with open(tmp_path / ".input_map.json", "r") as fp:
        input_map = json.load(fp)
        assert input_map["/flywheel/v0/input/dcm2niix_input/nifti2.nii.gz"] == str(
            tmp_path / "nifti2.nii.gz"
        )


def test_add_valid_input_file_manifest_not_present(default_man, cli):
    tmp_path = default_man()
    (tmp_path / "manifest.json").unlink()
    (tmp_path / "nifti.nii.gz").touch()
    res = cli.run(
        f"gear config -i dcm2niix_input={tmp_path / 'nifti.nii.gz'} {tmp_path}"
    )
    assert res.exit_code == 0
    assert "Need manifest in current dir to update input dcm2niix_input" in res.stdout
    assert "Adding input" not in res.stdout
    assert "Setting dcm2niix_input" not in res.stdout
    assert not (tmp_path / ".input_map.json").exists()


@pytest.fixture
def setup_add_input_from_path(default_man, mocker):
    tmp_path = default_man()
    (tmp_path / "test").mkdir()
    tmpdir = mocker.patch("fw_cli.commands.gear.config.tmpdir")
    tmpdir.__truediv__.side_effect = lambda val: tmp_path / f"test/{val}"
    return tmp_path


def resolve(api, c_type="file", res=200):
    api.add_response(
        "/api/resolve",
        {
            "path": [
                {"container_type": "acquisition", "_id": "test"},
                {"name": "nifti.nii.gz", "container_type": c_type},
            ]
        },
        method="POST",
        status=res,
    )


def test_add_input_from_fw_path_success(setup_add_input_from_path, api, cli):
    tmp_path = setup_add_input_from_path
    resolve(api)
    api.add_response("/api/acquisitions/test/files/nifti.nii.gz", {}, status=200)
    api.add_response(
        "/api/acquisitions/test/files/nifti.nii.gz/info", {"type": "dicom"}
    )
    res = cli.run(
        f"gear config -i dcm2niix_input=fw://g/p/sub/ses/acq/nifti.nii.gz {tmp_path}"
    )
    assert res.exit_code == 0
    assert (tmp_path / ".input_map.json").exists()
    with open(tmp_path / ".input_map.json", "r") as fp:
        input_map = json.load(fp)
        assert input_map["/flywheel/v0/input/dcm2niix_input/nifti.nii.gz"] == str(
            tmp_path / "test/downloads/nifti.nii.gz"
        )
    with open(tmp_path / "config.json", "r") as fp:
        config = json.load(fp)
        assert config["inputs"]["dcm2niix_input"]["object"]["type"] == "dicom"


def test_add_input_from_fw_path_resolve_not_a_file(setup_add_input_from_path, api, cli):
    tmp_path = setup_add_input_from_path
    resolve(api, c_type="analysis")
    res = cli.run(
        f"gear config -i dcm2niix_input=fw://g/p/sub/ses/acq/nifti.nii.gz {tmp_path}"
    )
    assert res.exit_code == 1


def test_add_input_from_fw_path_resolve_fails(setup_add_input_from_path, api, cli):
    tmp_path = setup_add_input_from_path
    resolve(api, res=3404)
    res = cli.run(
        f"gear config -i dcm2niix_input=fw://g/p/sub/ses/acq/nifti.nii.gz {tmp_path}"
    )
    assert res.exit_code == 1


def test_add_input_from_fw_path_download_fails(setup_add_input_from_path, api, cli):
    tmp_path = setup_add_input_from_path
    resolve(api)
    api.add_response("/api/acquisitions/test/files/nifti.nii.gz", {}, status=404)
    res = cli.run(
        f"gear config -i dcm2niix_input=fw://g/p/sub/ses/acq/nifti.nii.gz {tmp_path}"
    )
    assert res.exit_code == 1


def test_add_config_cant_parse(default_man, cli):
    tmp_path = default_man()
    res = cli.run(f"gear config -c test_input:true {tmp_path}")
    assert res.exit_code == 1
    assert "Could not parse config test_input:true" in res.stderr


def test_add_config_invalid_bool(default_man, cli):
    tmp_path = default_man()
    res = cli.run(f"gear config -c coil_combine=fdsfs {tmp_path}")
    assert "setting to False" in res.stdout


def test_config_option_manifest_not_present(default_man, cli):
    tmp_path = default_man()
    (tmp_path / "manifest.json").unlink()
    res = cli.run(f"gear config -c test=true {tmp_path}")
    assert res.exit_code == 0
    assert "Need manifest in current dir to update config test" in res.stdout
    assert "Setting test" not in res.stdout


def test_config_option_not_in_manifest(default_man, cli):
    tmp_path = default_man()
    res = cli.run(f"gear config -c test=true {tmp_path}")
    assert "Config value test not present" in res.stdout
    assert res.exit_code == 0


def test_config_validates_and_gets_type_success(default_man, cli):
    tmp_path = default_man()
    # Try boolean, string, int, and float values
    cmd = (
        "gear config "
        "-c coil_combine=true -c initial_x_coord=4 "
        "-c initial_y_coord=2.45 -c comment='initial run' "
        "-c params=1,2,1 -c bids_sidecar=n -c no_type=test "
        f"{tmp_path}"
    )
    res = cli.run(cmd)
    assert res.exit_code == 0
    with open(tmp_path / "config.json", "r") as fp:
        config = json.load(fp)
        assert config["config"]["coil_combine"] is True
        assert config["config"]["initial_x_coord"] == 4
        assert config["config"]["initial_y_coord"] == 2.45
        assert config["config"]["comment"] == "initial run"
        assert config["config"]["params"] == [1, 2, 1]
        assert config["config"]["no_type"] == "test"


@pytest.mark.parametrize(
    "val,err_str",
    [
        (
            "-c initial_x_coord=test",
            "Cannot convert 'initial_x_coord' with value 'test'",
        ),
        ("-c default_object=test", "JSON-schema type object, not handled."),
    ],
)
def test_config_validates_and_gets_type_warns(default_man, cli, val, err_str):
    tmp_path = default_man()
    res = cli.run(f"gear config {val} {tmp_path}")
    assert res.exit_code == 0
    assert err_str in res.stdout


@pytest.mark.parametrize(
    "val,err_str",
    [
        (
            "-c params=1,2,3,4",
            "Config option params[3]: given value 4 not in allowed values",
        ),
        (
            "-c bids_sidecar=true",
            "Config option bids_sidecar: given value true not in allowed values",
        ),
    ],
)
def test_config_validates_and_gets_type_raises(default_man, cli, val, err_str):
    tmp_path = default_man()
    res = cli.run(f"gear config {val} {tmp_path}")
    assert res.exit_code == 1
    assert err_str in res.stderr


def test_config_add_destination_from_path(api, cli, default_man):
    tmp_path = default_man()
    api.add_response(
        "/api/resolve",
        {"path": [{"_id": "test", "container_type": "acquisition"}]},
        method="POST",
        status=200,
    )
    res = cli.run(f"gear config -d fw://dest {tmp_path}")
    assert res.exit_code == 0
    with open(tmp_path / "config.json", "r") as fp:
        config = json.load(fp)
        assert config["destination"] == {"id": "test", "type": "acquisition"}


def test_config_add_destination_from_path_not_found(api, cli, default_man):
    tmp_path = default_man()
    api.add_response(
        "/api/resolve",
        {"path": [{"_id": "test", "container_type": "acquisition"}]},
        method="POST",
        status=404,
    )
    res = cli.run(f"gear config -d fw://dest {tmp_path}")
    assert res.exit_code == 1


def test_config_add_destination_from_id(api, cli, default_man):
    tmp_path = default_man()
    api.add_response(
        "/api/containers/62a37f1c328005456b6c22cb",
        {"_id": "test", "container_type": "acquisition"},
        status=200,
    )
    res = cli.run(f"gear config -d 62a37f1c328005456b6c22cb {tmp_path}")
    assert res.exit_code == 0
    with open(tmp_path / "config.json", "r") as fp:
        config = json.load(fp)
        assert config["destination"] == {"id": "test", "type": "acquisition"}


def test_config_add_destination_from_id_not_found(api, cli, default_man):
    tmp_path = default_man()
    api.add_response(
        "/api/containers/62a37f1c328005456b6c22cb",
        {"_id": "test", "container_type": "acquisition"},
        status=404,
    )
    res = cli.run(f"gear config -d 62a37f1c328005456b6c22cb {tmp_path}")
    assert res.exit_code == 1
