import shutil

import pytest

from .conftest import assets

DOCKER_AUTH = {"username": "test", "password": "test"}


@pytest.fixture
def setup_gear_upload(tmp_path, mocker):
    shutil.copy(assets / "dcm2niix.json", tmp_path / "manifest.json")
    r_mock = mocker.patch("fw_cli.commands.gear.upload.get_container_client")
    return (
        tmp_path,
        r_mock,
    )


@pytest.mark.parametrize("category,input_", [(None, "utility"), ("analysis", None)])
def test_gear_upload(cli, api, setup_gear_upload, category, input_):
    # For some reason debug is true if I don't monkeypatch the env
    tmp_path, run = setup_gear_upload
    run.return_value.tag.return_value = {"Id": "test"}
    run.return_value.push.return_value = "test"
    api.add_response("/api/gears/prepare-add", {"ticket": "test"}, method="POST")
    api.add_response("/api/gears/save", {"_id": "test-gear"}, method="POST")
    runscript = "gear upload " + (f"-c {category} " if category else "") + str(tmp_path)
    if input_:
        res = cli.run(runscript, input=input_)
    else:
        res = cli.run(runscript)
    run.assert_called_once()
    run.return_value.tag.assert_called_once_with(
        "flywheel/dcm2niix:1.2.0_1.0.20201102",
        f"{api.addr}/dcm2niix:1.2.0_1.0.20201102",
    )
    run.return_value.push.assert_called_once_with(
        f"{api.addr}/dcm2niix:1.2.0_1.0.20201102"
    )
    assert res.exit_code == 0
    assert "Uploaded gear with id test-gear" in res.stdout
    assert "sha256:1e7b3b2918d55b0a7b" not in res.stderr


def test_gear_upload_invalid_category(cli, tmp_path, mocker):
    m_patch = mocker.patch("fw_cli.commands.gear.upload.validate_manifest")
    m_patch.return_value.get_value.return_value = "test"
    res = cli.run(f"gear upload {tmp_path}", input="test")
    assert res.exit_code == 1
    assert "Gear category test not allowed." in res.stderr


def test_gear_upload_cant_find_image_in_manifest(cli, tmp_path, mocker):
    shutil.copy(assets / "dcm2niix.json", tmp_path / "manifest.json")
    m_patch = mocker.patch("fw_cli.commands.gear.upload.validate_manifest")
    m_patch.return_value.get_value.return_value = "utility"
    m_patch.return_value.get_docker_image_name_tag.return_value = None
    res = cli.run(f"gear upload {tmp_path}")
    assert res.exit_code == 1
    assert "Could not find docker image in manifest" in res.stderr


def test_gear_image_not_found(cli, api, setup_gear_upload):
    tmp_path, run = setup_gear_upload
    run.return_value.tag.return_value = None
    api.add_response("/api/gears/prepare-add", status=200, method="POST")
    res = cli.run(f"gear upload {tmp_path}")
    assert res.exit_code == 1
    assert "Could not find image" in res.stderr


@pytest.mark.parametrize(
    "code,msg,ret", [(500, "Unhandled error", 1), (409, "Gear already exists", 0)]
)
def test_gear_upload_prepare_add_fails(cli, api, setup_gear_upload, code, msg, ret):
    tmp_path, run = setup_gear_upload
    run.return_value.tag.return_value = {"Id": "test"}
    api.add_response("/api/gears/prepare-add", status=code, method="POST")
    res = cli.run(f"gear upload {tmp_path}")
    assert res.exit_code == ret
    assert msg in res.stderr or msg in res.stdout


def test_gear_upload_docker_push_digest_not_present(cli, api, setup_gear_upload):
    tmp_path, run = setup_gear_upload
    run.return_value.tag.return_value = {"Id": "test"}
    run.return_value.push.return_value = None
    api.add_response("/api/gears/prepare-add", {"ticket": "test"}, method="POST")
    res = cli.run(f"gear upload {tmp_path}")
    assert res.exit_code == 1
    assert "Could not get digest from image push" in res.stderr


def test_gear_upload_gear_save_fails(cli, api, setup_gear_upload):
    tmp_path, run = setup_gear_upload
    run.return_value.tag.return_value = {"Id": "test"}
    run.return_value.push.return_value = "test"
    api.add_response("/api/gears/prepare-add", {"ticket": "test"}, method="POST")
    api.add_response("/api/gears/save", status=429, method="POST")
    res = cli.run(f"gear upload {tmp_path}")
    assert res.exit_code == 1
    assert "429 TOO MANY REQUESTS" in res.stderr


@pytest.mark.parametrize(
    "resp,id_",
    [({"_id": "test"}, "test"), ({"gear": "test"}, "test"), ({}, "Not Found")],
)
def test_gear_upload_save_multiple_resp(cli, api, setup_gear_upload, resp, id_):
    tmp_path, run = setup_gear_upload
    run.return_value.tag.return_value = {"Id": "test"}
    run.return_value.push.return_value = "test"
    api.add_response("/api/gears/prepare-add", {"ticket": "test"}, method="POST")
    api.add_response("/api/gears/save", resp, method="POST")
    res = cli.run(f"gear upload {tmp_path}")
    assert f"Uploaded gear with id {id_}" in res.stdout
