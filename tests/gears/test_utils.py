import pytest

from fw_cli.commands.gear.utils import (
    attempt_to_correct_manifest,
    extract_and_remove_param,
    run_cmd,
)
from fw_cli.exceptions import Error


@pytest.mark.parametrize(
    "in_dir,out_dir",
    [
        ({"capabilities": None}, {"capabilities": []}),
        ({"environment": None}, {"environment": {}}),
        ({"cite": None}, {"cite": ""}),
    ],
)
def test_attempt_to_correct_manifest(in_dir, out_dir):
    attempt_to_correct_manifest(in_dir)
    assert in_dir == out_dir


@pytest.mark.parametrize(
    "pass_thru, param, expected_val, expected_pass_thru",
    [
        (["--entrypoint", "bash"], "--entrypoint", "bash", []),
        (["--entrypoint=bash", "test"], "--entrypoint", "bash", ["test"]),
        (["-t", "tag", "test"], "--tag", None, ["-t", "tag", "test"]),
    ],
)
def test_extract_and_remove_param(pass_thru, param, expected_val, expected_pass_thru):
    val = extract_and_remove_param(pass_thru, param)
    assert val == expected_val
    assert pass_thru == expected_pass_thru


def test_run_cmd_missing():
    cmd = ["missing", "test"]
    with pytest.raises(Error, match=f"Missing required command {cmd[0]!r}"):
        run_cmd(cmd)
