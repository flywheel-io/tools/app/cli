import json
from unittest.mock import MagicMock

import pytest

from fw_cli.state import get_state

from .conftest import gear_response


def test_export_profile_not_found(api, cli):
    state = get_state()
    profile = MagicMock()
    profile.name = "mytest"
    profile.api_key = f"{api.url}:myuser"
    state.config.profiles.append(profile)
    api.add_response("/api/gears", gear_response([("test", False)]))
    res = cli.run("gear export -d test my_gear_1")
    assert res.exit_code == 1
    assert "Could not find profile test" in res.stderr


@pytest.mark.parametrize("to_handle", ["a,b", "1,2"])
def test_export_parse_error(cli, api, to_handle):
    state = get_state()
    profile = MagicMock()
    profile.name = "mytest"
    profile.api_key = f"{api.url}:user"
    state.config.profiles = [profile]
    api.add_response(
        "/api/auth/status",
        {
            "origin": {"id": "test@flywheel.io"},
            "user_is_admin": True,
            "is_device": False,
        },
    )
    api.add_response("/api/gears", gear_response([("test", False)]))
    state.profile_name = "mytest"
    state.profile = profile
    gears = gear_response([("test", False), ("test", False)])
    gears[1]["gear"]["version"] = "0.2.0"
    api.add_response("/api/gears", gears)
    res = cli.run("gear export -d mytest test-gear", input=to_handle)
    assert res.exit_code == 1
    assert "Could not parse" in res.stderr


def test_export_gear_full(cli, api, mocker, monkeypatch):
    monkeypatch.setenv("FW_CLI_DEBUG", "false")
    add_gear = mocker.patch("fw_cli.commands.gear.export.upload_gear")
    get_gear = mocker.patch("fw_cli.commands.gear.export.download_gear")
    mocker.patch("fw_cli.commands.gear.export.Manifest")
    client = mocker.patch("fw_cli.commands.gear.export.get_container_client")
    state = get_state()
    profile = MagicMock()
    profile.name = "mytest"
    profile.api_key = f"{api.url}:user"
    profile.id = "test@flywheel.io"
    state.config.profiles = [profile]
    api.add_response(
        "/api/auth/status",
        {
            "origin": {"id": "test@flywheel.io"},
            "user_is_admin": True,
            "is_device": False,
        },
    )
    api.add_response("/api/gears", gear_response([("test", False)]))
    state.profile_name = "mytest"
    state.profile = profile
    res = cli.run("gear export test:0.1.0 -d mytest")
    assert res.exit_code == 0
    assert "Exporting 1 gears" in res.stdout
    assert "test-gear:0.1.0" in res.stdout
    assert len(res.stderr) == 0
    client.return_value.login.assert_called_once_with(profile=profile)
    get_gear.assert_called_once()
    add_gear.assert_called_once()


def test_export_gear_full_debug(cli, api, mocker, monkeypatch):
    monkeypatch.setenv("FW_CLI_DEBUG", "true")
    add_gear = mocker.patch("fw_cli.commands.gear.export.upload_gear")
    get_gear = mocker.patch("fw_cli.commands.gear.export.download_gear")
    mocker.patch("fw_cli.commands.gear.export.Manifest")
    state = get_state()
    mocker.patch("fw_cli.commands.gear.export.get_container_client")
    profile = MagicMock()
    profile.name = "mytest"
    profile.api_key = f"{api.url}:user"
    profile.id = "test@flywheel.io"
    state.config.profiles = [profile]
    api.add_response(
        "/api/auth/status",
        {
            "origin": {"id": "test@flywheel.io"},
            "user_is_admin": True,
            "is_device": False,
        },
    )
    api.add_response("/api/gears", gear_response([("test", False)]))
    state.profile_name = "mytest"
    state.profile = profile
    res = cli.run("gear export test:0.1.0 -d mytest")
    assert res.exit_code == 0
    assert "Exporting 1 gears" in res.stdout
    assert "test-gear:0.1.0" in res.stdout
    assert "Overall Progress" not in res.stdout
    assert len(res.stderr) > 0
    get_gear.assert_called_once()
    add_gear.assert_called_once()


@pytest.mark.parametrize("debug,err", [("true", True), ("false", False)])
def test_export_gear_get_gear_error(cli, api, monkeypatch, debug, err):
    monkeypatch.setenv("FW_CLI_DEBUG", debug)
    state = get_state()
    profile = MagicMock()
    profile.name = "mytest"
    profile.api_key = f"{api.url}:user"
    state.config.profiles = [profile]
    api.add_response(
        "/api/auth/status",
        {
            "origin": {"id": "test@flywheel.io"},
            "user_is_admin": True,
            "is_device": False,
        },
    )
    state.profile_name = "mytest"
    state.profile = profile
    api.add_response("/api/gears", status=461)
    res = cli.run("gear export test:0.1.0 -d mytest")
    assert res.exit_code == 1
    assert "Could not get list of gears" in res.stderr
    assert "461" in res.stderr if err else "461" not in res.stderr


def test_export_gear_add_gear_error(cli, api, mocker):
    mocker.patch("fw_cli.commands.gear.export.upload_gear")
    add_gear = mocker.patch("fw_cli.commands.gear.export.download_gear")
    add_gear.side_effect = ValueError("Some error")
    state = get_state()
    profile = MagicMock()
    profile.name = "mytest"
    profile.api_key = f"{api.url}:user"
    state.config.profiles = [profile]
    api.add_response(
        "/api/auth/status",
        {
            "origin": {"id": "test@flywheel.io"},
            "user_is_admin": True,
            "is_device": False,
        },
    )
    api.add_response("/api/gears", gear_response([("test", False)]))
    state.profile_name = "mytest"
    state.profile = profile
    res = cli.run("gear export test:0.1.0 -d mytest")
    assert res.exit_code == 1
    assert "Some error" in res.stderr


def test_download(cli, api, tmp_path, mocker):
    mocker.patch("fw_cli.commands.gear.export.get_container_client")
    api.add_response("/api/gears", gear_response([("test", False)]))
    gear = gear_response([("test", False)])[0]
    gear["exchange"] = {"rootfs-url": "docker://domain/test-gear:0.1.0"}
    api.add_response("/api/gears/test", gear)
    res = cli.run(f"gear export test -p {tmp_path}", input="0\n")
    assert (tmp_path / "test-gear_0.1.0").is_dir()
    assert (tmp_path / "test-gear_0.1.0/manifest.json").is_file()
    assert res.exit_code == 0


@pytest.mark.filterwarnings("ignore:unclosed file <_io.TextIOWrapper")
def test_download_import(cli, api, tmp_path, mocker):
    mocker.patch("fw_cli.commands.gear.export.get_container_client")
    mocker.patch("fw_cli.containers.shutil.which", return_value="valid")
    sp_run = mocker.patch("subprocess.run")
    api.add_response("/api/gears", gear_response([("test", False)]))
    gear = gear_response([("test", False)])[0]
    gear["exchange"] = {"rootfs-url": "https://notdocker"}
    api.add_response("/api/gears/test", gear)
    res = cli.run(f"gear export test -p {tmp_path}", input="0\n")
    assert (tmp_path / "test-gear_0.1.0").is_dir()
    assert (tmp_path / "test-gear_0.1.0/manifest.json").is_file()
    assert res.exit_code == 0
    sp_run.assert_called_with(
        ["docker", "import", "https://notdocker", f"{api.addr}/test-gear:0.1.0"],
        check=True,
    )


def test_download_no_gears(cli, api, tmp_path, mocker):
    mocker.patch("fw_cli.commands.gear.export.get_container_client")
    api.add_response("/api/gears", [])
    res = cli.run(f"gear export test -p {tmp_path}", input="0\n")
    assert res.exit_code == 0
    assert "No gears found. Exiting" in res.stdout


def test_gear_download_full(cli, api, mocker, tmp_path, monkeypatch):
    monkeypatch.setenv("FW_CLI_DEBUG", "false")
    run = mocker.patch("fw_cli.commands.gear.export.get_container_client")
    api.add_response("/api/gears", gear_response([("test", False)]))
    gear = gear_response([("test", False)])[0]
    gear["exchange"] = {"rootfs-url": f"docker://{api.addr}/test-gear:0.1.0"}
    gear["gear"]["custom"]["gear-builder"] = {"image": "test:0.1.0"}
    # Add invalid capabilities in response
    gear["gear"]["capabilities"] = None
    api.add_response("/api/gears/test", gear)
    res = cli.run(f"gear export test-gear -p {tmp_path}")
    assert res.exit_code == 0
    manifest = tmp_path / "test-gear_0.1.0/manifest.json"
    assert manifest.is_file()
    with open(manifest, "r", encoding="utf-8") as fp:
        manifest_raw = json.load(fp)
        # Verify it is fixed locally
        assert manifest_raw["capabilities"] == []
    remote_im = f"{api.addr}/test-gear:0.1.0"
    run.return_value.pull.assert_called_once_with(remote_im)
    run.return_value.tag.assert_called_once_with(remote_im, "test:0.1.0")
    assert "Local image tagged as:" in res.stdout
