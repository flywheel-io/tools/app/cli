import json
import shutil
from subprocess import CompletedProcess

import pytest

from .conftest import assets

PATH = "/usr/lib/fsl/5.0:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"


def test_gear_run_missing_dockerfile(cli, mocker, tmp_path):
    _ = mocker.patch("fw_cli.commands.gear.build.validate_manifest")
    res = cli.run(f"gear build {tmp_path}")
    assert res.exit_code == 1
    assert "Could not find Dockerfile" in res.stderr


def test_gear_build_renders(cli, mocker, tmp_path):
    (tmp_path / "Dockerfile").touch()
    manifest = mocker.patch("fw_cli.commands.gear.build.validate_manifest")
    run = mocker.patch("fw_cli.containers.run")
    manifest.return_value.get_docker_image_name_tag.return_value = "test:0.1.1"
    res = cli.run(
        f"gear build {tmp_path} -- --no-cache "
        f"--build-arg=test=test_val --build-arg test2=test2_val"
    )
    assert res.exit_code == 0
    build_c = run.call_args_list[0]
    run_c = run.call_args_list[1]
    exp_build = (
        "docker -D build -t test:0.1.1 --no-cache --build-arg=test=test_val"
        f" --build-arg test2=test2_val {tmp_path}"
    )
    assert " ".join(build_c[0][0]) == exp_build
    exp_run = "docker -D run --entrypoint=/bin/sh test:0.1.1 -c printenv"
    assert " ".join(run_c[0][0]) == exp_run


def test_gear_build_custom_tag(cli, mocker, tmp_path):
    (tmp_path / "Dockerfile").touch()
    manifest = mocker.patch("fw_cli.commands.gear.build.validate_manifest")
    run = mocker.patch("fw_cli.containers.run")
    manifest.return_value.get_docker_image_name_tag.return_value = "test:0.1.1"
    res = cli.run(f"gear build {tmp_path} -- --tag alt:0.1.1")
    assert res.exit_code == 0
    build_c = run.call_args_list[0]
    run_c = run.call_args_list[1]
    exp_build = f"docker -D build -t alt:0.1.1 {tmp_path}"
    assert " ".join(build_c[0][0]) == exp_build
    assert "test:0.1.1" not in " ".join(build_c[0][0])
    exp_run = "docker -D run --entrypoint=/bin/sh alt:0.1.1 -c printenv"
    assert " ".join(run_c[0][0]) == exp_run


def test_gear_build_debug(cli, mocker, tmp_path):
    (tmp_path / "Dockerfile").touch()
    manifest = mocker.patch("fw_cli.commands.gear.build.validate_manifest")
    run = mocker.patch("fw_cli.containers.run")
    manifest.return_value.get_docker_image_name_tag.return_value = "test:0.1.1"
    res = cli.run(f"--debug gear build {tmp_path}")
    assert res.exit_code == 0
    build_c = run.call_args_list[0]
    run_c = run.call_args_list[1]
    exp_build = f"docker -D build -t test:0.1.1 {tmp_path}"
    assert " ".join(build_c[0][0]) == exp_build
    exp_run = "docker -D run --entrypoint=/bin/sh test:0.1.1 -c printenv"
    assert " ".join(run_c[0][0]) == exp_run


def test_gear_build_docker_error(cli, mocker, tmp_path):
    (tmp_path / "Dockerfile").touch()
    manifest = mocker.patch("fw_cli.commands.gear.build.validate_manifest")
    mocker.patch("fw_cli.containers.shutil.which", return_value="valid")
    run = mocker.patch("fw_cli.containers.sp.run")
    run.return_value.returncode = 1
    manifest.return_value.get_docker_image_name_tag.return_value = "test:0.1.1"
    res = cli.run(f"gear build {tmp_path}")
    assert res.exit_code == 1
    assert "Exception while running command" in res.stderr
    assert "docker -D build -t test:0.1.1 ..." in res.stderr


######### Test update-env


@pytest.fixture(scope="function")
def default_env():
    def gen(valid=True, to_add=None):
        env = (
            f"PATH={PATH}\n".encode() + b"HOSTNAME=e3fa780ccc91\n"
            b"LANG=C.UTF-8\n"
            b"PYTHON_VERSION=3.8.12\n"
        )
        if to_add:
            env += to_add
        if valid:
            env += b"HOME=/root\n"
        else:
            env += b"HOME/root\n"
        return env

    return gen


@pytest.fixture(scope="function")
def setup_update_env(tmp_path, mocker):
    def run():
        # Mock out handle docker
        run = mocker.patch("fw_cli.commands.gear.build.get_container_client")
        # Create a dockerfile
        (tmp_path / "Dockerfile").touch()
        # Copy a default manifest
        shutil.copy(assets / "dcm2niix.json", tmp_path / "manifest.json")
        return tmp_path, run

    return run


# TODO hunt down and fix in flywheel-gears
# Traceback (most recent call last):
#   File ".../site-packages/gears/generator.py", line 33, in load_json_from_file
#     contents = open(path).read()
#                ^^^^^^^^^^^^^^^^^
# ResourceWarning: unclosed file <_io.TextIOWrapper ...>
@pytest.mark.filterwarnings("ignore:unclosed file <_io.TextIOWrapper")
def test_update_env_cant_parse_blocklist(setup_update_env, cli):
    tmp_path, _ = setup_update_env()
    res = cli.run(f"gear build  -i test=yes -i test2=maybe {tmp_path}")
    assert res.exit_code == 1
    assert "Please enter `ignore` in the format" in res.stderr


def test_update_env_dry_run(default_env, setup_update_env, cli):
    tmp_path, run = setup_update_env()
    run.return_value.run.return_value = CompletedProcess(
        args=[], returncode=0, stdout=default_env()
    )
    res = cli.run(f"gear build --env-dry-run {tmp_path}")
    assert res.exit_code == 0
    assert "Would write to manifest" in res.stdout
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
        assert manifest["environment"] == {
            "LD_LIBRARY_PATH": "/usr/lib/fsl/5.0",
            "FSLDIR": "/usr/share/fsl/5.0",
            "PATH": PATH,
            "FSLOUTPUTTYPE": "NIFTI_GZ",
        }


# Updated for GEAR-1912
def test_update_env_wet_run(default_env, setup_update_env, cli):
    tmp_path, run = setup_update_env()
    run.return_value.run.return_value = CompletedProcess(
        args=[],
        returncode=0,
        stdout=default_env(to_add=b"MY_VAL=CURRENT_VERSION=2.1.3\n"),
    )
    print(tmp_path)
    res = cli.run(f"gear build {tmp_path}")
    assert res.exit_code == 0
    assert "Successfully wrote environment" in res.stdout
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
        assert manifest["environment"] == {
            "PATH": PATH,
            "LANG": "C.UTF-8",
            "PYTHON_VERSION": "3.8.12",
            "LD_LIBRARY_PATH": "/usr/lib/fsl/5.0",
            "FSLDIR": "/usr/share/fsl/5.0",
            "FSLOUTPUTTYPE": "NIFTI_GZ",
            "MY_VAL": "CURRENT_VERSION=2.1.3",
        }


def test_skip_invalid_var(default_env, setup_update_env, cli):
    tmp_path, run = setup_update_env()
    run.return_value.run.return_value = CompletedProcess(
        args=[], returncode=0, stdout=default_env(valid=False)
    )
    res = cli.run(f"gear build {tmp_path}")
    assert res.exit_code == 0
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
        assert manifest["environment"] == {
            "PATH": PATH,
            "LANG": "C.UTF-8",
            "PYTHON_VERSION": "3.8.12",
            "LD_LIBRARY_PATH": "/usr/lib/fsl/5.0",
            "FSLDIR": "/usr/share/fsl/5.0",
            "FSLOUTPUTTYPE": "NIFTI_GZ",
        }
    assert "Skipping invalid env variable 'HOME/root'" in res.stdout


def test_cannot_parse_env(default_env, setup_update_env, cli):
    tmp_path, run = setup_update_env()
    env = default_env()
    env += b"\xff"
    run.return_value.run.return_value = CompletedProcess(
        args=[], returncode=0, stdout=env
    )
    res = cli.run(f"gear build {tmp_path}")
    assert res.exit_code == 0
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
        assert manifest["environment"] == {
            "LD_LIBRARY_PATH": "/usr/lib/fsl/5.0",
            "FSLDIR": "/usr/share/fsl/5.0",
            "PATH": PATH,
            "FSLOUTPUTTYPE": "NIFTI_GZ",
        }
    assert "Could not parse" in res.stdout
    assert "Not updating environment" in res.stdout


def test_cannot_get_output(setup_update_env, cli):
    tmp_path, run = setup_update_env()
    run.return_value.run.return_value = None
    res = cli.run(f"gear build {tmp_path}")
    assert res.exit_code == 1
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
        assert manifest["environment"] == {
            "LD_LIBRARY_PATH": "/usr/lib/fsl/5.0",
            "FSLDIR": "/usr/share/fsl/5.0",
            "PATH": PATH,
            "FSLOUTPUTTYPE": "NIFTI_GZ",
        }
    assert "Could not get environment output" in res.stdout


def test_complex_blocklist(default_env, setup_update_env, cli):
    tmp_path, run = setup_update_env()
    run.return_value.run.return_value = CompletedProcess(
        args=[], returncode=0, stdout=default_env()
    )
    res = cli.run(f"gear build -i PATH -i HOME=no -i PYTHON_VERSION=yes {tmp_path}")
    assert res.exit_code == 0
    assert "Successfully wrote environment" in res.stdout
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
        assert manifest["environment"] == {
            "PATH": PATH,
            "LANG": "C.UTF-8",
            "HOME": "/root",
            "LD_LIBRARY_PATH": "/usr/lib/fsl/5.0",
            "FSLDIR": "/usr/share/fsl/5.0",
            "FSLOUTPUTTYPE": "NIFTI_GZ",
        }


# Created for bug GEAR-2451
def test_env_missing_in_manifest(default_env, setup_update_env, cli):
    tmp_path, run = setup_update_env()
    run.return_value.run.return_value = CompletedProcess(
        args=[], returncode=0, stdout=default_env()
    )
    # Before running command, delete environment key from manifest
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
    del manifest["environment"]
    with open(tmp_path / "manifest.json", "w", encoding="utf-8") as fp:
        json.dump(manifest, fp, indent=2)
    res = cli.run(f"gear build -i PATH -i HOME=no -i PYTHON_VERSION=yes {tmp_path}")
    # Should still exit with code 0 and success
    assert res.exit_code == 0
    assert "Successfully wrote environment" in res.stdout
