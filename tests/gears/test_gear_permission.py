import pytest


@pytest.fixture
def gear_perms(api):
    def resp(enabled=True):
        version = {
            "_id": "version",
            "release": "16.4.3",
            "flywheel_release": "16.4.4",
            "applied_fixes": {},
        }
        if enabled:
            version["applied_fixes"]["fix_add_gear_series_for_all_gears"] = (
                "2019-02-08T00:02:42.873000+00:00"
            )
        api.add_response("/api/version", version)
        return api

    return resp


def test_gear_perms_disabled(gear_perms, cli):
    gear_perms(False)
    res = cli.run("gear permission test")
    assert res.exit_code == 1
    expected = "Gear permissions not supported on Flywheel version 16.4.4"
    assert expected in res.stderr


def test_gear_perms_does_nothing(gear_perms, cli):
    gear_perms()
    res = cli.run("gear permission")
    assert res.exit_code == 2
    assert "Missing argument 'GEAR_NAME'" in res.stderr


def test_gear_perms_add_delete_err(gear_perms, cli):
    gear_perms()
    res = cli.run("gear permission -a -d test")
    assert res.exit_code == 1
    assert "Only one of (add | delete | reset) may be used at a time" in res.stderr


@pytest.mark.parametrize("perms", [None, {"users": [], "projects": []}])
def test_gear_perms_show_empty(gear_perms, cli, perms):
    api = gear_perms()
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "test",
            "permissions": perms,
            "created": "2022-03-22T19:51:21.464000+00:00",
            "modified": "2022-04-19T14:43:46.054000+00:00",
        },
    )
    res = cli.run("gear permission test")
    assert res.exit_code == 0
    if isinstance(perms, dict):
        assert "Users" in res.stdout
    else:
        assert "No permissions set on gear 'test'" in res.stdout


def test_gear_perms_show(gear_perms, cli):
    api = gear_perms()
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": {
                "users": ["naterichman@flywheel.io"],
                "projects": ["6203198dd1b5b5f8ef99f333"],
            },
            "created": "2022-03-22T19:51:21.464000+00:00",
            "modified": "2022-04-19T14:43:46.054000+00:00",
        },
    )
    api.add_response(
        "/api/projects/6203198dd1b5b5f8ef99f333",
        {
            "_id": "6203198dd1b5b5f8ef99f333",
            "label": "nifti-to-dicom",
            "parents": {"group": "nate"},
        },
    )
    res = cli.run("gear permission test")
    assert res.exit_code == 0
    assert "No permissions set on gear 'test'" not in res.stdout
    # Each permission is set
    assert "naterichman@flywheel.io" in res.stdout
    assert "6203198dd1b5b5f8ef99f333" in res.stdout


def test_gear_perms_update_add_success(gear_perms, cli):
    api = gear_perms()
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": {"users": [], "projects": []},
            "is_restricted": True,
        },
        method="GET",
    )
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": {"users": [], "projects": []},
            "is_restricted": True,
        },
        method="PUT",
    )
    api.add_response(
        "/api/resolve",
        {
            "children": [
                {"_id": "6203199bd1b5b5f8ef99f36f", "label": "indexing", "type": None}
            ],
            "path": [
                {"_id": "nate", "label": "nate"},
                {
                    "_id": "6203198dd1b5b5f8ef99f333",
                    "label": "test",
                },
            ],
        },
        method="POST",
    )
    api.add_response(
        "/api/projects/6203198dd1b5b5f8ef99f333",
        {
            "_id": "6203198dd1b5b5f8ef99f333",
            "label": "nifti-to-dicom",
            "parents": {"group": "nate"},
        },
    )
    api.add_response(
        "/api/gears/test/permissions",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": {
                "projects": ["6203198dd1b5b5f8ef99f333"],
                "users": ["test@example.io"],
            },
        },
        method="PUT",
    )
    res = cli.run("gear permission test -a -p test1/test2 -u test@example.io")
    assert res.exit_code == 0
    # Last request is a get on the project to look it up after changing
    # permissinos
    put = api.requests[-2]
    assert put["name"] == "PUT /api/gears/test/series"
    put = api.requests[-3]
    assert put["name"] == "PUT /api/gears/test/permissions"


def test_gear_perms_parse_input_lookup_not_found(gear_perms, cli):
    api = gear_perms()
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": {"users": [], "projects": [], "groups": []},
        },
    )
    api.add_response(
        "/api/resolve",
        status=404,
        method="POST",
    )
    res = cli.run("gear permission -a -p test/ test")
    assert res.exit_code == 1
    assert "Could not find project 'test/" in res.stderr


def test_gear_perms_parse_input_lookup_unknown_failure(gear_perms, cli):
    api = gear_perms()
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": {"users": [], "projects": []},
        },
    )
    api.add_response(
        "/api/resolve",
        status=418,
        method="POST",
    )
    res = cli.run("gear permission -a -p test/ test")
    assert res.exit_code == 1
    assert "Error looking up project:" in res.stderr
    assert "I'M A TEAPOT" in res.stderr


def test_gear_perms_parse_input_lookup_returns_nothing(gear_perms, cli):
    api = gear_perms()
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": {"users": [], "projects": []},
        },
    )
    api.add_response(
        "/api/resolve",
        {"path": []},
        method="POST",
    )
    res = cli.run("gear permission -a -p test/ test")
    assert res.exit_code == 1
    assert "No project returned for path 'test/'" in res.stderr


def test_gear_perms_fails(gear_perms, cli):
    api = gear_perms()
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": {
                "projects": ["6203198dd1b5b5f8ef99f333"],
                "users": ["test@example.io"],
            },
        },
    )
    api.add_response(
        "/api/projects/6203198dd1b5b5f8ef99f333",
        {
            "_id": "6203198dd1b5b5f8ef99f333",
            "label": "nifti-to-dicom",
            "parents": {"group": "nate"},
        },
    )
    api.add_response(
        "/api/gears/test/permissions",
        status=418,
        method="PUT",
    )
    res = cli.run("gear permission test -d -p 6203198dd1b5b5f8ef99f333")
    put = api.requests[-1]
    assert put["name"] == "PUT /api/gears/test/permissions"
    assert put["json"] == {
        "projects": [],
        "users": ["test@example.io"],
    }

    assert res.exit_code == 1
    assert "Could not update gear permissions" in res.stderr
    assert "I'M A TEAPOT" in res.stderr


@pytest.mark.parametrize("is_restricted", [False, True])
def test_gear_perms_reset(gear_perms, cli, is_restricted):
    api = gear_perms()
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": {
                "projects": ["6203198dd1b5b5f8ef99f333"],
                "users": ["test@example.io"],
            },
            "is_restricted": True,
        },
    )
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": None,
            "is_restricted": is_restricted,
        },
        method="PUT",
    )
    api.add_response(
        "/api/projects/6203198dd1b5b5f8ef99f333",
        {
            "_id": "6203198dd1b5b5f8ef99f333",
            "label": "nifti-to-dicom",
            "parents": {"group": "nate"},
        },
    )
    api.add_response(
        "/api/gears/test/permissions",
        method="DELETE",
    )
    res = cli.run("gear permission -r test")
    if is_restricted:
        assert res.exit_code == 1
        assert "Failed to correctly set gear" in res.stderr
    else:
        assert res.exit_code == 0
        assert "Resetting" in res.stdout
        assert "No permissions" in res.stdout


def test_gear_perms_reset_err(gear_perms, cli):
    api = gear_perms()
    api.add_response(
        "/api/gears/test/series",
        {
            "_id": "625ecaa2e3896fb14e70f56f",
            "name": "nifti-to-dicom",
            "permissions": {
                "projects": ["6203198dd1b5b5f8ef99f333"],
                "users": ["test@example.io"],
            },
            "is_restricted": True,
        },
    )
    api.add_response(
        "/api/gears/test/permissions",
        status=418,
        method="DELETE",
    )

    res = cli.run("gear permission -r test")
    assert res.exit_code == 1
    assert "Could not reset gear permissions" in res.stderr
