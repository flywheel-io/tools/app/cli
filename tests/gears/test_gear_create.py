import json
import subprocess

import pytest

from fw_cli.commands.gear.create import get_author


@pytest.fixture
def create_args():
    def gen(
        label="My Gear",
        name="my-gear",
        author="Flywheel <support@flywheel.io>",
        image="python:3.8-buster",
        description="test",
        cite="test",
        sdk_enabled=True,
        template="basic",
        category="utility",
    ):
        cmd = ""
        if label:
            cmd += f"-l '{label}' "
        if name:
            cmd += f"-n '{name}' "
        if author:
            cmd += f"-a '{author}' "
        if image:
            cmd += f"-i {image} "
        if description:
            cmd += f"-d '{description}' "
        if cite:
            cmd += f"--cite '{cite}' "
        if sdk_enabled:
            cmd += "-s "
        if template:
            cmd += f"-t {template} "
        if category:
            cmd += f"-c {category} "
        return cmd

    return gen


def test_exits_if_manifest(tmp_path, create_args, cli):
    (tmp_path / "manifest.json").touch()
    res = cli.run(f"gear create {tmp_path} {create_args()}")
    assert res.exit_code == 0
    assert "Manifest already exists" in res.stdout


def test_invalid_name(tmp_path, create_args, cli):
    res = cli.run(f"gear create {tmp_path} {create_args(name='Test gear')}")
    assert res.exit_code == 1
    assert "Gear name is invalid" in res.stderr


def test_invalid_label(tmp_path, create_args, cli):
    res = cli.run(f"gear create {tmp_path} {create_args(label='Test$ gear')}")
    assert res.exit_code == 1
    assert "Gear label is invalid" in res.stderr


def test_invalid_template(tmp_path, create_args, cli):
    res = cli.run(f"gear create {tmp_path} {create_args(template='na')}")
    assert res.exit_code == 1
    assert "Unknown template na" in res.stderr


def test_invalid_category(tmp_path, create_args, cli):
    res = cli.run(f"gear create {tmp_path} {create_args(category='na')}")
    assert res.exit_code == 1
    assert "Unknown category na" in res.stderr


def test_basic_template_full(tmp_path, create_args, cli):
    res = cli.run(f"gear create {tmp_path} {create_args()}")
    assert res.exit_code == 0
    assert "Using docker image 'python:3.8-buster' with template 'basic'" in res.stdout
    assert "Done, now build your gear with `gear build`" in res.stdout
    for file_ in ["Dockerfile", "run.py", "requirements.txt", "manifest.json"]:
        assert (tmp_path / file_).is_file()
    with open(tmp_path / "Dockerfile", "r", encoding="utf-8") as fp:
        assert fp.readlines()[0] == "FROM python:3.8-buster as base\n"
    with open(tmp_path / "requirements.txt", "r", encoding="utf-8") as fp:
        assert fp.readlines()[0] == "flywheel-gear-toolkit\n"
    with open(tmp_path / "run.py", "r", encoding="utf-8") as fp:
        assert fp.readlines()[0] == "#!/usr/bin/env python\n"
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
    assert manifest["label"] == "My Gear"
    assert manifest["name"] == "my-gear"
    assert manifest["author"] == "Flywheel <support@flywheel.io>"
    assert manifest["description"] == "test"
    assert manifest["cite"] == "test"
    assert manifest["license"] == "MIT"
    assert manifest["custom"]["gear-builder"] == {
        "image": "my-gear:0.1.0",
        "category": "utility",
    }


@pytest.mark.parametrize(
    "name,email,out",
    [
        (b"Test\n", b"test@email.com\n", "Test <test@email.com>"),
        (b"", b"test@email.com\n", "Firstname Lastname <email@example.com>"),
        (b"Test\n", b"", "Firstname Lastname <email@example.com>"),
    ],
)
def test_author(name, email, out, mocker):
    def _git(*args):
        if "user.name" in args[0]:
            return name
        return email

    sp = mocker.patch("fw_cli.commands.gear.create.sp.check_output")
    sp.side_effect = _git
    res = get_author()
    assert res == out


def test_author_git_config_raises(mocker):
    sp = mocker.patch("fw_cli.commands.gear.create.sp.check_output")
    sp.side_effect = subprocess.CalledProcessError(returncode=1, cmd="git")
    res = get_author()
    assert res == "Firstname Lastname <email@example.com>"


def test_author_default_no_git(mocker):
    have = mocker.patch("fw_cli.commands.gear.create.shutil.which")
    have.return_value = None
    res = get_author()
    assert res == "Firstname Lastname <email@example.com>"


def test_missing_image_prompts(tmp_path, create_args, cli):
    args = create_args(image=None)
    res = cli.run(f"gear create {tmp_path} {args}", input="neurodebian:xenial")
    assert res.exit_code == 0
    with open(tmp_path / "Dockerfile", "r", encoding="utf-8") as fp:
        assert fp.readlines()[0] == "FROM neurodebian:xenial as base\n"


def test_missing_image_prompt_sets_default(tmp_path, create_args, cli):
    args = create_args(image=None)
    res = cli.run(f"gear create {tmp_path} {args}", input="\n")
    assert res.exit_code == 0
    with open(tmp_path / "Dockerfile", "r", encoding="utf-8") as fp:
        assert fp.readlines()[0] == "FROM python:3.8-buster as base\n"


def test_missing_template_prompts(tmp_path, create_args, cli):
    args = create_args(template=None)
    res = cli.run(f"gear create {tmp_path} {args}", input="basic")
    assert res.exit_code == 0
    with open(tmp_path / "Dockerfile", "r", encoding="utf-8") as fp:
        assert fp.readlines()[0] == "FROM python:3.8-buster as base\n"


def test_missing_category_prompts(tmp_path, create_args, cli):
    args = create_args(category=None)
    res = cli.run(f"gear create {tmp_path} {args}", input="analysis")
    assert res.exit_code == 0
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
    assert manifest["custom"]["gear-builder"] == {
        "image": "my-gear:0.1.0",
        "category": "analysis",
    }


def test_git_template_no_git(tmp_path, create_args, cli, mocker):
    which = mocker.patch("fw_cli.commands.gear.utils.shutil.which")
    which.return_value = None
    args = create_args(template="flywheel_poetry")
    res = cli.run(f"gear create {tmp_path} {args}")
    assert res.exit_code == 1
    assert "Cannot use this template without `git` installed" in res.stderr


def test_git_template_dir_exists(tmp_path, create_args, cli):
    (tmp_path / "templates").mkdir()
    args = create_args(template="flywheel_poetry")
    res = cli.run(f"gear create {tmp_path} {args}")
    assert res.exit_code == 1
    assert f"Destination {tmp_path} already contains template folder" in res.stderr


def test_git_template_git_error(tmp_path, create_args, cli, mocker):
    sp = mocker.patch("fw_cli.commands.gear.create.run_cmd")

    def conditional_error(args):
        if args[0] == "git":
            raise subprocess.CalledProcessError(returncode=1, cmd=args)

    sp.side_effect = conditional_error
    args = create_args(template="flywheel_poetry")
    res = cli.run(f"gear create {tmp_path} {args}")
    assert res.exit_code == 1
    assert "Could not clone repository" in res.stderr


def test_git_template(tmp_path, create_args, cli):
    args = create_args(template="flywheel_poetry")
    res = cli.run(f"gear create {tmp_path} {args}")
    assert res.exit_code == 0
    assert (
        "Using docker image 'python:3.8-buster' with template 'flywheel_poetry'"
    ) in res.stdout
    assert "Done, now build your gear with `gear build`" in res.stdout
    for file_ in [
        "Dockerfile",
        "run.py",
        "pyproject.toml",
        "manifest.json",
        "CONTRIBUTING.md",
        "README.md",
        ".pre-commit-config.yaml",
        "fw_gear_my_gear/main.py",
        "tests/test_main.py",
    ]:
        assert (tmp_path / file_).is_file()
    with open(tmp_path / "Dockerfile", "r", encoding="utf-8") as fp:
        assert fp.readlines()[0] == "FROM python:3.8-buster as base\n"
    with open(tmp_path / "pyproject.toml", "r", encoding="utf-8") as fp:
        assert fp.readlines()[1] == 'name = "fw-gear-my-gear"\n'
    with open(tmp_path / "run.py", "r", encoding="utf-8") as fp:
        assert fp.readlines()[0] == "#!/usr/bin/env python\n"
    with open(tmp_path / "manifest.json", "r", encoding="utf-8") as fp:
        manifest = json.load(fp)
    assert manifest["label"] == "My Gear"
    assert manifest["name"] == "my-gear"
    assert manifest["author"] == "Flywheel <support@flywheel.io>"
    assert manifest["description"] == "test"
    assert manifest["cite"] == "test"
    assert manifest["license"] == "MIT"
    assert manifest["custom"]["gear-builder"] == {
        "image": "my-gear:0.1.0",
        "category": "utility",
    }
