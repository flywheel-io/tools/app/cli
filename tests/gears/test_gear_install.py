import base64

import pytest
from fw_client import ClientError

from fw_cli.containers import ContainerException
from fw_cli.exchange.interface import ExchangeInterace

GITLAB_ENDPT = "/api/v4/projects/36522442/repository/files"


@pytest.fixture
def gitlab(http_testserver):
    ExchangeInterace._ept = http_testserver.url + GITLAB_ENDPT
    return http_testserver


@pytest.fixture
def get_file(gitlab):
    gitlab.add_response(
        url=GITLAB_ENDPT + "/test-gear-0",
        body={
            "file_name": "flywheel-test-gear-sha384-123.json",
            "file_path": "manifests/flywheel/flywheel-test-gear-sha384-123.json",
            "encoding": "base64",
            "content": "eyJnZWFyIjogeyJuYW1lIjogInRlc3QtZ2VhciIsICJ2ZXJzaW9uIjogIjEuMC4wIn19",
        },
        method="GET",
    )
    gitlab.add_response(
        url=GITLAB_ENDPT + "/test-gear-1",
        body={
            "file_name": "flywheel-test-gear-sha384-234.json",
            "file_path": "manifests/flywheel/flywheel-test-gear-sha384-234.json",
            "encoding": "base64",
            "content": "eyJnZWFyIjogeyJuYW1lIjogInRlc3QtZ2VhciIsICJ2ZXJzaW9uIjogIjEuMC4xIn19",
        },
        method="GET",
    )
    gitlab.add_response(
        url=GITLAB_ENDPT + "/private-gear-1",
        body={
            "file_name": "flywheel-private-gear-sha384-123.json",
            "file_path": "manifests/flywheel/flywheel-private-gear-sha384-123.json",
            "encoding": "base64",
            "content": "ewogICAgImdlYXIiOiB7CiAgICAgICAgIm5hbWUiOiAicHJpdmF0ZS1nZWFyIiwKICAgICAgICAidmVyc2lvbiI6ICIxLjAuMCIsCiAgICAgICAgImN1c3RvbSI6IHsKICAgICAgICAgICAgImdlYXItYnVpbGRlciI6IHsKICAgICAgICAgICAgICAgICJpbWFnZSI6ICJmbHl3aGVlbC9wcml2YXRlLWdlYXI6MS4wLjAiCiAgICAgICAgICAgIH0sCiAgICAgICAgICAgICJmbHl3aGVlbCI6IHsKICAgICAgICAgICAgICAgICJwcml2YXRlIjogdHJ1ZQogICAgICAgICAgICB9CiAgICAgICAgfQogICAgfQp9=====",
        },
        method="GET",
    )


@pytest.fixture
def get_exc(gitlab):
    gitlab.add_response(
        url=GITLAB_ENDPT + "/exchange.json",
        body={
            "content": base64.b64encode(
                b"""[
                        [
                            {
                                "name": "private-gear",
                                "version": "1.0.0",
                                "exchange-path": "private-gear-1",
                                "latest": "true",
                                "custom":
                                    {
                                        "gear-builder":
                                            {
                                                "image": "imagepath"
                                            }
                                    },
                                "customer":
                                    {
                                        "flywheel":
                                            {
                                                "public": "false"
                                            }
                                    }
                            }
                        ],
                        [
                            {
                                "name": "test-gear",
                                "version": "1.0.1",
                                "exchange-path": "test-gear-1",
                                "latest": true,
                                "custom":
                                    {
                                        "gear-builder":
                                            {
                                                "category": "utility"
                                            }
                                    }
                            },
                            {
                                "name": "test-gear",
                                "version": "1.0.0",
                                "exchange-path": "test-gear-0",
                                "latest": false
                            }
                        ]
                    ]"""
            ).decode("utf-8")
        },
        method="GET",
    )


@pytest.fixture
def installed(mocker):
    m = mocker.patch("fw_cli.commands.gear.install.get_gears_by_filter")
    m.return_value = True


@pytest.fixture
def new_gear(mocker):
    m = mocker.patch("fw_cli.commands.gear.install.get_gears_by_filter")
    m.return_value = False


@pytest.fixture
def upload(api):
    api.add_response(
        url="/api/gears/test-gear", body={"ok": True}, status=200, method="POST"
    )


@pytest.fixture
def confirm(mocker):
    m = mocker.patch("fw_cli.commands.gear.install.confirm")
    m.return_value = True


def test_gear_install(cli, get_file, get_exc, new_gear, upload, confirm):
    runscript = "gear install -n test-gear -v 1.0.0 -c utility --yes"
    res = cli.run(runscript)
    assert "succeeded" in res.stdout


def test_gear_install_prompt_category(
    cli, get_file, get_exc, new_gear, upload, confirm, mocker
):
    m = mocker.patch("fw_cli.commands.gear.install.prompt")
    m.return_value = "utility"
    runscript = "gear install -n test-gear -v 1.0.0"
    res = cli.run(runscript)
    assert "succeeded" in res.stdout


def test_gear_install_no_category(
    cli, get_file, get_exc, new_gear, upload, confirm, mocker
):
    m = mocker.patch("fw_cli.commands.gear.install.prompt")
    m.return_value = "utility"
    runscript = "gear install -n test-gear -v 1.0.1"
    res = cli.run(runscript)
    assert "succeeded" in res.stdout


def test_gear_install_upload_failed(cli, get_file, get_exc, new_gear, api, confirm):
    api.add_response(
        "/api/gears/test-gear", body={"ok": False}, status=404, method="POST"
    )
    runscript = "gear install -n test-gear -v 1.0.0 -c utility"
    res = cli.run(runscript)
    assert "failed" in res.stderr


def test_gear_install_dont_confirm_upload(cli, get_exc, get_file, new_gear, mocker):
    m = mocker.patch("fw_cli.commands.gear.install.confirm")
    m.return_value = False
    runscript = "gear install -n test-gear -v 1.0.0 -c utility"
    res = cli.run(runscript)
    assert "Abandoned" in res.stderr


def test_gear_install_not_found_version(cli, get_exc, get_file, new_gear):
    runscript = "gear install -n test-gear -v 1.2.0"
    res = cli.run(runscript)
    assert "Failed to find manifest" in res.stderr


def test_gear_install_not_found_name(cli, get_exc, new_gear):
    runscript = "gear install -n testing-gear -v 1.0.0"
    res = cli.run(runscript)
    assert "Failed to find manifest" in res.stderr


def test_gear_install_name_mismatch(cli, get_exc, get_file, new_gear, upload):
    runscript = "gear install -n test-ge -v 1.0.0"
    res = cli.run(runscript)
    assert "Failed to find manifest" in res.stderr


def test_gear_install_already_installed(cli, installed):
    runscript = "gear install -n test-gear -v 1.0.0 -c utility"
    res = cli.run(runscript)
    assert "is already installed" in res.stdout


def test_gear_install_get_exchange_json_err(cli, new_gear, gitlab):
    gitlab.add_response(GITLAB_ENDPT + "/exchange.json", status=404, method="GET")
    runscript = "gear install -n test-gear -v 1.0.0 -c utility"
    res = cli.run(runscript)
    assert "Error in downloading exchange metadata" in res.stderr


def test_gear_install_get_file_err(cli, new_gear, get_exc, gitlab):
    gitlab.add_response(GITLAB_ENDPT + "/test-gear-0", status=404, method="GET")
    runscript = "gear install -n test-gear -v 1.0.0 -c utility"
    res = cli.run(runscript)
    assert "Error in downloading gear manifest" in res.stderr


def test_gear_install_gear_empty_file(cli, new_gear, get_exc, gitlab):
    gitlab.add_response(GITLAB_ENDPT + "/test-gear-0", body={}, method="GET")
    runscript = "gear install -n test-gear -v 1.0.0 -c utility"
    res = cli.run(runscript)
    print(res.stderr)
    print(res.stdout)
    assert "Error in decoding file from exchange" in res.stderr


def test_gear_install_gear_name_mismatch(cli, new_gear, get_exc, gitlab):
    gitlab.add_response(
        GITLAB_ENDPT + "/test-gear-0",
        body={
            "file_name": "flywheel-test-gear-sha384-123.json",
            "file_path": "manifests/flywheel/flywheel-test-gear-sha384-123.json",
            "encoding": "base64",
            "content": "ghjgjfjgfhgjfhgfh",
        },
        method="GET",
    )

    runscript = "gear install -n test-gear -v 1.0.0 -c utility"
    res = cli.run(runscript)
    assert "Error in decoding file from exchange" in res.stderr


def test_gear_install_latest(cli, new_gear, get_exc, get_file):
    runscript = "gear install -n test-gear -v latest -c utility"
    res = cli.run(runscript)
    assert "Upload to site?" in res.stdout
    assert "1.0.1" in res.stdout


def test_gear_install_latest_already_installed(cli, installed, get_exc, get_file):
    runscript = "gear install -n test-gear -v latest -c utility"
    res = cli.run(runscript)
    assert "gear is already installed" in res.stdout


def test_gear_install_private_gear(cli, get_exc, get_file, new_gear, mocker):
    m = mocker.patch("fw_cli.commands.gear.install.DockerClient")
    m.pull.return_value = None
    mocker.patch("fw_cli.commands.gear.install.upload_gear")

    runscript = "gear install -n private-gear -v latest -c utility --yes"
    res = cli.run(runscript)
    assert "succeeded" in res.stdout


@pytest.mark.parametrize("err_msg", ["docker login", "random error"])
def test_gear_install_private_gear_img_pull_err(
    cli, get_exc, get_file, new_gear, mocker, err_msg
):
    m = mocker.patch("fw_cli.commands.gear.install.DockerClient")
    m.return_value.pull.side_effect = ContainerException(
        "", r_code=1, out="", err=err_msg
    )

    runscript = "gear install -n private-gear -v latest -c utility --yes"
    res = cli.run(runscript)
    print(res.stderr)
    assert "Failed" in res.stderr
    assert err_msg in res.stderr


def test_gear_private_install_upload_err(cli, get_exc, get_file, new_gear, mocker):
    m = mocker.patch("fw_cli.commands.gear.install.DockerClient")
    m.pull.return_value = None
    u = mocker.patch("fw_cli.commands.gear.install.upload_gear")
    u.side_effect = ClientError("Error", request=None, response=None)

    runscript = "gear install -n private-gear -v latest -c utility --yes"
    res = cli.run(runscript)
    assert "failed" in res.stderr
