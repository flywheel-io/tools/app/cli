"""Test import."""

import copy
import gzip
import json
import re
from datetime import datetime, timedelta

import fw_utils
import pytest
import yaml
from fw_utils import assert_like, format_datetime, get_datetime

ID_0 = "000000000000000000000000"
DT_RE = r"\d\d/\d\d/\d\d\d\d \d\d:\d\d:\d\d"

P_ID = "000000000000000000000001"
S_ID = "000000000000000000000002"
I_ID = "000000000000000000000003"
SC_ID = "000000000000000000000004"
BS_ID = "000000000000000000000005"
B_ID = "000000000000000000000006"
PRJ = {"_id": P_ID, "label": "Project", "parents": {"group": "group"}}
STORAGE = {"config": {"type": "s3"}, "url": "s3://bucket"}
IMP_URL = f"/xfer/imports/{I_ID}"
IMP = {
    "_id": I_ID,
    "label": "Import",
    "status": "running",
    "total_time": 10,
    "rules": [{}],
    "refs": {"project": P_ID, "storage": S_ID, "report": None},
    "project": PRJ,
    "storage": STORAGE,
}
META = "{path}={sub}/{ses}/{acq}/*"
STATS_0 = {
    "status": "pending",
    "total_time": 0,
    "total_files": 0,
    "total_bytes": 0,
    "processed_files": 0,
    "processed_bytes": 0,
    "completed_files": 0,
    "completed_bytes": 0,
}
STATS_1 = {
    "status": "running",
    "stage": "processing",
    "total_time": 10,
    "total_files": 4,
    "total_bytes": 20,
    "processed_files": 2,
    "processed_bytes": 10,
    "completed_files": 2,
    "completed_bytes": 10,
    "bytes_per_sec": 1,
}
STATS_2 = {
    "status": "success",
    "total_time": 20,
    "total_files": 4,
    "total_bytes": 20,
    "processed_files": 4,
    "processed_bytes": 20,
    "completed_files": 3,
    "completed_bytes": 15,
    "skipped_files": 1,
    "skipped_bytes": 5,
}
SSE_HDR = {"content-type": "text/event-stream"}
PROG_0 = {
    "status": "pending",
    "elapsed_time": 0,
    "totals": {
        "discovered_files": 0,
        "discovered_bytes": 0,
        "success_files": 0,
        "success_bytes": 0,
        "skipped_files": 0,
        "skipped_bytes": 0,
        "failed_files": 0,
        "failed_bytes": 0,
        "processed_files": 0,
        "processed_bytes": 0,
    },
    "current_speed": {
        "files_per_sec": None,
        "bytes_per_sec": None,
    },
}
PROG_1 = {
    "status": "running",
    "stage": "processing",
    "elapsed_time": 10,
    "totals": {
        "discovered_files": 4,
        "discovered_bytes": 20,
        "success_files": 2,
        "success_bytes": 10,
        "skipped_files": 0,
        "skipped_bytes": 0,
        "failed_files": 0,
        "failed_bytes": 0,
        "processed_files": 2,
        "processed_bytes": 10,
    },
    "current_speed": {
        "files_per_sec": 0.2,
        "bytes_per_sec": 1,
    },
}
PROG_2 = {
    "status": "success",
    "elapsed_time": 20,
    "totals": {
        "discovered_files": 5,
        "discovered_bytes": 26,
        "success_files": 3,
        "success_bytes": 15,
        "skipped_files": 1,
        "skipped_bytes": 5,
        "failed_files": 0,
        "failed_bytes": 0,
        "processed_files": 5,
        "processed_bytes": 26,
        "review_files": 1,
        "review_bytes": 5,
    },
    "current_speed": {
        "files_per_sec": None,
        "bytes_per_sec": None,
    },
}
SCHEDULE = {
    "_id": SC_ID,
    "active": True,
    "start": "2022-01-01",
    "end": "2022-01-01",
    "cron": "* * * * *",
    "operation": {
        "type": "import",
        "label": "abc",
        "refs": {"project": P_ID, "storage": S_ID},
    },
    "origin": "cli",
    "project": PRJ,
    "storage": STORAGE,
}
SCHEDULE_PAGE = {"results": [SCHEDULE]}
BLOB_SESSION = {"_id": BS_ID}
BLOB = {"_id": B_ID}


@pytest.fixture
def api(api):
    def schedule_patch():
        response = SCHEDULE.copy()
        response.update(api.request.json)
        return response

    api.add_response(f"/api/projects/{P_ID}", PRJ)
    api.add_response(f"/xfer/storages/{S_ID}", STORAGE)
    api.add_response("/xfer/imports", IMP, method="POST")
    api.add_response(IMP_URL, IMP)
    api.add_response(f"{IMP_URL}/progress2/sse", "", method="HEAD")
    api.add_response(f"{IMP_URL}/progress2/sse", prog2_sse(PROG_1), headers=SSE_HDR)
    api.add_response("/xfer/schedules", SCHEDULE_PAGE, method="GET")
    api.add_response("/xfer/schedules", SCHEDULE, method="POST")
    api.add_response(f"/xfer/schedules/{SC_ID}", SCHEDULE)
    api.add_callback(f"/xfer/schedules/{SC_ID}", schedule_patch, methods=["PATCH"])
    return api


def test_run_import_with_invalid_project(cli):
    result = cli.run(f"import run -pprj -s{S_ID} -m{META}")
    assert result.exit_code == 1
    assert "Error: Invalid project path/id" in result.stderr


def test_run_import_with_invalid_project_not_exists(cli):
    result = cli.run(f"import run -p{ID_0} -s{S_ID} -m{META}")
    assert result.exit_code == 1
    assert "Error: Could not find project" in result.stderr


def test_run_import_with_invalid_storage(cli):
    result = cli.run(f"import run -p{P_ID} -sfoo -m{META}")
    assert result.exit_code == 1
    assert "Error: Invalid storage id" in result.stderr


def test_run_import_with_invalid_level(cli):
    result = cli.run(f"import run -p{P_ID} -s{S_ID} -m{META} --limit=-1")
    assert result.exit_code == 2
    expected = "Error: Invalid value for '--limit': Positive integer expected"
    assert expected in result.stderr


def test_run_import_with_invalid_fail_fast(cli):
    result = cli.run(f"import run -p{P_ID} -s{S_ID} -m{META} --fail-fast=-1")
    assert result.exit_code == 2
    expected = "Error: Invalid value for '--fail-fast': Invalid number/percent"
    assert expected in result.stderr


def test_run_import_with_defaults(cli, api, import_post):
    result = cli.run(f"import run -p {P_ID} -s {S_ID} --no-wait")
    assert result.exit_code == 0
    assert_like(import_post, api.request_map["POST /xfer/imports"].json)
    assert result.stdout == "Import 000000000000000000000003 started\n"


def test_run_import_yaml(cli, api, import_post):
    result = cli.run(f"import run -p {P_ID} -s {S_ID} --no-wait --output yaml")
    assert_like(import_post, api.request_map["POST /xfer/imports"].json)
    assert result.exit_code == 0
    assert result.stdout[0] != "{"
    assert_like(IMP, yaml.safe_load(result.stdout))


def test_run_import_json(cli, api, import_post):
    result = cli.run(f"import run -p {P_ID} -s {S_ID} --no-wait --output json")
    assert_like(import_post, api.request_map["POST /xfer/imports"].json)
    assert result.exit_code == 0
    assert result.stdout[0] == "{"
    assert_like(IMP, json.loads(result.stdout))


# pre FLYW-24304 backward compat
def test_run_import_with_defaults_compat(cli, api, import_post):
    def post_import():
        payload = api.request.get_json()
        if not len(payload.get("rules", [])):
            err_msg = "List should have at least 1 item after validation, not 0"
            return {"message": f"'loc': ('body', 'rules'), 'msg': '{err_msg}'"}, 422
        return IMP, 200

    api.add_callback("/xfer/imports", post_import, methods=["POST"])

    result = cli.run(f"import run -p {P_ID} -s {S_ID} --no-wait")
    assert result.exit_code == 0
    import_post["rules"] = [{}]
    assert_like(import_post, api.request_map["POST /xfer/imports"].json)


def test_run_import_with_storage_override(cli, api, import_post):
    import_post["storage_override"] = {"type": "s3", "prefix": "test"}
    cmd = f"import run -p{P_ID} -s{S_ID} --no-wait --storage-config prefix:test"
    result = cli.run(cmd)
    assert result.exit_code == 0
    assert_like(import_post, api.request_map["POST /xfer/imports"].json)


def test_run_import_with_conflict_strategy(cli, api, import_post):
    import_post["conflict_strategy"] = "update"
    cmd = f"import run -p{P_ID} -s{S_ID} --no-wait --conflict-strategy update"
    result = cli.run(cmd)
    assert result.exit_code == 0
    payload = api.request_map["POST /xfer/imports"].json
    assert_like(import_post, payload)
    assert payload["conflict_strategy"] == "update"


def test_run_import_with_dicom_mappings(cli, api, import_post):
    cmd = f"import run -p{P_ID} -s{S_ID} --no-wait -tdicom -mPatientName=sub"
    import_post["rules"] = [
        {"type": "dicom", "mappings": [["{PatientName}", "{subject.label}"]]},
    ]
    result = cli.run(cmd)
    assert result.exit_code == 0
    assert_like(import_post, api.request_map["POST /xfer/imports"].json)


def test_run_import_with_failure(cli, api):
    prog = copy.deepcopy(PROG_0)
    prog["status"] = "failed"
    prog["elapsed_time"] = 20
    prog["totals"].update(
        {
            "discovered_files": 4,
            "discovered_bytes": 20,
            "processed_files": 4,
            "processed_bytes": 20,
            "success_files": 3,
            "success_bytes": 15,
            "failed_files": 1,
            "failed_bytes": 5,
        }
    )
    api.add_response(f"{IMP_URL}/progress2/sse", "", method="HEAD")
    api.add_response(f"{IMP_URL}/progress2/sse", prog2_sse(prog), headers=SSE_HDR)
    result = cli.run(f"import run -p {P_ID} -s {S_ID} -m {META}")
    assert result.exit_code == 1
    assert result.stdout == (
        "ID 000000000000000000000003   failed  20s\n"
        "Storage s3://bucket\n"
        "Project fw://group/Project\n"
        "Import ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100%\n"
        "Discovered         4 files        20B\n"
        "Processed          4 files        20B\n"
        "Success            3 files        15B\n"
        "Failed              1 file         5B"
    )


def test_run_import_with_invalid_yaml(cli, api, import_post):
    rule = 'include:["path=~a"],mappings:["{path}={sub}"],type:x,zip:true'
    import_post.setdefault("rules", []).append(
        {
            "include": ["path=~a"],
            "mappings": [["{path}", "{subject.label}"]],
            "type": "x",
            "zip": True,
        }
    )
    result = cli.run(f"import run -p {P_ID} -s {S_ID} --rule '{rule}' --no-wait")
    assert result.exit_code == 0
    assert_like(import_post, api.request_map["POST /xfer/imports"].json)


def test_get_import_text(cli, api):
    prog = copy.deepcopy(PROG_1)
    prog["totals"].update(
        {
            "discovered_files": 4,
            "discovered_bytes": 20,
            "processed_files": 2,
            "processed_bytes": 10,
            "success_files": 2,
            "success_bytes": 10,
        }
    )
    sse = prog2_sse(prog)
    api.add_response(f"{IMP_URL}/progress2/sse", "", method="HEAD")
    api.add_response(f"{IMP_URL}/progress2/sse", sse, headers=SSE_HDR)
    result = cli.run(f"import get {I_ID}")
    assert result.exit_code == 0
    assert result.stdout == (
        "ID 000000000000000000000003   running  10s - processing ...\n"
        "Storage s3://bucket\n"
        "Project fw://group/Project\n"
        "Discovered         4 files        20B\n"
        "Processed          2 files        10B\n"
        "Success            2 files        10B"
    )


def test_get_import_wait(cli, api):
    prog = copy.deepcopy(PROG_2)
    prog["totals"].update(
        {
            "review_files": 0,
            "review_bytes": 0,
            "processed_files": 4,
            "processed_bytes": 20,
            "discovered_files": 4,
            "discovered_bytes": 20,
        }
    )
    sse = prog2_sse(PROG_0, PROG_1, prog)
    api.add_response(f"{IMP_URL}/progress2/sse", "", method="HEAD")
    api.add_response(f"{IMP_URL}/progress2/sse", sse, headers=SSE_HDR)
    result = cli.run(f"import get {I_ID} --wait")
    assert result.exit_code == 0
    assert result.stdout == (
        "ID 000000000000000000000003   success  20s\n"
        "Storage s3://bucket\n"
        "Project fw://group/Project\n"
        "Import ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100%\n"
        "Discovered         4 files        20B\n"
        "Processed          4 files        20B\n"
        "Success            3 files        15B\n"
        "Skipped             1 file         5B"
    )


def test_get_import_tree(cli, api):
    # TODO integrate progress2 into the api fixture (then drop compat/events/stats)
    # TODO improve test to not just cover progress2 handling, but verify output
    sse = prog2_sse(PROG_0, PROG_1, PROG_2)
    api.add_response(f"{IMP_URL}/progress2/sse", "", method="HEAD")
    api.add_response(f"{IMP_URL}/progress2/sse", sse, headers=SSE_HDR)
    result = cli.run(f"import get {I_ID} --wait --tree")
    assert result.exit_code == 0
    assert result.stdout == (
        "ID 000000000000000000000003   success  20s\n"
        "Storage s3://bucket\n"
        "Project fw://group/Project\n"
        "Import ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100%\n"
        "Discovered         5 files        26B\n"
        "Processed          5 files        26B\n"
        "Success            3 files        15B\n"
        "Skipped             1 file         5B\n"
        "Review              1 file         5B"
    )


def test_get_import_json(cli):
    result = cli.run(f"import get {I_ID} -o json")
    assert result.exit_code == 0
    assert result.stdout[0] == "{"
    assert_like(IMP, json.loads(result.stdout))


def test_get_import_yaml(cli):
    result = cli.run(f"import get {I_ID} -o yaml")
    assert result.exit_code == 0
    assert result.stdout[0] != "{"
    assert_like(IMP, yaml.safe_load(result.stdout))


def test_get_import_report(cli, api, mocker):
    mocker.patch("time.sleep")
    report = "foo,bar,baz\nx,z,y\n"
    report_gz = gzip.compress("foo,bar,baz\nx,z,y\n".encode("utf8"))
    responses = [
        ({"url": None}, 202, {}),
        ({"url": f"{api.url}/download"}, 200, {}),
    ]
    response_it = iter(responses)
    api.add_response(IMP_URL, {**IMP, "status": "success"})
    api.add_callback(f"{IMP_URL}/report", lambda: next(response_it))
    api.add_response("/download", report_gz)

    result = cli.run(f"import get {I_ID} --report=csv")

    assert result.exit_code == 0
    assert result.stdout == report


def test_get_import_report_before_completion(cli):
    result = cli.run(f"import get {I_ID} --report=jsonl")
    assert result.exit_code == 0
    assert result.stdout == "Reports are only available after completion\n"


def test_get_import_not_found(cli, api):
    resp = {"message": "No such import"}
    api.add_response(IMP_URL, resp, status=404)
    result = cli.run(f"import get {I_ID}")
    assert result.exit_code == 1
    assert "Error: No such import" in result.stderr


def test_import_cancel(cli, api):
    imp, prog = IMP.copy(), PROG_1.copy()
    api.add_response(IMP_URL, imp)
    api.add_response(f"{IMP_URL}/progress2/sse", "", method="HEAD")

    def cancel_cb():
        imp["status"] = prog["status"] = "canceling"
        imp["stage"] = prog["stage"] = None

        prog["totals"].update(
            {
                "success_files": 2,
                "success_bytes": 10,
                "processed_files": 2,
                "processed_bytes": 10,
                "discovered_files": 4,
                "discovered_bytes": 20,
            }
        )

        return imp

    def progress_cb():
        return prog2_sse(prog), 200, SSE_HDR

    api.add_callback(f"{IMP_URL}/cancel", cancel_cb, methods=["POST"])
    api.add_callback(f"{IMP_URL}/progress2/sse", progress_cb, methods=["GET"])
    result = cli.run(f"import cancel {I_ID} --no-wait")
    assert result.exit_code == 0
    assert result.stdout == (
        "ID 000000000000000000000003   cancelling  10s\n"
        "Storage s3://bucket\n"
        "Project fw://group/Project\n"
        "Discovered         4 files        20B\n"
        "Processed          2 files        10B\n"
        "Success            2 files        10B"
    )


def test_import_rerun(cli, api):
    api.add_response(f"{IMP_URL}/rerun", IMP, method="POST")
    result = cli.run(f"import rerun {I_ID} --no-wait")
    assert api.request_log[0] == f"POST /xfer/imports/{I_ID}/rerun"
    assert result.exit_code == 0
    assert result.stdout == (
        "ID 000000000000000000000003   running  10s - processing ...\n"
        "Storage s3://bucket\n"
        "Project fw://group/Project\n"
        "Discovered         4 files        20B\n"
        "Processed          2 files        10B\n"
        "Success            2 files        10B"
    )


def test_import_list(cli, api, mocker):
    now = datetime.now()
    mocker.patch("fw_utils.get_datetime", now)
    id_0 = f"{'ID':<24}"  # header
    imp_1 = IMP.copy()
    imp_1["_id"] = id_1 = f"{0:024d}"
    imp_1["project"] = {"parents": {"group": "grp"}, "label": "prj"}
    imp_1["storage"] = {"url": "s3://bucket"}
    imp_2 = imp_1.copy()
    imp_2["_id"] = id_2 = f"{1:024d}"
    imp_2["status"] = "success"
    imp_2["completed"] = fw_utils.format_datetime(now - timedelta(seconds=10))
    imp_3 = imp_1.copy()
    imp_3["_id"] = id_3 = f"{2:024d}"
    imp_3["blob_session"] = BLOB_SESSION | {"local_path": "/tmp"}
    api.add_response("/xfer/imports", {"results": [imp_1, imp_2, imp_3]})
    result = cli.run("imports list")
    assert result.exit_code == 0
    assert result.stdout == (
        f"{id_0}  Status   Elapsed  Completed  Source       Destination   \n"
        f"{id_1}  running  10s      -          s3://bucket  fw://grp/prj  \n"
        f"{id_2}  success  10s      10s ago    s3://bucket  fw://grp/prj  \n"
        f"{id_3}  running  10s      -          /tmp         fw://grp/prj  \n"
    )


def test_import_list_filter(cli, api, mocker):
    result = cli.run("imports list --filter foobar")
    assert result.exit_code == 1
    api.add_response("/xfer/imports", {"results": []})
    result = cli.run("imports list --filter status==cancelled --filter project=={P_ID}")
    assert result.exit_code == 0
    params = api.request_map["GET /xfer/imports"].params
    assert params["filter"] == "project=={P_ID},status2==cancelled,status==canceled"
    result = cli.run("imports list --filter status2==canceling")
    assert result.exit_code == 0
    params = api.request_map["GET /xfer/imports"].params
    assert params["filter"] == "status2==cancelling,status==canceling"


def test_import_list_empty(cli, api):
    api.add_response("/xfer/imports", {"results": []})
    result = cli.run("imports list")
    assert result.exit_code == 0
    assert result.stdout == "Nothing to show\n"


def test_import_schedule_create(cli, api):
    cmd = f"import schedule create -p{P_ID} -s{S_ID} "
    cmd += "--start 2022-01-01 --end 2022-01-02 --cron '* * * * *'"
    result = cli.run(cmd)

    assert result.exit_code == 0
    assert result.stdout.strip() == f"Created schedule with ID {SC_ID}"
    assert_like(
        {
            "start": format_datetime(get_datetime("2022-01-01")),
            "end": format_datetime(get_datetime("2022-01-02")),
            "cron": "* * * * *",
            "operation": {
                "type": "import",
                "label": "Scheduled import - created via CLI",
            },
        },
        api.request_map["POST /xfer/schedules"].json,
    )


# pre FLYW-24304 backward compat
def test_import_schedule_create_compat(cli, api, import_post):
    def post_import():
        payload = api.request.get_json()
        if not len(payload["operation"].get("rules", [])):
            err_msg = "List should have at least 1 item after validation, not 0"
            return {"message": f"'loc': ('body', 'rules'), 'msg': '{err_msg}'"}, 422
        return SCHEDULE, 200

    api.add_callback("/xfer/schedules", post_import, methods=["POST"])

    cmd = f"import schedule create -p{P_ID} -s{S_ID} "
    cmd += "--start 2022-01-01 --end 2022-01-02 --cron '* * * * *'"
    result = cli.run(cmd)

    assert result.exit_code == 0
    assert result.stdout.strip() == f"Created schedule with ID {SC_ID}"
    assert_like(
        {
            "start": format_datetime(get_datetime("2022-01-01")),
            "end": format_datetime(get_datetime("2022-01-02")),
            "cron": "* * * * *",
            "operation": {
                "type": "import",
                "label": "Scheduled import - created via CLI",
            },
        },
        api.request_map["POST /xfer/schedules"].json,
    )


def test_import_schedule_list(cli, api):
    cmd = "import schedule list"
    result = cli.run(cmd)

    assert result.exit_code == 0
    assert result.stdout == (
        "ID                        Source       Destination         \n"
        "000000000000000000000004  s3://bucket  fw://group/Project  \n"
    )


def test_import_schedule_list_all(cli, api):
    cmd = "import schedule list --all"
    result = cli.run(cmd)

    assert result.exit_code == 0
    assert result.stdout == (
        "ID                        Source       Destination         \n"
        "000000000000000000000004  s3://bucket  fw://group/Project  \n"
    )


def test_import_schedule_list_empty(cli, api):
    api.add_response("/xfer/schedules", {"results": []}, method="GET")
    cmd = "import schedule list"
    result = cli.run(cmd)

    assert result.exit_code == 0
    assert result.stdout == "Nothing to show\n"


def test_import_schedule_get(cli, api):
    cmd = f"import schedule get {SC_ID}"
    result = cli.run(cmd)

    assert result.exit_code == 0
    assert result.stdout == (
        "ID 000000000000000000000004\n"
        "Start 2022-01-01 00:00:00\n"
        "End 2022-01-01 00:00:00\n"
        "Cron * * * * *\n"
        "Project fw://group/Project\n"
        "Storage s3://bucket\n"
    )


def test_import_schedule_get_invalid(cli, api):
    sch = copy.deepcopy(SCHEDULE)
    sch["operation"]["type"] = "invalid"
    api.add_response(f"/xfer/schedules/{SC_ID}", sch, method="GET")

    cmd = f"import schedule get {SC_ID}"
    result = cli.run(cmd)

    assert result.exit_code == 1


def test_import_schedule_get_inactive(cli, api):
    sch = copy.deepcopy(SCHEDULE)
    sch["active"] = False
    api.add_response(f"/xfer/schedules/{SC_ID}", sch, method="GET")

    cmd = f"import schedule get {SC_ID}"
    result = cli.run(cmd)

    assert result.exit_code == 0
    assert result.stdout == (
        "ID 000000000000000000000004\n"
        "Active False\n"
        "Start 2022-01-01 00:00:00\n"
        "End 2022-01-01 00:00:00\n"
        "Cron * * * * *\n"
        "Project fw://group/Project\n"
        "Storage s3://bucket\n"
    )


def test_import_schedule_update(cli, api):
    cmd = f"import schedule update {SC_ID} --cron '*/5 * * * *' --start null"
    result = cli.run(cmd)
    assert result.exit_code == 0
    assert_like(
        {"cron": "*/5 * * * *", "start": None},
        api.request_map[f"PATCH /xfer/schedules/{SC_ID}"].json,
    )


def test_import_schedule_update_raises_w_invalid_op_type(cli, api):
    schedule = {"_id": SC_ID, "operation": {"type": "export"}}
    api.add_response(f"/xfer/schedules/{SC_ID}", schedule)
    cmd = f"import schedule update {SC_ID} --cron '*/5 * * * *'"
    result = cli.run(cmd)
    assert result.exit_code == 1
    assert "Unexpected schedule operation type" in result.stderr


def test_import_schedule_cancel(cli, api):
    cmd = f"import schedule cancel {SC_ID}"
    result = cli.run(cmd)
    assert result.exit_code == 0
    assert_like(
        {"active": False},
        api.request_map[f"PATCH /xfer/schedules/{SC_ID}"].json,
    )


def test_import_schedule_cancel_raises_w_invalid_op_type(cli, api):
    schedule = {"_id": SC_ID, "operation": {"type": "export"}}
    api.add_response(f"/xfer/schedules/{SC_ID}", schedule)
    cmd = f"import schedule cancel {SC_ID}"
    result = cli.run(cmd)
    assert result.exit_code == 1
    assert "Unexpected schedule operation type" in result.stderr


@pytest.fixture
def blob_api(api):
    # prep api
    blob_session = copy.deepcopy(BLOB_SESSION)
    imp = copy.deepcopy(IMP)
    imp["refs"]["blob_session"] = blob_session["_id"]
    imp["storage"] = copy.deepcopy(STORAGE)
    imp["blob_session"] = blob_session
    import_prog = copy.deepcopy(PROG_0)
    import_prog["status"] = "running"
    import_prog["elapsed_time"] = 20

    api.blob_session, api.imp, api.import_prog = blob_session, imp, import_prog

    def cb_imp():
        if api.request.method != "POST":
            return imp
        imp["refs"]["blob_session"] = api.request.json["refs"]["blob_session"]
        imp["rules"] = api.request.json.get("rules", [{}])
        return imp

    def cb_progress2():
        def stream():
            while import_prog["status"] == "running":
                yield prog2_sse(import_prog)
            yield prog2_sse(import_prog)

        return stream(), SSE_HDR

    def cb_finish_blob_ses():
        import_prog["status"] = "success"
        return blob_session

    def cb_finish_blob():
        import_prog["totals"]["discovered_files"] += 1
        import_prog["totals"]["discovered_bytes"] += 1
        import_prog["totals"]["success_files"] += 1
        import_prog["totals"]["success_bytes"] += 1
        import_prog["totals"]["processed_files"] += 1
        import_prog["totals"]["processed_bytes"] += 1
        return BLOB

    api.cb_imp = cb_imp
    api.cb_progress2 = cb_progress2
    api.cb_finish_blob_ses = cb_finish_blob_ses
    api.cb_finish_blob = cb_finish_blob

    api.add_callback("/xfer/imports", cb_imp, methods=["POST"])
    api.add_callback(f"/xfer/imports/{I_ID}", cb_imp)
    api.add_callback(f"/xfer/imports/{I_ID}/progress2/sse", cb_progress2)
    api.add_response("/xfer/blob-sessions", blob_session, method="POST")
    api.add_response(f"/xfer/blob-sessions/{BS_ID}", blob_session)
    api.add_callback(
        f"/xfer/blob-sessions/{BS_ID}/finish",
        cb_finish_blob_ses,
        methods=["POST"],
    )
    api.add_response("/xfer/blobs", BLOB, method="POST")
    upload_resp = {"upload_url": f"{api.url}/uploads/{B_ID}"}
    api.add_response(f"/xfer/blobs/{B_ID}/upload", upload_resp, method="POST")
    api.add_response(f"/uploads/{B_ID}", method="PUT")
    api.add_callback(f"/xfer/blobs/{B_ID}/finish", cb_finish_blob, methods=["POST"])
    return api


@pytest.mark.timeout(3)
def test_import_local_files(cli, blob_api, tmp_path):
    # prep file
    src_dir = tmp_path / "src"
    src_dir.mkdir()
    file = src_dir / "test.txt"
    file.write_text("test")
    blob_api.blob_session["local_path"] = str(src_dir)
    # run
    cmd = f"import run -p{P_ID} -s{src_dir}"
    result = cli.run(cmd)
    assert result.exit_code == 0
    assert result.stdout == (
        "ID 000000000000000000000003   success  20s\n"
        f"Storage {src_dir}\n"
        "Project fw://group/Project\n"
        "Upload ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100%\n"
        "Import ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100%\n"
        "Discovered          1 file         1B\n"
        "Processed           1 file         1B\n"
        "Success             1 file         1B"
    )
    # run with filters
    blob_api.import_prog["totals"].update(PROG_0["totals"])
    cmd = f"import run -p{P_ID} -s{src_dir} --include path=non-matching"
    result = cli.run(cmd)
    assert result.exit_code == 0
    assert result.stdout == (
        "ID 000000000000000000000003   success  20s\n"
        f"Storage {src_dir}\n"
        "Project fw://group/Project\n"
        "Discovered         0 files         0B\n"
        "Processed          0 files         0B"
    )


@pytest.mark.timeout(3)
def test_import_local_files_resume(cli, blob_api, tmp_path):
    blob_api.imp.pop("blob_session")
    blob_api.import_prog["totals"] = {
        "discovered_files": 1,
        "discovered_bytes": 1,
        "success_files": 1,
        "success_bytes": 1,
        "processed_files": 1,
        "processed_bytes": 1,
    }
    blob_api.blob_session["last_seq_no"] = 1
    blob_api.blob_session["last_abspath"] = "p/u/bs/test_a.txt"
    uploaded_blobs = []

    def cb_create_blob():
        uploaded_blobs.append(blob_api.request.json["path"])
        return BLOB

    blob_api.add_callback("/xfer/blobs", cb_create_blob, methods=["POST"])

    # prep file
    src_dir = tmp_path / "src"
    src_dir.mkdir()
    file = src_dir / "test_a.txt"
    file.write_text("test")
    file = src_dir / "test_b.txt"
    file.write_text("test")
    # run - usage errors
    cmd = f"import run --resume-local {I_ID}"
    blob_api.imp["status"] = "success"
    result = cli.run(cmd)
    assert result.exit_code == 1
    assert "already terminated" in result.stderr
    blob_api.imp["status"] = "running"
    result = cli.run(cmd)
    assert result.exit_code == 1
    assert "storage is not local" in result.stderr
    blob_api.imp["blob_session"] = blob_api.blob_session
    blob_api.imp["refs"]["blob_session"] = BS_ID
    result = cli.run(cmd)
    assert result.exit_code == 1
    assert "storage is not local" in result.stderr
    blob_api.blob_session["local_path"] = "foobarbaz-nonexistent"
    result = cli.run(cmd)
    assert result.exit_code == 1
    assert "local path is not a directory" in result.stderr
    blob_api.blob_session["local_path"] = str(src_dir)
    # run - fail
    blob_api.add_response(f"/xfer/blobs/{B_ID}/finish", method="POST", status=500)
    result = cli.run(cmd)
    assert result.exit_code == 1
    expected = (
        r".*Exception while uploading files for local import:\n"
        r".*500 INTERNAL SERVER ERROR.*\n\n"
        f"To resume the upload:\n> fw-beta import run --resume-local {I_ID}\n"
        f"To cancel the import:\n> fw-beta import cancel {I_ID}\n"
    )
    assert re.search(expected, result.stderr, re.M)
    assert len(uploaded_blobs) == 1
    assert uploaded_blobs[0] == "test_b.txt"
    # run - success
    blob_api.add_callback(
        f"/xfer/blobs/{B_ID}/finish",
        blob_api.cb_finish_blob,
        methods=["POST"],
    )
    result = cli.run(cmd)
    assert result.exit_code == 0
    assert len(uploaded_blobs) == 2
    assert uploaded_blobs[1] == "test_b.txt"
    assert result.stdout == (
        "ID 000000000000000000000003   success  20s\n"
        f"Storage {src_dir}\n"
        "Project fw://group/Project\n"
        "Upload ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100%\n"
        "Import ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100%\n"
        "Discovered         2 files         2B\n"
        "Processed          2 files         2B\n"
        "Success            2 files         2B"
    )


# HELPERS


@pytest.fixture
def import_post():
    """Return new instance of default import POST payload expected from CLI."""
    return {
        "label": re.compile(f"Import via CLI from {DT_RE}"),
        "refs": {"project": P_ID, "storage": S_ID},
    }


def prog2_sse(*events):
    evt = "id: {}\nevent: progress\ndata: {}\n\n"
    return "".join(evt.format(idx, json.dumps(data)) for idx, data in enumerate(events))
