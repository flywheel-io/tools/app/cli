import json
from pathlib import Path

import pytest


@pytest.mark.parametrize("version", [None, 2])
def test_success(cli, api, tmp_path, mocker, version):
    run = mocker.patch("fw_cli.commands.job.pull.get_container_client")
    job = {
        "id": "test",
        "gear_id": "test-gear",
        "gear_info": {"name": "test-gear", "version": "0.1.0"},
        "config": {
            "inputs": {
                "api-key": {"base": "api-key"},
                "test1": {
                    "base": "file",
                    "location": {
                        "name": "test-file",
                        "path": "/flywheel/v0/input/test1/test-file",
                    },
                    "hierarchy": {"id": "test", "type": "acquisition"},
                    "object": {},
                },
            },
        },
    }
    if version:
        job["config"]["inputs"]["test1"]["object"] = {"version": version}
    api.add_response("/api/jobs/test", job)
    api.add_response("/api/acquisitions/test/files/test-file", {})
    api.add_response(
        "/api/gears/test-gear/",
        {
            "gear": {
                "name": "test-gear",
                "environment": {"PATH": "/my/favorite/path"},
                "command": "python3 run.py",
            }
        },
    )
    res = cli.run(f"job pull test {tmp_path}")
    base_dir = tmp_path / "test-gear-0.1.0-test"
    endpoint = "GET /api/acquisitions/test/files/test-file"
    assert api.request_map[endpoint]["params"] == ({"version": "2"} if version else {})
    assert (base_dir / "input").is_dir()
    assert (base_dir / "output").is_dir()
    assert (base_dir / "work").is_dir()
    assert (base_dir / "input/test1/test-file").is_file()
    assert (base_dir / "manifest.json").is_file()
    assert (base_dir / "config.json").is_file()
    assert res.exit_code == 0
    assert "Pulling manifest" in res.stdout
    assert "Pulling job inputs" in res.stdout
    with open(base_dir / ".input_map.json", "r", encoding="utf-8") as fp:
        i_map = json.load(fp)
        assert len(i_map) == 1
        assert i_map["/flywheel/v0/input/test1/test-file"] == str(
            Path(base_dir / "input/test1/test-file")
        )
    run.return_value.inspect.assert_called_once()
    assert "No image tag found" in res.stdout
    req = api.request_map["GET /api/acquisitions/test/files/test-file"]
    if version:
        assert req["params"] == {"version": str(version)}
    else:
        assert req["params"] == {}


def test_no_input_skips_downloading(cli_admin, api, tmp_path, mocker):
    _ = mocker.patch("fw_cli.commands.job.pull.get_container_client")
    api.add_response(
        "/api/gears/test-gear/",
        {
            "gear": {
                "name": "test-gear",
                "environment": {"PATH": "/my/favorite/path"},
            }
        },
    )
    api.add_response(
        "/api/jobs/test",
        {
            "id": "test",
            "gear_id": "test-gear",
            "gear_info": {"name": "test-gear", "version": "0.1.0"},
            "config": {"inputs": {}},
        },
    )
    res = cli_admin.run(f"job pull test {tmp_path}")
    assert res.exit_code == 0
    assert "Pulling job inputs" not in res.stdout


def test_image_present(cli_admin, api, tmp_path, mocker):
    run = mocker.patch("fw_cli.commands.job.pull.get_container_client")
    download = mocker.patch("fw_cli.commands.job.pull.download_gear")
    api.add_response(
        "/api/jobs/test",
        {
            "id": "test",
            "gear_id": "test-gear",
            "gear_info": {"name": "test-gear", "version": "0.1.0"},
            "config": {"inputs": {}},
        },
    )
    api.add_response(
        "/api/gears/test-gear/",
        {
            "gear": {
                "name": "test-gear",
                "environment": {"PATH": "/my/favorite/path"},
                "custom": {"gear-builder": {"image": "test:0.1.0"}},
            }
        },
    )
    res = cli_admin.run(f"job pull test {tmp_path}")
    assert res.exit_code == 0
    run.return_value.inspect.assert_called_once_with("test:0.1.0")
    download.assert_not_called()


def test_image_not_present(cli_admin, api, tmp_path, mocker):
    run = mocker.patch("fw_cli.commands.job.pull.get_container_client")
    run.return_value.inspect.return_value = None
    download = mocker.patch("fw_cli.commands.job.pull.download_gear")
    api.add_response(
        "/api/jobs/test",
        {
            "id": "test",
            "gear_id": "test-gear",
            "gear_info": {"name": "test-gear", "version": "0.1.0"},
            "config": {"inputs": {}},
        },
    )
    api.add_response(
        "/api/gears/test-gear/",
        {
            "gear": {
                "name": "test-gear",
                "environment": {"PATH": "/my/favorite/path"},
                "custom": {"gear-builder": {"image": "test:0.1.0"}},
            }
        },
    )
    res = cli_admin.run(f"job pull test {tmp_path}")
    assert res.exit_code == 0
    download.assert_called_once()
    assert "Pulling image test:0.1.0" in res.stdout


@pytest.mark.parametrize(
    "code,exp",
    [
        (404, "Input file doesn't exist"),
        (405, "Unhandled error downloading input"),
    ],
)
def test_fails_downloading_input(cli_admin, api, tmp_path, code, exp):
    api.add_response("/api/users/self", {"root": True})
    api.add_response(
        "/api/jobs/test",
        {
            "id": "test",
            "gear_id": "test-gear",
            "gear_info": {"name": "test-gear", "version": "0.1.0"},
            "config": {
                "inputs": {
                    "api-key": {"base": "api-key"},
                    "test1": {
                        "base": "file",
                        "location": {
                            "name": "test-file",
                            "path": "/flywheel/v0/input/test1/test-file",
                        },
                        "hierarchy": {"id": "test", "type": "acquisition"},
                    },
                }
            },
        },
    )
    api.add_response("/api/acquisitions/test/files/test-file", status=code)
    api.add_response("/api/gears/test-gear/", {"gear": {"name": "test-gear"}})
    res = cli_admin.run(f"job pull test {tmp_path}")
    assert res.exit_code == 1
    assert exp in res.stderr
    req = api.request_map["GET /api/acquisitions/test/files/test-file"]
    assert req["params"] == {}


@pytest.mark.filterwarnings("ignore:unclosed file <_io.TextIOWrapper")
@pytest.mark.parametrize("client", ["docker", "podman"])
def test_missing_command(api, client, cli_admin, mocker, tmp_path):
    mocker.patch("fw_cli.containers.shutil.which", return_value=None)
    api.add_response(
        "/api/jobs/test",
        {
            "id": "test",
            "gear_id": "test-gear",
            "gear_info": {"name": "test-gear", "version": "0.1.0"},
            "config": {"inputs": {}},
        },
    )
    api.add_response(
        "/api/gears/test-gear/",
        {
            "gear": {
                "name": "test-gear",
                "environment": {"PATH": "/my/favorite/path"},
                "custom": {"gear-builder": {"image": "test:0.1.0"}},
            }
        },
    )
    res = cli_admin.run(f"--container-client {client} job pull test {tmp_path} ")
    assert res.exit_code == 1
    assert "Getting job test" in res.stdout
    assert "Pulling manifest for test-gear" in res.stdout
    assert f"Using container client {client}" in res.stderr
    assert f"Missing required command {client!r}" in res.stderr
