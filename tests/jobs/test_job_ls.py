import json

import pytest
import yaml
from fw_utils import assert_like
from rich.console import Console, ConsoleDimensions

JOB_RESPONSE = {
    "_id": "602da2a117d9612818a294a9",
    "attempt": 1,
    "batch": None,
    "compute_provider_id": "5f071f2c61ebf8000ec33404",
    "config": {},
    "created": "2021-02-17T23:11:29.669000+00:00",
    "deleted_outputs": [],
    "destination": {"id": "602da21917d9612818a2949b", "type": "acquisition"},
    "failed_output_accepted": None,
    "failure_reason": None,
    "gear_id": "602da28917d9612818a294a4",
    "gear_info": {
        "capabilities": None,
        "category": "converter",
        "id": "602da28917d9612818a294a4",
        "name": "splitter",
        "version": "3.0.0-dev.5",
    },
    "inputs": [],
    "label": "",
    "modified": "2021-02-17T23:14:37.546000+00:00",
    "origin": {"id": "naterichman@flywheel.io", "type": "user"},
    "outputs": [],
    "parent_info": None,
    "parents": {},
    "previous_job_id": None,
    "profile": {"upload_time_ms": None},
    "related_container_ids": [],
    "request": {},
    "retried": None,
    "saved_files": [],
    "state": "complete",
    "tags": ["ad-hoc", "splitter"],
    "transitions": {},
}


@pytest.fixture(autouse=True)
def api(api):
    api.add_response("/api/jobs", [JOB_RESPONSE])
    return api


def test_basic_success(api, cli, monkeypatch):
    # Force dimensions to be large enough to not wrap output.
    dim = ConsoleDimensions(width=200, height=200)
    monkeypatch.setattr(Console, "size", dim)
    res = cli.run("job ls")
    assert res.exit_code == 0
    components = [
        "602da2a117d9612818a294a9",  # Job id
        "splitter",  # Gear name
        "3.0.0-dev.5",  # Gear version
        "naterichman@flywheel.io",  # Gear runner
        "Feb 17, 2021 23:11:29",  # Created date
        "complete",  # State
    ]
    assert all(c in res.stdout for c in components)
    assert api.request_map["GET /api/jobs"]["params"] == {
        "filter": "",
        "limit": "100",
        "sort": "created:desc",
    }


# Compatible flywheel release
@pytest.mark.parametrize("release", ["16.5.0", "16.6.2-rc.1", "17.0.0"])
def test_multi_filter(api, cli, release):
    api.add_response("/api/version", {"flywheel_release": release})
    api.add_response(
        "/api/projects/61a8fb7a1a1b845113a7bc1e",
        {"_id": "61a8fb7a1a1b845113a7bc1e", "label": "test"},
    )
    api.add_response(
        "/api/lookup",
        {"_id": "proj1id", "node_type": "project", "label": "test"},
        method="POST",
    )
    res = cli.run(
        "job ls "
        "-u naterichman@flywheel.io "  # Filter by user
        "-g test-gear "  # Filter by gear name
        "-p test/proj1 -p 61a8fb7a1a1b845113a7bc1e "  # Multiple project filters
        "-s running "  # Filter by state
        "-l 50 "  # Limit of 50 jobs
        "-a"  # Filter descending
    )
    assert res.exit_code == 0
    assert api.request_map["GET /api/jobs"]["params"] == {
        "filter": (
            "gear_info.name=test-gear,"
            "origin.id=naterichman@flywheel.io,"
            "state=running,"
            "parents.project=|[proj1id,61a8fb7a1a1b845113a7bc1e]"
        ),
        "limit": "50",
        "sort": "created:asc",
    }


@pytest.mark.parametrize("release", ["16.4.0", "15.0.0"])
def test_multi_filter_not_compatible(api, cli, release):
    api.add_response("/api/version", {"flywheel_release": release})
    res = cli.run("job ls -p test/proj1 -p 61a8fb7a1a1b845113a7bc1e ")
    assert res.exit_code == 1
    assert "Multiple values per filter not supported" in res.stderr


def test_get_jobs_error(api, cli):
    api.add_response("/api/jobs", status=418)
    res = cli.run("job ls")
    assert res.exit_code == 1
    assert "Could not get list of jobs" in res.stderr
    assert "I'M A TEAPOT" in res.stderr


def test_get_jobs_custom_filter(api, cli):
    res = cli.run("job ls -p nate -g test1 -f gear_info.name=test")
    assert res.exit_code == 0
    assert api.request_map["GET /api/jobs"]["params"] == {
        "filter": "gear_info.name=test",
        "limit": "100",
        "sort": "created:desc",
    }


def test_get_jobs_cant_find_project_path(cli):
    res = cli.run("job ls -p nate/test")
    assert res.exit_code == 1
    assert "Could not find project 'nate/test'" in res.stderr


def test_job_list_json(cli, api):
    result = cli.run("job ls -ojson")
    assert result.exit_code == 0
    assert result.stdout[0] == "["
    assert_like([JOB_RESPONSE], json.loads(result.stdout))


def test_job_list_yaml(cli, api):
    result = cli.run("job ls -oyaml")
    assert result.exit_code == 0
    assert result.stdout[0] != "["
    assert_like([JOB_RESPONSE], yaml.safe_load(result.stdout))
