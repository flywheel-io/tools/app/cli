"""Tests for job retry."""

import pytest
from fw_utils import assert_like


def test_retry_succeeds(cli, api):
    api.add_response("/api/jobs/test/retry", {"id": "test1"}, method="POST")
    res = cli.run("job retry test")
    assert res.exit_code == 0
    assert "Job test retried with job id: test1" in res.stdout


def test_retry_forbidden(cli, api):
    api.add_response("/api/jobs/test/retry", status=403, method="POST")
    res = cli.run("job retry test")
    assert res.exit_code == 1
    assert "User does not have permissions" in res.stderr


def test_retry_raises(cli, api):
    api.add_response("/api/jobs/test/retry", status=422, method="POST")
    res = cli.run("job retry test")
    assert res.exit_code == 1
    assert "fw_client.errors.ClientError" in res.stderr
    assert "422" in res.stderr


@pytest.fixture
def job():
    return {
        "id": "test",
        "attempt": 1,
        "batch": None,
        "compute_provider_id": "",
        "config": {"config": {}, "inputs": {}},
        "destination": {"type": "acquisition", "id": "test"},
        "gear_id": "test",
        "label": "",
        "origin": {"type": "user", "id": "test@flywheel.io"},
        "parents": {},
        "previous_job_id": None,
        "tags": [],
    }


@pytest.fixture
def job_input():
    return {
        "test": {
            "base": "file",
            "hierarchy": {"id": "6189aa6603cc1227fdaed253", "type": "acquisition"},
            "location": {
                "name": "SER00009.dcm",
                "path": "/flywheel/v0/input/anatomical/SER00009.dcm",
            },
            "object": {
                "classification": {},
                "file_id": "6189aa7903cc1227fdaed256",
                "info": {},
                "mimetype": "application/octet-stream",
                "modality": None,
                "origin": {"id": "naterichman@flywheel.io", "type": "user"},
                "size": 58197336,
                "tags": [],
                "type": "dicom",
                "version": 2,
                "zip_member_count": None,
            },
        }
    }


def test_retry_already_retried_force(cli, api, job):
    api.add_response("/api/jobs/test/retry", status=409, method="POST")
    api.add_response("/api/jobs/test", job)
    api.add_response("/api/jobs/add", {"_id": "test"}, method="POST")
    res = cli.run("job retry test --force")
    assert res.exit_code == 0
    assert (
        "Job already retried, creating a new job with identical configuration."
    ) in res.stdout
    assert "Job successfully retried with id test" in res.stdout


def test_retry_already_retried_confirm(cli, api, job):
    api.add_response("/api/jobs/test/retry", status=409, method="POST")
    api.add_response("/api/jobs/test", job)
    api.add_response("/api/jobs/add", {"_id": "test"}, method="POST")
    res = cli.run("job retry test", input="y\n")
    assert res.exit_code == 0
    assert (
        "Job already retried, create a new job with identical configuration?"
    ) in res.stdout
    assert "Job successfully retried with id test" in res.stdout


def test_retry_already_retried_converts_inputs(cli, api, job, job_input):
    api.add_response("/api/jobs/test/retry", status=409, method="POST")
    job["config"]["inputs"] = job_input
    api.add_response("/api/jobs/test", job)
    api.add_response("/api/jobs/add", {"_id": "test"}, method="POST")
    res = cli.run("job retry test", input="y\n")
    assert res.exit_code == 0
    call_data = {
        "attempt": 2,
        "destination": {"id": "test", "type": "acquisition"},
        "gear_id": "test",
        "inputs": {"test": {"file_id": "6189aa7903cc1227fdaed256", "version": 2}},
        "previous_job_id": "test",
    }
    assert (
        "Job already retried, create a new job with identical configuration?"
    ) in res.stdout
    assert "Job successfully retried with id test" in res.stdout
    call_data = {
        "inputs": {"test": {"file_id": "6189aa7903cc1227fdaed256", "version": 2}},
    }
    assert_like(call_data, api.requests[-1]["json"], allow_extra=True)


def test_retry_already_retried_converts_inputs_old_core(cli, api, job, job_input):
    api.add_response("/api/jobs/test/retry", status=409, method="POST")
    job["config"]["inputs"] = job_input
    job["config"]["inputs"]["test"]["object"].pop("file_id")
    api.add_response("/api/jobs/test", job)
    api.add_response("/api/jobs/add", {"_id": "test"}, method="POST")
    res = cli.run("job retry test", input="y\n")
    assert res.exit_code == 0
    call_data = {
        "inputs": {
            "test": {
                "type": "acquisition",
                "id": "6189aa6603cc1227fdaed253",
                "name": "SER00009.dcm",
            }
        },
    }
    assert_like(call_data, api.requests[-1]["json"], allow_extra=True)


def test_retry_already_retried_converts_inputs_analysis(cli, api, job, job_input):
    api.add_response("/api/jobs/test/retry", status=409, method="POST")
    job["config"]["inputs"] = job_input
    job["destination"] = {"type": "analysis", "id": "test"}
    job["parents"]["acquisition"] = "test_acquisition"
    api.add_response("/api/jobs/test", job)
    api.add_response("/api/jobs/add", {"_id": "test"}, method="POST")
    res = cli.run("job retry test", input="y\n")
    assert res.exit_code == 0
    call_data = {
        "attempt": 2,
        "destination": {"id": "test_acquisition", "type": "acquisition"},
        "gear_id": "test",
        "inputs": {"test": {"file_id": "6189aa7903cc1227fdaed256", "version": 2}},
        "origin": {"id": "user@flywheel.io", "type": "user"},
        "parents": {"acquisition": "test_acquisition"},
        "previous_job_id": "test",
    }
    assert_like(call_data, api.requests[-1]["json"], allow_extra=True)
