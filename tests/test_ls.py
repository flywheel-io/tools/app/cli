"""Test ls."""

import pytest

ID_0 = "000000000000000000000000"
DT = "1999-12-31T23:59:59.000000+00:00"
CM = {"created": DT, "modified": DT}
GRP_PERMS = [{"_id": "user@flywheel.io", "access": "admin"}]
PRJ_PERMS = [{"_id": "user@flywheel.io", "role_ids": [ID_0]}]


def node(node_type, **kw):
    kw.setdefault("_id", ID_0)
    kw.setdefault("created", DT)
    kw.setdefault("modified", DT)
    kw.setdefault("permissions", [])
    return {"node_type": node_type, **kw}


GRP = node("group", _id="group", label="Group", permissions=GRP_PERMS)
PRJ = node("project", label="Project", permissions=PRJ_PERMS)
SUB = node("subject", label="Subject")
SES = node("session", label="Session")
ACQ = node("acquisition", label="Acquisition")
FILE = node("file", name="file.txt", size=512, origin={"type": "device"})
ROLE = {
    "_id": ID_0,
    "actions": [
        "containers_create_hierarchy",
        "containers_delete_hierarchy",
        "containers_delete_project",
        "containers_modify_metadata",
        "containers_view_metadata",
        "files_create_upload",
        "files_delete_device_data",
        "files_delete_non_device_data",
        "files_download",
        "files_modify_metadata",
        "files_view_contents",
        "files_view_metadata",
    ],
}


@pytest.fixture
def api(api):
    api.add_response(f"/api/roles/{ID_0}", ROLE)
    return api


def test_ls_empty(api, cli):
    response = {"path": [], "children": []}
    api.add_response("/api/resolve", response, method="POST")
    result = cli.run("ls")
    request = api.pop_first()
    assert request.name == "POST /api/resolve"
    assert request.json == {"path": []}
    assert result.exit_code == 0
    assert result.stdout == "Nothing to show\n"


def test_ls_site(api, cli):
    response = {"path": [], "children": [GRP]}
    api.add_response("/api/resolve", response, method="POST")
    result = cli.run("ls")
    request = api.pop_first()
    assert request.name == "POST /api/resolve"
    assert request.json == {"path": []}
    assert result.exit_code == 0
    exp = "Perms Size Modified         Name  \ngrwd     - 1999-12-31 23:59 group \n"
    assert result.stdout == exp


def test_ls_group(api, cli):
    response = {"path": [GRP], "children": [PRJ]}
    api.add_response("/api/resolve", response, method="POST")
    result = cli.run("ls group")
    request = api.pop_first()
    assert request.name == "POST /api/resolve"
    assert request.json == {"path": ["group"]}
    assert result.exit_code == 0
    assert result.stdout == (
        "Perms Size Modified         Name    \nprwd     - 1999-12-31 23:59 Project \n"
    )


def test_ls_group_no_project_perms(api, cli):
    prj = PRJ.copy()
    prj["permissions"] = []
    response = {"path": [GRP], "children": [prj]}
    api.add_response("/api/resolve", response, method="POST")
    result = cli.run("ls group")
    request = api.pop_first()
    assert request.name == "POST /api/resolve"
    assert request.json == {"path": ["group"]}
    assert result.exit_code == 0
    assert result.stdout == (
        "Perms Size Modified         Name    \np---     - 1999-12-31 23:59 Project \n"
    )


def test_ls_project(api, cli):
    response = {"path": [GRP, PRJ], "children": [SUB, FILE]}
    api.add_response("/api/resolve", response, method="POST")
    result = cli.run("ls group/Project")
    request = api.pop_first()
    assert request.name == "POST /api/resolve"
    assert request.json == {"path": ["group", "Project"]}
    assert result.exit_code == 0
    assert result.stdout == (
        "Perms Size Modified         Name     \n"
        "Srwd     - 1999-12-31 23:59 Subject  \n"
        ".rwd  512B 1999-12-31 23:59 file.txt \n"
    )


def test_ls_subject(api, cli):
    response = {"path": [GRP, PRJ, SUB], "children": [SES, FILE]}
    api.add_response("/api/resolve", response, method="POST")
    result = cli.run("ls group/Project/Subject")
    request = api.pop_first()
    assert request.name == "POST /api/resolve"
    assert request.json == {"path": ["group", "Project", "Subject"]}
    assert result.exit_code == 0
    assert result.stdout == (
        "Perms Size Modified         Name     \n"
        "srwd     - 1999-12-31 23:59 Session  \n"
        ".rwd  512B 1999-12-31 23:59 file.txt \n"
    )


def test_ls_session(api, cli):
    response = {"path": [GRP, PRJ, SUB, SES], "children": [ACQ, FILE]}
    api.add_response("/api/resolve", response, method="POST")
    result = cli.run("ls group/Project/Subject/Session")
    request = api.pop_first()
    assert request.name == "POST /api/resolve"
    assert request.json == {"path": ["group", "Project", "Subject", "Session"]}
    assert result.exit_code == 0
    assert result.stdout == (
        "Perms Size Modified         Name        \n"
        "arwd     - 1999-12-31 23:59 Acquisition \n"
        ".rwd  512B 1999-12-31 23:59 file.txt    \n"
    )


def test_ls_acquisition(api, cli):
    response = {"path": [GRP, PRJ, SUB, SES, ACQ], "children": [FILE]}
    api.add_response("/api/resolve", response, method="POST")
    result = cli.run("ls group/Project/Subject/Session/Acquisition")
    request = api.pop_first()
    assert request.name == "POST /api/resolve"
    assert request.json == {
        "path": ["group", "Project", "Subject", "Session", "Acquisition"]
    }
    assert result.exit_code == 0
    assert result.stdout == (
        "Perms Size Modified         Name     \n.rwd  512B 1999-12-31 23:59 file.txt \n"
    )
