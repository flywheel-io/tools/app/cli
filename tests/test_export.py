"""Test export."""

import copy
import gzip
import json
import re
from datetime import datetime, timedelta

import pytest
import yaml
from fw_utils import assert_like, attrify, format_datetime, get_datetime, hrsize, hrtime

ID_0 = "000000000000000000000000"
DT_RE = r"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d"

P_ID = "000000000000000000000001"
S_ID = "000000000000000000000002"
E_ID = "000000000000000000000003"
SC_ID = "000000000000000000000004"
PRJ = {"_id": P_ID, "label": "prj", "parents": {"group": "grp"}}
STORAGE = {"config": {"type": "s3"}, "url": "s3://bucket"}
EXP_URL = f"/xfer/exports/{E_ID}"
EXP = {
    "_id": E_ID,
    "label": "Export",
    "status": "running",
    "total_time": 10,
    "refs": {"project": P_ID, "storage": S_ID, "report": None},
    "project": PRJ,
    "storage": STORAGE,
}
PROGRESS = {
    "status": "running",
    "totals": {
        "discovered_files": 10,
        "discovered_bytes": 20,
        "skipped_files": 3,
        "skipped_bytes": 5,
        "processed_files": 6,
        "processed_bytes": 10,
        "created_files": 2,
        "created_bytes": 3,
        "replaced_files": 1,
        "replaced_bytes": 2,
    },
    "current_speed": {"bytes_per_sec": 10},
    "elapsed_time": 10,
}
SCHEDULE = {
    "_id": SC_ID,
    "active": True,
    "start": "2022-01-01",
    "end": "2022-01-01",
    "cron": "* * * * *",
    "operation": {
        "type": "export",
        "label": "abc",
        "refs": {"project": P_ID, "storage": S_ID},
    },
    "origin": "cli",
    "project": PRJ,
    "storage": STORAGE,
}
SCHEDULE_PAGE = {"results": [SCHEDULE]}


@pytest.fixture
def api(api):
    def schedule_patch():
        response = SCHEDULE.copy()
        response.update(api.request.json)
        return response

    api.add_response(f"/api/projects/{P_ID}", PRJ)
    api.add_response(f"/xfer/storages/{S_ID}", STORAGE)
    api.add_response("/xfer/exports", EXP, method="POST")
    api.add_response(f"/xfer/exports/{E_ID}", EXP)
    api.add_response(f"/xfer/exports/{E_ID}/progress2", PROGRESS)
    api.add_response("/xfer/schedules", SCHEDULE_PAGE, method="GET")
    api.add_response("/xfer/schedules", SCHEDULE, method="POST")
    api.add_response(f"/xfer/schedules/{SC_ID}", SCHEDULE)
    api.add_callback(f"/xfer/schedules/{SC_ID}", schedule_patch, methods=["PATCH"])
    return api


def test_export_with_invalid_project(cli):
    result = cli.run("export run -p prj -s storage")
    assert result.exit_code == 1
    assert "Error: Invalid project path/id" in result.stderr


def test_export_with_invalid_project_not_exists(cli):
    result = cli.run(f"export run -p {ID_0} -s storage")
    assert result.exit_code == 1
    assert "Error: Could not find project" in result.stderr


def test_export_with_invalid_storage(cli):
    result = cli.run(f"export run -p {P_ID} -s storage --no-wait")
    assert result.exit_code == 1
    assert "Error: Invalid storage id" in result.stderr


EXPORT_CMD = f"export run -p {P_ID} -s {S_ID} --no-wait"


def test_export_with_defaults(cli, api):
    result = cli.run(EXPORT_CMD)
    assert result.exit_code == 0
    assert result.stdout == "Export 000000000000000000000003 started\n"
    assert_like(export_post(), api.request_map["POST /xfer/exports"].json)


def test_export_with_wait(cli, api):
    progress = copy.deepcopy(PROGRESS)
    progress["status"] = "success"
    api.add_response(f"/xfer/exports/{E_ID}/progress2", progress)
    result = cli.run(EXPORT_CMD + " --wait")
    assert result.exit_code == 0
    assert result.stdout == (
        "Export 000000000000000000000003   success  10s\n"
        "Project fw://grp/prj\n"
        "Storage s3://bucket\n"
        "━━━━━━━━━━━━━━━━━━━━━━━━━━                          60% 10B/s\n"
        "Discovered         10 files        20B\n"
        "Processed           6 files        10B\n"
        "Skipped             3 files         5B\n"
        "Transferred         3 files         5B"
    )
    assert_like(export_post(), api.request_map["POST /xfer/exports"].json)


def test_run_export_yaml(cli, api):
    result = cli.run(EXPORT_CMD + " --no-wait --output yaml")
    assert result.exit_code == 0
    assert result.stdout[0] != "{"
    assert_like(EXP, yaml.safe_load(result.stdout))


def test_run_export_json(cli, api):
    result = cli.run(EXPORT_CMD + " --no-wait --output json")
    assert result.exit_code == 0
    assert result.stdout[0] == "{"
    assert_like(EXP, json.loads(result.stdout))


def test_export_metadata(cli, api):
    result = cli.run(
        EXPORT_CMD
        + " --no-wait --metadata --include sub=~a --rule 'level:subject, metadata:True', --rule 'level:project,metadata: False'"
    )
    assert result.exit_code == 0
    include = ["subject.label=~a"]
    expected = export_post(
        rules=[
            {"include": include, "metadata": True},
            {"level": "subject", "metadata": True},
            {"level": "project", "metadata": False},
        ]
    )
    assert_like(expected, api.request_map["POST /xfer/exports"].json)


def test_export_with_include_exclude(cli, api):
    result = cli.run(f"{EXPORT_CMD} --include sub=~a --include ses=b --exclude acq!=c")
    assert result.exit_code == 0
    include = ["subject.label=~a", "session.label=b"]
    exclude = ["acquisition.label!=c"]
    expected = export_post(rules=[{"include": include, "exclude": exclude}])
    assert_like(expected, api.request_map["POST /xfer/exports"].json)


def test_export_with_extra_rule(cli, api):
    result = cli.run(f"{EXPORT_CMD} --rule 'level: subject'")
    assert result.exit_code == 0
    expected = export_post(rules=[{}, {"level": "subject"}])
    assert_like(expected, api.request_map["POST /xfer/exports"].json)


def test_export_with_storage_override(cli, api):
    result = cli.run(f"{EXPORT_CMD} --storage-config prefix:test")
    assert result.exit_code == 0
    expected = export_post(storage_override={"type": "s3", "prefix": "test"})
    assert_like(expected, api.request_map["POST /xfer/exports"].json)


def test_export_with_project_path(cli, api):
    lookup_resp = {"_id": P_ID, "label": "Project", "node_type": "project"}
    api.add_response("/api/lookup", lookup_resp, method="POST")
    result = cli.run(f"export run -p grp/prj -s {S_ID} --no-wait")
    assert result.exit_code == 0
    assert result.stdout == "Export 000000000000000000000003 started\n"


def test_export_get(cli):
    result = cli.run(f"export get {E_ID}")
    assert result.exit_code == 0
    assert result.stdout.strip() == (
        "Export 000000000000000000000003   running  10s\n"
        "Project fw://grp/prj\n"
        "Storage s3://bucket\n"
        "Discovered         10 files        20B\n"
        "Processed           6 files        10B\n"
        "Skipped             3 files         5B\n"
        "Transferred         3 files         5B"
    )


def test_export_get_with_stage(cli, api):
    progress = copy.deepcopy(PROGRESS)
    progress["stage"] = "processing_items"
    api.add_response(f"/xfer/exports/{E_ID}/progress2", progress)
    result = cli.run(f"export get {E_ID}")
    assert result.exit_code == 0
    assert result.stdout.strip() == (
        "Export 000000000000000000000003   running  10s - processing items ...\n"
        "Project fw://grp/prj\n"
        "Storage s3://bucket\n"
        "Discovered         10 files        20B\n"
        "Processed           6 files        10B\n"
        "Skipped             3 files         5B\n"
        "Transferred         3 files         5B"
    )


def test_export_get_failed(cli, api):
    progress = copy.deepcopy(PROGRESS)
    progress["status"] = "failed"
    progress["status_reason"] = "oops"
    progress["totals"]["failed_files"] = 1
    progress["totals"]["failed_bytes"] = 5
    progress["totals"]["processed_files"] = 7
    progress["totals"]["processed_bytes"] = 15
    api.add_response(f"/xfer/exports/{E_ID}/progress2", progress)
    result = cli.run(f"export get {E_ID}")
    assert result.exit_code == 1
    assert result.stdout.strip() == (
        "Export 000000000000000000000003   failed  10s\n"
        "Project fw://grp/prj\n"
        "Storage s3://bucket\n"
        "Discovered         10 files        20B\n"
        "Processed           7 files        15B\n"
        "Skipped             3 files         5B\n"
        "Transferred         3 files         5B\n"
        "Failed               1 file         5B"
    )


def test_export_get_not_found(cli, api):
    resp = {"message": "No such export"}
    api.add_response(f"/xfer/exports/{ID_0}", resp, status=404)
    result = cli.run(f"export get {ID_0}")
    assert result.exit_code == 1
    assert "Error: No such export" in result.stderr


def test_export_get_json(cli):
    result = cli.run(f"export get {E_ID} -o json")
    assert result.exit_code == 0
    assert result.stdout[0] == "{"
    assert_like(EXP, json.loads(result.stdout))


def test_export_get_yaml(cli):
    result = cli.run(f"export get {E_ID} -o yaml")
    assert result.exit_code == 0
    assert result.stdout[0] != "{"
    assert_like(EXP, yaml.safe_load(result.stdout))


def test_export_get_report(cli, api, mocker):
    mocker.patch("time.sleep")
    report = "foo,bar,baz\nx,z,y\n"
    report_gz = gzip.compress("foo,bar,baz\nx,z,y\n".encode("utf8"))
    responses = [
        ({"url": None}, 202, {}),
        ({"url": f"{api.url}/download"}, 200, {}),
    ]
    response_it = iter(responses)
    api.add_response(EXP_URL, {**EXP, "status2": "completed"})
    api.add_callback(f"{EXP_URL}/report", lambda: next(response_it))
    api.add_response("/download", report_gz)

    result = cli.run(f"export get {E_ID} --report=csv")

    assert result.exit_code == 0
    assert result.stdout == report


def test_export_get_report_before_completion(cli):
    result = cli.run(f"export get {E_ID} --report=jsonl")
    assert result.exit_code == 0
    assert result.stdout == "Reports are only available after completion\n"


def test_export_cancel(cli, api):
    progress = copy.deepcopy(PROGRESS)
    progress["status"] = "canceled"
    for k, v in progress["totals"].items():
        progress["totals"][k] = 0
    api.add_response(f"/xfer/exports/{E_ID}/progress2", progress, method="GET")
    api.add_response(f"/xfer/exports/{E_ID}/cancel", EXP, method="POST")
    result = cli.run(f"export cancel {E_ID} --no-wait")
    assert result.exit_code == 0
    assert result.stdout.strip() == (
        "Export 000000000000000000000003   cancelled  10s\n"
        "Project fw://grp/prj\n"
        "Storage s3://bucket"
    )


def test_export_rerun(cli, api):
    api.add_response(f"/xfer/exports/{E_ID}/rerun", EXP, method="POST")
    result = cli.run(f"export rerun {E_ID} --no-wait")
    assert api.request_log[0] == f"POST /xfer/exports/{E_ID}/rerun"
    assert result.exit_code == 0
    assert result.stdout.strip() == (
        "Export 000000000000000000000003   running  10s\n"
        "Project fw://grp/prj\n"
        "Storage s3://bucket\n"
        "Discovered         10 files        20B\n"
        "Processed           6 files        10B\n"
        "Skipped             3 files         5B\n"
        "Transferred         3 files         5B"
    )


def test_export_path_escape_curly(cli, api):
    result = cli.run(f"{EXPORT_CMD} --path " + r"'{acq}\{\}'")
    assert result.exit_code == 0
    expected = export_post(rules=[{"path": r"{acquisition.label}\{\}"}])
    assert_like(expected, api.request_map["POST /xfer/exports"].json)


def test_export_raises_with_invalid_field(cli):
    result = cli.run(f"{EXPORT_CMD} --level subject --path '{{acq}}'")
    assert result.exit_code == 1
    result = cli.run(f"{EXPORT_CMD} --path '{{invalid}}'")
    assert result.exit_code == 1
    # TODO improve and validate error message


def test_export_raises_with_invalid_filter_filed(cli):
    result = cli.run(f"{EXPORT_CMD} --level subject --include acq=b")
    assert result.exit_code == 1
    # TODO improve and validate error message


def test_list_export(cli, api, mocker):
    now = datetime.now()
    mocker.patch("fw_utils.get_datetime", now)
    id_0 = f"{'ID':<24}"  # header
    exp_1 = EXP.copy()
    exp_1["_id"] = id_1 = f"{0:024d}"
    exp_1["project"] = {"parents": {"group": "grp"}, "label": "prj"}
    exp_1["storage"] = {"url": "s3://bucket"}
    exp_2 = exp_1.copy()
    exp_2["_id"] = id_2 = f"{1:024d}"
    exp_2["status"] = "success"
    exp_2["completed"] = format_datetime(now - timedelta(seconds=10))
    exp_3 = EXP.copy()
    exp_3["_id"] = id_3 = f"{0:024d}"
    exp_3["status"] = "success"
    del exp_3["total_time"]
    api.add_response("/xfer/exports", {"results": [exp_1, exp_2, exp_3]})
    result = cli.run("export list")
    assert result.exit_code == 0
    assert result.stdout == (
        f"{id_0}  Status   Elapsed  Completed  Source        Destination  \n"
        f"{id_1}  running  10s      -          fw://grp/prj  s3://bucket  \n"
        f"{id_2}  success  10s      10s ago    fw://grp/prj  s3://bucket  \n"
        f"{id_3}  success  -        -          fw://grp/prj  s3://bucket  \n"
    )


def test_list_exports_empty(cli, api):
    api.add_response("/xfer/exports", {"results": []})
    result = cli.run("exports list")
    assert result.exit_code == 0
    assert result.stdout == "Nothing to show\n"


def export_post(**kwargs):
    kwargs.setdefault("label", re.compile(f"Export - created via CLI at {DT_RE}"))
    refs = kwargs.setdefault("refs", {})
    refs.setdefault("project", P_ID)
    refs.setdefault("storage", S_ID)
    root_tmpl = kwargs.setdefault("rules", [{}])[0]
    root_tmpl.setdefault("level", "acquisition")
    root_tmpl.setdefault("include", [])
    root_tmpl.setdefault("exclude", [])
    path = (
        "{project.label}/{subject.label}/{session.label}/"
        "{acquisition.label}/{file.name}"
    )
    root_tmpl.setdefault("path", path)
    root_tmpl.setdefault("unzip", False)
    root_tmpl.setdefault("unzip_path", "basename")
    kwargs.setdefault("overwrite_existing", "auto")
    kwargs.setdefault("delete_extra", False)
    return kwargs


def test_report(cli, api):
    csv = b"src_path,dst_path,status,status_reason\r\nsrc,dst,success,\r\n"
    api.add_response(f"/xfer/exports/{E_ID}/report", {"url": f"{api.url}/signed-url"})
    api.add_callback("/signed-url", lambda: (gzip.compress(csv), 200))
    exp_resp = EXP.copy()
    exp_resp["status"] = "success"
    api.add_response(f"/xfer/exports/{E_ID}", exp_resp)
    result = cli.run(f"export get {E_ID} --report=csv")
    assert result.exit_code == 0


def test_export_schedule_create(cli, api):
    cmd = f"export schedule create -p {P_ID} -s {S_ID} "
    cmd += "--start 2022-01-01 --end 2022-01-02 --cron '* * * * *'"
    result = cli.run(cmd)
    assert result.exit_code == 0
    assert result.stdout.strip() == f"Created schedule with ID {SC_ID}"
    assert_like(
        {
            "start": format_datetime(get_datetime("2022-01-01")),
            "end": format_datetime(get_datetime("2022-01-02")),
            "cron": "* * * * *",
            "operation": {
                "type": "export",
                "label": "Scheduled export - created via CLI",
            },
        },
        api.request_map["POST /xfer/schedules"].json,
    )


def test_export_schedule_list(cli, api):
    cmd = "export schedule list"
    result = cli.run(cmd)

    assert result.exit_code == 0
    assert result.stdout == (
        "ID                        Source        Destination  \n"
        "000000000000000000000004  fw://grp/prj  s3://bucket  \n"
    )


def test_export_schedule_list_all(cli, api):
    cmd = "export schedule list --all"
    result = cli.run(cmd)

    assert result.exit_code == 0
    assert result.stdout == (
        "ID                        Source        Destination  \n"
        "000000000000000000000004  fw://grp/prj  s3://bucket  \n"
    )


def test_export_schedule_list_empty(cli, api):
    api.add_response("/xfer/schedules", {"results": []}, method="GET")
    cmd = "export schedule list"
    result = cli.run(cmd)

    assert result.exit_code == 0
    assert result.stdout == "Nothing to show\n"


def test_export_schedule_get(cli, api):
    cmd = f"export schedule get {SC_ID}"
    result = cli.run(cmd)

    assert result.exit_code == 0

    assert result.stdout == (
        "ID 000000000000000000000004\n"
        "Start 2022-01-01 00:00:00\n"
        "End 2022-01-01 00:00:00\n"
        "Cron * * * * *\n"
        "Project fw://grp/prj\n"
        "Storage s3://bucket\n"
    )


def test_export_schedule_get_invalid(cli, api):
    sch = copy.deepcopy(SCHEDULE)
    sch["operation"]["type"] = "invalid"
    api.add_response(f"/xfer/schedules/{SC_ID}", sch, method="GET")

    cmd = f"export schedule get {SC_ID}"
    result = cli.run(cmd)

    assert result.exit_code == 1


def test_export_schedule_get_not_found(cli, api):
    resp = {"message": "No such schedule"}
    api.add_response(f"/xfer/schedules/{ID_0}", resp, status=404)
    cmd = f"export schedule get {ID_0}"
    result = cli.run(cmd)
    assert result.exit_code == 1
    assert "Error: No such schedule" in result.stderr


def test_export_schedule_update(cli, api):
    cmd = f"export schedule update {SC_ID} --cron '*/5 * * * *' --start null"
    result = cli.run(cmd)
    assert result.exit_code == 0
    assert_like(
        {"cron": "*/5 * * * *", "start": None},
        api.request_map[f"PATCH /xfer/schedules/{SC_ID}"].json,
    )


def test_export_schedule_update_raises_w_invalid_op_type(cli, api):
    schedule = {"_id": SC_ID, "operation": {"type": "import"}}
    api.add_response(f"/xfer/schedules/{SC_ID}", schedule)
    cmd = f"export schedule update {SC_ID} --cron '*/5 * * * *'"
    result = cli.run(cmd)
    assert result.exit_code == 1
    assert "Unexpected schedule operation type" in result.stderr


def test_export_schedule_cancel(cli, api):
    cmd = f"export schedule cancel {SC_ID}"
    result = cli.run(cmd)
    assert result.exit_code == 0
    assert_like(
        {"active": False}, api.request_map[f"PATCH /xfer/schedules/{SC_ID}"].json
    )


def test_export_schedule_cancel_raises_w_invalid_op_type(cli, api):
    schedule = {"_id": SC_ID, "operation": {"type": "import"}}
    api.add_response(f"/xfer/schedules/{SC_ID}", schedule)
    cmd = f"export schedule cancel {SC_ID}"
    result = cli.run(cmd)
    assert result.exit_code == 1
    assert "Unexpected schedule operation type" in result.stderr


def test_export_with_invalid_fail_fast(cli):
    result = cli.run(f"export run -p {P_ID} -s storage --fail-fast invalid")
    assert result.exit_code == 2
    expected = "Error: Invalid value for '--fail-fast': Invalid number/percent"
    assert expected in result.stderr


def expected_output(stats):
    stats = attrify(stats)
    id_line_parts = [f"Export {ID_0}"]
    stage = stats.get("stage")
    if stage:
        id_line_parts.append(f"- {stage.replace('_', ' ')} ...")
    cb, tb = stats.completed_bytes, stats.total_bytes
    c_bar = int((40 / (tb or 1)) * (cb or 0))
    pbar_parts = [f"{stats.status:<10}", f"{'━' * c_bar:<40}"]
    if stats.total_time is not None:
        pbar_parts.append(f"{hrtime(stats.total_time):>8}")
    if cb is not None and tb is not None:
        cb_tb = f"{hrsize(cb)}/{hrsize(tb)}"
        pbar_parts.append(f"{cb_tb:>10}")
    if stats.bytes_per_sec is not None:
        speed = f"{hrsize(stats.bytes_per_sec)}/s"
        pbar_parts.append(f"{speed:>8}")
    return f"{' '.join(id_line_parts)}\n{' '.join(pbar_parts)}"
