"""Test login and logout."""

import re

import pytest
from fw_utils import AttrDict, assert_like


@pytest.fixture(autouse=True)
def config():
    return


@pytest.fixture()
def profiles(cli):
    default = {
        "name": "default",
        "api_key": "default.flywheel.io:user",
        "id": "user@flywheel.io",
        "is_admin": False,
        "is_drone": False,
    }
    other = {
        "name": "other",
        "api_key": "other.flywheel.io:user",
        "id": "user@flywheel.io",
        "is_admin": False,
        "is_drone": False,
    }
    cli.set_config({"profiles": [default, other]})
    return AttrDict(
        default="default.flywheel.io as user@flywheel.io (profile: default)",
        other="other.flywheel.io as user@flywheel.io (profile: other)",
    )


def test_login_status_without_profiles(cli):
    result = cli.run("login --status")
    assert result.exit_code == 0
    assert result.stdout == "Not logged in\n"


def test_login_status_with_profiles(cli, profiles):
    result = cli.run("login --status")
    assert result.exit_code == 0
    assert result.stdout == f"Logged in to {profiles.default}\n"


def test_login_status_with_profiles_as_other(cli, profiles):
    result = cli.run("--profile other login --status")
    assert result.exit_code == 0
    assert result.stdout == f"Logged in to {profiles.other}\n"


def test_login_list_without_profiles(cli):
    result = cli.run("login --list")
    assert result.exit_code == 0
    assert result.stdout == "No login profiles found\n"


def test_login_list_with_profiles(cli, profiles):
    result = cli.run("login --list")
    assert result.exit_code == 0
    assert result.stdout == f"* {profiles.default}\n  {profiles.other}\n"


def test_login_list_with_profiles_as_other(cli, profiles):
    result = cli.run("--profile other login --list")
    assert result.exit_code == 0
    assert result.stdout == f"  {profiles.default}\n* {profiles.other}\n"


def test_login_with_invalid_api_key_format(cli):
    result = cli.run("login --api-key foo")
    assert result.exit_code == 1
    assert result.stderr.endswith(
        "Error: Invalid API key format - expected <site>:<key>\n"
    )


def test_login_with_invalid_api_url(cli):
    result = cli.run("login --api-key flywheel.test:key")
    assert result.exit_code == 1
    assert re.search(r"not (known|resolve)", result.stderr)


def test_login_with_envvar(cli, api):
    api_key = f"{api.url}:key"
    auth_status = {
        "origin": {"id": "user@flywheel.io"},
        "user_is_admin": False,
        "is_device": False,
    }
    api.add_response("/api/auth/status", auth_status)

    result = cli.run("login", env={"FW_CLI_API_KEY": api_key})

    assert result.exit_code == 0
    assert result.stdout == (
        f"Logged in to {api.url} as user@flywheel.io (profile: default)\n"
    )
    expected_request = {
        "method": "GET",
        "url": "/api/auth/status",
        "headers": {"Authorization": "scitran-user key"},
    }
    assert_like([expected_request], api.requests)
    expected_profile = {
        "name": "default",
        "api_key": api_key,
        "id": "user@flywheel.io",
        "is_admin": False,
        "is_drone": False,
    }
    assert_like({"profiles": [expected_profile]}, cli.get_config())


def test_login_replace_profile(cli, api, profiles):
    api_key = f"{api.url}:admin"
    auth_status = {
        "origin": {"id": "admin@flywheel.io"},
        "user_is_admin": True,
        "is_device": False,
    }
    api.add_response("/api/auth/status", auth_status)

    result = cli.run("login", env={"FW_CLI_API_KEY": api_key})

    assert result.exit_code == 0
    assert result.stdout == (
        f"Logged in to {api.url} as admin@flywheel.io (profile: default)\n"
    )
    expected_profile = {
        "name": "default",
        "api_key": api_key,
        "id": "admin@flywheel.io",
        "is_admin": True,
        "is_drone": False,
    }
    assert_like({"profiles": [expected_profile, ...]}, cli.get_config())


def test_logout_all_with_profiles(cli, profiles):
    result = cli.run("logout --all")
    assert result.exit_code == 0
    assert result.stdout == "Logged out from all saved profiles\n"
    assert cli.get_config() == {"profiles": []}


def test_logout_all_without_profiles(cli):
    result = cli.run("logout --all")
    assert result.exit_code == 0
    assert result.stdout == "Not logged in\n"


def test_logout_with_profiles(cli, profiles):
    result = cli.run("logout")
    assert result.exit_code == 0
    assert result.stdout == f"Logged out from {profiles.default}\n"
    assert_like({"profiles": [{"name": "other"}]}, cli.get_config())


def test_logout_with_profiles_as_other(cli, profiles):
    result = cli.run("--profile other logout")
    assert result.exit_code == 0
    assert result.stdout == f"Logged out from {profiles.other}\n"
    assert_like({"profiles": [{"name": "default"}]}, cli.get_config())


def test_logout_without_profiles(cli):
    result = cli.run("logout")
    assert result.exit_code == 0
    assert result.stdout == "Not logged in\n"
