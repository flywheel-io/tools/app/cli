"""Shared test fixtures."""

import os
import shlex
import shutil
from functools import partial
from pathlib import Path

import pytest
import rich
import yaml
from typer.testing import CliRunner

import fw_cli.main
import fw_cli.state
from fw_cli.main import fw
from fw_cli.state import get_fw_client, get_state

pytest_plugins = "fw_http_testserver"
assets = Path(__file__).parent / "assets"
# patch retry total to avoid hanging tests
fw_cli.state.RETRY_TOTAL = 0


def pytest_addoption(parser):
    parser.addoption("--no-wrap", action="store_true", help="raise raw cli exceptions")


@pytest.fixture(scope="session", autouse=True)
def no_wrap_session(request):
    # NOTE alters behavior for tests relying on the broad exc handler in main
    fw_cli.main.wrap_errors = not request.config.getoption("--no-wrap")


@pytest.fixture
def no_wrap(mocker):
    with mocker.patch("fw_cli.main.wrap_errors", False):
        yield


@pytest.fixture
def cli(app_dir, env):
    return FWRunner(mix_stderr=False)


@pytest.fixture
def cli_admin(cli):
    existing = cli.get_config()
    existing["profiles"][0]["is_admin"] = True
    cli.set_config(existing)
    return cli


@pytest.fixture
def app_dir(tmp_path):
    cwd = os.getcwd()
    os.chdir(tmp_path)
    try:
        yield tmp_path
    finally:
        os.chdir(cwd)


@pytest.fixture
def env(app_dir, mocker):
    if "fw" in vars(get_state()):
        get_state().fw.close()
    get_state.cache_clear()
    env = {
        "LANG": "C.UTF-8",
        "LC_ALL": "C.UTF-8",
        "FW_CLI_CONFIG_DIR": str(app_dir),
        "FW_CLI_DEBUG": "true",
        "COLUMNS": "100",
    }
    return mocker.patch.dict(os.environ, env, clear=True)


@pytest.fixture
def api(http_testserver):
    return http_testserver


@pytest.fixture(autouse=True)
def config(cli, api):
    profile = {
        "name": "default",
        "api_key": f"{api.url}:user",
        "id": "user@flywheel.io",
        "is_admin": False,
        "is_drone": False,
    }
    cli.set_config({"profiles": [profile]})
    fw_cli.state.get_fw_client = partial(get_fw_client, xfer_url=api.url)


@pytest.fixture
def default_gear(tmp_path):
    def gen():
        shutil.copy(
            assets / "dcm2niix.json",
            tmp_path / "manifest.json",
        )
        shutil.copy(
            assets / "dcm2niix_default_config.json",
            tmp_path / "config.json",
        )
        return tmp_path

    return gen


class FWRunner(CliRunner):
    config_file = Path("config.yml")

    def get_config(self):
        if self.config_file.exists():
            return yaml.safe_load(self.config_file.read_text(encoding="utf8"))
        return None

    def set_config(self, config):
        if config is not None:
            self.config_file.write_text(yaml.safe_dump(config), encoding="utf8")
        elif self.config_file.exists():
            self.config_file.unlink()

    def run(self, cmd, **kwargs):
        kwargs.setdefault("catch_exceptions", False)
        rich.reconfigure(highlighter=None, soft_wrap=True)
        return self.invoke(fw, args=shlex.split(cmd), prog_name="fw", **kwargs)
