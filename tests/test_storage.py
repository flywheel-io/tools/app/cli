"""Storage admin commands tests."""

import json
import re

import pytest
import yaml
from fw_utils import assert_like

ID_0 = "000000000000000000000000"
DT_RE = r"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d"
STORAGE = {
    "_id": ID_0,
    "config": {"type": "fs"},
    "label": "Storage",
    "status_check": "success",
    "url": "fs:///tmp",
}


def test_create_with_defaults(cli, api):
    api.add_response("/xfer/storages", STORAGE, method="POST")
    result = cli.run(f"admin storage create --url {STORAGE['url']}")
    assert result.exit_code == 0
    assert result.stdout == (
        "   ID 000000000000000000000000 \n"
        "  URL fs:///tmp                \n"
        "Label Storage                  \n"
        "Check success                  "
    )
    request = api.pop_first()
    expected = {
        "label": re.compile(f"Created via CLI at {DT_RE}"),
        "url": "fs:///tmp",
    }
    assert_like(expected, request.json)


def test_create_for_import_only(cli, api):
    api.add_response("/xfer/storages", STORAGE, method="POST")
    result = cli.run(f"admin storage create --url {STORAGE['url']} -m import")
    assert result.exit_code == 0
    request = api.pop_first()
    expected = {
        "imports_enabled": True,
        "exports_enabled": False,
    }
    assert_like(expected, request.json)


def test_create_for_export_only(cli, api):
    api.add_response("/xfer/storages", STORAGE, method="POST")
    result = cli.run(f"admin storage create --url {STORAGE['url']} -m export")
    assert result.exit_code == 0
    request = api.pop_first()
    expected = {
        "imports_enabled": False,
        "exports_enabled": True,
    }
    assert_like(expected, request.json)


def test_create_with_options(cli, api):
    api.add_response("/xfer/storages", STORAGE, method="POST")
    cmd = "admin storage create --url fs:///tmp --label label"
    cmd = f"{cmd} --connector {ID_0} --group {ID_0} --project {ID_0}"
    result = cli.run(cmd)
    assert result.exit_code == 0
    request = api.pop_first()
    assert request.json == {
        "label": "label",
        "url": "fs:///tmp",
        "refs": {"connector": ID_0, "group": ID_0, "project": ID_0},
        "imports_enabled": True,
        "exports_enabled": True,
    }


def test_create_fs_w_relative_path_raises(cli):
    result = cli.run("admin storage create --url=fs://tmp --label=label")
    assert result.exit_code == 1
    assert "Absolute fs:// storage path required" in result.stderr


@pytest.mark.parametrize("status,code", [("failed", 1), ("success", 0)])
def test_create_exit_codes(cli, api, status, code):
    storage = STORAGE.copy()
    storage["status_check"] = status
    api.add_response("/xfer/storages", storage, method="POST")
    result = cli.run(f"admin storage create --url {STORAGE['url']}")
    assert result.exit_code == code


def test_create_gs_key_json(cli, api):
    api.add_response("/xfer/storages", STORAGE, method="POST")
    cmd = (
        "admin storage create --label label --url gs://bucket/prefix?"
        'application_credentials={\'"type":"service_account"\'}'
    )
    cmd = f"{cmd} --connector {ID_0} --group {ID_0} --project {ID_0}"
    result = cli.run(cmd)
    assert result.exit_code == 0
    request = api.pop_first()
    assert request.json == {
        "label": "label",
        "url": 'gs://bucket/prefix?application_credentials={"type":"service_account"}',
        "refs": {"connector": ID_0, "group": ID_0, "project": ID_0},
        "imports_enabled": True,
        "exports_enabled": True,
    }


def test_create_gs_key_file(cli, api, tmp_path):
    key_json = tmp_path / "key.json"
    key_json.write_text('{"type":"service_account"}')
    api.add_response("/xfer/storages", STORAGE, method="POST")
    cmd = (
        "admin storage create --label label --url gs://bucket/prefix?"
        f"application_credentials={key_json}"
    )
    cmd = f"{cmd} --connector {ID_0} --group {ID_0} --project {ID_0}"
    result = cli.run(cmd)
    assert result.exit_code == 0
    request = api.pop_first()
    assert request.json == {
        "label": "label",
        "url": 'gs://bucket/prefix?application_credentials={"type":"service_account"}',
        "refs": {"connector": ID_0, "group": ID_0, "project": ID_0},
        "imports_enabled": True,
        "exports_enabled": True,
    }


def test_create_from_provider(cli, api):
    storage = STORAGE.copy()
    storage["provider"] = ID_0
    api.add_response("/xfer/storages", storage, method="POST")
    result = cli.run(f"admin storage create --provider {ID_0}")
    assert result.exit_code == 0
    assert result.stdout == (
        "       ID 000000000000000000000000 \n"
        "      URL fs:///tmp                \n"
        "    Label Storage                  \n"
        "Reference True                     \n"
        "    Check success                  "
    )
    request = api.pop_first()
    expected = {
        "label": re.compile(f"Created via CLI at {DT_RE}"),
        "provider": ID_0,
    }
    assert_like(expected, request.json)


def test_create_raises_when_wrong_params_provided(cli):
    # none of url/provider
    result = cli.run("admin storage create")
    assert result.exit_code == 1
    # both url and provider
    result = cli.run(f"admin storage create --provider {ID_0} --url fs:///tmp")
    assert result.exit_code == 1


def test_list_storages_empty(cli, api):
    api.add_response("/xfer/storages", {"total": 0, "results": []})
    result = cli.run("admin storages list")
    assert result.exit_code == 0
    assert result.stdout == "Nothing to show\n"


def test_list_storages(cli, api):
    storage_2 = STORAGE.copy()
    storage_2["label"] = "Storage 2"
    api.add_response("/xfer/storages", {"results": [STORAGE, storage_2]})
    result = cli.run("admin storages list")
    assert result.exit_code == 0
    assert result.stdout == (
        "ID                       Type Check   Label     \n"
        "000000000000000000000000 fs   success Storage   \n"
        "000000000000000000000000 fs   success Storage 2 \n"
    )


def test_list_storages_pagination(cli, api):
    def callback():
        if api.request.args.get("after_id") == ID_0:
            return {"results": [STORAGE]}
        return {"results": []}, 200

    api.add_callback("/xfer/storages", callback)
    result = cli.run(f"admin storages list --after-id {ID_0}")
    assert result.exit_code == 0
    assert result.stdout == (
        "ID                       Type Check   Label   \n"
        "000000000000000000000000 fs   success Storage \n"
    )


def test_list_storages_filtering(cli, api):
    def callback():
        if api.request.args.get("filter") == "config.type==fs,status_check==success":
            return {"results": [STORAGE]}
        return {"results": []}, 200

    api.add_callback("/xfer/storages", callback)
    filters = "--filter config.type==fs --filter status_check==success"
    result = cli.run(f"admin storages list {filters}")
    assert result.exit_code == 0
    assert result.stdout == (
        "ID                       Type Check   Label   \n"
        "000000000000000000000000 fs   success Storage \n"
    )


def test_list_storages_sorting(cli, api):
    def callback():
        if api.request.args.get("sort") == "created:desc":
            return {"results": [STORAGE]}
        return {"results": []}, 200

    api.add_callback("/xfer/storages", callback)
    result = cli.run("admin storages list --sort created:desc")
    assert result.exit_code == 0
    assert result.stdout == (
        "ID                       Type Check   Label   \n"
        "000000000000000000000000 fs   success Storage \n"
    )


def test_list_storages_json_output(cli, api):
    page = {"count": 1, "total": 2, "after_id": ID_0, "results": [STORAGE]}
    api.add_response("/xfer/storages", page)
    result = cli.run("admin storages list --output json")
    assert result.exit_code == 0
    assert_like(page, json.loads(result.stdout))


def test_list_storages_yaml_output(cli, api):
    page = {"count": 1, "total": 2, "after_id": ID_0, "results": [STORAGE]}
    api.add_response("/xfer/storages", page)
    result = cli.run("admin storages list --output yaml")
    assert result.exit_code == 0
    assert_like(page, yaml.safe_load(result.stdout))


def test_get_not_found(cli, api):
    resp = {"message": "No such storage"}
    api.add_response(f"/xfer/storages/{ID_0}", resp, status=404)
    result = cli.run(f"admin storage get {ID_0}")
    assert result.exit_code == 1
    assert "Error: No such storage" in result.stderr


def test_get(cli, api):
    api.add_response(f"/xfer/storages/{ID_0}", STORAGE)
    result = cli.run(f"admin storage get {ID_0}")
    assert result.exit_code == 0
    assert result.stdout == (
        "   ID 000000000000000000000000 \n"
        "  URL fs:///tmp                \n"
        "Label Storage                  \n"
        "Check success                  \n"
    )


def test_get_storage_with_notes(cli, api):
    storage = STORAGE.copy()
    storage["notes"] = "some note"
    api.add_response(f"/xfer/storages/{ID_0}", storage)
    result = cli.run(f"admin storage get {ID_0}")
    assert result.exit_code == 0
    assert result.stdout == (
        "   ID 000000000000000000000000 \n"
        "  URL fs:///tmp                \n"
        "Label Storage                  \n"
        "Check success                  \n"
        "Notes some note                \n"
    )


def test_get_storage_with_failed_status_check(cli, api):
    storage = STORAGE.copy()
    storage["status_check"] = "failed"
    storage["status_check_reason"] = "failure reason"
    api.add_response(f"/xfer/storages/{ID_0}", storage)
    result = cli.run(f"admin storage get {ID_0}")
    assert result.exit_code == 1
    assert result.stdout == (
        "    ID 000000000000000000000000 \n"
        "   URL fs:///tmp                \n"
        " Label Storage                  \n"
        " Check failed                   \n"
        "Reason failure reason           \n"
    )


def test_get_json_output(cli, api):
    api.add_response(f"/xfer/storages/{ID_0}", STORAGE)
    result = cli.run(f"admin storage get {ID_0} -o json")
    assert result.exit_code == 0
    assert_like(STORAGE, json.loads(result.stdout))


def test_get_yaml_output(cli, api):
    api.add_response(f"/xfer/storages/{ID_0}", STORAGE)
    result = cli.run(f"admin storage get {ID_0} -o yaml")
    assert result.exit_code == 0
    assert_like(STORAGE, yaml.safe_load(result.stdout))


@pytest.mark.parametrize("status,code", [("failed", 1), ("success", 0)])
def test_get_exit_codes(cli, api, status, code):
    storage = STORAGE.copy()
    storage["status_check"] = status
    api.add_response(f"/xfer/storages/{ID_0}", storage)
    result = cli.run(f"admin storage get {ID_0}")
    assert result.exit_code == code


def test_update(cli, api):
    storage = STORAGE.copy()
    api.add_response(f"/xfer/storages/{ID_0}", storage, method="GET")
    api.add_response(f"/xfer/storages/{ID_0}", storage, method="PATCH")
    result = cli.run(f"admin storage update {ID_0} --label foo")
    assert result.exit_code == 0
    assert result.stdout == (
        "   ID 000000000000000000000000 \n"
        "  URL fs:///tmp                \n"
        "Label Storage                  \n"
        "Check success                  "
    )
    request = api.requests[-1]
    expected = {"label": "foo"}
    assert_like(expected, request.json)
    result = cli.run(f"admin storage update {ID_0} --no-scan")
    assert result.exit_code == 0
    request = api.requests[-1]
    expected = {"malware_scan": False}
    assert_like(expected, request.json)
    result = cli.run(f"admin storage update {ID_0} --config chown")
    assert result.exit_code == 1
    result = cli.run(f"admin storage update {ID_0} --config type=s3")
    assert result.exit_code == 1
    result = cli.run(f"admin storage update {ID_0} --config path=/tmp")
    assert result.exit_code == 1
    result = cli.run(f"admin storage update {ID_0} --config bucket=foo")
    assert result.exit_code == 1
    config = "--config chown=None --config chmod=664"
    result = cli.run(f"admin storage update {ID_0} {config}")
    assert result.exit_code == 0
    request = api.requests[-1]
    expected = {"config": {"chown": None, "chmod": "664"}}
    assert_like(expected, request.json)


def test_delete_storage(cli, api):
    api.add_response(f"/xfer/storages/{ID_0}", STORAGE, method="DELETE")
    result = cli.run(f"admin storage delete {ID_0}")
    assert result.exit_code == 0
    assert result.stdout.strip() == "Storage deleted"


def test_delete_storage_not_found(cli):
    result = cli.run(f"admin storage delete {ID_0}")
    assert result.exit_code == 1
    assert "Error: Storage not found" in result.stderr
