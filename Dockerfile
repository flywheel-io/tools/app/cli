FROM flywheel/python:3.12-build AS base
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
WORKDIR /src
ENV FW_INIT="test -z \"\${FW_CLI_API_KEY:-}\" || fw-beta login"
RUN apk add --no-cache bash-completion git; \
    echo ". /usr/share/bash-completion/bash_completion" >>~/.bashrc;
COPY requirements.txt ./
RUN uv pip install -rrequirements.txt

FROM base AS dev
RUN apk add --no-cache cargo make; \
    uv pip install gsutil poetry pyopenssl==24.2.1
COPY requirements-dev.txt ./
RUN uv pip install -rrequirements-dev.txt
COPY . .
RUN uv pip install --no-deps -e.; \
    fw-beta --completion >/etc/bash_completion.d/fw-beta

FROM base
COPY . .
RUN uv pip install --no-deps -e.; \
    fw-beta --completion >/etc/bash_completion.d/fw-beta; \
    /cleanup_build_deps.sh
USER flywheel
