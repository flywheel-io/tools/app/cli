SHELL     := bash
SHELLOPT  := -Eeuo pipefail
PLATFORM  ?= $(shell uname -sm | tr "[:upper:]" "[:lower:]" | tr " " "-")
BUILD_DIR := build
CACHE_DIR := .cache

PYTHON_VERSION   := $(shell cat PYTHON_VERSION)
PYTHON_MINOR     := $(shell PYTHON_VERSION=$(PYTHON_VERSION); echo $${PYTHON_VERSION%.*})
PYTHON_BUILD_DIR := $(BUILD_DIR)/python-$(PLATFORM)
PYTHON_ARCHIVE   := dist/python-$(PYTHON_VERSION)-$(PLATFORM).tar.gz

PYTHON_RELEASES := $(CACHE_DIR)/python-build-standalone-releases.json
PYTHON_DOWNLOAD := $(CACHE_DIR)/python-$(PYTHON_VERSION)_$(PLATFORM).tar.zst

ifdef CI_COMMIT_TAG
CLI_FULLVER := $(CI_COMMIT_TAG)
CLI_VERSION := $(CI_COMMIT_TAG)
else
VERSION     := $(shell grep "^version" pyproject.toml | sed -E 's/.*"(.*)"/\1/')
BRANCH      := $(or $(CI_COMMIT_REF_NAME),$(shell git rev-parse --abbrev-ref HEAD))
COMMIT      := $(or $(CI_COMMIT_SHORT_SHA),$(shell git rev-parse --short=8 HEAD))
CLI_FULLVER := $(VERSION)+$(BRANCH).$(COMMIT)
CLI_VERSION := $(subst main,latest,$(BRANCH))
endif
INSTALL_SCRIPT   := install.sh
CLI_BUILD_DIR    := $(BUILD_DIR)/cli-$(PLATFORM)
CLI_BUILD_SUBDIR := $(CLI_BUILD_DIR)/lib/python$(PYTHON_MINOR)/site-packages
CLI_ARCHIVE      := dist/fw-cli-$(CLI_VERSION)-$(PLATFORM).tar.gz
WHEEL_VER        := $(shell echo $(CLI_FULLVER) | tr '[:upper:]' '[:lower:]' | tr - .)
WHEEL_NAME       := fw_cli-$(WHEEL_VER)-py3-none-any.whl
WHEEL            := dist/$(WHEEL_NAME)
WHEEL_ARCHIVE    := dist/fw-cli-$(CLI_VERSION)-wheel.tar.gz

ifeq ($(PLATFORM),linux-x86_64)
PLATFORM_PYTHON  := cpython-$(PYTHON_VERSION).*x86_64-unknown-linux-gnu-pgo%2Blto
PLATFORM_WHEEL   := manylinux_2_17_x86_64  # glibc sync w/ py-standalone and LTS
else ifeq ($(PLATFORM),linux-aarch64)
PLATFORM_PYTHON  := cpython-$(PYTHON_VERSION).*aarch64-unknown-linux-gnu-lto
PLATFORM_WHEEL   := manylinux_2_17_aarch64
else ifeq ($(PLATFORM),darwin-x86_64)
PLATFORM_PYTHON  := cpython-$(PYTHON_VERSION).*x86_64-apple-darwin-pgo%2Blto
PLATFORM_WHEEL   := macosx_12_0_x86_64
else ifeq ($(PLATFORM),darwin-arm64)
PLATFORM_PYTHON  := cpython-$(PYTHON_VERSION).*aarch64-apple-darwin-pgo%2Blto
PLATFORM_WHEEL   := macosx_12_0_arm64
else ifeq ($(PLATFORM),windows-x86_64)
PLATFORM_PYTHON  := cpython-$(PYTHON_VERSION).*x86_64-pc-windows-msvc-pgo
PLATFORM_WHEEL   := win_amd64
CLI_BUILD_SUBDIR := $(CLI_BUILD_DIR)/Lib/site-packages
INSTALL_SCRIPT   := install.ps1
endif
PLATFORM_SYSTEM  := $(shell PLATFORM=$(PLATFORM) echo $${PLATFORM^} | cut -d "-" -f 1)
CLI_INSTALLER    := dist/$(INSTALL_SCRIPT)
PIP_INSTALL_ARGS := --no-deps --no-compile --upgrade --target $(CLI_BUILD_SUBDIR)

# github started playing dirty against bots - adding auth if available
ifdef GITHUB_TOKEN
GITHUB_HEADERS := 'authorization:bearer $(GITHUB_TOKEN)'
endif

all: linux-x86_64 linux-aarch64 darwin-x86_64 darwin-arm64 windows-x86_64  ## Create all-platform dist/ folder
	make dist
linux-x86_64 linux-aarch64 darwin-x86_64 darwin-arm64 windows-x86_64:
	PLATFORM=$@ make build

build: $(PYTHON_ARCHIVE) $(CLI_ARCHIVE) $(CLI_INSTALLER) $(WHEEL_ARCHIVE)  ## Build python and cli for PLATFORM

dist: $(PLATFORM)  ## Create dist/ folder for PLATFORM
	cd dist; \
	SHA_CMD=`which shasum &>/dev/null && echo "shasum -a256" || echo sha256sum`; \
	for FILE in `find . -type f -name '*.tar.gz' | sed -E 's|^./||' | sort`; do \
		$$SHA_CMD $$FILE >$$FILE.sha256; \
	done

clean:  ## Delete all files generated via make
	rm -rf $(BUILD_DIR) $(CACHE_DIR) dist install

help:  ## Show this help and exit
	@printf "Usage: make TARGET\n\n"
	@grep "##" Makefile | grep -v Makefile | sed -E "s/(.*):.*## (.*)/\1\t\2/;s/PLATFORM/$(PLATFORM)/"


$(PYTHON_ARCHIVE):
	mkdir -p $(PYTHON_BUILD_DIR) $(CACHE_DIR) dist
	test -s $(PYTHON_RELEASES) || xh -IFo$(PYTHON_RELEASES) \
		https://api.github.com/repos/astral-sh/python-build-standalone/releases \
		$(GITHUB_HEADERS)
	jq -r ".[].assets[].browser_download_url" $(PYTHON_RELEASES) \
		| grep $(PLATFORM_PYTHON) | head -n1 >$(PYTHON_BUILD_DIR)/python-build-standalone
	test -s $(PYTHON_DOWNLOAD) || xh -IFo$(PYTHON_DOWNLOAD) \
		`cat $(PYTHON_BUILD_DIR)/python-build-standalone` \
		$(GITHUB_HEADERS)
	zstd -cfd $(PYTHON_DOWNLOAD) | tar xC $(PYTHON_BUILD_DIR)
	cd $(PYTHON_BUILD_DIR)/python/install/$(shell test $(PLATFORM) = windows-x86_64 && echo "Lib" || echo "lib" ); \
		find . -type d \( -name test -o -name tests -o -name __pycache__ \) | xargs rm -rf; \
		python$(PYTHON_MINOR) -m compileall -bqj0 . 2>&1 | grep -v WmDefault.py || :; \
		find . -type f -name '*.py' | xargs rm -f
	rm -rf $(PYTHON_BUILD_DIR)/python/install/share
	tar czf $(PYTHON_ARCHIVE) -C $(PYTHON_BUILD_DIR)/python/install .

$(CLI_ARCHIVE): $(WHEEL)
	mkdir -p $(CLI_BUILD_SUBDIR) dist
	sed 's/\(\s*and\s*\)\?platform_system == "$(PLATFORM_SYSTEM)"//g' requirements.txt > $(BUILD_DIR)/requirements-$(PLATFORM).txt
	poetry run pip install --upgrade pip
	poetry run pip install $(PIP_INSTALL_ARGS) $(WHEEL)
	poetry run pip install $(PIP_INSTALL_ARGS) \
		--platform=$(PLATFORM_WHEEL) \
		--python-version=$(PYTHON_VERSION) \
		--only-binary=:all: \
		--requirement=$(BUILD_DIR)/requirements-$(PLATFORM).txt
	cd $(CLI_BUILD_SUBDIR); \
		find . -type d \( -name test -o -name tests -o -name __pycache__ \) | xargs rm -rf; \
		python$(PYTHON_MINOR) -m compileall -bqj0 . 2>&1 | grep -v WmDefault.py || :; \
		find . -type f -name '*.py' | xargs rm -f
	cp PYTHON_VERSION $(CLI_BUILD_DIR)/PYTHON_VERSION
	tar czf $(CLI_ARCHIVE) -C $(CLI_BUILD_DIR) .

$(CLI_INSTALLER): $(WHEEL)
	cp $(INSTALL_SCRIPT) dist && cd dist; \
	sed -Ei "s/CLI_VERSION=.*/CLI_VERSION=\"$(CLI_VERSION)\"/" $(INSTALL_SCRIPT)

$(WHEEL_ARCHIVE): $(WHEEL)
	cp requirements.txt dist
	tar czf $(WHEEL_ARCHIVE) -C dist requirements.txt $(WHEEL_NAME)
	SHA_CMD=`which shasum &>/dev/null && echo "shasum -a256" || echo sha256sum`; \
	$$SHA_CMD $(WHEEL_ARCHIVE) >$(WHEEL_ARCHIVE).sha256

$(WHEEL):
	sed -Ei.bkp "s/^version.*/version = \"$(CLI_FULLVER)\"/" pyproject.toml
	poetry build --format wheel
	mv pyproject.toml.bkp pyproject.toml
