$CLI_VERSION="latest"
$CMD = "fw-beta"
$USAGE = @"
Usage:
  TODO

Download and install Flywheel CLI beta to `$FW_CLI_INSTALL_DIR [default: $HOME\.fw]
The '$CMD' command is added to the `$PATH via the current user's environment variables.
"@

$DIST = "https://storage.googleapis.com/flywheel-dist/fw-cli"
$DIR = "$HOME\.fw"
$DIST_DIR = "$DIR\dist"
$INST_DIR = "$DIR\inst"

$ErrorActionPreference = "Stop"
$ProgressPreference = 'SilentlyContinue'

function main {
    param($args)

    if ($args -contains "help" -or $args -contains "-h" -or $args -contains "--help") {
        Write-Host $USAGE
        exit
    }

    $platform = "$([System.Environment]::OSVersion.Platform)"
    if ($platform -notmatch "Win32NT") {
        die "Unsupported platform: $platform"
    }

    $deps = @("curl", "tar")
    foreach ($dep in $deps) {
        if (-not (Get-Command $dep -ErrorAction SilentlyContinue)) {
            die "Missing required command: $dep"
        }
    }
    try {
        log "Installing Flywheel CLI $CLI_VERSION for Windows to $DIR..."

        if (Test-Path $INST_DIR) {
            log "Attempting to remove $INST_DIR"
            Remove-Item -Recurse -Force $INST_DIR | Out-Null
        }

        New-Item -Path $DIST_DIR -ItemType Directory -Force | Out-Null
        New-Item -Path $INST_DIR -ItemType Directory -Force | Out-Null
        Set-Location -Path $DIR
        download "fw-cli-$CLI_VERSION-windows-x86_64.tar.gz"
        $version = Get-Content -Path "$INST_DIR/PYTHON_VERSION"
        log "Found python version $version"
        download "python-$version-windows-x86_64.tar.gz"

create_bin
$paths=$env:Path -split ';'
$paths += "$DIR"
$sysPath=$paths -join ';'
[System.Environment]::SetEnvironmentVariable("Path", "$sysPath", "User")
        log "Installation complete"
    } catch {
        err "Installation Failed: $_"

        # Remove-Item -Recurse -Force $INST_DIR
    }
}

function download {
    param($arc)
    $arc_sha = "$arc.sha256"
    log "Downloading $arc_sha"
    Invoke-WebRequest -Uri "$DIST/$arc_sha" -OutFile "$DIST_DIR\$arc_sha"
    if (checksum $arc_sha) {
        log "Using cached $arc"
    } else {
        log "Downloading $arc"
        Invoke-WebRequest -Uri "$DIST/$arc" -OutFile "$DIST_DIR\$arc"
    }
    log "Extracting $arc to $INST_DIR"
    tar -xzf "$DIST_DIR\$arc" -C "$INST_DIR"
}

function create_bin {
    log "Creating executable in $DIR\$CMD"
    $bootstrapScriptPath = "$DIR\$CMD.bat"
    Set-Content -Path $bootstrapScriptPath -Value @'
@echo off
setlocal
set "DIR=%~dp0"
if exist "%DIR%inst" >nul 2>&1 (
    rmdir /s /q "%DIR%run" >nul 2>&1
    move "%DIR%inst" "%DIR%run" >nul 2>&1
)
set "FW_CLI_INSTALL_DIR=%DIR%"
"%FW_CLI_INSTALL_DIR%run\python.exe" -Im fw_cli.main %*
'@
}

function log {
    param([string]$message)
    Write-Host "INFO: $message"
}

function err {
    param([string]$message)
    Write-Host "ERROR: $message" -ForegroundColor Red
}

function die {
    param([string]$messaage)
    err $message
    exit 1
}

function checksum {
    param($file)
    $checksum = ((Get-Content "$DIST_DIR\$file") -Split "  ")[0]
    $origFile = ((Get-Content "$DIST_DIR\$file") -Split "  ")[1]
    try {
        $calculatedChecksum = (Get-FileHash "$DIST_DIR\$origFile" -Algorithm SHA256).Hash.ToLower()
        if ($checksum -eq $calculatedChecksum) {
            return $true
        }
    } catch {}
    return $false
}

main $args
